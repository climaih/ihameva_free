function [ colores ] = colores_diferentes_Javi( t)
%% Funcion que encuentra el numero de colores m?s diferentes estadisticamente mediante MDA

matriz=colorcube(100000);

[final,bmus,acierto_cien]=classification_function([matriz(:,1), matriz(:,2), matriz(:,3)],[],t,'MDA','esp');%%%[4 5] le indicas los vectores que son direccionales


X=[1:t];
Y=[1:t];

figure(1);

axis on;
grid on
hold on;
for i=1:length(X);
    color=final(i,:);
    scatter(X(i),Y(i),50,color,'filled')
end

colormap(final);
colorbar;

colores=final;

end

