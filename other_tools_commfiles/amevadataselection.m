function amevadataselection(varargin)
%   AMEVADATASELECTION
%   AMEVADATASELECTION.
% Examples 1
%   ameva-->Data Preprocessing
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   aeva (main window v0.5.6 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   29-11-2013 - The first distribution version
%   16-01-2014 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v0.5.6 (140820)';
nmfctn='amevads';
Ax1Pos=[0.465 0.2 0.515 0.68];
figPos=[];

idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

% Texto idioma:
texto=amevaclass.amevatextoidioma(idioma);

namesoftware=[texto('nameAmevaDS'),' ',versionumber];


if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana datos

% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.132;
btnHt=0.05;
spacing=0.020;
top=0.95;
xPos=[0.01 0.1475 0.28];

%GLOBAL VARIALBES solo usar las necesarias!!
awinc=amevaclass;%ameva windows commons methods
aevadsf=aeva_ds_functions;%funciones aeva - ds
awinc.nmfctn=nmfctn;
workPath=[];%Work Path
N=0;%number of selected data
newX=[];newY=[];newD=[];newTime=[];
Xaux=newX;
Yaux=newY;
Diraux=newD;
Timeaux=newTime;
timedefine=false;
Isdire=true;
%name of variables globales
var_lab={'Time:' '* X-Data' 'Y-Data' 'D-Data'};
var_name={'Time' 'X-Data' 'Y-Data' 'D-Data'};
carpeta=[];
xmin=0;xmax=0;qxmin=0;qxmax=1;%x minimo, maximo, quantil inferior y superior
ymin=0;ymax=0;qymin=0;qymax=1;%y minimo, maximo, quantil inferior y superior
tini=0;tfin=0;%tiempo inicial y final
sini=0;sfin=360;%sector inicial y final
nsec=16;%numero de sectores direccionales
%---LA FIGURA-VENTANA PRINCIPAL-
if isempty(figPos), figPos=awinc.amevaconst('mw'); end
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off','Position',figPos,'Tag',['Tag_' nmfctn],'NextPlot','New');
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');
set(hf,'CurrentAxes',ax1);

if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=0.725;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('On'));

% Ameva Settings'CloseRequestFcn',@(source,event)FigDataO2('off')
spacing=0.012;

uipanel('BorderType','etchedin','Units','normalized','Position',[0.007 0.575 0.41 0.26],'Parent',hf);
uipanel('BorderType','etchedin','Units','normalized','Position',[0.007 0.380 0.41 0.19],'Parent',hf);
uipanel('BorderType','etchedin','Units','normalized','Position',[0.007 0.118 0.41 0.26],'Parent',hf);
btnN=btnN+1.25;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_nsd=uicontrol('Style','Text','Units','normalized','Parent',hf,'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid*3 btnHt],'String',[texto('dataNumber') ' ',num2str(N)]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.ti=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[xPos(1) yPos btnWid*2 btnHt],'String','time options');
uicontrol('Style','text','Units','normalized','Parent',hf,...
    'Position',[xPos(2) yPos-2*spacing btnWid btnHt],'String','inicial date');
uicontrol('Style','text','Units','normalized','Parent',hf, ...
    'Position',[xPos(3) yPos-2*spacing btnWid btnHt],'String','final date');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_imy(1)=uicontrol('Style','radiobutton','Units','normalized','Parent',hf,'String','by interval',...
    'Position',[xPos(1) yPos btnWid btnHt],'Value',1);
ui_tini=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid btnHt],'String','','UserData',[],'Callback',@(source,event)ActTinifin('Tini',source,event));
ui_tfin=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','','UserData',[],'Callback',@(source,event)ActTinifin('Tfin',source,event));

%by interval, by year o by month
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_imy(2)=uicontrol('Style','radiobutton','Units','normalized','Parent',hf,'String','by month',...
    'Position',[xPos(1) yPos btnWid btnHt]);

serieMonth='';
eval(['serieMonth=' texto('meslong') ';']);
ui_mon=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',serieMonth,...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)ActByMonth('month',source,event));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_imy(3)=uicontrol('Style','radiobutton','Units','normalized','Parent',hf,'String','by season',...
    'Position',[xPos(1) yPos btnWid btnHt]);

serieSeason='';
eval(['serieSeason=' texto('interval') ';']);
ui_mons=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',serieSeason,...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)ActByMonth('season',source,event));
set(ui_imy(1),'userdata',[ui_imy(2),ui_imy(3)]);
set(ui_imy(2),'userdata',[ui_imy(1),ui_imy(3)]);
set(ui_imy(3),'userdata',[ui_imy(1),ui_imy(2)]);
call=['me=get(gcf,''CurrentObject'');',...
    'if (get(me,''Value'')==1),',...
    'set(get(me,''userdata''),''Value'',0),',...
    'else,','set(me,''Value'',1),','end;clear me;'];
set(ui_imy, 'callback',call);

btnN=1.035*btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.se=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[xPos(1) yPos btnWid*2 btnHt],'String','sector options');
ui_sini=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid btnHt],'String',num2str(sini),'Callback',@ActSe);
uicontrol('Style','text','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','inicial direction','HorizontalAlignment','left');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_sfin=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid btnHt],'String',num2str(sfin),'Callback',@ActSe);
uicontrol('Style','text','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','final direction','HorizontalAlignment','left');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'Position',[xPos(1) yPos btnWid btnHt],'String','[0:360]  sectors','HorizontalAlignment','right');
uicontrol('Style','text','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','sector width','HorizontalAlignment','left');
ui_nsg=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid/2 btnHt],...
    'String',num2str(nsec),'callback',@ActNSect);
ui_nsi=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],...
    'String',num2str(abs(360-0)/nsec),'Enable','off');

Typo='x';
btnN=1.035*btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.x=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[xPos(1) yPos btnWid btnHt],'String','Xmin Xmax');
ui_min.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(xmin),'UserData',xmin,'Enable','Off','Callback',@(source,event)ActQXY('XY','Xmin',source,event));
ui_max.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(xmax),'UserData',xmax,'Enable','Off','Callback',@(source,event)ActQXY('XY','Xmax',source,event));
ui_v.(Typo)(1)=uicontrol('Style','radiobutton','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','by value','Value',1,'callback',@(source,event)CallXY(Typo,1));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'Position',[xPos(1) yPos btnWid btnHt],'String','Xqmin Xqmax','HorizontalAlignment','Right');
ui_qmin.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(qxmin),'UserData',qxmin,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Xmin',source,event));
ui_qmax.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(qxmax),'UserData',qxmax,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Xmax',source,event));
ui_v.(Typo)(2)=uicontrol('Style','radiobutton','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','by quantile','Value',0,'callback',@(source,event)CallXY(Typo,2));

Typo='y';
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.y=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[xPos(1) yPos btnWid btnHt],'String','Ymin Ymax');
ui_min.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(ymin),'UserData',ymin,'Enable','Off','Callback',@(source,event)ActQXY('XY','Ymin',source,event));
ui_max.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(ymax),'UserData',ymax,'Enable','Off','Callback',@(source,event)ActQXY('XY','Ymax',source,event));
ui_v.(Typo)(1)=uicontrol('Style','radiobutton','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','by value','Value',1,'callback',@(source,event)CallXY(Typo,1));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'Position',[xPos(1) yPos btnWid btnHt],'String','Yqmin Yqmax','HorizontalAlignment','Right');
ui_qmin.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(qymin),'UserData',qymin,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Ymin',source,event));
ui_qmax.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hf,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(qymax),'UserData',qymax,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Ymax',source,event));
ui_v.(Typo)(2)=uicontrol('Style','radiobutton','Units','normalized','Parent',hf,'Position',[xPos(3) yPos btnWid btnHt],'String','by quantile','Value',0,'callback',@(source,event)CallXY(Typo,2));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hf,'String','Apply', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)AmevadsApplyData('on'));
ui_rst=uicontrol('Style','push','Units','normalized','Parent',hf,'String','Reset','Enable','Off', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)AmevadsResetData('on'));

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('dataButton')],'HorizontalAlignment','left');

%% Data
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('ds');
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(end) btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
ui_td=zeros(1,length(var_lab));
ui_all=zeros(1,length(var_lab));
ui_cm=zeros(1,length(var_lab));
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid(5) btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(ii));
    if ii==4;
        ui_ck=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(1)+btnWid(6) yPos btnWid(4) btnHt],'String','D','Value',1,'Enable','Off');
    end
end; clear ii;

btnN=btnN+2;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Set Data','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@AmevadfSetData);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close','Position',[xPos(1) yPos btnWid(1) btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;

FigDataAvMode('Off');

%% C1 Open,Close figure data
    function FigDataOO(state,varargin)
        set(hg,'Visible',state);
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(varargin) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);
    end
%% C0
    function AmevaClose(varargin) % Close all Ameva windows
        awinc.AmevaClose([hf,hg], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data']});
    end
%% actualizar nsectores a grados sectores
    function ActNSect(varargin)
        nsec=str2num(get(ui_nsg,'String'));
        set(ui_nsi,'String',num2str(abs(360-0)/nsec));
    end
%% call X Y
    function CallXY(Typo,val,varargin)
        aevadsf.CallXY(Typo,val,ui_v,ui_min,ui_max,ui_qmin,ui_qmax);
    end
%% acutaliza X Y (MAX MIN) --> Qx Qy introducido manualmente &  acutaliza Qx Qy (MAX MIN) -->X Y introducido manualmente
    function ActQXY(strqxy,strxy,varargin)
        [xmax,xmin,ymax,ymin,qxmax,qxmin,qymax,qymin]=aevadsf.ActXY_Qxy(strqxy,strxy,ui_ckv,Xaux,Yaux,ui_min,ui_max,ui_qmin,ui_qmax);
    end
%% actualizar nsectores a grados sectores
    function ActSe(varargin)
        [sini,sfin]=aevadsf.ActSe(nsec,ui_ckv.se,ui_nsi,ui_sini,ui_sfin);
    end
%% acutaliza tini and tfin
    function ActTinifin(strtif,varargin)
        aevadsf.ActTinifin(strtif,timedefine,ui_ckv.ti,ui_imy,ui_tini,ui_tfin)
    end
%% actualiza by month & season
    function ActByMonth(strtif,varargin)
        set(ui_ckv.ti,'Value',1);
        if strcmp(strtif,'month')
            set(ui_imy(1),'Value',0);
            set(ui_imy(2),'Value',1);
            set(ui_imy(3),'Value',0);
        else
            set(ui_imy(1),'Value',0);
            set(ui_imy(2),'Value',0);
            set(ui_imy(3),'Value',1);
        end
    end
%% C1
    function FigDataAvMode(state,varargin)
        %Creo el directorio de trabajo para almacenamiento
        awinc.NewFolderCheck(carpeta,nmfctn,workPath);carpeta=awinc.carpeta;% Methods for "amevaclass"
        N=length(newX);
        set(ui_nsd,'String',[texto('dataNumber') ' ',num2str(N)]);
        
        auxstate='Off';if length(newX)>1, auxstate='On'; end; EnableButton([ui_ckv.ti,ui_ckv.x,ui_min.x,ui_max.x,ui_v.x,ui_qmin.x,ui_qmax.x],'Enable',cmpnblonoff(auxstate,state));
        auxstate='Off';if length(newY)>1, auxstate='On'; end; EnableButton([ui_ckv.y,ui_min.y,ui_max.y,ui_v.y,ui_qmin.y,ui_qmax.y],'Enable',cmpnblonoff(auxstate,state));
        auxstate='Off';if length(newD)>1 && Isdire, auxstate='On'; end; EnableButton([ui_ckv.se,ui_sini,ui_sfin,ui_nsg,ui_nsi],'Enable',cmpnblonoff(auxstate,state));
        auxstate='Off';if timedefine, auxstate='On'; end; EnableButton([ui_imy,ui_mon,ui_mons],'Enable',cmpnblonoff(auxstate,state));
        function stt=cmpnblonoff(auxstate,state)
            if strcmpi(auxstate,state) &&  strcmpi(auxstate,'On')
                stt='On';
            else
                stt='Off';
            end
        end
        if strcmpi(auxstate,'On'), 
            aevadsf.SetQXY_Enable('x',ui_v.x(1),ui_min,ui_max,ui_qmin,ui_qmax);
            aevadsf.SetQXY_Enable('y',ui_v.y(1),ui_min,ui_max,ui_qmin,ui_qmax);
        end
    end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,varargin)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
        if nval==4 && get(ui_all(nval),'Value')>1
            set(ui_ck,'Enable','On');
        else
            set(ui_ck,'Enable','Off');
        end
        ltsplot(nval);% Refresh plot
    end
%% C1 Refresh plot
    function ltsplot(nlv,varargin)
        if get(ui_all(nlv),'Value')>1,
            nlvn=get(ui_td(nlv),'String');
            nlvn=char(nlvn(1));
            try
                awinc.AmevaEvalData(nlv,ui_cm,ui_all);
                X_=awinc.data;
                if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end
                nmsize=size(X_);
                set(ui_td(nlv),'string',{nlvn,sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
                set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
                if min(nmsize)==1
                    plot(ax1,X_,'k');
                else
                    plot(ax1,X_);
                end
                grid on;
                title(ax1,'Serie','FontWeight','bold');
            catch ME
                disp(texto('variableErase'));
                rethrow(ME);
            end
        end
    end
%% Reset default options
    function AmevadsResetData(varargin)
        set(hg,'Visible','off');
        set(ui_tbd,'String',texto('resetData'));drawnow ;
        if isempty(newX) && isempty(newY) && isempty(newD),
            set(ui_tbd,'String',[nmfctn,texto('needData')]);
            disp(texto('notData'));set(hg,'Visible','on'); return;
        end;
        
        newX=Xaux;
        newY=Yaux;
        newD=Diraux;
        newTime=Timeaux;
        tini=newTime(1);tfin=newTime(end);
        nsec=16;
        xmin=min(newX);xmax=max(newX);
        ymin=min(newY);ymax=max(newY);
        qxmin=0;qxmax=1;
        qymin=0;qymax=1;
        sini=0;sfin=360;
        N=length(newX);
        set(ui_nsd,'String',[texto('dataNumber') ' ',num2str(N)]);
        %TIME OPTIONS
        set(ui_ckv.ti,'Value',0);
        if timedefine,
            set(ui_tini,'String',datestr(tini,'dd/mm/yyyy'),'UserData',tini);
            set(ui_tfin,'String',datestr(tfin,'dd/mm/yyyy'),'UserData',tfin);
        else
            set(ui_tini,'String',num2str(tini),'UserData',tini);
            set(ui_tfin,'String',num2str(tfin),'UserData',tfin);
        end
        set(ui_mon,'Value',1);
        %SECTOR OPTION
        set(ui_ckv.se,'Value',0);%if selected sector option
        set(ui_sini,'String',num2str(sini));set(ui_sfin,'String',num2str(sfin));%sector inicial y final
        set(ui_nsg,'String',nsec);%numero de sectores direccionales
        set(ui_nsi,'String',num2str(abs(360-0)/nsec),'UserData',abs(360-0)/nsec);
        %X e Y OPTIONS
        set(ui_ckv.x,'Value',0);%if selected X option
        set(ui_min.x,'String',num2str(xmin),'UserData',xmin);
        set(ui_max.x,'String',num2str(xmax),'UserData',xmax);
        set(ui_qmin.x,'String',num2str(qxmin));
        set(ui_qmax.x,'String',num2str(qxmax));
        set(ui_ckv.y,'Value',0);%if selected Y option
        set(ui_min.y,'String',num2str(ymin),'UserData',ymin);
        set(ui_max.y,'String',num2str(ymax),'UserData',ymax);
        set(ui_qmin.y,'String',num2str(qymin));
        set(ui_qmax.y,'String',num2str(qymax));
        
        %re-inicializo los ejes y variables
        set(ui_tbd,'String',[nmfctn,texto('timeOptions')]);drawnow;
        EnableButton(ui_rst,'Enable','on');
        %set(hf,'Toolbar','figure');
        %
        set(0,'CurrentFigure',hf)
        set(hf,'CurrentAxes',ax1);
        cla(ax1,'reset');
        XYPlot(newTime,newX,newY,[],var_name,[],ax1);
        set(ui_tb,'String',[nmfctn,texto('ready_1')]);drawnow ;
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
    end
%% Set data from setting windows with options
    function AmevadsApplyData(Tipo,varargin)
        EnableButton(hg,'Visible','off');
        set(ui_tbd,'String',[nmfctn, texto('applyData')]);drawnow ;
        if get(ui_ckv.ti,'Value')+get(ui_ckv.se,'Value')+get(ui_ckv.x,'Value')+get(ui_ckv.y,'Value')<1, return; end
        %TIME OPTIONS
        cktime=get(ui_ckv.ti,'Value');%if selected time option
        tini=get(ui_tini,'UserData');tfin=get(ui_tfin,'UserData');%tiempo inicial y final
        ifcimy(1)=get(ui_imy(1),'Value');%if selected interval
        ifcimy(2)=get(ui_imy(2),'Value');%if selected month
        ifcimy(3)=get(ui_imy(3),'Value');%if selected season
        montsc=get(ui_mon,'Value');%month selected 1-12 -> Jan-Dec
        seassc=get(ui_mons,'Value');%month selected 1-4 -> DJF.....
        %SECTOR OPTION
        cksect=get(ui_ckv.se,'Value');%if selected sector option
        sini=str2num(get(ui_sini,'String'));sfin=str2num(get(ui_sfin,'String'));%sector inicial y final
        nsec=str2num(get(ui_nsg,'String'));%numero de sectores direccionales
        set(ui_nsi,'String',num2str(abs(360-0)/nsec),'UserData',abs(360-0)/nsec);
        %X e Y OPTIONS
        ckxopt=get(ui_ckv.x,'Value');%if selected x option
        ifcxvq(1)=get(ui_v.x(1),'Value');
        ifcxvq(2)=get(ui_v.x(2),'Value');
        xmin=get(ui_min.x,'UserData');xmax=get(ui_max.x,'UserData');%x minimo, maximo
        qxmin=str2num(get(ui_qmin.x,'String'));qxmax=str2num(get(ui_qmax.x,'String'));%#ok<*ST2NM> %x quantil inferior y superior
        ckyopt=get(ui_ckv.y,'Value');%if selected y option
        ifcyvq(1)=get(ui_v.y(1),'Value');
        ifcyvq(2)=get(ui_v.y(2),'Value');
        ymin=get(ui_min.y,'UserData');ymax=get(ui_max.y,'UserData');%y minimo, maximo
        qymin=str2num(get(ui_qmin.y,'String'));qymax=str2num(get(ui_qmax.y,'String'));%y quantil inferior y superior
        
        %APLICO LAS DISTINTAS OPCIONES SELECCIONADAS
        %1) Los pongo como vector sino lo estan
        Timeaux=Timeaux(:);
        Xaux=Xaux(:);
        Yaux=Yaux(:);
        Diraux=Diraux(:);
        newTime=Timeaux;
        newX=Xaux;
        newY=Yaux;
        newD=Diraux;
        newTime=newTime(:);
        
        %APLICO LAS DISTINTAS OPCIONES SELECCIONADAS
        [newTime,newX,newY,newD]=aevadsf.AmevadsApplyData(ifcimy,ifcyvq,ifcxvq,cktime,ckxopt,ckyopt,cksect,timedefine,tini,tfin,newTime,newX,newY,newD,Isdire,sini,sfin,montsc,seassc,xmin,xmax,qxmin,qxmax,ymin,ymax,qymin,qymax);
        
        N=length(newX);
        set(ui_nsd,'String',[texto('dataNumber') ' ',num2str(N)]);
        
        %Enable setting reset button
        set(ui_rst,'Enable','on');
        %re-inicializo los ejes y variables
        set(ui_tbd,'String',texto('timeOptions'));drawnow;
        
        FigDataAvMode(Tipo);
        
        set(0,'CurrentFigure',hf)
        set(hf,'CurrentAxes',ax1);cla(ax1,'reset');
        XYPlot(newTime,newX,newY,'none',var_name,[],ax1);
        set(ui_tb,'String',[nmfctn,texto('ready_1')]);drawnow;
        if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;refresh(hg);
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
        %save data to workspace
        try            
            if length(newX)>1, assignin('base','newX',newX); end
            if length(newY)>1, assignin('base','newY',newY); end
            if length(newD)>1, assignin('base','newD',newD); end
            if timedefine, assignin('base','newTime',newTime); end
        catch ME
            rethrow(ME);
        end
        set(ui_tb,'String',texto('newDataWorks'));drawnow;
    end
%% Set data from data windows
    function AmevadfSetData(varargin)
        %por defecto pongo los tiempo ini y fin 1 y N
        newX=[];newY=[];newD=[];newTime=[];
        
        ione=get(ui_all(1),'Value');
        itwo=get(ui_all(2),'Value');
        ithr=get(ui_all(3),'Value');
        ifou=get(ui_all(4),'Value');
        if itwo>1,
            awinc.AmevaEvalData(2,ui_cm,ui_all);
            newX=awinc.data;Xaux=newX;N=length(Xaux);
            set(ui_nsd,'String',[texto('dataNumber'),num2str(N)]);
            xmin=min(Xaux);xmax=max(Xaux);
        else
            error(texto('dataFault_1'));
        end
        set(ui_min.x,'String',num2str(xmin),'UserData',xmin);
        set(ui_max.x,'String',num2str(xmax),'UserData',xmax);
        set(ui_tini,'String','1','UserData',1);
        set(ui_tfin,'String',num2str(N),'UserData',N);
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        if ione>1,
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            newTime=awinc.data;Timeaux=newTime;
            set(ui_tini,'String',datestr(Timeaux(1),'dd/mm/yyyy'),'UserData',Timeaux(1));
            set(ui_tfin,'String',datestr(Timeaux(end),'dd/mm/yyyy'),'UserData',Timeaux(end));
            timedefine=true;
            awinc.AmevaTimeCheck(ui_tbd,newTime);	
            if awinc.state==true,
                disp(texto('dataFormat_1'));
                set(hg,'Visible','on'); return;
            end
        else
            newTime=(1:length(newX))';Timeaux=newTime;
            set(ui_tini,'String',1,'UserData',Timeaux(1));
            set(ui_tfin,'String',N,'UserData',Timeaux(end));
            timedefine=false;
        end
        if ithr>1,
            awinc.AmevaEvalData(3,ui_cm,ui_all);
            newY=awinc.data; Yaux=newY;
            ymin=min(Yaux);ymax=max(Yaux);
        else
            newY=[]; Yaux=newY;
            ymin=min(Yaux);ymax=max(Yaux);
        end
        set(ui_min.y,'String',num2str(ymin),'UserData',ymin);
        set(ui_max.y,'String',num2str(ymax),'UserData',ymax);
        if ifou>1,
            awinc.AmevaEvalData(4,ui_cm,ui_all);
            newD=awinc.data; Diraux=newD;
            Isdire=get(ui_ck,'Value');
            dirname='Dir';
        else
            newD=[]; Diraux=newD;
            set(ui_ck,'Enable','Off')
            Isdire=false;
            dirname='D-Data';
        end
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        %compruebo que las tres variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,newY,newX,'Y-Data','X-Data');        if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newD,newX,dirname,'X-Data');         if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newD,newY,dirname,'Y-Data');         if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newTime,newD,'T-Data',dirname);      if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newTime,newX,'T-Data','X-Data');     if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newTime,newY,'T-Data','Y-Data');     if awinc.state, set(hg,'Visible','on'); return; end,
        %Reinicio las seleccione por defecto
        %cierro la ventana de datos si todo ha ido bien!
        set(hg,'Visible','off');
        
        %Enable setting reset button
        set(ui_rst,'Enable','on');
        %re-inicializo los ejes y variables
        set(ui_tbd,'String',texto('timeOptions'));drawnow;
        
        FigDataAvMode('On');
        %
        set(0,'CurrentFigure',hf)
        set(hf,'CurrentAxes',ax1);
        cla(ax1,'reset');
        XYPlot(newTime,newX,newY,[],var_name,[],ax1);
    end

end