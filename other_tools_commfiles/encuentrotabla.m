function [TB,lowupx,lowupy,MB,SB]=encuentrotabla(x,y,nx,ny,isdire,porcentageorn)
NX=length(x);
NY=length(y);
% Division de las clases
if ~exist('nx','var') || isempty(nx)
    [lowupx,nx]=tickupdate(x);
elseif length(nx)==1
    ndx=(max(x)-min(x))/nx;
    lowupx=(min(x):ndx:max(x));
elseif length(nx)>1
    lowupx=nx;
    nx=length(lowupx)-1;
else
    error('encuentrotabla.m   no se pueden dividir las clases en x');
end
if ~exist('ny','var') || isempty(ny)
    [lowupy,ny]=tickupdate(y);
elseif length(ny)==1
    ndy=(max(y)-min(y))/ny;
    lowupy=(min(y):ndy:max(y));
elseif length(ny)>1
    lowupy=ny;
    ny=length(lowupy)-1;
else
    error('encuentrotabla.m   no se pueden dividir las clases en y');
end

TB=zeros(ny,nx);%Tabla de encuentros
SB=zeros(1,4);
k=1;l=1;
for i=1:nx, %if monthly i=1:12 if sector i=1:nsec
    %absice x
    auxx=zeros(NX,1);
    aux=find( x >= lowupx(i) & x < lowupx(i+1) );
    if i==nx, aux=find( x >= lowupx(i) & x <= lowupx(i+1) ); end
    auxx(aux)=1;
    auxi=[];
    for j=1:ny,
        %ordenade y
        auxy=zeros(NY,1);
        aux_=find( y >= lowupy(j) & y < lowupy(j+1) );
        if j==ny, aux_=find( y >= lowupy(j) & y <= lowupy(j+1) ); end
        auxy(aux_)=1;
        if NX==NY
            duration_aux=auxx.*auxy;
        elseif NX>NY
            duration_aux=auxx(1:NY).*auxy;
        elseif NY>NX
            duration_aux=auxx.*auxy(1:NX);
        end
        TB(j,i)=sum(duration_aux);
        auxi=vertcat(auxi,find(duration_aux==1));
    end
    if ~isempty(auxi)>0
        MB(l,1)=mean(x(aux));
        MB(l,2)=mean(y(auxi));
        if isdire==1
            MB(l,2)=mediadirecciones(y(auxi),true);
        end
        l=l+1;
    end
    if sum(TB(:,i))/NY>0.001 && std(x(aux))>0 && std(y(auxi))>0, %0.5 porciento.. se muestra a partir del 0.1
        SB(k,1)=std(x(aux));
        SB(k,2)=std(y(auxi));
        SB(k,3)=mean(x(aux));
        SB(k,4)=mean(y(auxi));
        if isdire==1
            [maux,saux]=mediadirecciones(y(auxi),true);
            SB(k,2)=saux;
            SB(k,4)=maux;
        end
        k=k+1;
    end
end
if porcentageorn
    TB=TB/sum(sum(TB))*100;%Talba de encuentro en porcentaje
end
end
