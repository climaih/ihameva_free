function amevasavefigure(h,carpeta,svopt,savename,plothc,closefigtf,renderoc)

if not(exist('renderoc','var')) || isempty(renderoc), renderoc='zbuffer'; end
if not(exist('plothc','var')) || isempty(plothc), plothc=false; end
set(h, 'Renderer',renderoc);%opengl
if plothc, set(h,'InvertHardcopy','off'); end;

if svopt(3), saveas(h,fullfile(pwd,carpeta,[savename,'.eps']),'psc2'); end
if svopt(2), saveas(h,fullfile(pwd,carpeta,[savename,'.png'])); end
set(h,'Visible','on');
if svopt(1), saveas(h,fullfile(pwd,carpeta,[savename,'.fig'])); end

%cierro la ventana, salvo que exista y sea closefigtf=false;
if exist('closefigtf','var') && islogical(closefigtf)
    if closefigtf, delete(h); end
else %sum(ismember(get(get(h,'Children'),'Tag'),'Colorbar'))~=0, 
    delete(h);
end