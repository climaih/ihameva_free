function amevaworkspace(varargin)
%   amevaworkspace. opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   Windows to view, save the work space variables and work/save with this variables
% Examples 1
%   amevaworkspace
% Examples 2
%   amevaworkspace esp
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   amevaworkspace (main window v1.2.3 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   27-11-2012 - Old distribution version
%   07-05-2013 - Old distribution version
%   10-12-2013 - Old distribution version
%   20-08-2014 - Last distribution version

nmfctn='amevaworkspace';
versionumber='v1.2.3 (140820)';
idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

% Texto idioma:
texto=amevaclass.amevatextoidioma(idioma);

namesoftware=[texto('nameAmevaWorks') ' ',versionumber];


if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    close(findobj('Tag',['Tag_' nmfctn]));
end

% Information for all buttons and Spacing between the button
colorG='w';	%Color general/Background color
btnWid=0.145;
btnHt=0.08;
spacing=0.01;
screenSize=get(0,'ScreenSize');
fctsc=[1 340/380.80 340/421.60];
ini=0.7;
%GLOBAL VARIALBES solo usar las necesarias OJO!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
data_aux=[];
sa=1;
xPos(1,:)=[0.01 ini ini ini];
xPos(2,:)=[0.01 ini*fctsc(2) ini*fctsc(2)+(1-ini*fctsc(2))/2 ini*fctsc(2)+(1-ini*fctsc(2))/2];
xPos(3,:)=[0.01 ini*fctsc(3) ini*fctsc(3)+(1-ini*fctsc(3))/3 ini*fctsc(3)+(1-ini*fctsc(3))*2/3];
figPos(1)=340;
figPos(2)=340/fctsc(2);
btnWidA(1,:)=[0.65 0.65*fctsc(2) 0.65*fctsc(3)];
btnWidA(2,:)=[0.31 0.31*fctsc(2) 0.31*fctsc(3)];

LabelMenu.File=texto('file');
LabelMenu.ImpData=texto('impData');
LabelMenu.Save=texto('save');
LabelMenu.SOSD=texto('saveSelData');
LabelMenu.SOCS=texto('saveOneData');
LabelMenu.SOCW=texto('selOneVari');
LabelMenu.SOCX=texto('variNotSave');
LabelMenu.Quit=texto('quit');
LabelMenu.PWD=texto('currentFolder');
LabelMenu.CL=texto('commandLine');
LabelMenu.Eval=texto('eval');
LabelMenu.Upd=texto('upd');

%---LA FIGURA-VENTANA PRINCIPAL-
hf=figure('Name',namesoftware,'MenuBar','None','CloseRequestFcn','delete(gcf)',...
    'NumberTitle','Off','DockControls','Off','Position',[9 screenSize(4)-580 340 340],'Tag',['Tag_' nmfctn],...
    'Color',colorG,'NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
e = uimenu('Label',LabelMenu.File);
ImpDatCall=['[auxFileName,auxPathName] = uigetfile(',...
    '{''*.dat;*.txt;*.mat'';''*.dat'';''*.txt'';''*.mat'';''*.*''},''File Selector'',''MultiSelect'', ''on'');',...
    'if isempty(auxFileName), return; end;',...
    'if length(auxFileName)==1, clear auxFileName auxPathName; return; end;',...
    'try ',...
    'h=waitbar(0.3,''Please wait...'');',...
    'auxs = whos(''-var'', ''auxFileName'');',...
    'if auxs.class==''char''',...
    'load([auxPathName,auxFileName]);',...
    'elseif auxs.class==''cell''',...
    'for auxi=1:length(auxFileName), load([auxPathName,auxFileName{auxi}]); end;',...
    'else, ',...
    'disp(''NO DATA FILE!'');',...
    'end;',...
    'catch ME;',...
    'waitbar(1,h);','close(h);','clear auxFileName auxPathName auxs h auxi;amevaws_update_listbox_ext;',...
    'disp(''Error in data format, cannot load it!'');',...
    'rethrow(ME);clear ME;',...
    'end;',...
    'waitbar(1,h);','close(h);','clear auxFileName auxPathName auxs h auxi;amevaws_update_listbox_ext;'];
uimenu(e,'Label',LabelMenu.ImpData,'Callback',ImpDatCall,'Separator','on');
saveuisave=['[FileName,PathName] = uiputfile(''matlab.mat'',''Save Workspace As'');', ...
    'if FileName==0, clear FileName PathName; return; end;', ...
    'save(fullfile(PathName,FileName),char(42));','clear FileName PathName;'];
uimenu(e,'Label',LabelMenu.Save,'Callback',saveuisave);
uimenu(e,'Label',LabelMenu.SOSD,'Callback',@save_ws);
uimenu(e,'Label',LabelMenu.SOCS,'Callback',@save_txt);
uimenu(e,'Label',LabelMenu.PWD,'Callback',@AmevaPWD,'Separator','on');
uimenu(e,'Label',LabelMenu.Quit,'Callback','close(gcf)','Accelerator','Q','Separator','on');

MaxListBox=30;
num_elementos=0;
vars_aux=cell(2,1);
vars_aux{1}=sprintf(' %s       %s      %s','Name','Size','Class');
vars_aux{2}='---- workspace -----------------------';
uicontrol('Style','text','Units','normalized','Parent',hf,'String',LabelMenu.CL, ...
    'Position',[0.00 0.96 1 btnHt/2],'HorizontalAlignment','left','BackgroundColor',colorG);
ui_wstxt=uicontrol('Style','edit','Units','normalized','Parent',hf,'String','','BackgroundColor',colorG, ...
    'Position',[xPos(1,1) 0.87 btnWidA(1) btnHt],'HorizontalAlignment','left');
uicontrol('Style','push','Units','normalized','Parent',hf,'String',LabelMenu.Eval, ...
    'Position',[xPos(1,2) 0.87 btnWid btnHt],'Callback',@ExportDat);
uicontrol('Style','push','Units','normalized','Parent',hf,'String',LabelMenu.Upd, ...
    'Position',[xPos(1,2)+btnWid+spacing 0.87 btnWid btnHt],'Callback',@update_listbox);
%listbox 1 struct
listbox=uicontrol('Style','listbox','Units','normalized','Parent',hf,'Min',1,'Max',MaxListBox,'Tag','tg_wslbox', ...
    'BackgroundColor',colorG,'Value',1,'Position',[xPos(1,1) xPos(1,1) btnWidA(1,1) 0.85],'Callback',@LoadStruc,'String',vars_aux);
listbox1=uicontrol('Style','listbox','Units','normalized','Parent',hf,'Min',1,'Max',MaxListBox, ...
    'BackgroundColor',colorG,'Value',1,'Position',[xPos(1,2) xPos(1,1) btnWidA(2,1) 0.85]);
listbox2=uicontrol('Style','listbox','Units','normalized','Parent',hf,'Min',1,'Max',MaxListBox, ...
    'BackgroundColor',colorG,'Value',1,'Position',[xPos(1,2) xPos(1,1) btnWidA(2,1) 0.85],'Visible','off');
listbox3=uicontrol('Style','listbox','Units','normalized','Parent',hf,'Min',1,'Max',MaxListBox, ...
    'BackgroundColor',colorG,'Value',1,'Position',[xPos(1,2) xPos(1,1) btnWidA(2,1) 0.85],'Visible','off');

update_listbox;
%%update function
set(hf,'WindowKeyPressFcn',@key_press_ExportDat);
if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
%%
    function key_press_ExportDat(source,event)
        %length(event.Modifier)
        %strcmp(event.Modifier{:},'control')
        %event.Key
        %event.Character
        if strcmp(event.Key,'return'),
            evalin('caller',get(ui_wstxt,'String'));
            drawnow;
            evalin('caller',get(ui_wstxt,'String'));
            update_listbox;
            drawnow;
        end
    end
%%
    function ExportDat(varargin)
        evalin('caller',get(ui_wstxt,'String'));
        update_listbox;
        drawnow;
    end
%%
    function update_listbox(varargin)
        if ~exist('MaxListBox','var'), MaxListBox=27; end;
        if ~exist('vars_aux','var'),
            vars_aux=cell(2,1);
            vars_aux{1}=sprintf(' %s       %s      %s','Name','Size','Class');
            vars_aux{2}='---- workspace -----------------------';
        end;
        s = evalin('base','whos');
        set(findobj('Tag','tg_wslbox'),'String',evalin('base','who'));
        
        list_entries = get(findobj('Tag','tg_wslbox'),'String');
        num_elementos=length(list_entries)+2;
        if num_elementos>MaxListBox, disp(texto('maxLiskBox')); end;
        if num_elementos <= 2
            disp([datestr(now,'yyyymmddHHMMSS') texto('notDataUse')]);
            %errordlg('No data in workspace',...
            %        'Incorrect Selection','modal')
        else
            varsI=cell(num_elementos,1);
            varsI{1}=vars_aux{1};
            varsI{2}=vars_aux{2};
            for k=1:num_elementos-2,
                varsI{k+2}=(sprintf(' %s     %dx%d     %s',s(k).name,s(k).size,s(k).class));
            end
            set(findobj('Tag','tg_wslbox'),'String',varsI,'Value',3)
        end
        LoadStruc;
        drawnow;
    end

%% update windows and list position
    function UpdatePos(pos,onoff,callbk)
        set(listbox,'Position',[xPos(1,1) xPos(1,1) btnWidA(1,pos) 0.85]),
        set(listbox1,'Position',[xPos(pos,2) xPos(1,1) btnWidA(2,pos) 0.85],'Callback',callbk),
        set(listbox2,'Position',[xPos(pos,3) xPos(1,1) btnWidA(2,pos) 0.85],'Visible',onoff),
    end
%% struc name
    function LoadStruc(varargin)
        set(listbox1,'String','');
        UpdatePos(sa,'off','');
        ni=get(listbox,'Value');
        s = evalin('base','whos');
        if isempty(ni), return; end;
        if isempty(s), return; end;
        if ni(1)<=2, return; end;
        if max(size(ni))>1, return; end;
        sa=1;
        svar=LoadWsUpd(evalin('base',s(ni-2).name));
        set(listbox1,'String',svar,'Value',1)
        if strcmp(s(ni(1)-2).class,'struct'), %OJO OJO aqui continuar
            set(listbox2,'String','');sa=2;
            data_aux=evalin('base',s(ni(1)-2).name);
            UpdatePos(2,'on',@LoadWsUpd);%update pos
            LoadWsUpd;
        else
            UpdatePos(1,'off','');%update pos
            sa=1;
        end
    end
%% struc name ws
    function svar=LoadWsUpd(s,varargin)
        if sa==2,
            s=[];
            ni=get(listbox1,'Value');
            if max(size(ni))>1, return; end;
            si=get(listbox1,'String');
            s=eval(['data_aux.',si{ni}]);
            sa=2;
        end
        if isstruct(s),
            svar=fieldnames(s,'-full');
        elseif (strcmp(class(s),'double') || strcmp(class(s),'single')) && min(size(s))==1 && max(size(s))==1,
            svar=num2str(s);
        elseif strcmp(class(s),'double') && min(size(s))==1 && max(size(s))>1,
            aux=size(s);
            svar={['<' num2str(aux(1)) 'x' num2str(aux(2)) ' ' class(s) '>'], ...
                ['min->' num2str(min(s))], ...
                ['max->' num2str(max(s))], ...
                ['mean->' num2str(mean(s))], ...
                ['std->' num2str(std(s))]};
        elseif strcmp(class(s),'double') && min(size(s))>1 && max(size(s))>1,
            aux=size(s);
            svar={['<' num2str(aux(1)) 'x' num2str(aux(2)) ' ' class(s) '>'], ...
                ['min->' num2str(min(min(s)))], ...
                ['max->' num2str(max(max(s)))], ...
                ['mean->' num2str(mean(mean(s)))], ...
                ['std->' num2str(std(std(s)))]};
        elseif islogical(s) && max(size(s))==1,
            if s, svar={'true'}; else svar={'false'}; end
        elseif ischar(s),
            svar=s;
        else
            svar='';
        end
        
        if sa==2, 
            set(listbox2,'String',svar,'Visible','on'); 
        else
            sa=1;
            data_aux=s;
        end
    end

%% save select workspace var to mat
    function save_ws(varargin)
        ni=get(findobj('Tag','tg_wslbox'),'Value');%selecte item
        s = evalin('base','whos');
        num_elementos=length(ni);
        [FileName,PathName] = uiputfile('.mat','Save Workspace As mat');
        if FileName==0, return; end;
        for k=1:num_elementos,
            result.(s(ni(k)-2).name)=evalin('base',s(ni(k)-2).name);
        end
        save(fullfile(PathName,FileName),'-struct','result');
    end
%% save only one select workspace var to ascii
    function save_txt(varargin)
        ni=get(findobj('Tag','tg_wslbox'),'Value');%selecte item
        ni=ni-2;
        if ni<1, return; end; % no se han selecciona datos
        if length(ni)>1, %warnig if
            disp(LabelMenu.SOCW);
            msgbox(LabelMenu.SOCW,'Save to text','warn'); return;
        end
        s = evalin('base','whos');
        num_elementos=length(ni);
        result=evalin('base',s(ni).name);
        if strcmp(s(ni).class,'char'),
            [FileName,PathName] = uiputfile('.txt','Save Workspace As text');
            if FileName==0, return; end;
            fid=fopen(fullfile(PathName,FileName),'w');
            fprintf(fid, '%s\n', result);
            fclose(fid);
        elseif strcmp(s(ni).class,'double')
            if isvector(result), result=result(:); end
            [FileName,PathName] = uiputfile('.txt','Save Workspace As text');
            if FileName==0, return; end;
            save(fullfile(PathName,FileName),'result','-ascii');
            %    elseif strcmp(s(ni).class,'struct')
        else
            disp(LabelMenu.SOCX);
            msgbox(LabelMenu.SOCX,'Save to text','warn'); return;
        end
    end
%% Set PWD
    function AmevaPWD(varargin)
        workPath=uigetdir;
        if ~isempty(workPath) && length(workPath)>1 && ~strcmp(workPath(end),filesep)
            cd(workPath);
        end
    end
end
