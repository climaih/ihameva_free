function mpcolor=escaladocircular(Tipo,central,idioma)
%Tipo=1 -> monthly; 2 -> sector(16); 3 -> sector(8)
%                        ESCALA DE COLORES CIRCULAR:
if not(exist('idioma','var')) || not(ischar(idioma)) || isempty(idioma), idioma='eng'; end
if not(exist('central','var')) || isempty(central), central=false; end

texto=amevaclass.amevatextoidioma(idioma);

rad=pi/180;
mapa_colores=colormap(hsv(128));
lmc=length(mapa_colores);
if Tipo==1 % texto meses
    [angul,rhol]=meshgrid((15:30:345).*rad,1:2); %meses
    eval(['Texto=' texto('mes') ';'])
    nAngles=12;desp=pi/12;
    mapa_colores=mapa_colores(end:-1:1,:);
    mapa_colores=mapa_colores([round(lmc/2.3):end 1:round(lmc/2.3)-1],:);
    mpcolor=mapa_colores(round(linspace(lmc/12/2,lmc-lmc/12/2,12)),:);%colores meses
    %    mpcolor=colores_diferentes_Javi(nAngles);
    escalaranual=texto('anual');
elseif Tipo==2
    [angul,rhol]=meshgrid((0:22.5:360-22.5).*rad,1:2); %sector
    %texto=num2str([(22.5:22.5:360-22.5) 0]');
    eval(['Texto=' texto('dire16') ';'])
    nAngles=16;desp=0;
    mpcolor=mapa_colores(round(linspace(1,lmc,nAngles)),:);%colores para direcciones
    escalaranual=texto('escalar');
elseif Tipo==3
    [angul,rhol]=meshgrid((0:45:360-45).*rad,1:2); %sector
    eval(['Texto=' texto('dire8') ';'])
    nAngles=8;desp=0;
    mpcolor=mapa_colores(round(linspace(1,lmc,nAngles)),:);
    escalaranual=texto('escalar');
else
    error('Seleccione Tipo 1 2 o 3');
end
if nargout==0,
    [angu,rho]=meshgrid((0:360),1:2);
    [xx,yy]=pol2cart(angu*rad,rho);
    angu=-(angu-90); % pasar a nauticas
    angu(angu<0)=angu(angu<0)+360;
    h=pcolor(xx,yy,angu);
    set(h,'linestyle','none');
    colormap(mapa_colores);
    caxis([0,360]);
    axis equal; axis off; hold on;
    [xx,yy]=pol2cart(angul,rhol);
    plot(xx(:,1:2:end),yy(:,1:2:end),'w','linewidth',1);
    plot(xx(:,2:2:end),yy(:,2:2:end),'--w','linewidth',1);
    
    rmax=2.2;
    nAp=round(nAngles/2);
    th = -(1:nAp)*2*pi/nAngles+pi/2+desp;
    cst = cos(th); snt = sin(th);
    rt = 1.15*rmax;% annotate spokes in degrees
    for i = 1:length(th)
        text(rt*cst(i),rt*snt(i),Texto{i},'horizontalalignment','center',...
            'handlevisibility','off','fontsize',8);%,'fontweight','bold')
        text(-rt*cst(i),-rt*snt(i),Texto{i+nAp},'horizontalalignment','center',...
            'handlevisibility','off','fontsize',8);%,'fontweight','bold')
    end
    if central, %dibujar o no el centro Anual Scalar
        radin=0.8;
        rectangle('Position',[0-radin 0-radin radin*2 radin*2],'Curvature', [1 1],'LineWidth',1,'LineStyle','-')
        text(0,0,escalaranual,'horizontalalignment','center','handlevisibility','off','fontsize',8,'color','k');
    end
    hold off;
    freezeColors;% para fijar el colormap
end
