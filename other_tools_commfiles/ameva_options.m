classdef ameva_options
    properties
        svopt = [0 0 0];
        carpeta = '';
        idioma = '';
        var_name = {'','',''};
        var_unit = {'','',''};
        cityname='';
        poolparalelo=false; %Enables the parallel language
        forzarAQ=true; %Forzar calcular el cuantil agregado siempre (true)
        Ts=[]; %Periodos de retorno vacio para usar el que se define en la funcion amevaAgregateQuantiles.aqPlot ln15
        myMeses=[]; %Meses para calcular el Periodos de retorno. Vacio para usar todos los meses
        luint=[]; %Low Upper bound chi0 GEV amevaFuncAutoAdjust ln314
    end
    methods(Static)
        function ameva_options_obj = ameva_options(svopt,carpeta,idioma,var_name,var_unit,cityname,poolparalelo,forzarAQ,Ts,myMeses,luint)
            %  Structura con las Opciones generales para usa con ameva
            %  ameva_options_obj.svopt = [1 1 1]; (1 save .fig; 1 save .png; 1 save .eps), 0 para no guardar en ese formato
            %  ameva_options_obj.carpeta = pwd; nombre de la carpeta donde guardar las figuras/datos de salida (carpeta actual)
            %  ameva_options_obj.idioma = 'eng'; idioma de los texto eng/esp
            %  ameva_options_obj.var_name = {'Hs','Tp','Dir'}; Nombre de las variables (Cell string array)
            %  ameva_options_obj.var_unit = {'m','s','grad.'}; Unidades de las variables (Cell string array)
            %  ameva_options_obj.cityname = 'Santander_1'; Titulo en las figuras
            %  ameva_options_obj.poolparalelo = false; activar o deter calculo en paralelo true/false
            %  ameva_options_obj.forzarAQ = true; forzar el calculo de los quantiles agregados true/false
            %  ameva_options_obj.TS = []; Periodos de retorno para calcular quantiles agregados []/2/2 10 100 500
            %  ameva_options_obj.myMeses = []; Meses para calcular el Periodos de retorno. Vacio para usar todos los meses
            %  ameva_options_obj.luint = []; Low Upper bound chi0 0.2
            %
            % Ex : opciones = ameva_options([0 1 0],'prueba_m','esp',{'Hs'},{'m'},'Santander1');

            ameva_options_obj.svopt = [0 0 0];
            ameva_options_obj.carpeta = pwd;
            ameva_options_obj.idioma = 'eng';
            ameva_options_obj.var_name = {'','',''};
            ameva_options_obj.var_unit = {'','',''};
            ameva_options_obj.cityname = '';
            ameva_options_obj.poolparalelo = false;
            ameva_options_obj.forzarAQ = true;
            %if exist var input
            if exist('svopt','var') && ~isempty(svopt),
                ameva_options_obj.svopt = svopt;
            end
            if exist('carpeta','var') && ~isempty(carpeta),
                ameva_options_obj.carpeta = carpeta;
            end
            if exist('idioma','var') && ~isempty(idioma),
                ameva_options_obj.idioma = idioma;
            end
            if exist('var_name','var') && ~isempty(var_name),
                ameva_options_obj.var_name  = var_name;
            end
            if exist('var_unit','var') && ~isempty(var_unit),
                ameva_options_obj.var_unit  = var_unit;
            end
            if exist('cityname','var') && ~isempty(cityname),
                ameva_options_obj.cityname = cityname;
            end
            if exist('poolparalelo','var') && ~isempty(poolparalelo),
                ameva_options_obj.poolparalelo = poolparalelo;
            end
            if exist('forzarAQ','var') && ~isempty(forzarAQ),
                ameva_options_obj.forzarAQ = forzarAQ;
            end
            if exist('Ts','var') && ~isempty(Ts),
                ameva_options_obj.Ts = Ts;
            end
            if exist('myMeses','var') && ~isempty(myMeses),
                ameva_options_obj.myMeses = myMeses;
            end
            if exist('luint','var') && ~isempty(luint),
                ameva_options_obj.luint = luint;
            end
        end
        function [svopt,carpeta,idioma,var_name,var_unit,cityname,poolparalelo,forzarAQ,Ts,myMeses,luint] = ameva_options_toarray(s)
            svopt=existevar('svopt');
            carpeta=existevar('carpeta');
            idioma=existevar('idioma');
            var_name=existevar('var_name');
            var_unit=existevar('var_unit');
            cityname=existevar('cityname');
            poolparalelo=existevar('poolparalelo');
            forzarAQ=existevar('forzarAQ');
            Ts=existevar('Ts');
            myMeses=existevar('myMeses');
            luint=existevar('luint');
            function obj=existevar(vn)
                if isfield(s,vn),
                    obj=s.(vn);
                else
                    obj=[];
                end
            end
        end
    end
end