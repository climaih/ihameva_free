function Lab=labelsameva(var_name,var_unit,n)
% Ej load ameva;
% Lab=labelsameva(var_name,var_unit);
Lab='';
if iscell(var_name),
    if ~exist('n','var') || isempty(n), n=1:length(var_name); end
    for i=n,
        if ~exist('var_unit','var') || isempty(var_unit) || strcmp(var_unit(i),''),
            Lab{i}=labelsxydir_(var_name(i),'');
        else
            Lab{i}=labelsxydir_(var_name(i),var_unit(i));
        end
    end
elseif ischar(var_name)
    if ~exist('var_unit','var') || isempty(var_unit) || strcmp(var_unit,''),
        Lab=labelsxydir_(var_name,'');
    else
        Lab=labelsxydir_(var_name,var_unit);
    end
else
    disp('Solo para var_name tipo cell o string...!');
end
    function Lab_=labelsxydir_(var_name_,var_unit_)
        if  isempty(var_name_),
            Lab_='';
        else
            Lab_=char(var_name_);
            if ~isempty(var_unit_)
                Lab_=[Lab_ ' (' char(var_unit_) ')'];
            end
        end
        
    end
end
