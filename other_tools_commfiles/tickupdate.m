function [xtick,nx]=tickupdate(x)
%OC 2012
h=figure('visible','off');
plot(x,x);
L=get(gca,'ytick');
if length(L)<5
    L1=linspace(L(1),L(end),2*length(L));
    dt=round((L1(2)-L1(1))*10)/10;
    xtick=L1(1):dt:L1(end);
    if xtick(end)<L1(end),
        xtick(end+1)=xtick(end)+dt;
    end
else
    xtick=L;
end
nx=length(xtick)-1;
%set(gca,'xtick',xtick,'xlim',[xtick(1) xtick(end)]);
close(h);
end
