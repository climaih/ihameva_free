function [signal] = duration_t(y,tipo,Y)
%   DURATION_T (duration_t.m) Busca los grupos de unos en un vector de ceros y unos.
%   (numero de unos por grupo menos uno) y las posiciones de inicio y fin
%   dentro de cada grupo dentro del vector. Devuelve la cantidad de intervalos entre los unos de cada grupo
%   tipo .- true/false, por defecto -true- cuenta un solo valor como un paso.
%   (false) para no contar Es obligatorio introducir este dato.
%   Si ademas se introduce la serie completa, en el resultado puede
%   obtenerse el valor del maximo y la posicion del mismo en la 4 y 5 columna respectivamente
%
% -------------------------------------------------------------------------
%   Fisica Aplicada(MEPE). Universidad de Cantabria
%   Santander, Spain. 
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows
%   02-02-2009 - The first distribution version
%   22-03-2013 - Last distribution version (OJO OJO Comprobar otros casos OC)

error(nargchk(2,3,nargin));% errror numero de argumentos min dos argumentos
validateattributes(y,{'logical'},{'vector'});
validateattributes(tipo,{'logical'},{'scalar'});

N = length(y);
signal = zeros(length(y),3);
k2=0;
k=1;
while k <= N && N>=2
    ctr=1;
    if y(k)==0, 
        k=k+1;
        continue
    else
        if k<=1 && k2>0,
            continue; 
        end;
        j=k+1;
        while j<=N && y(j)==1
            ctr=ctr+1;
            if j<N
                j=j+1;
            else
                j=j+1;
                break
            end
        end
        k=j;
    end
    %para contar o no un solo dato, por defecto cuenta un solo dato 1 paso (true)
    if tipo,
        k2=k2+1;
        signal(k2,1)=(ctr);
        signal(k2,2) = k-ctr;
        signal(k2,3) = k-1;
    else
        if ctr>1,
            k2=k2+1;
            signal(k2,1)=(ctr-1);
            signal(k2,2) = k-ctr;
            signal(k2,3) = k-1;
        end
    end
    k=k+1;
end  
signal(k2+1:end,:) = [];

%Ecuentro el maximo y la posicion del mismo
if nargin==3
    aux=size(signal);
    for i=1:aux(1)
        pos=signal(i,2):signal(i,3);
        [ymax,posi]=max(Y(pos));
        signal(i,4)=ymax;
        signal(i,5)=pos(posi);
        signal(i,6)=mean(Y(pos));
    end    
end

end