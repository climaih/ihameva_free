function [statmoments,Hlim,calidadRHO]=statmoments_hlim_rho(HsI,HsR,Hscal)

error(nargchk(2, 3, nargin));   % check number of input arguments
if nargin==2, Hscal=[]; end

R=corrcoef(HsI,HsR);% disp('Coeficiente de correlacion instrumenta y reanalisis')
if isempty(Hscal)
    calidadRHO = R(1,2);
    Hlim=[0 ceil(max(max(HsI),max(HsR)))];
else
    Rcal=corrcoef(HsI,Hscal);% disp('Coeficiente de correlacion sin calibrar y calibrado')
    calidadRHO = [R(1,2) Rcal(1,2) 100*(Rcal(1,2)-R(1,2))/R(1,2)];
    Hlim=[0 ceil(max([max(HsI),max(HsR),max(Hscal)]))];
end

statmoments = [ mean(HsR) mean(Hscal) mean(HsI);
                std(HsR) std(Hscal) std(HsI);
                skewness(HsR) skewness(Hscal) skewness(HsI);
                kurtosis(HsR) kurtosis(Hscal) kurtosis(HsI);
                (mean(HsR)-mean(HsI))/mean(HsI) (mean(Hscal)-mean(HsI))/mean(HsI) 0;
                (std(HsR)-std(HsI))/std(HsI) (std(Hscal)-std(HsI))/std(HsI) 0;
                (skewness(HsR)-skewness(HsI))/skewness(HsI) (skewness(Hscal)-skewness(HsI))/skewness(HsI) 0;
                (kurtosis(HsR)-kurtosis(HsI))/kurtosis(HsI) (kurtosis(Hscal)-kurtosis(HsI))/kurtosis(HsI) 0];

end