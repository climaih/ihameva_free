function tf = amevaTheseToolboxesInstalled(requiredToolboxes)
%AMEVATHESETOOLBOXESINSTALLED takes a cell array of toolbox names and checks whether they are currently installed
% SYNOPSIS tf = amevaTheseToolboxesInstalled(requiredToolboxes)
%
% INPUT requiredToolboxes: cell array with toolbox names to test for. Eg. 
% {'Curve Fitting Toolbox','Econometrics Toolbox','Mapping Toolbox','Optimization Toolbox','Statistics Toolbox'}
% {'Curve Fitting Toolbox','Econometrics Toolbox','Optimization Toolbox','Statistics Toolbox'}
%
% OUTPUT tf: true or false if the required toolboxes are installed or not
%%%%%%%%%%%%%%%%%%%%%%%%%%

% get all installed toolbox names
v = ver;
% collect the names in a cell array
[installedToolboxes{1:length(v)}] = deal(v.Name);

% check 
tf = all(ismember(requiredToolboxes,installedToolboxes));

if not(tf), disp(['The ' requiredToolboxes ' is required for some plots and calculations, we continue without it.']); end