function amevaclear(tipo)
%function to clear all configure file for ameva programs
if ~exist('tipo','var'), tipo=''; end

if exist(fullfile(tempdir,'bathymetryconf.mat'),'file')==2
    delete(fullfile(tempdir,'bathymetryconf.mat'));
end

%eliminamos los ficheros descomprimidos zip
if strcmpi(tipo,'ALL')
    if exist([tempdir,'batimetriasoc'],'dir')==7
        delete([tempdir,'batimetriasoc',filesep,'*.dat'])
        rmdir([tempdir,'batimetriasoc']);
    end
    if exist([tempdir,'gebco'],'dir')==7
        delete([tempdir,'gebco',filesep,'*.dat'])
        rmdir([tempdir,'gebco']);
    end
    if exist([tempdir,'gshhs_shape_ameva'],'dir')==7
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.shp'])
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.dbf'])
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.shx'])
        delete([tempdir,'gshhs_shape_ameva',filesep,'*.mat'])
        rmdir([tempdir,'gshhs_shape_ameva']);
    end
    delete([tempdir,'matrix_*.mat'])
end