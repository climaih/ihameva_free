function [ht] = plottable(Ag,lowupx,lowupy,fs,control,alineado,colgroup)
% calc instensity subdivisions:
% GET SIZE OF X
[xrows xcols] = size(Ag);
% IF NO CONTROL STRING, INITIALIZE IT
if nargin<5,
   control = '%.2f';
end
% IF NO alineado default center
if nargin<6,
   alineado='Center';
end
% FIND NUMBER OF CONTROL STRINGS
numcontrol = size(control,1);
% IF NO COLUMN GROUPING INITIALIZE IT
if nargin<7,
   colgroup = ones(xcols,1);
end
% FIND THE NUMBER OF COLUMN GROUPINGS 
% AND THE TOTAL NUMBER OF ELEMENTS IN A ROW
numcolgroup = length(colgroup);
sumcolgroup = sum(colgroup);
% SET NUMBER OF ROWS AND COLUMNS IN THE TABLE 
numrow = xrows;
numcol = numcolgroup * floor(xcols/sumcolgroup);
% IF COLGROUP DESCRIBES MORE ELEMENTS THAN X HAS RETURN AN ERROR
if numcol==0,
   error('Column Grouping Describes More Elements Than A Single Row Of The Input Matrix');
end
% INITIALIZE PLOT AREA
ndx=diff(lowupx);
ndy=(lowupy(2)-lowupy(1));
plot(lowupx(1),lowupy(1));
axis([lowupx(1) lowupx(end) lowupy(1) lowupy(end)]);
if iscell(Ag), set(gca,'XTick',[],'YTick',[],'XTickLabel',[],'YTickLabel',[]); end
% DRAW LINES OF THE TABLE
lh = line( [lowupx ; lowupx],[ones(1,numcol+1)*lowupy(1) ; ones(1,numcol+1)*lowupy(end)]);
set(lh,'color','black');
lh = line( [ones(1,numrow+1)*lowupx(1) ; ones(1,numrow+1)*lowupx(end)],[lowupy ; lowupy]);
set(lh,'color','black');
% FILL IN ELEMENTS OF THE TABLE
for row=1:numrow,
   start_col = 1;
   stop_col  = 0;
   fw='bold';
   for col=1:numcol,
      % PICK CONTROL STRING TO USE
      con = mod( (col-1),numcontrol) + 1;
      % PICK NUMBER OF COLUMNS TO GROUP 
      % AND ADD TO THE COLUMN STOPPING POINT
      cg  = mod( (col-1),numcolgroup) + 1;
      stop_col = stop_col + colgroup(cg);
      % SET COLUMN LOCATIONS TO LOOK AT IN X
      columns = start_col:stop_col;
      % BUILD STRING TO PRINT
      if findstr(control(con,:),'rats'),
         s = rats(Ag(row,columns));
      end
      if isfloat(Ag)
         if Ag(row,columns)>=0.01 && Ag(row,columns)<0.1
            s = sprintf( '%.2f', Ag(row,columns) ); 
         else
            s = sprintf( control(con,:), Ag(row,columns) ); 
         end
      end
      if iscell(Ag)
         s = Ag(row,columns);
         if ~isnan(str2double(char(s))), 
             fw='normal'; 
         end
         if strcmp(s,'NaN'), % || str2double(char(s))<0.0001
             s='';
         end
      end
      %No pinto los ceros si es flot la matriz
      if isfloat(Ag)
          %solo pinto aquelos que mayores que 0.01 porciento
          if Ag(row,columns)<0.01 || isnan(Ag(row,columns)) 
              s='';
          else
              s=[s char(37)];
          end
      end
      % PLACE STRING IN TABLE
      lpy=lowupy(row)+ndy/2;
      if strcmp(alineado,'Left')
          lpx=lowupx(col);
      else
          lpx=lowupx(col)+ndx(col)/2;
      end
      % PLACE STRING IN TABLE
      ht(row,col)=text(lpx,lpy, s,'FontSize',fs-1,'FontWeight',fw,'HorizontalAlignment',alineado,'Interpreter','tex');
      % UPDATE NEXT STARTING POSITION
      start_col = stop_col + 1;
      
    end
end
end