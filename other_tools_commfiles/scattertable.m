function [polycoef,hh]=scattertable(x,y,Hlim,nx,ny,isdire,var_name,var_unit,fs,scattipo)
% scattertable
if nargin<9, % IF NO FONT SIZE, INITIALIZE IT
   fs = 11;
end
if nargin<10, % IF NO SCATTER TIPO, INITIALIZE IT
   scattipo = 'OC';
end
[TB,lowupx,lowupy,MB,SB]=encuentrotabla(x,y,nx,ny,isdire,true);
[polycoef,hh]=ScatterReaCal(x,y,MB,SB,Hlim,isdire,var_name,var_unit,'',fs,scattipo,lowupx);hold on;
plottable(TB,lowupx,lowupy,fs,'%.1f','Center');hold off;
if length(ny)>1, set(gca,'YTick',ny); end;
if length(nx)>1, set(gca,'XTick',nx); end;
set(gca,'FontSize',fs);

end
 