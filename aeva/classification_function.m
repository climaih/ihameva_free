function [final,bmus,acierto_cien]=classification_function(datos,direccional,t,tipo,idioma,var_name,var_unit,carpeta,svopt)
% funcion de calssificacion: tipo 'SOM' 'MDA' 'KMA'
% datos.- Matrix of data  - Classification
% direccional.- Positon of directional vector in the matrix -datos-, if not exist set empty
% tipo.-'SOM', 'MDA', o 'KMA'
% t.- size of claster en el caso SOM (t(1)*t(2))
% idioma.-'esp' o 'eng'
%
% Examples 1
%   load ameva;
%   classification_function([excal_HsI excal_HsR excal_Dire],3,[7 7],'SOM','esp',{'Hs';'Hr';'Dir'},{'m';'m';char(186)},[],[1 1 0]);
% Examples 2
%   carpeta='sample_clssification'
%   svopt=[0 1 0];
%   [final,bmus,acierto_cien]=classification_function([excal_HsI excal_HsR],[],[7 7],'SOM','esp',[],[],carpeta,svopt);
% Examples 3
%   [final,bmus,acierto_cien]=classification_function(exgev_Modos,[],[7 7],'SOM');
%
% See also:
%   castello@unican.es

if nargin<4, 
    error(['faltan parametros: classification_function(datos,escalar,direccional,t,tipo,idioma,var_name,var_unit,carpeta,svopt)' ...
           'help classification_function']); 
end
if not((strcmp(tipo,'SOM') || strcmp(tipo,'MDA') || strcmp(tipo,'KMA'))), error('Seleccione correctamente el tipo de classificacion: SOM MDA O KMA'); end
if ( strcmp(tipo,'MDA') || strcmp(tipo,'KMA') ) && length(t) ~=1, error('En el caso KMA y MDA ej, t=9 -escalar de  un entero-'); end
if strcmp(tipo,'SOM') && length(t)~=2, error('En el caso KMA y MDA ej, t=9 -escalar de  un entero-'); end
if length(t)==2 && (t(1)/floor(t(1))~=1 || t(2)/floor(t(2))~=1), error('En el caso SOM ej, t=[7 6] -vector de dos enteros-'); end
if length(t)==1 && t/floor(t)~=1, error('En el caso KMA y MDA ej, t=9 -escalar de  un entero-'); end
if ~exist('var_name','var'), var_name=''; end
if ~exist('var_unit','var'), var_unit=''; end
if ~exist('carpeta','var'), carpeta=''; end
if ~exist('svopt','var') || isempty(svopt), svopt=[0 0 0]; end %save option 1-fig 2-png 3-eps (1/0)
if isempty(direccional),
    escalar=(1:min(size(datos)));
else
    escalar=(1:min(size(datos)));
    escalar(direccional)=[];
end

N=length(datos);

if strcmp(tipo,'SOM') && N>10001, tipo=[tipo '_PreMDA']; end
if strcmp(tipo,'KMA') && N>10001, tipo=[tipo '_PreMDA']; end

switch tipo
    case 'SOM_PreMDA',   
        [final,bmus,acierto_cien]=SOM_3D_PreMDA_function(datos,escalar,direccional,t,var_name,var_unit,carpeta,tipo,svopt);
    case 'SOM',          
        [final,bmus,acierto_cien]=SOM_3D_function       (datos,escalar,direccional,t,var_name,var_unit,carpeta,tipo,svopt);
    case 'MDA',          
        [final,bmus,acierto_cien]=MDA_3D_function       (datos,escalar,direccional,t,var_name,var_unit,carpeta,tipo,svopt);
    case 'KMA_PreMDA',   
        [final,bmus,acierto_cien]=KMA_3D_PreMDA_function(datos,escalar,direccional,t,var_name,var_unit,carpeta,tipo,svopt);
    case 'KMA',          
        [final,bmus,acierto_cien]=KMA_3D_function       (datos,escalar,direccional,t,var_name,var_unit,carpeta,tipo,svopt);
end
