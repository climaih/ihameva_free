function varargout = OccurrenceTableData(varargin)
% OCCURRENCETABLEDATA MATLAB code for OccurrenceTableData.fig
%      OCCURRENCETABLEDATA, by itself, creates a new OCCURRENCETABLEDATA or raises the existing
%      singleton*.
%
%      H = OCCURRENCETABLEDATA returns the handle to a new OCCURRENCETABLEDATA or the handle to
%      the existing singleton*.
%
%      OCCURRENCETABLEDATA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OCCURRENCETABLEDATA.M with the given input arguments.
%
%      OCCURRENCETABLEDATA('Property','Value',...) creates a new OCCURRENCETABLEDATA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OccurrenceTableData_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OccurrenceTableData_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OccurrenceTableData

% Last Modified by GUIDE v2.5 11-Jun-2013 10:27:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OccurrenceTableData_OpeningFcn, ...
                   'gui_OutputFcn',  @OccurrenceTableData_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OccurrenceTableData is made visible.
function OccurrenceTableData_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OccurrenceTableData (see VARARGIN)

% Choose default command line output for OccurrenceTableData
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OccurrenceTableData wait for user response (see UIRESUME)
% uiwait(handles.Tag_occtable);
nmfctn='occurrencetable';
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;

if not(isdeployed), awinc.loadamevaico(gcf,true); end%Load ameva ico

%set(handles.Tag_occtable,'UserData',varargin{1});%Set control main windows
set(handles.Tag_occtable,'UserData',varargin{1});%Datos to table

% --- Outputs from this function are returned to the command line.
function varargout = OccurrenceTableData_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in gui_dataandws.
function gui_dataandws_Callback(hObject, eventdata, handles)
% hObject    handle to gui_dataandws (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
amevaworkspace;


% --- Executes on selection change in gui_time.
function gui_time_Callback(hObject, eventdata, handles)
% hObject    handle to gui_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_time contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_time
awinc=amevaclass;%ameva windows commons methods
awinc.AmevaUpdateName(1,handles.gui_cmtime,handles.gui_time,handles.gui_stime)
        
% --- Executes during object creation, after setting all properties.
function gui_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_xdata.
function gui_xdata_Callback(hObject, eventdata, handles)
% hObject    handle to gui_xdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_xdata contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_xdata
awinc=amevaclass;%ameva windows commons methods
awinc.AmevaUpdateName(1,handles.gui_cmx,handles.gui_xdata,handles.gui_sxdata)


% --- Executes during object creation, after setting all properties.
function gui_xdata_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_xdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_ydata.
function gui_ydata_Callback(hObject, eventdata, handles)
% hObject    handle to gui_ydata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_ydata contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_ydata
awinc=amevaclass;%ameva windows commons methods
awinc.AmevaUpdateName(1,handles.gui_cmy,handles.gui_ydata,handles.gui_sydata)


% --- Executes during object creation, after setting all properties.
function gui_ydata_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_ydata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_dir.
function gui_dir_Callback(hObject, eventdata, handles)
% hObject    handle to gui_dir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_dir contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_dir
awinc=amevaclass;%ameva windows commons methods
awinc.AmevaUpdateName(1,handles.gui_cmdir,handles.gui_dir,handles.gui_sdir)


% --- Executes during object creation, after setting all properties.
function gui_dir_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_dir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gui_setdata.
function gui_setdata_Callback(hObject, eventdata, handles)
% hObject    handle to gui_setdata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
awinc=amevaclass;%ameva windows commons methods

ione=get(handles.gui_time,'Value');
itwo=get(handles.gui_xdata,'Value');
ithr=get(handles.gui_ydata,'Value');
ifou=get(handles.gui_dir,'Value');
ifiv=get(handles.gui_vx,'Value');
isix=get(handles.gui_vy,'Value');

tbl.Time=[];tbl.X=[];tbl.Y=[];tbl.Dir=[];
tbl.timedefine=false;
tbl.nsec=[];tbl.sini=0;tbl.sfin=0;
tbls.Vx=[];tbls.Vy=[];
if itwo>1,
    awinc.AmevaEvalData(1,handles.gui_cmx,handles.gui_xdata);
    tbl.X=awinc.data;
    tbl.var_name(1)={awinc.var_name};
else
    error('X-Data input is necessary to continue!');
end
if ione>1,
    awinc.AmevaEvalData(1,handles.gui_cmtime,handles.gui_time);
    tbl.Time=awinc.data;
    tbl.timedefine=true;
    awinc.AmevaTimeCheck(handles.gui_tbd,tbl.Time);	
    if awinc.state==true,
        disp('Data: >> los datos temporales deben estar en formato datenum de matlab!');
        set(gcf,'Visible','on'); return;
    end
else
    tbl.Time=(1:length(tbl.X))';
end
if ithr>1,
    awinc.AmevaEvalData(1,handles.gui_cmy,handles.gui_ydata);
    tbl.Y=awinc.data;
    tbl.var_name(2)={awinc.var_name};
else
    tbl.Y=[];
end
if ifou>1,
    awinc.AmevaEvalData(1,handles.gui_cmdir,handles.gui_dir);
    tbl.Dir=awinc.data;
    tbl.var_name(3)={awinc.var_name};
    tbl.nsec=16;tbl.sini=0;tbl.sfin=360;
else
    tbl.Dir=[];
end
if ifiv>1,
    awinc.AmevaEvalData(1,[],handles.gui_vx);
    tbls.Vx=awinc.data;
    if max(size(tbls.Vx))<2, error('the vector must have at least 2 items'); end
    %if not(isempty(tbl.X)) && min(tbl.X)<tbls.Vx(1), error('x bin vector error min(X)<bin(1)!'); end
    %if not(isempty(tbl.X)) && max(tbl.X)>tbls.Vx(end), error('x bin vector error max(X)>bin(end)!'); end
else
    tbls.Vx=[];
end
if isix>1,
    awinc.AmevaEvalData(1,[],handles.gui_vy);
    tbls.Vy=awinc.data;
    if max(size(tbls.Vy))<2, error('the vector must have at least 2 items'); end
    %if not(isempty(tbl.Y)) && min(tbl.Y)<tbls.Vy(1), error('y bin vector error min(Y)<bin(1)!'); end
    %if not(isempty(tbl.Y)) && max(tbl.Y)>tbls.Vy(end), error('y bin vector error max(Y)>bin(end)!'); end
else
    tbls.Vy=[];
end
%compruebo que las tres variables sean diferentes
awinc.AmevaVarCheck(handles.gui_tbd,tbl.X,tbl.Time,'X-Data','Time');      if awinc.state, set(gcf,'Visible','on'); return; end,
awinc.AmevaVarCheck(handles.gui_tbd,tbl.Y,tbl.Time,'Y-Data','Time');      if awinc.state, set(gcf,'Visible','on'); return; end,
awinc.AmevaVarCheck(handles.gui_tbd,tbl.Y,tbl.X,'Y-Data','X-Data');       if awinc.state, set(gcf,'Visible','on'); return; end,
awinc.AmevaVarCheck(handles.gui_tbd,tbl.Dir,tbl.Time,'Dir','Time');       if awinc.state, set(gcf,'Visible','on'); return; end,
awinc.AmevaVarCheck(handles.gui_tbd,tbl.Dir,tbl.X,'Dir','X-Data');        if awinc.state, set(gcf,'Visible','on'); return; end,
awinc.AmevaVarCheck(handles.gui_tbd,tbl.Dir,tbl.Y,'Dir','Y-Data');        if awinc.state, set(gcf,'Visible','on'); return; end,

set(gcf,'Visible','off');

%compruebo
if ~isfield(tbl,'idioma') || isempty(tbl.idioma), tbl.idioma='eng'; end
if ~isfield(tbl,'var_name') || isempty(tbl.var_name), tbl.var_name={'Hs','Tm','Dir'}; end %Nombre de 3 variables por defecto sino existen 
tbl.texto=amevaclass.amevatextoidioma(tbl.idioma);

tbls.xmin=min(tbl.X);tbls.ymin=min(tbl.Y);tbls.dirmin=min(tbl.Dir);
tbls.xmax=max(tbl.X);tbls.ymax=max(tbl.Y);tbls.dirmax=max(tbl.Dir);
tbls.nsec=tbl.nsec;tbls.sini=tbl.sini;tbls.sfin=tbl.sfin;
%Set
hdls_main_windows=get(handles.Tag_occtable,'UserData');
setocurrencetable(hdls_main_windows,tbl,tbls);

%% C0
function ListBoxCallback1(source,event) % Load workspace vars into list box
awinc=amevaclass;%ameva windows commons methods
awinc.ListBoxCallback([handles.gui_time,handles.gui_xdata,handles.gui_ydata,handles.gui_dir],true);


% --- Executes on mouse motion over figure - except title and menu.
function Tag_occtable_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to Tag_occtable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
awinc=amevaclass;%ameva windows commons methods
awinc.ListBoxCallback([handles.gui_time,handles.gui_xdata,handles.gui_ydata,handles.gui_dir],true);
awinc.ListBoxCallback([handles.gui_vx,handles.gui_vy]);


% --- Executes on selection change in gui_cmtime.
function gui_cmtime_Callback(hObject, eventdata, handles)
% hObject    handle to gui_cmtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_cmtime contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_cmtime


% --- Executes during object creation, after setting all properties.
function gui_cmtime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_cmtime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_cmx.
function gui_cmx_Callback(hObject, eventdata, handles)
% hObject    handle to gui_cmx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_cmx contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_cmx


% --- Executes during object creation, after setting all properties.
function gui_cmx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_cmx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_cmy.
function gui_cmy_Callback(hObject, eventdata, handles)
% hObject    handle to gui_cmy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_cmy contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_cmy


% --- Executes during object creation, after setting all properties.
function gui_cmy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_cmy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_cmdir.
function gui_cmdir_Callback(hObject, eventdata, handles)
% hObject    handle to gui_cmdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_cmdir contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_cmdir


% --- Executes during object creation, after setting all properties.
function gui_cmdir_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_cmdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gui_close.
function gui_close_Callback(hObject, eventdata, handles)
% hObject    handle to gui_close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(gcf,'Visible','Off');


% --- Executes on selection change in gui_vy.
function gui_vy_Callback(hObject, eventdata, handles)
% hObject    handle to gui_vy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_vy contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_vy
awinc=amevaclass;%ameva windows commons methods
awinc.AmevaUpdateName(1,[],handles.gui_vy,handles.gui_svy)


% --- Executes during object creation, after setting all properties.
function gui_vy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_vy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_cmvy.
function gui_cmvy_Callback(hObject, eventdata, handles)
% hObject    handle to gui_cmvy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_cmvy contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_cmvy


% --- Executes during object creation, after setting all properties.
function gui_cmvy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_cmvy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_vx.
function gui_vx_Callback(hObject, eventdata, handles)
% hObject    handle to gui_vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_vx contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_vx
awinc=amevaclass;%ameva windows commons methods
awinc.AmevaUpdateName(1,[],handles.gui_vx,handles.gui_svx)


% --- Executes during object creation, after setting all properties.
function gui_vx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_vx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_cmvx.
function gui_cmvx_Callback(hObject, eventdata, handles)
% hObject    handle to gui_cmvx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns gui_cmvx contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_cmvx


% --- Executes during object creation, after setting all properties.
function gui_cmvx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_cmvx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
