function axdir=BoxPlotXY(X,Y,TimeDire,Tipo,nsec,idioma,var_name,var_unit,gcax,nyticks,yscale)
% Examples 1
%   load ameva;
%   figure;BoxPlotXY(excal_HsI,excal_HsR,excal_Dire,'sector',16)
% Examples 2
%   load ameva;
%   figure;BoxPlotXY([],excal_HsR,excal_datime,'monthly');

if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! XYPlot with axes'); end
set(gcf,'CurrentAxes',gcax)
if ~exist('nsec','var'), nsec=16; end
if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name={'X','Y'}; end
if ~exist('var_unit','var'), var_unit={'m','s'}; end

if ~isempty(X),
    if ~exist('nyticks','var') || isempty(nyticks)
        ceilmaxX=ceil(max(X));
        floorminX=floor(min(X));
        auxi=round((ceilmaxX-floorminX)/6);
        dirtickx=(floorminX:auxi:ceilmaxX);
        if auxi == 0
            auxi=(max(X)-min(X))/6;
            dirtickx=(min(X):auxi:max(X));
            ceilmaxX=max(X);
            floorminX=min(X);
        end
    else
        ceilmaxX=max(nyticks);
        floorminX=min(nyticks);
        dirtickx=nyticks;
    end
end
if ~isempty(Y),
    if ~exist('nyticks','var') || isempty(nyticks)
        ceilmaxY=ceil(max(Y));
        floorminY=floor(min(Y));
        auxi=round((ceilmaxY-floorminY)/6);
        dirticky=(floorminY:auxi:ceilmaxY);
        if auxi == 0
            auxi=(max(Y)-min(Y))/6;
            dirticky=(min(Y):auxi:max(Y));
            ceilmaxY=max(Y);
            floorminY=min(Y);
        end
    else
        ceilmaxY=max(nyticks);
        floorminY=min(nyticks);
        dirticky=nyticks;
    end
end

if isempty(char(var_unit(1))),
    Lab{1}=char(var_name(1));
else
    Lab{1}=[char(var_name(1)) ' (' char(var_unit(1)) ')'];
end
axis(gcax,'normal');
if ~isempty(X) && ~isempty(Y) %Si existe X and Y
    Lab{2}=[char(var_name(2)) ' (' char(var_unit(2)) ')'];
    switch Tipo
        case 'monthly'
        [Xbytimesec,TimeSecpos,dirtickst]=time_division(X,TimeDire,idioma);
        case 'sector'
        [Xbytimesec,TimeSecpos,dirtickst]=sector_division(X,TimeDire,nsec);
    end
    h2 = boxplotCsub(Xbytimesec,TimeSecpos,1,'+',1,1,'b',0,0.5,false,[1 2],Lab{1});
    set(h2(end,:),'MarkerSize',3);
    axdir=axes('Position',get(gcax,'Position'),'YAxisLocation','right','Color','none');
    switch Tipo
        case 'monthly'
        [Xbytimesec,TimeSecpos,dirtickst]=time_division(Y,TimeDire,idioma);
        case 'sector'
        [Xbytimesec,TimeSecpos,dirtickst]=sector_division(Y,TimeDire,nsec);
    end
    h1 = boxplotCsub(Xbytimesec,TimeSecpos,1,'+',1,1,'k',0,0.5,false,[2 2],Lab{2});
    set(h1(end,:),'MarkerSize',3);
    set(gcax  ,'YTick',dirtickx,'Ylim',[floorminX ceilmaxX],'XTickLabel',dirtickst,'YColor','b');
    set(axdir,'YTick',dirticky,'Ylim',[floorminY ceilmaxY],'XTickLabel',dirtickst);
    title(['Box plot of ',var_name{1},' & ',var_name{2}],'FontWeight','bold');
elseif (~isempty(X) && isempty(Y)) || isempty(X) && ~isempty(Y) %solo Y o solo X
    if ~exist('yscale','var') || isempty(yscale), yscale='linear'; end;
    if isempty(X),
        X=Y; dirtickx=dirticky;
        floorminX=floorminY; ceilmaxX=ceilmaxY;
    end
    switch Tipo
        case 'monthly'
        [Xbytimesec,TimeSecpos,dirtickst]=time_division(X,TimeDire,idioma);
        case 'sector'
        [Xbytimesec,TimeSecpos,dirtickst]=sector_division(X,TimeDire,nsec);
    end
    h2 = boxplotCsub(Xbytimesec,TimeSecpos,1,'+',1,1,'b',0,0.5,false,[1 1],Lab{1});
    set(h2(end,:),'MarkerSize',3);
    set(gcax,'YScale',yscale,'YTick',dirtickx,'Ylim',[floorminX ceilmaxX],'XTickLabel',dirtickst);
    title(['Box plot of ',var_name{1}],'FontWeight','bold');
end
grid on;

end
