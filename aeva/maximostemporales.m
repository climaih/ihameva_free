    function maximostemporales(varargin)
%   MAXIMOSTEMPORALES. Temporary Maximum
%   MAXIMOSTEMPORALES (maximostemporales.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%
% Examples 1
%   ameva-->Statistics-->Temporary Maximum
% Examples 2
%   load ameva;
%   maximostemporales(data_gow.time,data_gow.hs);
% Examples 3
%   load ameva;
%   Time = data_gow.time;	%Time
%   Hs= data_gow.hs;        %Variable X
%   Tm= data_gow.tm;        %Variable Y
%   timedefine= true;       %Time exist
%   idioma='eng';       %idioma-no implementado
%   var_name={'Hs' 'Tm'};   %Variable name
%   maximostemporales(Time,Hs,Tm,[],timedefine,idioma,var_name);
% Examples 4 maximostempfunction for IH DOW data format, with direction info
%   maximostemporales(data_dow_sdr1.time,data_dow_sdr1.hs,data_dow_sdr1.tp,data_dow_sdr1.wave_dir,true,'eng',{'Hs','Tp'});
% Examples 5 usando la funcion: maximostempfunction
%   [Tnew,Xnew]=maximostempfunction(data_gow.time,data_gow.hs,'monthly','maximum',80,1,3,'day');
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   maximostemporales(main program tool v2.0.3 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.12.0.365 (R2011a) on Windows 7
%   04-06-2011 - The first distribution version
%   16-04-2013 - Old distribution version
%   10-06-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v2.0.3 (140820)';
nmfctn='maximostemporales';
Ax1Pos=[0.095 0.130 0.725 0.815];%Posiciones plot
figPos=[];

if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar
global hsp line_xnew line_umbr;
% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.16;
btnHt=0.05;
spacing=0.01;
top=0.94;
xPos=[1-btnWid-spacing 1-2*btnWid-2*spacing];
var_lab={'Time:' '* X-Data:' 'Y-Data:' 'Dir'};
%GLOBAL VARIALBES solo usar las necesarias!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
Time=[];X=[];Y=[];Dir=[];
N=[];N_new=[];
umbral=[];qumbral=[];
nday=7;
iday=3;% independency days
timedefine.tf=false; timedefine.oo='Off';
idioma='eng'; usertype=1;
if length(varargin)==7,
    Time=varargin{1};
    X=varargin{2};
    Y=varargin{3};
    Dir=varargin{4};
    timedefine.tf=varargin{5};
    idioma=varargin{6};
    var_name=varargin{7};
    texto=amevaclass.amevatextoidioma(idioma);
elseif length(varargin)==6,
    Time=varargin{1};
    X=varargin{2};
    Y=varargin{3};
    Dir=varargin{4};
    timedefine.tf=varargin{5};
    idioma=varargin{6};
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==4,
    Time=varargin{1};
    X=varargin{2};
    Y=varargin{3};
    Dir=varargin{4};
    timedefine.tf=true;
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==3,
    Time=varargin{1};
    X=varargin{2};
    Y=varargin{3};
    timedefine.tf=true;
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==2,
    Time=varargin{1};
    X=varargin{2};
    timedefine.tf=true;
    texto=amevaclass.amevatextoidioma(idioma);
elseif nargin==1, %Los datos se seleccionan desde el espacio de trabajo
    idioma=varargin{1}.idioma;
    usertype=varargin{1}.usertype;
    texto=amevaclass.amevatextoidioma(idioma);
    disp(texto('dataClasif'));
elseif nargin==0, %Los datos se seleccionan desde el espacio de trabajo
    texto=amevaclass.amevatextoidioma(idioma);
    disp(texto('dataClasif'));
else
    texto=amevaclass.amevatextoidioma(idioma);
    error(texto('helpMaxTemp'));
end

namesoftware=[texto('nameMaxTemp'),' ',versionumber];

%compruebo
if ~exist('var_name','var') || isempty(var_name), var_name={'X'}; end %Nombre de 3 variables por defecto sino existen 
if ~isempty(find(isnan(Time),1)), disp(texto('nanTime')); end
if ~isempty(find(isnan(X),1)), disp(texto('nanX')); end
if ~isempty(find(isnan(Y),1)), disp(texto('nanY')); end
if ~isempty(find(isnan(Dir),1)), disp(texto('nanDir')); end
Tnew=[];Xnew=[];AnualTimeXnew=[];
lbx={''};lby={''};lbz={''};LabXY='';

%---LA FIGURA-VENTANA PRINCIPAL-
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off', 'Tag',['Tag_' nmfctn],'NextPlot','New');
if ~isempty(figPos), set(hf,'Position',figPos); end
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');
set(hf,'CurrentAxes',ax1);
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=0;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('on'));

btnN=btnN+1;
yPos=top-btnHt/2-(btnN-1)*(btnHt+spacing);
ui_str(1)=uicontrol('Style','Text','Units','normalized','Parent',hf,'String',['All:' num2str(N) ''], ...
    'Position',[xPos(1) yPos-btnHt*1.8 btnWid btnHt*2.5]);

btnN=btnN+1.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_str(2)=uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('varName'), ...
    'Position',[xPos(1) yPos+btnHt btnWid btnHt/2],'HorizontalAlignment','righ');
ui_ydata=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',lby, ...
    'FontWeight','Bold','Position',[xPos(1) yPos btnWid*2/3 btnHt],'Value',1,'Callback',@ActDatos);
ui_xyname=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',char(var_name(1)), ...
    'Position',[xPos(1)+btnWid*2/3 yPos btnWid/3 btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_str(3)=uicontrol('Style','Text','Units','normalized','Parent',hf,'String',texto('typeby'), ...
    'Position',[xPos(1) yPos-btnHt*1.8 btnWid btnHt*2.5]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_seltype=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',lbz, ...
    'FontWeight','Bold','Position',[xPos(1) yPos btnWid btnHt],'Value',1,'Callback',@ActSelType);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_quan=uicontrol('Style','edit','Units','normalized','Parent',hf,'String','0.95', ...
    'Position',[xPos(1) yPos btnWid btnHt]);
ui_umbral=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(umbral),'UserData',umbral, ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Callback',@(source,event)ActQUmbral('umbral',source,event));
ui_qumbral=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(qumbral),'UserData',qumbral, ...
    'Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt],'Callback',@(source,event)ActQUmbral('qumbral',source,event));

btnN=btnN+0.7;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_sumb=uicontrol('Style','text','Units','normalized','Parent',hf,'String','Threshold(x,q(x))', ...
    'Position',[xPos(1) yPos btnWid btnHt*.7]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_porc=uicontrol('Style','edit','Units','normalized','Parent',hf,'String','0', ...
    'Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt],'Tag','tg_uiporc','Enable','off');
calltogle=['me = get(gcf,''CurrentObject'');',...
    'if (get(me,''Value'')==1),',...
    'set(findobj(''Tag'',''tg_uiporc''),''string'',''80'',''Enable'',''on'');',...
    'else,','set(findobj(''Tag'',''tg_uiporc''),''string'',''0'',''Enable'',''off'');','end; clear me;'];
ui_tporc=uicontrol('Style','togglebutton','Units','normalized','Parent',hf,'String',{'min. %' 'of data'}, ...
    'Position',[xPos(1) yPos btnWid/2 btnHt*1.2],'Value',0,'Callback',calltogle);

btnN=btnN+1.35;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_d(1)=uicontrol('Style','text','Units','normalized','Parent',hf,'String','dir. ini','BackgroundColor',colorG,'Position',[xPos(1) yPos+0.05 btnWid/2 btnHt*0.66]);
ui_d(2)=uicontrol('Style','text','Units','normalized','Parent',hf,'String','dir. fin','BackgroundColor',colorG,'Position',[xPos(1)+btnWid/2 yPos+0.05 btnWid/2 btnHt*0.66]);
ui_d(3)=uicontrol('Style','edit','Units','normalized','Parent',hf,'String','0','Position',[xPos(1) yPos btnWid/2 btnHt],'Userdata',0,'Callback',@ActDir);
ui_d(4)=uicontrol('Style','edit','Units','normalized','Parent',hf,'String','360','Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt],'Userdata',1,'Callback',@ActDir);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_xdata=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',lbx, ...
    'FontWeight','Bold','Position',[xPos(1) yPos btnWid btnHt],'Value',1,'Callback',@ActVar);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_sdaydat=uicontrol('Style','text','Units','normalized','Parent',hf,'String','N days', ...
    'Position',[xPos(1) yPos btnWid btnHt]);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ndaydat=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(nday), ...
    'Position',[xPos(1) yPos btnWid btnHt]);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_itest=uicontrol('Style','Check','Units','normalized','Parent',hf,'String','Indepen. Text', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@checkit,'Value',1);

ui_txtfrm(1)=uicontrol('Tag','tag_txtf1','Style','frame','Units','normalized','Parent',hf,'Position',[xPos(2) yPos-2.3*btnHt btnWid*1.05 btnHt*3.8]);
ui_txtfrm(2)=uicontrol('Tag','tag_txtf2','Style', 'Edit','Units','normalized','Parent',hf,'Position',[xPos(2)+spacing yPos+spacing btnWid*0.95 btnHt],'String','');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_dhi=uicontrol('Style','popup','Units','normalized','Parent',hf,'String',{'day' 'hour'}, ...
    'Position',[xPos(1) yPos btnWid/2 btnHt],'Value',1);
ui_idays=uicontrol('Style','edit','Units','normalized','Parent',hf,'String',num2str(iday), ...
    'Position',[xPos(1)+btnWid/2 yPos btnWid/2 btnHt]);

ui_txtfrm(3)=uicontrol('Tag','tag_txtf3','Style', 'Edit','Units','normalized','Parent',hf,'Position',[xPos(2)+spacing yPos+spacing btnWid*0.95 btnHt],'String','');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_apply=uicontrol('Style','push','Units','normalized','Parent',hf,'String','set var. name', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Enable','off');

ui_txtfrm(4)=uicontrol('Tag','tag_txtf4','Style', 'Edit','Units','normalized','Parent',hf,'Position',[xPos(2)+spacing yPos+spacing btnWid*0.95 btnHt],'String','');
EnableButton(ui_txtfrm,'Visible','Off');
set(ui_apply,'Callback',@(source,event)EnableButton(ui_txtfrm,'Visible','on'));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_start=uicontrol('Style','push','Units','normalized','Parent',hf,'String','Start', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@MaxTempRun,'FontWeight','bold');

ui_ascale=uicontrol('Style','Check','Units','normalized','Parent',hf,'String','within a year', ...
    'Position',[xPos(2) yPos btnWid*1.05 btnHt],'Callback',@AnnualScale);

if timedefine.tf, timedefine.oo='On'; end
XY=X;%Por defecto se utiliza X
if not(isempty(XY)), ActPopup; end;%Acutliza los lbx lby lbz los popup
ActDatos;%Actualiza los datos y plots


ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.65 0.03],...
    'String',[nmfctn, texto('maxType')],'HorizontalAlignment','left');

%% Data
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('setting');
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(end) btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid(5) btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(ii));
end; clear ii;

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Set Data','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@AmevaSetData);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close','Position',[xPos(1) yPos btnWid(1) btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;

%% C0 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data']});
    end
%% C0 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
        ltsplot(nval);
    end
%% C1 Refresh plot
    function ltsplot(nlv,source,event)
        if get(ui_all(nlv),'Value')>1,
            nlvn=get(ui_td(nlv),'String');
            nlvn=char(nlvn(1));
            try
                awinc.AmevaEvalData(nlv,ui_cm,ui_all);
                X_=awinc.data;
                if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end
                nmsize=size(X_);
                set(ui_td(nlv),'string',{nlvn,sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
                set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
                if min(nmsize)==1
                    plot(ax1,X_,'k');
                else
                    plot(ax1,X_);
                end
                grid on;
                title(ax1,'Serie','FontWeight','bold');
            catch ME
                disp(texto('variableErase'));
                rethrow(ME);
            end
        end
    end
%% P0 Actualiza los popup lbx lby lbz
    function ActPopup
        lbx={'N_data'};%select
        lby={'X'};%select variable
        % serieTipo='';
        % eval(['serieTipo=' texto('serieMax') ';'])
        % lbz=serieTipo;
        lbz={'maximum','mean','quantile','threshold','pot'};
        set(ui_xdata,'String',lbx,'Value',1);
        set(ui_ydata,'String',lby,'Value',1);
        set(ui_seltype,'String',lbz,'Value',1);
        if ~isempty(Y),
            lby(end+1)={'Y'};
            set(ui_ydata,'String',lby,'Value',1);
        end
        if timedefine.tf, 
            lbx={'annual','monthly','days','N_data'};
            set(ui_xdata,'String',lbx,'Value',1);
        end
	end
%% P0 Acutaliza los DATOS X o Y
    function ActDatos(source,event)
        if isempty(Dir), EnableButton(ui_d,'Visible','off'); else EnableButton(ui_d,'Visible','on'); end;
        if get(ui_ydata,'Value')==1 && not(isempty(X)),
            XY=X;
            LabXY=labelsameva(var_name(1));
        elseif get(ui_ydata,'Value')==2 && not(isempty(Y)),
            XY=Y;
            LabXY=labelsameva(var_name(2));
        else
            ActAllButton('Off');
            set(ui_start,'Enable','Off');
            return;
        end
        N=length(XY);N_new=1:N;
        set(ui_xyname,'String',LabXY);%Label to save var_name
        
        ActPlots;
        ActDatosIni;
    end
%% P0 Acutaliza los datos
    function ActDatosIni
        Tnew=[];Xnew=[];AnualTimeXnew=[];
        umbral=min(XY(N_new));
        N=length(XY(N_new));
        AnnualScale;
        if get(ui_seltype,'Value')==4 || get(ui_seltype,'Value')==5,%1-annual, 2-monthly, 3-N days, 4-threshold, 5-POT
            set(ui_qumbral,'String','0.75','UserData',0.75);
            ActQUmbral('qumbral');
        end
        set(ui_str(1),'String',['All:' num2str(N) '']);
    end
%% P0 Acutaliza los PLOTS
    function ActPlots(source,event)
        %PLOT
        if timedefine.tf==false && length(XY)>1, Time=1:length(XY); end
        if length(XY)>1
            umbral=min(XY);
            set(ui_str(1),'String',['All:' num2str(N) '']);
            hsp=plot(ax1,Time,XY,'k'); ylabel(ax1,LabXY);
            line_xnew=line([Time(1) Time(1)],[umbral umbral],'Color','r','Marker','.','LineStyle','-','Parent',ax1);
            line_umbr=line([Time(1) Time(end)],[umbral umbral],'Color','b','Marker','.','LineStyle','-','Parent',ax1,'Visible','off');
            set(ax1,'XGrid','on','YGrid','on','XLim',[Time(1) Time(end)],'Visible','on')
            ylabel(ax1,LabXY);xlabel(ax1,'Time');
            if timedefine.tf
                set(ax1,'XTick',linspace(Time(1),Time(end),12)); datetick(ax1,'x','mmmyy','keepticks');
            end
            ActSelType;
            set(ui_start,'Enable','On');
        else
            set(ui_start,'Enable','off');
            disp(texto('noData'));
        end
    end
%% P0 Actualiza (visible on/off) the type of maximum: maximum/mean/quantile/threshold/POT
    function ActSelType(source,event)
        iins=get(ui_seltype,'String');
        tipo=iins{get(ui_seltype,'Value')};%1-maximum 2-mean 3-quantile 4-threshold 5-POT       
        EnableButton([ui_xdata,ui_tporc,ui_porc,ui_quan,ui_sumb,ui_umbral,ui_qumbral,line_umbr],'Visible','off');
        Tipo=false;%No test de independencia
        if strcmp(tipo,'maximum')
            Tipo=true;
            EnableButton([ui_xdata,ui_tporc,ui_porc],'Visible','on');
        elseif strcmp(tipo,'mean')
            EnableButton([ui_xdata,ui_tporc,ui_porc],'Visible','on');
        elseif strcmp(tipo,'quantile')
            EnableButton([ui_xdata,ui_tporc,ui_porc,ui_quan],'Visible','on');
        elseif strcmp(tipo,'threshold')
            set(ui_qumbral,'String','0.75','UserData',0.75);
            ActQUmbral('qumbral');
            EnableButton([ui_sumb,ui_umbral,ui_qumbral,line_umbr],'Visible','on');
        elseif strcmp(tipo,'pot')
            Tipo=true;
            set(ui_qumbral,'String','0.75','UserData',0.75);
            ActQUmbral('qumbral');
            EnableButton([ui_sumb,ui_umbral,ui_qumbral,line_umbr],'Visible','on');
        end
        ActVar(Tipo);
        EnableButton(ui_txtfrm,'String','');
    end
%% Actualizo todos los botones al encender o setdata
    function ActAllButton(tipo)
        EnableButton([ui_itest,ui_idays,ui_dhi,ui_ascale],'Visible',tipo);
        EnableButton([ui_sdaydat,ui_ndaydat,ui_quan,ui_umbral,ui_qumbral,ui_sumb],'Visible',tipo);
        EnableButton([ui_str,ui_xdata,ui_ydata,ui_xyname,ui_seltype,ui_porc,ui_tporc],'Visible',tipo);
    end
%%  Actualizo los botones de N data/day y los botones del test de indepen
    function ActVar(iTipo,source,event)
        % Actualizo los botones de N data/day
        iins=get(ui_xdata,'String');
        tipo=iins{get(ui_xdata,'Value')};
        EnableButton([ui_sdaydat,ui_ndaydat],'Visible','off');
        if  strcmpi(get(ui_xdata,'Visible'),'On') && sum(strcmp(tipo,{'N_data','days'}))>0
            EnableButton([ui_sdaydat,ui_ndaydat],'Visible','On');
            if strcmp(tipo,'N_data')
                set(ui_sdaydat,'String','N data');
            else
                set(ui_sdaydat,'String','N days');
            end
        end
        % Actualizo los botones del test de independencia
        if not(islogical(iTipo)), 
            iTipo=false; 
            if sum(strcmp(tipo,{'annual','monthly','days'}))>0
                iTipo=true;
            end
        end
        VisTimeB='Off';
        if iTipo
            if timedefine.tf && iTipo,
                VisTimeB='On';
            else
                set(ui_itest,'value',0);
                checkit;
            end
        end
        EnableButton([ui_itest,ui_idays,ui_dhi],'Visible',VisTimeB);
        set(ui_ascale,'Value',0,'Visible',timedefine.oo);
    end
%% P0 Actualiza dya hour par el test de independencia
    function checkit(source,event)
        if get(ui_itest,'value');
            set(ui_idays,'Enable','on');
            set(ui_dhi,'Enable','on');
        else
            set(ui_idays,'Enable','off');
            set(ui_dhi,'Enable','off');
        end
    end
%% P0 Acutaliza QUmbral cuando cambia el umbral y el quantil
    function ActQUmbral(strxy,source,event)
        if strcmp(strxy,'qumbral')
            qumbral=str2double(get(ui_qumbral,'String'));
            if qumbral<0 || qumbral>1 || isnan(qumbral),
                qumbral=0.75;        set(ui_qumbral,'String',num2str(qumbral));
                disp(texto('qValue'));
            end
            umbral=quantile(XY(N_new),qumbral);
        elseif strcmp(strxy,'umbral')
            umbral=str2double(get(ui_umbral,'String'));
            if umbral<min(XY(N_new)),
                umbral=min(XY(N_new));        set(ui_umbral,'String',num2str(umbral));
                disp([texto('umbralValue') ' ',num2str(min(XY(N_new))),'. ',texto('setMin')]);
            end
            if umbral>max(XY(N_new)),
                umbral=max(XY(N_new));        set(ui_umbral,'String',num2str(umbral));
                disp([texto('umbralValue_1') ' ',num2str(max(XY(N_new))),'. ',texto('setMax')]);
            end
            if isnan(umbral),
                umbral=quantile(XY(N_new),0.75);        set(ui_umbral,'String',num2str(umbral));
                disp(texto('umbralValue_2'));
            end
            [aax,lagsx]=ecdf(XY(N_new));
            qumbral=interp1q(lagsx,aax,umbral);
        end
        set(line_umbr,'YData',[umbral umbral],'Visible','on');
        set(ui_qumbral,'String',num2str(qumbral),'UserData',qumbral);
        set(ui_umbral,'String',num2str(umbral),'UserData',umbral);
    end
%% P0 Actualiza sini sfin
    function ActDir(source,event)
        tipo=get(source,'UserData');
        angulo=str2double(get(source,'String'));
        if angulo>360 || angulo<0, %verifico que el angulo este entre 0 360
            disp(texto('attenAngle'));
            if tipo,
                set(source,'String','360');
            else
                set(source,'String','0');
            end
        end
        if str2double(get(ui_d(3),'String'))==str2double(get(ui_d(4),'String')),
            disp(texto('valid_1'));
            set(ui_d(4),'String','360');
            set(ui_d(3),'String','0');
        end
        %por defecto se busca el intervalo del sector seleccionado
        %Seleccion por direccion solo sino esta vacio Dir
        if ~isempty(Dir)
            aux=selectdirection;
            if ~isempty(aux), N_new=aux; end
            ActDatosIni;
        end
    end
%% P0 Annual scale
    function AnnualScale(source,event)
        if N<10, disp([texto('noData') ' N=',num2str(N)]); return; end
        if  get(ui_ascale,'Value')
            set(hsp,'YData',XY(N_new),'Xdata',mod(yearlytimescale(Time(N_new)),1),'LineStyle','none','Marker','.','MarkerSize',4);
            set(line_xnew,'YData',Xnew,'XData',mod(AnualTimeXnew,1),'LineStyle','none','Marker','^','MarkerSize',4);
            set(line_umbr,'XData',[0 1]);
            set(ax1,'XTick',0:1/24:1);
            % auxVari='';
            % eval(['auxVari=' texto('mesespacio') ';']);
            % set(ax1,'XTickLabel',auxVari)
            set(ax1,'XTickLabel',{'','J','','F','','M','','A','','M','','J','','J','','A','','S','','O','','N','','D',''});
            set(ax1,'XLim',[0 1]);
        else
            set(hsp,'YData',XY(N_new),'Xdata',Time(N_new),'LineStyle','-','Marker','none');
            set(line_xnew,'YData',Xnew,'XData',Tnew,'LineStyle','none','Marker','^','MarkerSize',4);
            set(line_umbr,'XData',[Time(N_new(1)) Time(N_new(end))]);
            if timedefine.tf
                set(ax1,'XTick',linspace(Time(N_new(1)),Time(N_new(end)),12)); datetick(ax1,'x','mmmyy','keepticks');
            end
        end
        ylabel(ax1,LabXY);
    end
%% P0 Set data comprobamos que los Data para calcular la calibaracion son
    function AmevaSetData(source,event)
        ione=get(ui_all(1),'Value');
        itwo=get(ui_all(2),'Value');
        ithr=get(ui_all(3),'Value');
        ifou=get(ui_all(4),'Value');
        timedefine.tf=false; timedefine.oo='Off';
        set(ui_ascale,'Value',0);
        if itwo>1,
            awinc.AmevaEvalData(2,ui_cm,ui_all);
            X=awinc.data;
            var_name(1)={awinc.var_name};
            N_new=(1:length(X));% Todos los datos por defecto!;
        else
            Time=[];X=[];Y=[];Dir=[];
            N=[];N_new=[];
            umbral=[];qumbral=[];
%             nday=7;
%             iday=3;% independency days
%             timedefine.tf=false; timedefine.oo='Off';
            ActDatos;drawnow;
            error(texto('xData'));
        end
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        if ione>1,
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            Time=awinc.data;
            timedefine.tf=true;
            awinc.AmevaTimeCheck(ui_tbd,Time);	
            if awinc.state==true, 
                set(ui_tbd,'String',texto('dataFormat_1'));
                disp(texto('dataFormat_1'));
                set(hg,'Visible','on'); return;
            end
        else
            disp(texto('notTemporaryData'));
            Time=(1:length(X))';
            timedefine.tf=false;
        end
        if ithr>1,
            awinc.AmevaEvalData(3,ui_cm,ui_all);
            Y=awinc.data;
            var_name(2)={awinc.var_name};
        else
            Y=[];
        end
        if ifou>1,
            awinc.AmevaEvalData(4,ui_cm,ui_all);
            Dir=awinc.data;
        else
            Dir=[];
        end
        %compruebo que las tres variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,X,Time,'X-Data','Time');      if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Y,Time,'Y-Data','Time');      if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Y,X,'Y-Data','X-Data');       if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,Time,'Dir','Time');       if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,X,'Dir','X-Data');        if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,Dir,Y,'Dir','y-Data');        if awinc.state, set(hg,'Visible','on'); return; end,
        
        ActAllButton('On');%Activo todos los botones
        set(ui_ndaydat,'String','7');
        if timedefine.tf, timedefine.oo='On'; end
        XY=X;%Por defecto se utiliza X
        if not(isempty(XY)), ActPopup; end;%Acutliza los lbx lby lbz los popup
        ActDatos;%Actualiza los datos y plots

        
        
        set(ui_tbd,'String',texto('timeOptions'));drawnow ;
        set([ui_start,ui_apply],'Enable','on');
    end
%% Run data
    function MaxTempRun(source,event)
        set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
        FigDataOO('Off');
        
        iins=get(ui_xdata,'String');
        tipo=iins{get(ui_xdata,'Value')};%1-annual, 2-monthly, 3-N days
        
        %compruebo que el maximo cada n dias se correcto
        ndaysm=[];
        if strcmp(tipo,'days')
            ndaysm=round(str2double(get(ui_ndaydat,'String')));
            if isnan(ndaysm) || ndaysm<1 || ndaysm>length(Time(N_new)),
                ndaysm=nday;        set(ui_ndaydat,'String',num2str(ndaysm));
                disp([texto('missing') '[' num2str(nday) ']']);
                
            end
        end
        idtest=get(ui_itest,'Value');%usar o no test de idenpendencia 0/1
        iins=get(ui_dhi,'String');
        dayhouri=iins{get(ui_dhi,'Value')};%1-day, 2-hour
        %compruebo que la seperacion de dias sea correcta
        ndaysi=str2double(get(ui_idays,'String'));%dias independientes
        if isnan(ndaysi) || ndaysi<1,
            ndaysi=iday;        set(ui_idays,'String',num2str(ndaysi));
            disp(texto('idayValue'));
        end
        if  get(ui_itest,'Value')%test de independencia comprobaciones
            if strcmp(tipo,'days') && strcmp(dayhouri,'day') && ndaysi>=round(ndaysm/2), msgbox(texto('separateDays_1')); return;
            elseif strcmp(tipo,'monthly') && strcmp(dayhouri,'day') && ndaysi>=30/2, msgbox(texto('separateDays_2')); return;
            elseif strcmp(tipo,'annual') && strcmp(dayhouri,'day') && ndaysi>=364/2, msgbox(texto('separateDays_3')); return;
            end
        end
        
        %SELECCION: selected type
        iins=get(ui_seltype,'String');
        maxtype=iins{get(ui_seltype,'Value')};%{'maximum','mean','quantile','threshold','pot'}
        %Compruebo que el quantile entra correctamente
        p=str2double(get(ui_quan,'String'));%quantile 0-1
        if p<0 || p>1 || isnan(p),
            p=0.95;        set(ui_quan,'String',num2str(p));
            disp(texto('quantileValue'));
        end
        %Compruebo que el porcengaje entra correctamente
        porcentage=str2double(get(ui_porc,'String'));
        if porcentage<0 || porcentage>100 || isnan(porcentage),
            porcentage=80;        set(ui_porc,'String',num2str(porcentage));
            disp(texto('percentageValue'));
        end
        
        %Seleccion por direccion solo sino esta vacio Dir
        if ~isempty(Dir)
            aux=selectdirection;
            if ~isempty(aux), N_new=aux; end
        end
        
        try
            set([ui_start,ui_apply],'Enable','off');drawnow;
            if strcmp(maxtype,'pot') && timedefine.tf==false %Solo si no existe tiempo y se quiere pot se hace pot simple
                auxx=duration_t((XY(N_new)>=umbral),true,XY(N_new));
                auxt=Time(N_new);
                Tnew=auxt(auxx(:,5));Tnew=Tnew(:);AnualTimeXnew=Tnew;
                Xnew=auxx(:,4);clear auxt auxx;
                %OJO OJO OMAR, intentar ver si esta opcion se puede meter dentro de la funcion maximostempfunction
            else
                [Tnew,Xnew,AnualTimeXnew]=maximostempfunction(Time(N_new),XY(N_new),tipo,maxtype,porcentage,idtest,ndaysi,dayhouri,ndaysm,p,umbral);
            end
        catch ME
            set(ui_start,'Enable','on');
            rethrow(ME)
        end
        
        if ~isempty(Tnew) && ~isempty(Xnew)
            val=get(ui_seltype,'Value');% para saber si es pot
            maxtype_=maxtype;%maxtype aux
            if length(maxtype)>4, maxtype_=maxtype(1:3); end
            laby=char(get(ui_xyname,'String'));%Label to save var_name
            
            if isempty(get(ui_txtfrm(2),'String')), set(ui_txtfrm(2),'String',[laby,'_',maxtype_,'_X']); end
            if isempty(get(ui_txtfrm(3),'String')), set(ui_txtfrm(3),'String',[laby,'_',maxtype_,'_Time']); end
            if ismember(val,[4,5])
                if isempty(get(ui_txtfrm(4),'String')), set(ui_txtfrm(4),'String',[laby,'_',maxtype_,'_umbral']); end
            end
            %save var to workspace
            assignin('base',get(ui_txtfrm(2),'String'),Xnew);
            assignin('base',get(ui_txtfrm(3),'String'),Tnew);
            if not(timedefine.tf), assignin('base',get(ui_txtfrm(3),'String'),(1:length(Xnew))'); end
            if ismember(val,[4,5])
                assignin('base',get(ui_txtfrm(2),'String'),Xnew-umbral);
                assignin('base',get(ui_txtfrm(4),'String'),umbral);
            end
            assignin('base',[get(ui_txtfrm(3),'String') '_posindx'],find(ismember(Time,Tnew)));
            disp([datestr(now,'yyyymmddHHMMSS') texto('dataWorks')]);
            amevaws_update_listbox_ext;
        else
            AnualTimeXnew=[];
        end
        set(ui_txtfrm,'Visible','Off');
        set([ui_start,ui_apply],'Enable','on');
                
        %set plots
        AnnualScale(XY(N_new),Time(N_new));
        set(ui_str(1),'String',['Sel:' num2str(length(Tnew))]);
        set(ui_tb,'String',[nmfctn, texto('ready_1')]);drawnow ;
        ylabel(ax1,laby);
    end
        function aux=selectdirection
            sini=str2num(get(ui_d(3),'String'));
            sfin=str2num(get(ui_d(4),'String'));
            if ~isempty(Dir) && sini>=0 && sfin<=360 && sini<=360 && sfin>=0,%SECTOR OPTIONS POT
                if (sini<sfin),
                    aux=find(sini<=Dir & Dir<sfin);
                    if isempty(aux)
                        disp([texto('valid_1'),num2str(sini), ' ' texto('valid_2'),num2str(sfin)]); set(ui_start,'Enable','on'); return;
                    end
                else
                    aux=find(sini<=Dir | Dir<sfin);
                    if isempty(aux)
                        disp([texto('valid_1'),num2str(sini), ' ' texto('valid_2'),num2str(sfin)]); set(ui_start,'Enable','on'); return;
                    end
                end
            end
        end

end
