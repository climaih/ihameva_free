function XYPlot(Time,X,Y,tipolinea,var_name,var_unit,gcax)

if ~exist('tipolinea','var') || isempty(tipolinea), tipolinea='-';end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! XYPlot with axes'); end
set(gcf,'CurrentAxes',gcax)
if ~exist('var_name','var'), var_name={'X','Y'}; end
if ~exist('var_unit','var'), var_unit={'m','s'}; end

Lab=labelsameva(var_name(2:end),var_unit(2:end));%Labels


axis(gcax,'xy');
%if Timeaux(1)~=1, datetick(ax1,'x','mmmyy','keepticks'); end
taux=datevec(Time(1));
xData=linspace(Time(1),Time(end),12);%Tick of time data
if ~isempty(X) && ~isempty(Y) %Si existe X and Y
	[AX,H1,H2]=plotyy(gcax,Time,X,Time,Y,'plot');
    set(get(AX(1),'Ylabel'),'String',Lab(1))
    set(get(AX(2),'Ylabel'),'String',Lab(2))
    if length(X)>1, xlim(AX(1),[Time(1),Time(end)]); end
    if length(X)>1, xlim(AX(2),[Time(1),Time(end)]); end
    xlabel(gcax,var_name{1});
    title(gcax,['Serie of ',var_name{2} ' & ' var_name{3}],'FontWeight','bold'); 
    if ~strcmp(tipolinea,'none')
        set(H1,'LineStyle',tipolinea)
        set(H2,'LineStyle',tipolinea)
    else
        set(H1,'LineStyle',tipolinea)
        set(H2,'LineStyle',tipolinea)
        set(H1,'Marker','+')
        set(H2,'Marker','x')
    end
    grid on;
    if taux(1)>1947
        set(AX(1),'XTick',xData);
        set(AX(2),'XTick',xData);
        datetick(AX(1),'x','mmmyy','keepticks');
        datetick(AX(2),'x','mmmyy','keepticks');
    end
end

if ~isempty(X) && isempty(Y) %Si existe solo X
    xData=linspace(Time(1),Time(end),12);
	H1=plot(gcax,Time,X,'k');
    ylabel(gcax,Lab(1))
    if length(X)>1, xlim(gcax,[Time(1),Time(end)]); end
    xlabel(gcax,var_name{1});
    title(gcax,['Serie of ',var_name{2}]); 
    if ~strcmp(tipolinea,'none')
        set(H1,'LineStyle',tipolinea)
    else
        set(H1,'LineStyle',tipolinea)
        set(H1,'Marker','+')
    end
    grid on;
    if taux(1)>1947
        set(gcax,'XTick',xData);
        datetick(gcax,'x','mmmyy','keepticks');
    end
end

