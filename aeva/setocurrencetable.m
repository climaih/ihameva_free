function setocurrencetable(hdls_main_windows,tbl,tbls)

gui_occurrencetable=hdls_main_windows(1);
gui_start=hdls_main_windows(2);
gui_popp_xvar=hdls_main_windows(3);
gui_popp_yvar=hdls_main_windows(4);
ui_xdiv=hdls_main_windows(5);
ui_nxdiv=hdls_main_windows(6);
ui_ydiv=hdls_main_windows(7);
ui_nydiv=hdls_main_windows(8);

set(gui_occurrencetable,'UserData',tbl);%Datos to table All
set(gui_start,'UserData',tbls);%No data to start for memory
if isempty(tbl.X), 
    set(gui_start,'Enable','off'); 
else
    set(gui_start,'Enable','on'); 
end
    
%set el x listbox
lbx={'none'};
if not(isempty(tbl.X)), lbx(1)={'X'}; end
if not(isempty(tbl.Y)), lbx(end+1)={'Y'}; end
if not(isempty(tbl.Dir)), lbx(end+1)={'Dir'}; end
if not(isempty(tbl.Dir)) && tbl.nsec>0, lbx(end+1)={'Sector'}; end
if length(tbl.Time)>0 && tbl.timedefine, lbx(end+1)={'Monthly'}; end
set(gui_popp_xvar,'String',lbx,'Value',min(1,length(lbx)));

%set el y listbox
lby={'none'};
if not(isempty(tbl.X)), lby(1)={'X'}; end
if not(isempty(tbl.Y)), lby(end+1)={'Y'}; end
if not(isempty(tbl.Dir)), lby(end+1)={'Dir'}; end
set(gui_popp_yvar,'String',lby,'Value',min(2,length(lby)));

%set nx ndx
if isempty(tbls.Vx),
    set(ui_xdiv,'Enable','On','String','10');
    set(ui_nxdiv,'String','0');
    if not(isempty(tbl.X)), 
        set(ui_nxdiv,'String',num2str((ceil(max(tbl.X))-floor(min(tbl.X)))/10));
    end
else
    set(ui_xdiv,'Enable','Off','String',num2str(length(tbls.Vx)-1));
    set(ui_nxdiv,'String','');
end
%set ny ndy
if isempty(tbls.Vy) && not(strcmp(char(lby(get(gui_popp_yvar,'Value'))),'none')),
    set(ui_ydiv,'Enable','On','String','10');
    set(ui_nydiv,'String','0');
    if  eval(['not(isempty(tbl.' char(lby(get(gui_popp_yvar,'Value'))) '))']), 
        set(ui_nydiv,'String',num2str(eval(['(ceil(max(tbl.' char(lby(get(gui_popp_yvar,'Value'))) '))-floor(min(tbl.' char(lby(get(gui_popp_yvar,'Value'))) ')))/10'])));
    end
else
    set(ui_ydiv,'Enable','Off','String',num2str(length(tbls.Vy)-1));
    set(ui_nydiv,'String','');
end

end