function [Xbytime,Timepos,dirtickst]=time_division(X,Time,idioma)
if ~exist('idioma','var') || isempty(idioma) || ~ischar(idioma), idioma='eng'; end;

Xbytime=[];
Timepos=[];
pareto=[];

dirtickst_={'J','F','M','A','M','J','J','A','S','O','N','D'};
if strcmp(idioma,'esp')
    dirtickst_={'E','F','M','A','M','J','J','A','S','O','N','D'};
end

TimeVec=datevec(Time);
j=1;
for i=1:12
    aux=find(TimeVec(:,2)==i);
    if ~isempty(aux)
        Xbytime=[Xbytime X(aux)'];
        Timepos=[Timepos i*ones(1,length(aux))];
        dirtickst(j)=dirtickst_(i);
        j=j+1;
    end
end

end