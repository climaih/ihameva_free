function [TB,B,NX,NY,ndx,ndy,labx,laby,lowupx,lowupy]=occurrencetablefnt(X_,Y_,nx,ny,sx,sy,vx,vy,sini,sfin,nsec,Time,Dir,rndclass,var_name,Vx,Vy)

% rndclass 0/1 .- si desea redondear las clases en las que se dividen las variables

if strcmp(sx(vx),'X'),
    X=X_; 
    ndx=(ceil(max(X))-floor(min(X)))/nx; % Division de las clases redondeando
    if rndclass,
        ndx=(max(X)-min(X))/nx;
    end
    NX=length(X);labx=char(var_name(1));
    if not(isempty(Vx)), lowupx=Vx; nx=length(Vx)-1; end
elseif strcmp(sx(vx),'Y'),
    X=Y_; 
    ndx=(ceil(max(X))-floor(min(X)))/nx; % Division de las clases redondeando
    if rndclass,
        ndx=(max(X)-min(X))/nx;
    end
    NX=length(X);labx=char(var_name(2));
    if not(isempty(Vy)), lowupx=Vy; nx=length(Vy)-1; end
elseif strcmp(sx(vx),'Dir'),
    X=Dir;
    ndx=(ceil(max(X))-floor(min(X)))/nx; % Division de las clases redondeando
    if rndclass,
        ndx=(max(X)-min(X))/nx;
    end
    NX=length(X);labx=char(var_name(3));
elseif strcmp(sx(vx),'Sector'),
    if sfin==360 && sini==0, 
        X=(sini:abs(sfin-sini)/nsec:sfin);
    elseif sfin<360 || sini>0,
        X=[(sini:abs(sfin-sini)/nsec:sfin) sfin+abs(sfin-sini)/nsec];
    else
        error('ATENTION! sector final and initial must be 0<sfin,sini<=360');
    end
    ndx=X(end-1)-X(end-2);
    NX=length(Dir);labx='Sector';
elseif strcmp(sx(vx),'Monthly'),
    TimeVec=datevec(Time);
    X=(0:12);
    ndx=1; 
    NX=length(Time);labx='Monthly';
end
if strcmp(sy(vy),'X'),
    Y=X_; 
    ndy=(ceil(max(Y))-floor(min(Y)))/ny; % Division de las clases redondeando
    if rndclass,
        ndy=(max(Y)-min(Y))/ny;
    end
    laby=char(var_name(1));
    if not(isempty(Vx)), lowupy=Vx; ny=length(Vx)-1; end
elseif strcmp(sy(vy),'Y'),
    Y=Y_; 
    ndy=(ceil(max(Y))-floor(min(Y)))/ny; % Division de las clases redondeando
    if rndclass,
        ndy=(max(Y)-min(Y))/ny;
    end
    laby=char(var_name(2));
    if not(isempty(Vy)), lowupy=Vy; ny=length(Vy)-1; end
elseif strcmp(sy(vy),'Dir'),
    Y=Dir;
    ndy=(ceil(max(Y))-floor(min(Y)))/ny; % Division de las clases redondeando
    if rndclass,
        ndy=(max(Y)-min(Y))/ny;
    end
    laby=char(var_name(3));
end
NY=length(Y);
% Redondear las class  X
if strcmp(sx(vx),'Monthly') || strcmp(sx(vx),'Sector') %No se redonde de ninguna manera Monthly o Sector
    lowupx=(min(X):ndx:max(X));
else
    if isempty(Vx) || strcmp(sx(vx),'Dir') || not(exist('lowupx','var'))
        lowupx=(floor(min(X)):ndx:ceil(max(X)));
        if rndclass, % Division de las clases sin redondear
            lowupx=(min(X):ndx:max(X));
        end
    end
end
% Redondear las class  Y
if isempty(Vy) || strcmp(sy(vy),'Dir') || not(exist('lowupy','var'))
    lowupy=(floor(min(Y)):ndy:ceil(max(Y)));
    if rndclass, % Division de las clases sin redondear
        lowupy=(min(Y):ndy:max(Y));
    end
end
TB=zeros(nx,ny);%Tabla de encuentros
for j=1:ny, B(1,j+2)={lowupy(j)}; end
for j=1:ny, B(2,j+2)={lowupy(j+1)}; end
if strcmp(sx(vx),'Sector') %En el caso de sectores estos se centran en la mitad!
    for i=1:nx,
        auxini=lowupx(i)-ndx/2;if auxini<0, auxini=360+auxini; end
        auxfin=lowupx(i+1)-ndx/2;if auxfin>360, auxfin=auxfin-360; end
        B(i+2,1)={auxini}; 
        B(i+2,2)={auxfin};
    end
else
    for i=1:nx, B(i+2,1)={lowupx(i)}; end
    for i=1:nx, B(i+2,2)={lowupx(i+1)}; end
end
for i=1:nx, %if monthly i=1:12 if sector i=1:nsec
    for j=1:ny,
        %absice X Y Dir Sector Monthly
        auxx=zeros(NX,1);
        if strcmp(sx(vx),'X') || strcmp(sx(vx),'Y') || strcmp(sx(vx),'Dir'),
            aux_=find( X >= lowupx(i) & X < lowupx(i+1) );
            if i==nx, aux_=find( X >= lowupx(i) & X <= lowupx(i+1) ); end
        elseif strcmp(sx(vx),'Monthly')
            TimeVec=datevec(Time);
            aux_=find(TimeVec(:,2)==i);
        elseif strcmp(sx(vx),'Sector') %En el caso de sectores estos se centran en la mitad!
            lmini=X(i)-ndx/2;lmfin=X(i)+ndx/2;
            if lmini*lmfin<0,
                aux_=find( Dir >= 360+lmini | Dir < lmfin );
            else
                aux_=find( Dir >= lmini & Dir < lmfin );
            end
        end
        auxx(aux_)=1;
        %ordenade X Y Dir
        auxy=zeros(NY,1);
        aux_=find( Y >= lowupy(j) & Y < lowupy(j+1) );
        if j==ny, aux_=find( Y >= lowupy(j) & Y <= lowupy(j+1) ); end
        auxy(aux_)=1;
        if NX==NY
            duration_aux=auxx.*auxy;
        elseif NX>NY
            duration_aux=auxx(1:NY).*auxy;
        elseif NY>NX
            duration_aux=auxx.*auxy(1:NX);
        end
        TB(i,j)=sum(duration_aux);
    end
end
%percentage or number of event

end