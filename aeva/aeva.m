function aeva(varargin)
%   AEVA Statistical Analysis of Environmental variables
%   AEVA (aeva.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
% Examples 1
%   ameva-->Tools-->Statistics
% Examples 2
%   aeva
% Examples 2
%   aeva esp
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   aeva (main window v1.2.9 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   04-10-2011 - Old distribution version
%   07-05-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v1.2.9 (140820)';
nmfctn='aeva';
Ax1Pos=[0.39 0.2 0.55 0.7];
figPos=[];

idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

% Texto idioma:
texto=amevaclass.amevatextoidioma(idioma);auxTexto='';

namesoftware=[texto('name') ' ',versionumber];

if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana datos
hi=[];%ventana auxiliar

% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.135;
btnHt=0.05;
spacing=0.020;
top=0.95;
xPos=[0.01 0.15 0.28 0.42];

%GLOBAL VARIALBES solo usar las necesarias!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
aevadsf=aeva_ds_functions;%funciones aeva - ds
workPath=[];%Work Path
N=0;%number of selected data
ndx=1;%number of bins for histogram
newX=[];newY=[];newD=[];newTime=[];
Xaux=newX;
Yaux=newY;
Diraux=newD;
Timeaux=newTime;
timedefine=false;
Isdire=true;
statmoments=[];
%name of variables globales
var_lab={'Time:' '* X-Data' 'Y-Data' 'D-Data'};
var_name={'Time' 'Hs' 'Tm' 'Dir'};
var_unit={'' 'm' 's' '{\circ}'};
carpeta=[];
xmin=0;xmax=0;qxmin=0;qxmax=1;%x minimo, maximo, quantil inferior y superior
ymin=0;ymax=0;qymin=0;qymax=1;%y minimo, maximo, quantil inferior y superior
tini=0;tfin=0;%tiempo inicial y final
sini=0;sfin=360;%sector inicial y final
nsec=16;%numero de sectores direccionales
validchildren=[];
svopt=[1 1 0];%save option 1-fig 2-png 3-eps (1/0)
%---LA FIGURA-VENTANA PRINCIPAL-
if isempty(figPos), figPos=awinc.amevaconst('mw'); end
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off','Position',figPos,'Tag',['Tag_' nmfctn],'NextPlot','New');
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');
set(hf,'CurrentAxes',ax1);
axdir=axes('Position',get(ax1,'Position'),'Parent',hf,'YAxisLocation','right','Color','none');
delete(axdir);

if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_1a'), ...
    'BackgroundColor',colorG,'Position',[xPos(2) 0.94 btnWid btnHt*0.66]);

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('On'));

ui_tp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_1'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlot);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_avm=uicontrol('Style','push','Units','normalized','Parent',hf,'Enable','Off','String','Settings', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataO2('on'));

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_2'), ...
    'BackgroundColor',colorG,'Position',[xPos(2) yPos+0.036 btnWid btnHt*0.66]);
ui_pp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_3'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlotToPrint);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_start=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String','Start', ...
    'Interruptible','on','Position',[xPos(1) yPos btnWid btnHt],'Callback',@AevaRun);

btnN=btnN+2;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_dur=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String',texto('persist'), ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback','','Visible','on');

ui_cla=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String',texto('classif'), ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback','','Visible','on');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_tma=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String',texto('nameMaxTemp'), ...
    'Interruptible','on','Position',[xPos(1) yPos btnWid btnHt],'Callback','','Visible','on');

ui_ten=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String',texto('nameOccu'), ...
    'Interruptible','on','Position',[xPos(2) yPos btnWid btnHt],'Callback','','Visible','on');
%OJO OJO actualmente no usamos la salida de esta tabla Visible off. cambiar
ui_ttd=uicontrol('Style','listbox','Units','normalized','Parent',hf,'String','','FontSize',7.5, ...
    'Position',[xPos(1) 0.055 0.312 0.57-btnHt*1.5],'FontName', 'FixedWidth','Visible','off');%Tabla de salida

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',svopt(2),'Callback',@(source,event)EnableButton(ui_data,'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,':' texto('data_2')],'HorizontalAlignment','left');


%% Ameva Data Simple
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('ds');
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(end) btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid(5) btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(ii));
    if ii==4;
        ui_ck=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(1)+btnWid(6) yPos btnWid(4) btnHt],'String','D','Value',1,'Enable','Off');
    end
    ui_vns(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_name(ii),'BackgroundColor','white','Position',[xPos(4) yPos btnWid(3) btnHt]);
    ui_vus(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_unit(ii),'BackgroundColor','white','Position',[xPos(5) yPos btnWid(3) btnHt]);
end; clear ii;

btnN=btnN+2;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Set Data','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@AmevaSetData);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close','Position',[xPos(1) yPos btnWid(1) btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;

%OJO OJO Solo para el ejecutable para roberto. Comentar no es necesario
%EnableButton([ui_dur,ui_cla,ui_tma,ui_ten],'Visible','off');

%% Ameva Settings
% Spacing between the button and the next command's label
btnWid=0.31;
btnHt=0.06;
spacing=0.012;
top=0.98;
xPos=[0.02 0.35 0.67];
hi=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Settings'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataO2('off'),...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('dsa'),'Resize','Off','Visible','Off','NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

uipanel('BorderType','etchedin','Units','normalized','Position',[0.01 0.620  0.98 0.300],'Parent',hi);
uipanel('BorderType','etchedin','Units','normalized','Position',[0.01 0.380  0.98 0.233],'Parent',hi);
uipanel('BorderType','etchedin','Units','normalized','Position',[0.01 0.080  0.98 0.298],'Parent',hi);
btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_nsd=uicontrol('Style','Text','Units','normalized','Parent',hi,'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid*3 btnHt],'String',[texto('dataNumber') ' ',num2str(N)]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.ti=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid*2 btnHt],'String','time options');
uicontrol('Style','text','Units','normalized','Parent',hi,...
    'Position',[xPos(2) yPos-2*spacing btnWid btnHt],'String','inicial date');
uicontrol('Style','text','Units','normalized','Parent',hi, ...
    'Position',[xPos(3) yPos-2*spacing btnWid btnHt],'String','final date');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_imy(1)=uicontrol('Style','radiobutton','Units','normalized','Parent',hi,'String','by interval',...
    'Position',[xPos(1) yPos btnWid btnHt],'Value',1);
ui_tini=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid btnHt],'String','','UserData',[],'Callback',@(source,event)ActTinifin('Tini',source,event));
ui_tfin=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','','UserData',[],'Callback',@(source,event)ActTinifin('Tfin',source,event));

%by interval, by year o by month
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_imy(2)=uicontrol('Style','radiobutton','Units','normalized','Parent',hi,'String','by month',...
    'Position',[xPos(1) yPos btnWid btnHt]);

eval(['auxTexto=' texto('meslong') ';']);
ui_mon=uicontrol('Style','popup','Units','normalized','Parent',hi,'String',auxTexto,...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)ActByMonth('month',source,event));
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_imy(3)=uicontrol('Style','radiobutton','Units','normalized','Parent',hi,'String','by season',...
    'Position',[xPos(1) yPos btnWid btnHt]);

eval(['auxTexto=' texto('interval') ';']);
ui_mons=uicontrol('Style','popup','Units','normalized','Parent',hi,'String',auxTexto,...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)ActByMonth('season',source,event));
set(ui_imy(1),'userdata',[ui_imy(2),ui_imy(3)]);
set(ui_imy(2),'userdata',[ui_imy(1),ui_imy(3)]);
set(ui_imy(3),'userdata',[ui_imy(1),ui_imy(2)]);
call=['me=get(gcf,''CurrentObject'');',...
    'if (get(me,''Value'')==1),',...
    'set(get(me,''userdata''),''Value'',0),',...
    'else,','set(me,''Value'',1),','end;clear me;'];
set(ui_imy, 'callback',call);

btnN=1.035*btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.se=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid*2 btnHt],'String','sector options');
ui_sini=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid btnHt],'String',num2str(sini),'Callback',@ActSe);
uicontrol('Style','text','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','ini direction','HorizontalAlignment','left');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_sfin=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid btnHt],'String',num2str(sfin),'Callback',@ActSe);
uicontrol('Style','text','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','fin direction','HorizontalAlignment','left');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid btnHt],'String','[0:360] sectors','HorizontalAlignment','right');
uicontrol('Style','text','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','sector width','HorizontalAlignment','left');
ui_nsg=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid/2 btnHt],...
    'String',num2str(nsec),'callback',@ActNSect);
ui_nsi=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],...
    'String',num2str(abs(360-0)/nsec),'Enable','off');

Typo='x';
btnN=1.035*btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.x=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid btnHt],'String','Xmin Xmax');
ui_min.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(xmin),'UserData',xmin,'Enable','Off','Callback',@(source,event)ActQXY('XY','Xmin',source,event));
ui_max.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(xmax),'UserData',xmax,'Enable','Off','Callback',@(source,event)ActQXY('XY','Xmax',source,event));
ui_v.(Typo)(1)=uicontrol('Style','radiobutton','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','by value','Value',1,'callback',@(source,event)CallXY(Typo,1));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid btnHt],'String','Xqmin Xqmax','HorizontalAlignment','Right');
ui_qmin.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(qxmin),'UserData',qxmin,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Xmin',source,event));
ui_qmax.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(qxmax),'UserData',qxmax,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Xmax',source,event));
ui_v.(Typo)(2)=uicontrol('Style','radiobutton','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','by quantile','Value',0,'callback',@(source,event)CallXY(Typo,2));

Typo='y';
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ckv.y=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid btnHt],'String','Ymin Ymax');
ui_min.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(ymin),'UserData',ymin,'Enable','Off','Callback',@(source,event)ActQXY('XY','Ymin',source,event));
ui_max.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(ymax),'UserData',ymax,'Enable','Off','Callback',@(source,event)ActQXY('XY','Ymax',source,event));
ui_v.(Typo)(1)=uicontrol('Style','radiobutton','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','by value','Value',1,'callback',@(source,event)CallXY(Typo,1));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid btnHt],'String','Yqmin Yqmax','HorizontalAlignment','Right');
ui_qmin.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid/2 btnHt],'String',num2str(qymin),'UserData',qymin,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Ymin',source,event));
ui_qmax.(Typo)=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt],'String',num2str(qymax),'UserData',qymax,'Enable','Off','Callback',@(source,event)ActQXY('Qxy','Ymax',source,event));
ui_v.(Typo)(2)=uicontrol('Style','radiobutton','Units','normalized','Parent',hi,'Position',[xPos(3) yPos btnWid btnHt],'String','by quantile','Value',0,'callback',@(source,event)CallXY(Typo,2));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hi,'String','Close', ...
    'Position',[xPos(3) yPos btnWid btnHt],'Callback',@(source,event)FigDataO2('off'));
uicontrol('Style','push','Units','normalized','Parent',hi,'String','Apply', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevadsApplyData);
ui_rst=uicontrol('Style','push','Units','normalized','Parent',hi,'String','Reset','Enable','Off', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)AmevadsResetData('on'));


%% C1 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
        if strcmpi(state,'On')
            FigDataO2('Off');
        end
    end
%% C0 Open,Close figure adv
    function FigDataO2(state,source,event)
        set(hi,'Visible',state);
        if strcmpi(state,'On')
            FigDataAvMode('On');
        end
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hi], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data'] [upper(nmfctn(1)),nmfctn(2:end),'Settings']});
    end
%% C0 Methods for "amevaclass"
    function AmevaPlotToPrint(source,event) % List of fig
        awinc.AmevaPlotToPrint(ui_pp,carpeta);
    end
%% actualizar nsectores a grados sectores
    function ActNSect(source,event)
        nsec=str2num(get(ui_nsg,'String'));
        set(ui_nsi,'String',num2str(abs(360-0)/nsec));
    end
%% call X Y
    function CallXY(Typo,val,source,event)
        aevadsf.CallXY(Typo,val,ui_v,ui_min,ui_max,ui_qmin,ui_qmax);
    end
%% acutaliza X Y (MAX MIN) --> Qx Qy introducido manualmente &  acutaliza Qx Qy (MAX MIN) -->X Y introducido manualmente
    function ActQXY(strqxy,strxy,source,event)
        [xmax,xmin,ymax,ymin,qxmax,qxmin,qymax,qymin]=aevadsf.ActXY_Qxy(strqxy,strxy,ui_ckv,Xaux,Yaux,ui_min,ui_max,ui_qmin,ui_qmax);
    end
%% actualizar nsectores a grados sectores
    function ActSe(source,event)
        [sini,sfin]=aevadsf.ActSe(nsec,ui_ckv.se,ui_nsi,ui_sini,ui_sfin);
    end
%% acutaliza tini and tfin
    function ActTinifin(strtif,source,event)
        aevadsf.ActTinifin(strtif,timedefine,ui_ckv.ti,ui_imy,ui_tini,ui_tfin)
    end
%% actualiza by month & season
    function ActByMonth(strtif,source,event)
        set(ui_ckv.ti,'Value',1);
        if strcmp(strtif,'month')
            set(ui_imy(1),'Value',0);
            set(ui_imy(2),'Value',1);
            set(ui_imy(3),'Value',0);
        else
            set(ui_imy(1),'Value',0);
            set(ui_imy(2),'Value',0);
            set(ui_imy(3),'Value',1);
        end
    end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
        if nval==4 && get(ui_all(nval),'Value')>1
            set(ui_ck,'Enable','On');
        else
            set(ui_ck,'Enable','Off');
        end
        ltsplot(nval);% Refresh plot
    end
%% C1 Refresh plot
    function ltsplot(nlv,source,event)
        if get(ui_all(nlv),'Value')>1,
            nlvn=get(ui_td(nlv),'String');
            nlvn=char(nlvn(1));
            try
                awinc.AmevaEvalData(nlv,ui_cm,ui_all);
                X_=awinc.data;
                if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end  
                nmsize=size(X_);
                set(ui_td(nlv),'string',{nlvn,sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
                set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
                if min(nmsize)==1
                    plot(ax1,X_,'k');
                else
                    plot(ax1,X_);
                end
                grid on;
                title(ax1,'Serie','FontWeight','bold');
            catch ME
                disp(texto('variableErase'));
                rethrow(ME);
            end
        end
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% Reset default options
    function AmevadsResetData(strrst,source,event)
        set(hg,'Visible','off');
        set(ui_tbd,'String',texto('resetData'));drawnow ;
        if isempty(newX) && isempty(newY) && isempty(newD),
            set(ui_tbd,'String',[nmfctn,texto('needData')]);
            disp(texto('notData'));set(hg,'Visible','on'); return;
        end;
        
        newX=Xaux;
        newY=Yaux;
        newD=Diraux;
        newTime=Timeaux;
        tini=newTime(1);tfin=newTime(end);
        nsec=16;
        xmin=min(newX);xmax=max(newX);
        ymin=min(newY);ymax=max(newY);
        qxmin=0;qxmax=1;
        qymin=0;qymax=1;
        sini=0;sfin=360;
        N=length(newX);ndx=round(3/2+log(N)/log(2))*2;
        set(ui_nsd,'String',[texto('dataNumber') ' ',num2str(N)]);
        %TIME OPTIONS
        set(ui_ckv.ti,'Value',0);
        if timedefine,
            set(ui_tini,'String',datestr(tini,'dd/mm/yyyy'),'UserData',tini);
            set(ui_tfin,'String',datestr(tfin,'dd/mm/yyyy'),'UserData',tfin);
        else
            set(ui_tini,'String',num2str(tini),'UserData',tini);
            set(ui_tfin,'String',num2str(tfin),'UserData',tfin);
        end
        set(ui_mon,'Value',1);
        %SECTOR OPTION
        set(ui_ckv.se,'Value',0);%if selected sector option
        set(ui_sini,'String',num2str(sini));set(ui_sfin,'String',num2str(sfin));%sector inicial y final
        set(ui_nsg,'String',nsec);%numero de sectores direccionales
        set(ui_nsi,'String',num2str(abs(360-0)/nsec),'UserData',abs(360-0)/nsec);
        %X e Y OPTIONS
        set(ui_ckv.x,'Value',0);%if selected X option
        set(ui_min.x,'String',num2str(xmin),'UserData',xmin);
        set(ui_max.x,'String',num2str(xmax),'UserData',xmax);
        set(ui_qmin.x,'String',num2str(qxmin));
        set(ui_qmax.x,'String',num2str(qxmax));
        set(ui_ckv.y,'Value',0);%if selected Y option
        set(ui_min.y,'String',num2str(ymin),'UserData',ymin);
        set(ui_max.y,'String',num2str(ymax),'UserData',ymax);
        set(ui_qmin.y,'String',num2str(qymin));
        set(ui_qmax.y,'String',num2str(qymax));
        
        %re-inicializo los ejes y variables
        set(ui_tbd,'String',[nmfctn,texto('timeOptions')]);drawnow;
        EnableButton(ui_rst,'Enable','on');
        %set(hf,'Toolbar','figure');
        %
        set(0,'CurrentFigure',hf)
        set(hf,'CurrentAxes',ax1);
        cla(ax1,'reset');axes(ax1);
        XYPlot(newTime,newX,newY,[],var_name,var_unit,ax1);
        set(ui_tb,'String',[nmfctn, texto('ready')]);drawnow ;
        set(hi,'Visible',strrst);
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
        if ~strcmp(get(hi,'NextPlot'),'new'), set(hi,'NextPlot','new'); end;refresh(hi);
        if strcmp(strrst,'off'),
            set(hg,'Visible','on');
        end
    end
%% Set data from setting windows with options
    function AmevadsApplyData(source,event)
        EnableButton(hg,'Visible','off');
        set(ui_tbd,'String',[nmfctn, texto('applyData')]);drawnow ;
        if get(ui_ckv.ti,'Value')+get(ui_ckv.se,'Value')+get(ui_ckv.x,'Value')+get(ui_ckv.y,'Value')<1, return; end
        %TIME OPTIONS
        cktime=get(ui_ckv.ti,'Value');%if selected time option
        tini=get(ui_tini,'UserData');tfin=get(ui_tfin,'UserData');%tiempo inicial y final
        ifcimy(1)=get(ui_imy(1),'Value');%if selected interval
        ifcimy(2)=get(ui_imy(2),'Value');%if selected month
        ifcimy(3)=get(ui_imy(3),'Value');%if selected season
        montsc=get(ui_mon,'Value');%month selected 1-12 -> Jan-Dec
        seassc=get(ui_mons,'Value');%month selected 1-4 -> DJF.....
        %SECTOR OPTION
        cksect=get(ui_ckv.se,'Value');%if selected sector option
        sini=str2num(get(ui_sini,'String'));sfin=str2num(get(ui_sfin,'String'));%sector inicial y final
        nsec=str2num(get(ui_nsg,'String'));%numero de sectores direccionales
        set(ui_nsi,'String',num2str(abs(360-0)/nsec),'UserData',abs(360-0)/nsec);
        %X e Y OPTIONS
        ckxopt=get(ui_ckv.x,'Value');%if selected x option
        ifcxvq(1)=get(ui_v.x(1),'Value');
        ifcxvq(2)=get(ui_v.x(2),'Value');
        xmin=get(ui_min.x,'UserData');xmax=get(ui_max.x,'UserData');%x minimo, maximo
        qxmin=str2num(get(ui_qmin.x,'String'));qxmax=str2num(get(ui_qmax.x,'String'));%#ok<*ST2NM> %x quantil inferior y superior
        ckyopt=get(ui_ckv.y,'Value');%if selected y option
        ifcyvq(1)=get(ui_v.y(1),'Value');
        ifcyvq(2)=get(ui_v.y(2),'Value');
        ymin=get(ui_min.y,'UserData');ymax=get(ui_max.y,'UserData');%y minimo, maximo
        qymin=str2num(get(ui_qmin.y,'String'));qymax=str2num(get(ui_qmax.y,'String'));%y quantil inferior y superior
        
        %APLICO LAS DISTINTAS OPCIONES SELECCIONADAS
        %compruebo las variables al inicio y al final
        %1) Los pongo como vector sino lo estan
        Timeaux=Timeaux(:);
        Xaux=Xaux(:);
        Yaux=Yaux(:);
        Diraux=Diraux(:);
        newTime=Timeaux;
        newX=Xaux;
        newY=Yaux;
        newD=Diraux;
        newTime=newTime(:);
        [mi,ni]=size(newX);
        [mr,nr]=size(newY); if isempty(newY), mr=mi; nr=ni; end
        [mt,nt]=size(newD); if isempty(newD), mt=mi; nt=ni; end
        if(ni~=1 || nr~=1 || nt~=1),
            disp([nmfctn,texto('dataNoMS')]);
            set(ui_tbd,'String',[nmfctn, texto('dataNoMS')]);return;
        end
        if(mi~=mr || mr~=mt),
            disp([nmfctn, texto('dataNoMS')]);
            set(ui_tbd,'String',[nmfctn, texto('dataNoMS')]);return;
        end
        if(mi<1000),
            disp([nmfctn,texto('data_1') ' ',num2str(mi),texto('notEnoughData')]);
            set(ui_tbd,'String',[nmfctn,texto('data_1') ' ',num2str(mi),texto('notEnoughData')]);
        end
        
        %APLICO LAS DISTINTAS OPCIONES SELECCIONADAS
        [newTime,newX,newY,newD]=aevadsf.AmevadsApplyData(ifcimy,ifcyvq,ifcxvq,cktime,ckxopt,ckyopt,cksect,timedefine,tini,tfin,newTime,newX,newY,newD,Isdire,sini,sfin,montsc,seassc,xmin,xmax,qxmin,qxmax,ymin,ymax,qymin,qymax);
        
        N=length(newX);ndx=round(3/2+log(N)/log(2))*2;
        set(ui_nsd,'String',[texto('dataNumber') ' ',num2str(N)]);
        
        %compruebo que las tres variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,newY,newX,'Y-Data','X-Data');          if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newD,newX,'Dir','X-Data');             if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newD,newY,'Dir','Y-Data');             if awinc.state, set(hg,'Visible','on'); return; end,
        %cierro la ventana de datos si todo ha ido bien!
        set(hg,'Visible','off');
        
        %Enable setting reset button
        set(ui_rst,'Enable','on');
        
        %re-inicializo los ejes y variables
        set(ui_tbd,'String',texto('timeOptions'));drawnow;
        set(ui_tp,'String',texto('buttonPlot_1'),'Enable','Off','Value',1);
        set(ui_pp,'String',texto('buttonPlot_3'),'Enable','Off','Value',1);
        EnableButton([ui_start,ui_rst],'Enable','on');
        EnblAllFunction;
        
        set(0,'CurrentFigure',hf)
        set(hf,'CurrentAxes',ax1);cla(ax1,'reset');
        if ishandle(axdir),  delete(axdir); end
        XYPlot(newTime,newX,newY,'none',var_name,var_unit,ax1);
        set(ui_tb,'String',[nmfctn,texto('ready')]);drawnow;
        if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;refresh(hg);
        if ~strcmp(get(hi,'NextPlot'),'new'), set(hi,'NextPlot','new'); end;refresh(hi);
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
    end
%% C1
    function FigDataAvMode(state,source,event)
        %Creo el directorio de trabajo para almacenamiento
        awinc.NewFolderCheck(carpeta,nmfctn,workPath);carpeta=awinc.carpeta;% Methods for "amevaclass"
        FigDataOO('Off');
        N=length(newX);ndx=round(3/2+log(N)/log(2))*2;
        set(ui_nsd,'String',[texto('dataNumber') ' ',num2str(N)]);
        set(hi,'Visible',state);
        
        auxstate='Off';if length(newX)>1, auxstate='On'; end; EnableButton([ui_ckv.ti,ui_ckv.x,ui_min.x,ui_max.x,ui_v.x,ui_qmin.x,ui_qmax.x],'Enable',cmpnblonoff(auxstate,state));
        auxstate='Off';if length(newY)>1, auxstate='On'; end; EnableButton([ui_ckv.y,ui_min.y,ui_max.y,ui_v.y,ui_qmin.y,ui_qmax.y],'Enable',cmpnblonoff(auxstate,state));
        auxstate='Off';if length(newD)>1 && Isdire, auxstate='On'; end; EnableButton([ui_ckv.se,ui_sini,ui_sfin,ui_nsg,ui_nsi],'Enable',cmpnblonoff(auxstate,state));
        auxstate='Off';if timedefine, auxstate='On'; end; EnableButton([ui_imy,ui_mon,ui_mons],'Enable',cmpnblonoff(auxstate,state));
        function stt=cmpnblonoff(auxstate,state)
            if strcmpi(auxstate,state) &&  strcmpi(auxstate,'On')
                stt='On';
            else
                stt='Off';
            end
        end
        if strcmpi(auxstate,'On'), 
            aevadsf.SetQXY_Enable('x',ui_v.x(1),ui_min,ui_max,ui_qmin,ui_qmax);
            aevadsf.SetQXY_Enable('y',ui_v.y(1),ui_min,ui_max,ui_qmin,ui_qmax);
        end
    end
%% Set data from data windows
    function AmevaSetData(source,event)
        %por defecto pongo los tiempo ini y fin 1 y N
        newX=[];newY=[];newD=[];newTime=[];var_name=cell(1,1);var_unit=cell(1,1);
        
        for i=1:4
            var_name(i)=get(ui_vns(i),'String');
            var_unit(i)=get(ui_vus(i),'String');
        end
        ione=get(ui_all(1),'Value');
        itwo=get(ui_all(2),'Value');
        ithr=get(ui_all(3),'Value');
        ifou=get(ui_all(4),'Value');
        if itwo>1,
            awinc.AmevaEvalData(2,ui_cm,ui_all);
            newX=awinc.data;Xaux=newX;N=length(Xaux);
            set(ui_nsd,'String',[texto('dataNumber') ' ',num2str(N)]);
            xmin=min(Xaux);xmax=max(Xaux);
        else
            error(texto('dataFault_1'));
        end
        set(ui_min.x,'String',num2str(xmin),'UserData',xmin);
        set(ui_max.x,'String',num2str(xmax),'UserData',xmax);
        set(ui_tini,'String','1','UserData',1);
        set(ui_tfin,'String',num2str(N),'UserData',N);
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        if ione>1,
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            newTime=awinc.data;Timeaux=newTime;
            set(ui_tini,'String',datestr(Timeaux(1),'dd/mm/yyyy'),'UserData',Timeaux(1));
            set(ui_tfin,'String',datestr(Timeaux(end),'dd/mm/yyyy'),'UserData',Timeaux(end));
            timedefine=true;
            awinc.AmevaTimeCheck(ui_tbd,newTime);
            if awinc.state==true,
                disp(texto('dataFormat_1'));
                set(hg,'Visible','on'); return;
            end
        else
            newTime=(1:length(newX))';Timeaux=newTime;
            set(ui_tini,'String',1,'UserData',Timeaux(1));
            set(ui_tfin,'String',N,'UserData',Timeaux(end));
            timedefine=false;
        end
        if ithr>1,
            awinc.AmevaEvalData(3,ui_cm,ui_all);
            newY=awinc.data; Yaux=newY;
            ymin=min(Yaux);ymax=max(Yaux);
        else
            newY=[]; Yaux=newY;
            ymin=min(Yaux);ymax=max(Yaux);
        end
        set(ui_min.y,'String',num2str(ymin),'UserData',ymin);
        set(ui_max.y,'String',num2str(ymax),'UserData',ymax);
        if ifou>1,
            awinc.AmevaEvalData(4,ui_cm,ui_all);
            newD=awinc.data; Diraux=newD;
            Isdire=get(ui_ck,'Value');
        else
            newD=[]; Diraux=newD;
            set(ui_ck,'Enable','Off')
            Isdire=false;
        end
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        EnableButton([ui_start,ui_avm],'Enable','On');
        %compruebo que las tres variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,newY,newX,'Y-Data','X-Data');        if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newD,newX,'Dir','X-Data');           if awinc.state, set(hg,'Visible','on'); return; end,
        awinc.AmevaVarCheck(ui_tbd,newD,newY,'Dir','Y-Data');           if awinc.state, set(hg,'Visible','on'); return; end,
        %activo los programas que se pueden ejecutar
        if ~strcmp(get(hi,'NextPlot'),'new'), set(hi,'NextPlot','new'); end;refresh(hi);
        %Reinicio las seleccione por defecto
        EnblAllFunction;
        FigDataAvMode('Off');
        %
        set(0,'CurrentFigure',hf)
        set(hf,'CurrentAxes',ax1);
        cla(ax1,'reset');axes(ax1);
        if ishandle(axdir),  delete(axdir); end
        XYPlot(newTime,newX,newY,'-',var_name,var_unit,ax1);
    end
%% Run data
    function AevaRun(source,event)
        %this have a valid children for figure
        if isempty(validchildren)
            validchildren=get(gcf,'children');
        else
            newchildren=get(gcf,'children');%borro los nuevos children de la figura
            if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
                [CI,II]=setdiff(newchildren,validchildren);
                delete(newchildren(II));
            end
        end
        EnableButton([ui_data,ui_start,ui_cla,ui_dur,ui_ten,ui_tma,ui_tp,ui_pp,ui_avm],'Enable','off');
        set(hi,'Visible','off');
        set(ui_tb,'String',[nmfctn, texto('run')]);drawnow ;
        
        %Sting name for the plots
        i=1;
        if ~isempty(newX),  tpstring{1,i}='Serie(X)'; i=i+1; end
        if ~isempty(newY),  tpstring{1,i}='Serie(Y)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newY),  tpstring{1,i}='Serie(X,Y)'; i=i+1; end
        if ~isempty(newD),  tpstring{1,i}='Serie(Dir)'; i=i+1; end
        if timedefine && ~isempty(newX),  tpstring{1,i}='YearSerie(X)'; i=i+1; end
        if timedefine && ~isempty(newY),  tpstring{1,i}='YearSerie(Y)'; i=i+1; end
        if timedefine && ~isempty(newD),  tpstring{1,i}='YearSerie(Dir)'; i=i+1; end
        if ~isempty(newX),  tpstring{1,i}='PDF(X)'; i=i+1; end
        if ~isempty(newY),  tpstring{1,i}='PDF(Y)'; i=i+1; end
        if ~isempty(newD),  tpstring{1,i}='PDF(Dir)'; i=i+1; end
        if ~isempty(newX),  tpstring{1,i}='CDF(X)'; i=i+1; end
        if ~isempty(newY),  tpstring{1,i}='CDF(Y)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newY),  tpstring{1,i}='Scatter(X,Y)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newD),  tpstring{1,i}='Scatter(X,Dir)'; i=i+1; end
        if ~isempty(newY) && ~isempty(newD),  tpstring{1,i}='Scatter(Y,Dir)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newY) && ~isempty(newD),  tpstring{1,i}='Scatter3d(X,Y,Dir)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newY),  tpstring{1,i}='PDF3d(X,Y)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newD),  tpstring{1,i}='PDF3d(X,Dir)'; i=i+1; end
        if ~isempty(newY) && ~isempty(newD),  tpstring{1,i}='PDF3d(Y,Dir)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newY),  tpstring{1,i}='PDF2d(X,Y)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newD),  tpstring{1,i}='PDF2d(X,Dir)'; i=i+1; end
        if ~isempty(newY) && ~isempty(newD),  tpstring{1,i}='PDF2d(Y,Dir)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newD) && Isdire,  tpstring{1,i}='Rose(X,Dir)'; i=i+1; end
        if ~isempty(newY) && ~isempty(newD) && Isdire,  tpstring{1,i}='Rose(Y,Dir)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newD) && Isdire,  tpstring{1,i}='RoseP(X,Dir)'; i=i+1; end
        if ~isempty(newY) && ~isempty(newD) && Isdire,  tpstring{1,i}='RoseP(Y,Dir)'; i=i+1; end
        if ~isempty(newX) && ~isempty(newD) && Isdire,  tpstring{1,i}='RoseQ(X,Dir)'; i=i+1; end
        if ~isempty(newY) && ~isempty(newD) && Isdire,  tpstring{1,i}='RoseQ(Y,Dir)'; i=i+1; end
        if ~isempty(newD) && (~isempty(newX) || ~isempty(newY)) && Isdire,  tpstring{1,i}='BoxPlot(sector)'; i=i+1; end
        if timedefine && (~isempty(newX) || ~isempty(newY)),  tpstring{1,i}='BoxPlot(monthly)'; i=i+1; end
        
        %Run function
        try
            %save options
            svopt(1)=get(ui_ckfig,'Value');%fig
            svopt(2)=get(ui_ckpng,'Value');%png
            svopt(3)=get(ui_ckeps,'Value');%eps
            %Estadisticos de las variables
            statmoments = [mean(newX) mean(newY);
                std(newX) std(newY);
                skewness(newX) skewness(newY);
                kurtosis(newX) kurtosis(newY);
                mean(newX)-mean(newY)/mean(newY) 0;
                std(newX)-std(newY)/std(newY) 0;
                skewness(newX)-skewness(newY)/skewness(newY) 0;
                kurtosis(newX)-kurtosis(newY)/kurtosis(newY) 0];
            %Regimen Medio plots
            set(ui_tp,'Value',1);
            AmevaPlot;
            [Xxx,Xyout,Yxx,Yyout,Dxx,Dyout,XYnn,XYctrs,XDnn,XDctrs,YDnn,YDctrs,Xq,Yq]= ...
                aevaplots(newX,newY,newD,newTime,statmoments,tpstring,timedefine,ndx,nsec,sini,sfin,svopt,carpeta,'eng',var_name,var_unit,Isdire);
            save(fullfile(carpeta,'aeva_statistic_rm.mat'), ...
                'newX','newY','newD','newTime','statmoments','tpstring','ndx','var_name','var_unit',...
                'Xxx','Xyout','Yxx','Yyout','Dxx','Dyout','XYnn','XYctrs','XDnn','XDctrs','YDnn','YDctrs','Xq','Yq');
        catch ME
            EnableButton([ui_data,ui_start,ui_tp,ui_pp,ui_avm],'Enable','on');
            rethrow(ME);
        end
        set(ui_tb,'String',[nmfctn,texto('start')]);drawnow ;
        
        set(ui_tp,'String', tpstring);%Set Sting Plots. set(ui_pp,'String','Plot to print');
        %listar los plot para imprimir
        awinc.ListBoxCallbackP(ui_pp,carpeta);% Methods for "amevaclass"
        EnblAllFunction;
        %     set(ui_ttd,'String',faGAP,'Value',numabc,'Visible','on','Max',numabc,'FontSize',8,'FontWeight', 'light');
        %     refresh(hf);
        clear tpstring;
        EnableButton([ui_data,ui_start,ui_ten,ui_tma,ui_tp,ui_pp,ui_avm],'Enable','on');
    end
%% Enable acces funtion
    function EnblAllFunction
        %set classification callback
        if ~isempty(newX) && ~isempty(newY) && ~isempty(newD),
            ckvd=[]; if Isdire, ckvd=3; end;
            set(ui_cla,'Enable','on','Callback',@(source,event)classification([newX,newY,newD],carpeta,idioma,var_name(2:end),var_unit(2:end),svopt,ckvd));
        else
            set(ui_cla,'Enable','off');
            disp(texto('enableClassification'));
        end
        %set duration callback
        Dir_=[]; if Isdire, Dir_=newD; end;
        if ~isempty(newX),
            set(ui_dur,'Enable','on','Callback',@(source,event)durationtime(newTime,newX,newY,Dir_,timedefine,carpeta,idioma,var_name,var_unit));
        else
            set(ui_dur,'Enable','off');
            disp(texto('enableDuration'));
        end
        %set tabla de encuentro
        if ~isempty(newX) && (~isempty(newY) || ~isempty(newD))
            set(ui_ten,'Enable','on','Callback',@(source,event)occurrencetable(newX,newY,Dir_,newTime,timedefine,nsec*Isdire,0,360*Isdire,carpeta,idioma,var_name(2:end-not(Isdire))));
        else
            set(ui_ten,'Enable','off');
            disp(texto('enableOcurrence'));
        end
        %set temporary maximum
        if ~isempty(newX),
            set(ui_tma,'Enable','on','Callback',@(source,event)maximostemporales(newTime,newX,newY,Dir_,timedefine,idioma,var_name));
        else
            set(ui_tma,'Enable','off');
            disp(texto('enableMaxTemp'));
        end
        clear Dir_;
    end


%% Regimen Medio plot's
    function AmevaPlot(source,event)
        iins=get(ui_tp,'String');
        [ni mi]=size(iins);
        if ni==1, return; end;
        plotname=iins(get(ui_tp,'Value'));%plot name
        
        if ~ishandle(ax1), disp([texto('plot') ' ',nmfctn]); return; end
        if ishandle(axdir),  delete(axdir); end
        
        newchildren=get(gcf,'children');%borro los nuevos children de la figura
        if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
            [CI,II]=setdiff(newchildren,validchildren);
            delete(newchildren(II));
        end
        
        newchildren=get(gcf,'children');%reset all axis
        for i=1:length(newchildren)%reset only axes !!
            if strcmp(get(newchildren(i),'type'),'axes')
                newchildren(i);
                cla(newchildren(i),'reset');
                %axes(newchildren(i));
                axis normal;%selects ax1 as the current axes
                %if ishandle(cbar_ax1), colorbar('delete'); end
                %cbar_ax1 = colorbar;
            end
        end
        
        if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
        try
            aevaplots(newX,newY,newD,newTime,statmoments,plotname,timedefine,ndx,nsec,sini,sfin,svopt,carpeta,idioma,var_name,var_unit,Isdire,ax1);
        catch ME
            rethrow(ME);
        end
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
    end

end
