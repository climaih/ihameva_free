function [Xbysec,Secpos,dirtick]=sector_division(X,Dir,nsec,centrada)
%la direccion debe estar en grado entre 0 y 360 grados
%centrada para centrar o no el sector. Por defecto sino existe es centrada

if ~exist('centrada','var') || isempty(centrada) || ~islogical(centrada), centrada=true; end;
if ~exist('nsec','var'), nsec=16; end;

Xbysec=[];
Secpos=[];
sectores=(0:360/nsec:360);%length: nsec+1 
dirticks={'N','NE','E','SE','S','SW','W','NW','N',''};%axdir tick
dirtickn=[0,45,90,135,180,225,270,315,360];%axdir tick
deltasec=(sectores(2)-sectores(1))/2;
j=1;
for i=1:nsec
    if centrada, % centrar o no el sector
        if i==1, % el primer sector es especial
            aux=[find(0<Dir & Dir<=deltasec);find(360-deltasec<Dir & Dir<=360)];
        else
            aux=find(sectores(i)-deltasec<Dir & Dir<=sectores(i)+deltasec);
        end
    else
        aux=find(sectores(i)<Dir & Dir<=sectores(i+1));
    end
    Xbysec=[Xbysec X(aux)'];
    Secpos=[Secpos i*ones(1,length(aux))];
    dirtick{i}=sectores(i);
%     for j=1:length(dirtickn)-1,
%         if abs(sectores(i)-dirtickn(j))<0.1
%             dirtick{i}=dirticks{j};break;
%         end
%         dirtick{i}=dirticks{end};
%     end
    if mod((i+1),2)==0,
        dirtick{i}=[num2str(sectores(i)) '{\circ}'];
        j=j+1;
    else
        dirtick{i}=[];
    end
end

end
