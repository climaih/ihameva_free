function varargout = occurrencetable(varargin)
%   (occurrencetable) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
% TAG_OCCURRENCETABLE M-file for Tag_occurrencetable.fig
%      TAG_OCCURRENCETABLE, by itself, creates a new TAG_OCCURRENCETABLE or raises the existing
%      singleton*.
%
%      H = TAG_OCCURRENCETABLE returns the handle to a new TAG_OCCURRENCETABLE or the handle to
%      the existing singleton*.
%
%      TAG_OCCURRENCETABLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TAG_OCCURRENCETABLE.M with the given input arguments.
%
%      TAG_OCCURRENCETABLE('Property','Value',...) creates a new TAG_OCCURRENCETABLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before occurrencetable_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to occurrencetable_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% Examples 1
%   Tools-->Statistics-->Occurrence Table
% Examples 2
%   load ameva;
%   occurrencetable(data_gow.hs,data_gow.tm,data_gow.dir,data_gow.time);
% Examples 3
%   load ameva;
%   occurrencetable(data_gow.hs,data_gow.tm);
% Examples 4
%   load ameva;
%   Time = data_gow.time;	%Time
%   Hs= data_gow.hs;        %Variable X
%   Tm= data_gow.tm;        %Variable Y
%   Dir= data_gow.dir;      %Variable Dir
%   timedefine= true;       %Time exist
%   nsec= 16;               %Sector number
%   sini= 0;                %Initial sector
%   sfin= 360;              %Final sector
%   carpeta='sample_dir';	%Directory name-relative to work directory
%   idioma='eng';       %idioma-no implementado
%   var_name={'Hs' 'Tm' 'Dir'};   %Variable name
%   occurrencetable(Hs,Tm,Dir,Time,timedefine,nsec,sini,sfin,carpeta,idioma,var_name);
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   occurrencetable(main program tool v2.0.2 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   12-06-2013 - The first distribution version
%   20-08-2014 - Last distribution version

% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Tag_occurrencetable

% Last Modified by GUIDE v2.5 12-Jun-2013 08:08:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @occurrencetable_OpeningFcn, ...
                   'gui_OutputFcn',  @occurrencetable_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Tag_occurrencetable is made visible.
function occurrencetable_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Tag_occurrencetable (see VARARGIN)

% Choose default command line output for Tag_occurrencetable
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Tag_occurrencetable wait for user response (see UIRESUME)
% uiwait(handles.Tag_occurrencetable);
% Can't proceed unless we have desktop java support

versionumber='v2.0.2 (140820)';
nmfctn='occurrencetable';

carpeta=[];
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
tbl.Time=[];tbl.X=[];tbl.Y=[];tbl.Dir=[];
tbl.timedefine=false;
tbl.nsec=[];tbl.sini=0;tbl.sfin=0;
tbls.Vx=[];tbls.Vy=[];
tbl.idioma='eng'; tbl.usertype=1;
if length(varargin)==11,
    tbl.X=varargin{1};
    tbl.Y=varargin{2};
    tbl.Dir=varargin{3};
    tbl.Time=varargin{4};
    tbl.timedefine=varargin{5};
    tbl.nsec=varargin{6};
    tbl.sini=varargin{7};
    tbl.sfin=varargin{8};
    carpeta=varargin{9};
    tbl.idioma=varargin{10};
    tbl.var_name=varargin{11};
    tbl.texto=amevaclass.amevatextoidioma(tbl.idioma);
elseif length(varargin)==4,
    tbl.X=varargin{1};
    tbl.Y=varargin{2};
    tbl.Dir=varargin{3};
    tbl.Time=varargin{4};
    tbl.timedefine=true;
    tbl.nsec=16;
    tbl.sini=0;
    tbl.sfin=360;
    tbl.texto=amevaclass.amevatextoidioma(tbl.idioma);
    set(handles.ui_disp,'String',tbl.texto('occuData'));
elseif length(varargin)==2,
    tbl.X=varargin{1};
    tbl.Y=varargin{2};
    tbl.texto=amevaclass.amevatextoidioma(tbl.idioma);
    set(handles.ui_disp,'String',tbl.texto('occuData_1'));
elseif length(varargin)==1, %Los datos se seleccionan desde el espacio de trabajo
    tbl.idioma=varargin{1}.idioma;
    tbl.usertype=varargin{1}.usertype;
    tbl.texto=amevaclass.amevatextoidioma(tbl.idioma);
    disp(tbl.texto('dataClasif'));
elseif isempty(varargin), %Los datos se seleccionan desde el espacio de trabajo
    tbl.texto=amevaclass.amevatextoidioma(tbl.idioma);
    disp(tbl.texto('dataClasif'));
else
    tbl.texto=amevaclass.amevatextoidioma(tbl.idioma);
    error(tbl.texto('helpOccu'));
end
namesoftware=[tbl.texto('nameOccu') ' ',versionumber];
%compruebo
if ~isfield(tbl,'idioma') || isempty(tbl.idioma), tbl.idioma='eng'; end
if ~isfield(tbl,'var_name') || isempty(tbl.var_name), tbl.var_name={'Hs','Tm','Dir'}; end %Nombre de 3 variables por defecto sino existen 
if ~isempty(find(isnan(tbl.Time),1)), disp(tbl.texto('nanTime')); end
if ~isempty(find(isnan(tbl.X),1)), disp(tbl.texto('nanX')); end
if ~isempty(find(isnan(tbl.Y),1)), disp(tbl.texto('nanY')); end
if ~isempty(find(isnan(tbl.Dir),1)), disp(tbl.texto('nanDir')); end

tbls.xmin=min(tbl.X);tbls.ymin=min(tbl.Y);tbls.dirmin=min(tbl.Dir);
tbls.xmax=max(tbl.X);tbls.ymax=max(tbl.Y);tbls.dirmax=max(tbl.Dir);
tbls.nsec=tbl.nsec;tbls.sini=tbl.sini;tbls.sfin=tbl.sfin;

set(gcf,'Name',namesoftware);
if not(isdeployed) && tbl.usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
set(handles.ui_nxdiv,'UserData',carpeta);
%Set 
setocurrencetable([handles.Tag_occurrencetable,handles.gui_start,handles.gui_popp_xvar,handles.gui_popp_yvar,handles.ui_xdiv,handles.ui_nxdiv,handles.ui_ydiv,handles.ui_nydiv],tbl,tbls);

% --- Outputs from this function are returned to the command line.
function varargout = occurrencetable_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function ui_xdiv_Callback(hObject, eventdata, handles)
% hObject    handle to ui_xdiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ui_xdiv as text
%        str2double(get(hObject,'String')) returns contents of ui_xdiv as a double
tbl=get(handles.Tag_occurrencetable,'UserData');
tbl.nsec=str2double(get(handles.ui_xdiv,'String'));
set(handles.ui_nxdiv,'String',num2str(abs(tbl.sfin-tbl.sini)/tbl.nsec));
set(handles.Tag_occurrencetable,'UserData',tbl);


% --- Executes during object creation, after setting all properties.
function ui_xdiv_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ui_xdiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ui_nxdiv_Callback(hObject, eventdata, handles)
% hObject    handle to ui_nxdiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ui_nxdiv as text
%        str2double(get(hObject,'String')) returns contents of ui_nxdiv as a double


% --- Executes during object creation, after setting all properties.
function ui_nxdiv_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ui_nxdiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ui_ydiv_Callback(hObject, eventdata, handles)
% hObject    handle to ui_ydiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ui_ydiv as text
%        str2double(get(hObject,'String')) returns contents of ui_ydiv as a double


% --- Executes during object creation, after setting all properties.
function ui_ydiv_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ui_ydiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ui_nydiv_Callback(hObject, eventdata, handles)
% hObject    handle to ui_nydiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ui_nydiv as text
%        str2double(get(hObject,'String')) returns contents of ui_nydiv as a double


% --- Executes during object creation, after setting all properties.
function ui_nydiv_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ui_nydiv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gui_start.
function gui_start_Callback(hObject, eventdata, handles)
% hObject    handle to gui_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
nmfctn='occurrencetable';
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
%Creo el directorio de trabajo para almacenamiento. Solo aqui 
carpeta=get(handles.ui_nxdiv,'UserData');
awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
set(handles.ui_nxdiv,'UserData',carpeta);


tbl=get(handles.Tag_occurrencetable,'UserData');
X_=tbl.X;
Y_=tbl.Y;
Dir=tbl.Dir;
Time=tbl.Time;
timedefine=tbl.timedefine;
idioma=tbl.idioma;
var_name=tbl.var_name;
tbls=get(handles.gui_start,'UserData');
Vx=tbls.Vx;
Vy=tbls.Vy;

set(handles.ui_disp,'String',tbl.texto('busy'));drawnow;
sini=tbl.sini;
sfin=tbl.sfin;
nsec=str2num(get(handles.ui_xdiv,'String'));%El numero es igual al numero de divisiones de x xdiv
nx=str2num(get(handles.ui_xdiv,'String'));
ny=str2num(get(handles.ui_ydiv,'String'));

% Division de las clases -redondeando por defecto-
rndclass=get(handles.ui_chkat,'Value');

%seleccion de las variables ordenadas y absicas
vx=get(handles.gui_popp_xvar,'Value');
sx=get(handles.gui_popp_xvar,'String');
vy=get(handles.gui_popp_yvar,'Value');
sy=get(handles.gui_popp_yvar,'String');

if strcmp(sx(vx),'Sector') && (sfin<360 || sini>0), nx=nx+1; end

tbl_colname{1}=['lower' char(sy(vy))];tbl_colname{2}=['upper' char(sy(vy))];
tbl_rowname{1}=['lower' char(sx(vx))];tbl_rowname{2}=['upper' char(sx(vx))];
for i=1:nx, tbl_colname{i+2}=['class' num2str(i)]; end; tbl_colname{end+1}='total';
for i=1:ny, tbl_rowname{i+2}=['class' char(sy(1)) num2str(i)]; end; tbl_rowname{end+1}='total';
set(handles.gui_table_xy,'Data',[],'ColumnName',tbl_colname,'RowName',tbl_rowname,'ColumnWidth','auto',...
                        'RowStriping','on','BackgroundColor',[1 1 .9; .9 1 1],'Units','Normalized');

[TB,B,NX,NY,ndx,ndy,labx,laby,lowupx,lowupy]=occurrencetablefnt(X_,Y_,nx,ny,sx,sy,vx,vy,sini,sfin,nsec,Time,Dir,rndclass,var_name,Vx,Vy);                    

set(handles.ui_nxdiv,'String',num2str(ndx));
set(handles.ui_nydiv,'String',num2str(ndy));
set(handles.ui_xlabel,'String',labx);
set(handles.ui_ylabel,'String',laby);

strpe=get(get(handles.ui_pnltype,'SelectedObject'),'String');
if strcmp(strpe,'Percent')
    TB=TB/sum(sum(TB))*100;%Talba de encuentro en porcentaje
end
for i=1:nx,
    for j=1:ny,
        B(i+2,j+2)={TB(i,j)};
    end
end
B{nx+3,1}=NX;
B{1,ny+3}=NY;
for i=1:nx, B{i+2,ny+3}=sum(TB(i,:)); end;
for j=1:ny, B{nx+3,j+2}=sum(TB(:,j)); end;
B{nx+3,ny+3}=sum(sum(TB));
%set table
set(handles.gui_table_xy,'Data',B');%s_optimo of each var.
%set color rows
rcolormatrix=get(handles.gui_table_xy,'Units');
set(handles.ui_disp,'String', tbl.texto('occuReady'));

%Save mat, xls csv
myfile=char(['e_table_',labx,'_',laby]);
if (get(handles.gui_chksave,'Value'))
    if exist(carpeta,'file')==7
        save(fullfile(pwd,carpeta,[myfile,'.mat']),'tbl_colname','tbl_rowname','B','lowupx','lowupy','TB');%save to .mat
        if ispc
            xlswrite([myfile,'.xls'],tbl_colname,'Hoja1','B1');
            xlswrite([myfile,'.xls'],tbl_rowname','Hoja1','A2');
            xlswrite([myfile,'.xls'],B','Hoja1','B2');
            
%             xlswrite([myfile,'.xls'],tbl_colname,'Hoja2','B1');
%             xlswrite([myfile,'.xls'],tbl_rowname','Hoja2','A2');
%             xlswrite([myfile,'.xls'],lowupx(1:end-1),'Hoja2','D2');
%             xlswrite([myfile,'.xls'],lowupx(2:end),'Hoja2','D3');
%             xlswrite([myfile,'.xls'],lowupy(1:end-1)','Hoja2','B4');
%             xlswrite([myfile,'.xls'],lowupy(2:end)','Hoja2','C4');
%             xlswrite([myfile,'.xls'],TB','Hoja2','D4');
            
            movefile([myfile,'.xls'],fullfile(pwd,carpeta,[myfile,'.xls']));%save to excel
        elseif isunix || ismac
            B(cellfun(@isempty,B)) = {0};
            csvwrite(fullfile(pwd,carpeta,[myfile,'.csv']),B');%save to csv
        end
    end
end


% --- Executes on selection change in gui_popp_yvar.
function gui_popp_yvar_Callback(hObject, eventdata, handles)
% hObject    handle to gui_popp_yvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns gui_popp_yvar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_popp_yvar
v=get(hObject,'Value');
s=get(hObject,'String');
tbls=get(handles.gui_start,'UserData');
if strcmp(s(v),'X')
    set(handles.ui_ydiv,'Enable','On','String','10'); set(handles.ui_nydiv,'String','0');
    if isempty(tbls.Vx),
        set(handles.ui_ydiv,'String','10','Enable','on');
        set(handles.ui_nydiv,'String',num2str((tbls.xmax-tbls.xmin)/10));
    else
        set(handles.ui_ydiv,'Enable','Off','String',num2str(length(tbls.Vx)-1)); set(handles.ui_nydiv,'String','');
    end
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','off');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','off');
elseif strcmp(s(v),'Y')
    set(handles.ui_ydiv,'Enable','On','String','10'); set(handles.ui_nydiv,'String','0');
    if isempty(tbls.Vy),
        set(handles.ui_ydiv,'String','10','Enable','on');
        set(handles.ui_nydiv,'String',num2str((tbls.ymax-tbls.ymin)/10));
    else
        set(handles.ui_ydiv,'Enable','Off','String',num2str(length(tbls.Vy)-1)); set(handles.ui_nydiv,'String','');
    end
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','off');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','off');
elseif strcmp(s(v),'Dir')
    set(handles.ui_ydiv,'String','10','Enable','on');
    set(handles.ui_nydiv,'String',num2str((tbls.dirmax-tbls.dirmin)/10));
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','off');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','off');
end

% --- Executes during object creation, after setting all properties.
function gui_popp_yvar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_popp_yvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in gui_popp_xvar.
function gui_popp_xvar_Callback(hObject, eventdata, handles)
% hObject    handle to gui_popp_xvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns gui_popp_xvar contents as cell array
%        contents{get(hObject,'Value')} returns selected item from gui_popp_xvar
v=get(hObject,'Value');
s=get(hObject,'String');
tbls=get(handles.gui_start,'UserData');
if strcmp(s(v),'X')
    set(handles.ui_xdiv,'Enable','On','String','10'); set(handles.ui_nxdiv,'String','0');
    if isempty(tbls.Vx),
        set(handles.ui_xdiv,'String','10','Enable','on');
        set(handles.ui_nxdiv,'String',num2str((tbls.xmax-tbls.xmin)/10));
    else
        set(handles.ui_xdiv,'Enable','Off','String',num2str(length(tbls.Vx)-1)); set(handles.ui_nxdiv,'String','');
    end
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','off');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','off');
elseif strcmp(s(v),'Y')
    set(handles.ui_xdiv,'Enable','On','String','10'); set(handles.ui_nxdiv,'String','0');
    if isempty(tbls.Vy),
        set(handles.ui_xdiv,'String','10','Enable','on');
        set(handles.ui_nxdiv,'String',num2str((tbls.ymax-tbls.ymin)/10));
    else
        set(handles.ui_xdiv,'Enable','Off','String',num2str(length(tbls.Vy)-1)); set(handles.ui_nxdiv,'String','');
    end
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','off');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','off');
elseif strcmp(s(v),'Dir')
    set(handles.ui_xdiv,'String','10','Enable','on');
    set(handles.ui_nxdiv,'String',num2str((tbls.dirmax-tbls.dirmin)/10));
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','off');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','off');
elseif strcmp(s(v),'Sector')
    set(handles.ui_xdiv,'String',num2str(tbls.nsec),'Enable','on');
    set(handles.ui_nxdiv,'String',num2str(abs(tbls.sini-tbls.sfin)/tbls.nsec));
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','on');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','on');
elseif strcmp(s(v),'Monthly')
    set(handles.ui_xdiv,'String','12','Enable','off');
    set(handles.ui_nxdiv,'String','1');
    set(handles.editsini,'String',num2str(tbls.sini),'Visible','off');
    set(handles.editsfin,'String',num2str(tbls.sfin),'Visible','off');
end

% --- Executes during object creation, after setting all properties.
function gui_popp_xvar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gui_popp_xvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in ui_pnltype.
function ui_pnltype_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in ui_pnltype 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ui_chkat.
function ui_chkat_Callback(hObject, eventdata, handles)
% hObject    handle to ui_chkat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ui_chkat


% --- Executes on button press in gui_chksave.
function gui_chksave_Callback(hObject, eventdata, handles)
% hObject    handle to gui_chksave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of gui_chksave



function editsini_Callback(hObject, eventdata, handles)
% hObject    handle to editsini (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editsini as text
%        str2double(get(hObject,'String')) returns contents of editsini as a double
tbl=get(handles.Tag_occurrencetable,'UserData');
if str2double(get(handles.editsini,'String'))<0 || str2double(get(handles.editsini,'String'))>=tbl.sfin, 
    disp(tbl.texto('sectorIni')); set(handles.editsini,'String','0'); 
end
tbl.sini=str2double(get(handles.editsini,'String'));
set(handles.ui_nxdiv,'String',num2str(abs(tbl.sfin-tbl.sini)/tbl.nsec));
if tbl.sini==tbl.sfin, displ(tbl.texto('notDataOccu')), end
set(handles.Tag_occurrencetable,'UserData',tbl);


% --- Executes during object creation, after setting all properties.
function editsini_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editsini (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editsfin_Callback(hObject, eventdata, handles)
% hObject    handle to editsfin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editsfin as text
%        str2double(get(hObject,'String')) returns contents of editsfin as a double
tbl=get(handles.Tag_occurrencetable,'UserData');
if str2double(get(handles.editsfin,'String'))>360 || str2double(get(handles.editsfin,'String'))<=tbl.sini, 
    disp(tbl.texto('attentionData')); set(handles.editsfin,'String','360'); 
end
tbl.sfin=str2double(get(handles.editsfin,'String'));
set(handles.ui_nxdiv,'String',num2str(abs(tbl.sfin-tbl.sini)/tbl.nsec));
if tbl.sini==tbl.sfin, displ(tbl.texto('notDataOccu')), end
set(handles.Tag_occurrencetable,'UserData',tbl);

% --- Executes during object creation, after setting all properties.
function editsfin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editsfin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% actualizar nsectores a grados sectores
    %nsec=str2num(get(ui_nsg,'String'));
    %set(ui_nsi,'String',num2str(abs(360-0)/nsec));


% --- Executes on button press in gui_data.
function gui_data_Callback(hObject, eventdata, handles)
% hObject    handle to gui_data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hdls_main_windows(1)=handles.Tag_occurrencetable;
hdls_main_windows(2)=handles.gui_start;
hdls_main_windows(3)=handles.gui_popp_xvar;
hdls_main_windows(4)=handles.gui_popp_yvar;
hdls_main_windows(5)=handles.ui_xdiv;
hdls_main_windows(6)=handles.ui_nxdiv;
hdls_main_windows(7)=handles.ui_ydiv;
hdls_main_windows(8)=handles.ui_nydiv;

OccurrenceTableData(hdls_main_windows);


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6


% --- Executes when user attempts to close Tag_occurrencetable.
function Tag_occurrencetable_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to Tag_occurrencetable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
if ishandle(findobj('Tag','Tag_occtable')),
	close(findobj('Tag','Tag_occtable'));
end
delete(hObject);
