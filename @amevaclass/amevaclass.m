classdef amevaclass < handle
    %   Methods for my ameva windows
    %   $Revision: 1.2.0.1 $  $Date: 07/Dec/2012 14:44:40 $
    
    properties(GetAccess='public', SetAccess='public')
        source='';
        carpeta='';
        workPath='';
        nmfctn='';
        state=false;
        timestate=[];
        var_name='';
        data=[];
    end
    methods
        function hf = amevaclass()%(carpeta)
            % Assign to object properties
            %hf.carpeta = carpeta;
        end % constructor
        %     function delete(obj)
        %         %ishandle(obj.plot),delete(obj.plot);return
        %     end
        %     function obj = set.carpeta(obj,val)
        %         obj.carpeta = val;
        %     end
        function AmevaUpdateName(obj,nval,ui_cm,ui_all,ui_td) %%Update cell,struct and matrix name
            %covgevid para gargar toda la matrix de covariables o solo
            %determinada columnas
            if ~isempty(ui_cm)
                set(ui_cm(nval),'string','(none)','Value',1,'visible','off');
            end
            ionea=get(ui_all(nval),'Value');
            sonea=get(ui_all(nval),'String');
            if ionea>1
                if isstruct(evalin('base',char(sonea(ionea))))
                    fld=fieldnames(evalin('base',char(sonea(ionea))),'-full');
                elseif strcmp(class(evalin('base',char(sonea(ionea)))),'double') && max(size(evalin('base',char(sonea(ionea)))))>1 && min(size(evalin('base',char(sonea(ionea)))))>1
                    fld=num2cell((1:min(size(evalin('base',char(sonea(ionea))))))');
                end
                s=get(ui_td(nval),'string');
                set(ui_td(nval),'string',{char(s(1,:)),['(',sprintf('%dx%d %s',size(evalin('base',char(sonea(ionea)))),class(evalin('base',char(sonea(ionea))))),')']});
            else
                s=get(ui_td(nval),'string');
                set(ui_td(nval),'string',s(1,:));
            end
            if  ~isempty(ui_cm)
                if exist('fld','var') && ~isempty(fld)
                    set(ui_cm(nval),'string',fld,'visible','on');
                    ST = dbstack('-completenames');
                    if  nval==4 && any(cell2mat(strfind({ST.file},'gev'))), 
                        set(ui_cm(nval),'Value',length(fld)); 
                        %disp('Modos ON, GEV Tool'); 
                    end 
                else
                    set(ui_cm(nval),'string','(none)','Value',1,'visible','off');
                end
            end
        end
        function obj=AmevaEvalData(obj,nval,ui_cm,ui_all) %% Eval data, pasarlos a la ventana
            sname=get(ui_all(nval),'String');
            vname=sname{get(ui_all(nval),'Value')};
            tipf=class(evalin('base',vname));
            tamf=size(evalin('base',vname));
            [fmps fmpi]=min(tamf);%find the position of minimum
            if strcmp(tipf,'double') && min(tamf)==1
                tpname='';
                vn=vname;
            elseif strcmp(tipf,'double') && min(tamf)>1
                ipos=get(ui_cm(nval),'Value');
                spos=get(ui_cm(nval),'String');
                if fmpi==1,
                    tpname=['(',num2str(ipos),',:)'];
                else
                    tpname=['(:,',num2str(ipos),')'];
                    ST = dbstack('-completenames');
                    if  nval==4 && any(cell2mat(strfind({ST.file},'gev'))), tpname=['(:,1:',num2str(ipos),')']; disp('Modos ON, GEV Tool'); end 
                end
                vn=vname;
            elseif strcmp(tipf,'struct')
                ipos=get(ui_cm(nval),'Value');
                spos=get(ui_cm(nval),'String');
                tpname=['.' spos{ipos}];
                vn=spos{ipos};
            else
                error('tipo de data de entrada erroneo!');
            end
            aux=strfind(vn,'_');if isempty(aux), aux=2; end
            vn=regexprep(vn,'_','','preservecase');
            obj.var_name=[vn(1) vn(aux:end)];
            data_aux=evalin('base',[vname tpname]);
            if min(size(data_aux))==1, data_aux=data_aux(:); end
            obj.data=data_aux;
        end
        function obj=AmevaPlotToPrint(obj,val1,val2,val3)%% Callback for list of fig to print
            %val2: 'Santander1'
            %val3: 'C:\Users\fernando\Documents\MATLAB\'
            %Ej1. awinc.ListBoxCallbackP(ui_pp,carpeta);
            %Ej2. awinc.ListBoxCallbackP(ui_pp,carpeta,workDir);
            aux=strfind(val1,filesep);%quito el path del val1 si existe
            if ~isempty(aux), val1=val1(aux(end)+1:end); end
            obj.source=val1;
            obj.carpeta=val2;
            if exist('val3','var') && ~isempty(val3), pathd=val3(1:end-1); else pathd=pwd; end
            ival=get(obj.source,'Value');
            iins=get(obj.source,'String');
            [ni mi]=size(iins);
            if ni>0
                figurename=char(iins(ival,1));
                if strcmpi(figurename(end-2:end),'fig')
                    open([obj.carpeta,filesep,figurename]);
                elseif strcmpi(figurename(end-2:end),'png')
                    if ispc,
                        winopen(fullfile(obj.carpeta,figurename));
                    elseif isunix
                        unix(strcat('eog "',fullfile(obj.carpeta,figurename),'" &'));
                    end
                end
            end
            if exist('val3','var') && ~isempty(val3), obj.carpeta=([pathd filesep obj.carpeta]); end
        end
        function obj=ListBoxCallback(obj,val1,objstr)%% Callback for list box
            % Load workspace vars into list box
            vars = evalin('base','who');
            if ~isempty(vars),
                li = length(vars);
                j=1;
                varsI{j}='(none)';
                j=j+1;
                for i=1:li,
                    if exist('objstr','var') && strcmp(objstr,'double')
                        if (strcmp(class(evalin('base',vars{i})),'double') && max(size(evalin('base',vars{i})))==1 && min(size(evalin('base',vars{i})))==1 )
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    elseif exist('objstr','var') && strcmp(objstr,'struct')
                        if strcmp(class(evalin('base',vars{i})),'struct')
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    elseif exist('objstr','var') && objstr,
                        if ( isstruct(evalin('base',vars{i})) || (strcmp(class(evalin('base',vars{i})),'double') && max(size(evalin('base',vars{i})))>1) )
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    else
                        if (strcmp(class(evalin('base',vars{i})),'double') && max(size(evalin('base',vars{i})))>1 )
                            varsI{j}=vars{i};
                            j=j+1;
                        end
                    end
                end
            else
                varsI{1}='(none)';
            end
            %compruebo que no se hayan borrado variables
            for i=1:length(val1)
                if get(val1(i),'value')>length(varsI)
                    set(val1(i),'String',varsI{1},'value',1);
                end
            end
            obj.source=val1;
            set(obj.source,'String',varsI);
        end
        function obj=ListBoxCallbackP(obj,val1,val2,val3,val4,val5)%% Callback for list plot box
            %val2: 'Santander1'
            %val3: 'C:\Users\fernando\Documents\MATLAB\'
            %val4: '*.fig' '*.png', etc etc
            %Ej1. awinc.ListBoxCallbackP(ui_pp,carpeta);
            %Ej2. awinc.ListBoxCallbackP(ui_pp,carpeta,workDir,'*.png');
            aux=strfind(val2,filesep);%quito el path del val1 si existe
            if ~isempty(aux), val2=val2(aux(end)+1:end); end
            obj.source=val1;
            obj.carpeta=val2;
            if exist('val3','var') && ~isempty(val3),
                if strcmp(val3(end),filesep)
                    pathd=val3(1:end-1);
                else
                    pathd=val3;
                end
            else
                pathd=pwd;
            end
            if exist('val4','var') && ~isempty(val4), defft=val4; else defft='*.fig'; end %defaul format .fig
            % Load workspace fig,png, etc into list box
            figlist=dir(fullfile(pathd,[obj.carpeta,filesep,defft]));
            %solo me quedo con los especificado en val5 ej: val5={'SOM';'MDA';'KMA'}
            if exist('val5','var') && ~isempty(val5),
                pos=[];
                for j=1:length(figlist)
                    thisString = figlist(j).name;
                    for k=1:length(val5)
                        if strcmp(thisString(1:length(val5{k})), val5{k}), pos(1+end)=j; break; end
                    end
                end
                figlist=figlist(pos);
            end
            if ~isempty(figlist),
                li = length(figlist);
                varsI{1}='(Plot to print)';
                for i=1:li,
                    varsI{i}=figlist(i,1).name;
                end
            else
                varsI{1}='(Plot to print)';
            end
            if exist('val3','var') && ~isempty(val3), obj.carpeta=([pathd filesep obj.carpeta]); end
            set(obj.source,'String',varsI,'Value',1);
        end
        function obj=AmevaClose(obj,val1,val2)%% Close all Ameva windows
            obj.source=val1;
            obj.nmfctn=val2;
            set(0,'ShowHiddenHandles','on');
            [ni mi]=size(obj.nmfctn);
            for i=1:ni,
                delete(findobj('type','figure','Name',obj.nmfctn{i}));
            end
            li = length(obj.source);
            for i=1:li,
                if ishandle(obj.source(i)), delete(obj.source(i)); end
            end
            if ishandle(findobj('Tag','Tag_amevaworkspace')), delete(findobj('Tag','Tag_amevaworkspace')); end
        end
        function obj=NewFolderCheck(obj,val1,val2,val3)%% Creo el driectorio de salida de resultados y actualizo el nombre del directorio
            aux=strfind(val1,filesep);%quito el path del val1 si existe
            if ~isempty(aux), val1=val1(aux(end)+1:end); end
            obj.carpeta=val1;
            obj.nmfctn=val2;
            if exist('val3','var') && ~isempty(val3),
                if strcmp(val3(end),filesep)
                    pathd=val3(1:end-1);
                else
                    pathd=val3;
                end
            else
                pathd=pwd;
            end
            if isempty(obj.carpeta), obj.carpeta=[obj.nmfctn,'[',datestr(now,'yyyymmddHHMMSS'),']']; end
            if ~exist([pathd filesep obj.carpeta],'dir'), mkdir([pathd filesep obj.carpeta]); end
            if exist([pathd filesep obj.carpeta],'dir') && ( ~isempty(dir(fullfile(pathd,[obj.carpeta,filesep,'*.mat']))) || ~isempty(dir(fullfile(pathd,[obj.carpeta,filesep,'*.fig']))) || ~isempty(dir(fullfile(pathd,[obj.carpeta,filesep,'*.txt']))) )
                NewDir=questdlg('Do you want to create a new folder?','Create folder Question','Yes', 'No','Yes');
                if strcmp(NewDir,'Yes'),
                    obj.carpeta=[obj.nmfctn,'[',datestr(now,'yyyymmddHHMMSS'),']'];
                    mkdir([pathd filesep obj.carpeta]);
                end
            end
            if exist('val3','var') && ~isempty(val3), obj.carpeta=([pathd filesep obj.carpeta]); end
        end
        function obj=LoadPath(obj)%% Path work directorio
            obj.workPath = uigetdir;
            if ~isempty(obj.workPath) && ~strcmp(obj.workPath(end),filesep)
                obj.workPath=[obj.workPath filesep];
            end
        end
        function obj=AmevaVarCheck(obj,val,var1,var2,str1,str2)%% Comprobar Variables
            %compruebo que dos variables son diferentes y no estan vacias
            obj.source=val;
            obj.state=false;
            uistate=false;
            if ~isempty(val) && ishandle(val), uistate=true; end
            if ~isempty(var1)
                if ~isempty(var2)
                    if isequal(var1,var2),
                        if uistate, set(obj.source,'String',['[Data]: >> ATENTION! they need different ',str1,' and ',str2,' vectors to continue!']); end
                        disp(['ATENTION! the vectors ',str1,' and ',str2,' must be different to continue!']);
                        obj.state=true; return;
                    else
                        [mi,ni]=size(var1);
                        [mr,nr]=size(var2);
                        if ni~=1 || nr~=1 || mi~=mr,
                            if uistate, set(obj.source,'String',['[Data]: >> ATENTION! not macht size array ' str1 '['  num2str(mi) ']; '  str2 '['  num2str(mr) ']']); end
                            disp(['ATENTION! not macht size array ' str1 '['  num2str(mi) ']; '  str2 '['  num2str(mr) ']']);
                            obj.state=true; return;
                        end
                        if strcmpi(str1,'Dir'), %Compruebo que es una direccion [0 180]
                            if  min(var1)<0 || max(var1)>360,
                                disp(['The Dir vector can only contain values between 0',char(176),' and 360',char(176),'. min(' str1 ')='  num2str(min(var1)) ' y max(' str1 ')='  num2str(max(var1))]);
                                obj.state=true; return;
                            end
                            if max(var1)<1, %Compruebo que es una direccion y que tiene valores mayores que 1 grado
                                disp(['The maximum value of the Dir vector must be greater than 1',char(176),'. max(' str1 ')='  num2str(max(var1))]);
                                obj.state=true; return;
                            end
                        end
                        if strcmpi(str2,'Dir'), %Compruebo que es una direccion [0 180]
                            if  min(var2)<0 || max(var2)>360,
                                disp(['The Dir vector can only contain values?? between 0',char(176),' and 360',char(176),'. min(' str2 ')='  num2str(min(var2)) ' y max(' str2 ')='  num2str(max(var2))]);
                                obj.state=true; return;
                            end
                            if max(var2)<1, %Compruebo que es una direccion y que tiene valores mayores que 1 grado
                                disp(['The maximum value of the Dir vector must be greater than 1',char(176),'. max(' str2 ')='  num2str(max(var2))]);
                                obj.state=true; return;
                            end
                        end
                        if ~isempty(find(isnan(var1),1)), disp(['nan exist in your ',str1]); end
                        if ~isempty(find(isnan(var2),1)), disp(['nan exist in your ',str2]); end
                        if ~isempty(find(isinf(var1),1)), disp(['inf exist in your ',str1]); end
                        if ~isempty(find(isinf(var2),1)), disp(['inf exist in your ',str2]); end
                        if(mi<50), disp('[Data]: >> WARNING! not enought data'); end
                    end
                end
            end
        end
        function obj=AmevaTimeCheck(obj,val,var1)%% Comprobar Time
            obj.source=val;
            obj.state=false;
            uistate=false;
            if ~isempty(val) && ishandle(val), uistate=true; end
            if ~isempty(var1)
                if ~isempty(find(isnan(var1),1)), disp(['nan exist in your ',str1]); end
                if ~isempty(find(isinf(var1),1)), disp(['inf exist in your ',str1]); end
                if length(var1)<50, disp(['[Data]: >> WARNING! not enought data: ' num2str(length(var1))]); end
                if not(isequal(var1,sort(var1))),
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector data must be sorted in ascending!'); end
                    disp('ATENTION! The temporary vector data must be sorted in ascending!');
                    obj.state=true; return;
                end
                if not(isfloat(var1)),
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector data must be a float vector!'); end
                    disp('ATENTION! The temporary vector data must be sorted in ascending!');
                    obj.state=true; return;
                end
                if not(isequal(var1,unique(var1))),
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector data must be diferents elements!'); end
                    disp('ATENTION! The temporary vector data must be diferents elements!');
                    obj.state=true; return;
                end
                if min(var1)>=365244 && max(var1)<=1096275,
                    if uistate, set(obj.source,'String','[Data]: >> Your temporary vector are in matlab time format!'); end
                    disp('Your temporary vector are in matlab time format');
                    obj.timestate=true;%true is a matlab datetime
                elseif min(var1)>=0 && max(var1)<=2000,
                    if uistate, set(obj.source,'String','[Data]: >> Your temporary vector are in yearly scale!'); end
                    disp('Your temporary vector are in yearly scale!');
                    obj.timestate=false;%false-is  a rescale yearly time scale
                else
                    if uistate, set(obj.source,'String','[Data]: >> ATENTION! The temporary vector is not in the proper format!'); end
                    disp('ATENTION! The temporary vector is not in the proper format!');
                    obj.state=true; return;
                end
            end
        end
    end
    methods (Static=true)
        %figure position of diferent windows for ameva
        function [figPos,xPos,btnWid,btnHt,top,spacing]=amevaconst(tipo)
            figPos=[];btnWid=[];btnHt=[];spacing=[];top=[];xPos=[];
            screenSize=get(0,'ScreenSize');
            switch tipo
                case 'mw', %main window
                    figPos=[9 screenSize(4)-590 800 500];
                case 'mlp', %main location point meshselection
                    figPos=[9 screenSize(4)-750 550 550];
                case 'setting', %setting
                    figPos=[570 screenSize(4)-610 250 350];
                    xPos=[0.02 0.45 0.82];
                    btnWid=[0.35 0.15 0.10 0.09 0.16 0.35 0.40];
                    btnHt=0.075;
                    top=0.98;
                    spacing=0.02;
                case 'ds', %data
                    figPos=[570 screenSize(4)-610 300 350];
                    xPos=[0.01 0.37 0.67 0.82 0.92];
                    btnWid=[0.30 0.15 0.10 0.09 0.14 0.27 0.35];
                    btnHt=0.075;
                    top=0.98;
                    spacing=0.02;
                case 'dsc', %data characterized data
                    figPos=[570 screenSize(4)-610 300 450];
                    xPos=[0.01 0.37 0.67 0.82 0.92];
                    btnWid=[0.30 0.15 0.10 0.09 0.14 0.27 0.35];
                    btnHt=0.075;
                    top=0.98;
                    spacing=0.02;
                case 'dda', %ameva-ihdata
                    figPos=[150 screenSize(4)-610 750 450];
                case 'dsa', %setting aeva
                    figPos=[500 screenSize(4)-600 300 360];
                case 'wf', %worldfilter [50 100 1250 800]
                    figPos=[50 100 1250 800];
                otherwise
                    figPos=[9 screenSize(4)-590 800 500];
                    btnWid=0.35;
                    btnHt=0.075;
                    spacing=0.02;
                    top=0.98;
                    xPos=[0.02 0.47 0.84];
            end
        end
        %%Load or not load ameva.gif ico
        function loadamevaico(hf,tipo)
            if not(exist('tipo','var')) || isempty(tipo), tipo=false; end
            if tipo && not(isempty(which('ameva.gif'))),
                % Can't proceed unless we have desktop java support
                if ~usejava('swing')
                    error('stats:dfittool:JavaSwingRequired','requires Java Swing to run.');
                end
                warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
                hfframe=get(hf,'javaframe');
                jIcon=javax.swing.ImageIcon(which('ameva.gif'));
                hfframe.setFigureIcon(jIcon);
            end
        end
        %%Carga linea de costa media y alta def.
        function obj=UnZipCoastLine_H_I(obj)
            if exist(which('fichaclass'),'file')~=2
                disp('Add ihameva_clima function folder to use it!');
                return;
            end
            if exist([tempdir,'gshhs_shape_ameva'],'dir')~=7
                [bathystatus,bathypath,bathyname]=amevaloadunidad;
                unzip([bathypath,'gshhs_shape_ameva.zip'], tempdir);
            end
        end
        %%Load text idioma
        function txtDict = amevatextoidioma(varargin)
            %Fuciones para regresar solo texto especifico en el idioma que sequiera
            error(nargchk(0,2,nargin));
            if nargin==0,
                idioma='eng';
            elseif nargin==1,
                idioma=varargin{1};
            elseif nargin==2,
                idioma=varargin{1};
            end
            S=which('amevatest');
            Sp=strfind(S,filesep);
            S=S(1:Sp(end));
            file=fullfile(S,'texts',idioma,'main.txt');
            fid=fopen(file,'r','n','UTF-8');
            if fid==-1, error('FOPEN cannot open the file'); end
            txt=textscan(fid,'%s %s','Delimiter','=');
            fclose(fid);
            txtDict=containers.Map(txt{1},txt{2});
            
            if nargin==2% Solo en el caso textType='fecha'
                if strcmp(varargin{2},'fecha')
                    texto=[datestr(date,'mmmm') ' ' num2str(str2num(datestr(date,'dd'))) ', ' datestr(date,'yyyy')];%eng por defecto
                    if strcmp(idioma,'esp')
                        aux=[]; eval(['aux=' txtDict('meslong') ';']);
                        texto=[num2str(str2num(datestr(date,'dd'))) ' de '  aux{str2double( datestr(date,'mm') )} ' de ' datestr(date,'yyyy')];
                    end
                    clear txtDict;
                    txtDict=texto;
                end
            end
            
        end
        function AMEVAscriptversions=ihameva_checkUpdates
            try
                %% Funciona para las tools de ameva_free
                fid_myversions=fopen(fullfile(regexprep(which('ameva.m'),'ameva.m',''),'README.txt'));
                myversionsinfo = textscan(fid_myversions,'%s');
                fclose(fid_myversions);
                
                %ameva,aeva,disfitanalysis,gev,pot,calibration,heteroscedasticmodel,amevadataselection
                scriptnames={myversionsinfo{1}{1} myversionsinfo{1}{143:2:155}};
                scriptversionsO={myversionsinfo{1}{2} myversionsinfo{1}{144:2:156}}; scriptversionsO=char(scriptversionsO);
                slvO=uint8([str2num(scriptversionsO(:,2)) str2num(scriptversionsO(:,4)) str2num(scriptversionsO(:,6))]);
                
                [READMEtxt,status]=urlread('https://bitbucket.org/ouc/ihameva_free/raw/master/README.txt');clear aux;
                
                if status
                    lastversionsinfo = textscan(READMEtxt,'%s');
                    scriptversionsN = char({lastversionsinfo{1}{2} lastversionsinfo{1}{144:2:156}});
                    slvN = uint8([str2num(scriptversionsN(:,2)) str2num(scriptversionsN(:,4)) str2num(scriptversionsN(:,6))]);
                    CheckTF = CheckFunctionVersion(scriptnames,scriptversionsN,scriptversionsO,slvN,slvO);
                    AMEVAscriptversions = scriptversionsO(1,:);%Devuelve la version solo de AMEVA
                    if CheckTF == 0,
                        disp([scriptnames{1},' ' AMEVAscriptversions ' is updated']);
                    end
                    return;
                end
                AMEVAscriptversions='unknown version';
                disp('Warning: ihameva_checkUpdates unable to successfully check for updates AMEVA functions');
                %% OJO OJO hacer para ameva_clima  falta introducir passw y añadir versiones en leeme.txt
                %[READMEtxt,status]=urlread('https://bitbucket.org/ouc/ihameva_clima/raw/master/meshselection/leeme.txt','Username','climaih','Password','climaclima')
                %exist(which('fichaclass'),'file')==2
                %fid_myversions=fopen(fullfile(regexprep(which('amevainfo.m'),'amevainfo.m',''),'leeme.txt'))
                
            catch E1
                AMEVAscriptversions='unknown version';
                disp('Warning: ihameva_checkUpdates unable to successfully check for updates AMEVA functions');
            end
            function CheckTF = CheckFunctionVersion(scriptnames,scriptversionsN,scriptversionsO,slvN,slvO)
                n=length(scriptnames);
                CheckTF = 0;
                for i = 1:n,
                    if not(isequal(slvN(i,:),slvO(i,:))),
                        CheckTF = CheckTF+1;
                        disp(['Warning:[Update AMEVA] You are using an outdated version of ',scriptnames{i},' ' scriptversionsO(i,:)]);
                        if isequal(slvN(i,1:2),slvO(i,1:2)) && slvN(i,3)>slvO(i,3),
                            disp([' It is ADVISABLE to download the latest version: ',scriptnames{i},' ' scriptversionsN(i,:)]);
                        elseif isequal(slvN(i,1),slvO(i,1)) && (slvN(i,2)>slvO(i,2) || slvN(i,1)>slvO(i,1)),
                            disp([' It is NECESSARY to download the latest version: ',scriptnames{i},' ' scriptversionsN(i,:)]);
                        end
                    end
                end
            end
            % -------------------------------------------------------------------------
%             persistent Timeout_error
%             Timeout=2;%seconds
%             t0_urlread=now;
%             if isempty(Timeout_error)
%                 try
%                     url=sprintf('https://bitbucket.org/ouc/ihameva_free/raw/master/README.txt');
%                     urlread(url);
%                 catch E2
%                     disp('Warning: urlread timeout error')
%                 end
%             end
%             if 24*3600*(now-t0_urlread)>Timeout
%                 Timeout_error=true;
%             end
        end
    end
end
