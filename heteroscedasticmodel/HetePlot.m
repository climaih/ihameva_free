function HetePlot(Hsmax,Time,idx,fmunew,uppermean,lowermean,upperpredict,lowerpredict,modelstr,var_name,var_unit,gcax)

if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! XYPlot with axes'); end
set(gcf,'CurrentAxes',gcax)
if ~exist('var_name','var'), var_name={'X','Y'}; end
if ~exist('var_unit','var'), var_unit={'m','s'}; end

Lab=labelsameva(var_name,var_unit);%Labels

axis(gcax,'xy');
mksz=1.5;
if length(Hsmax)<10000, mksz=3; end

    plot(Time,Hsmax,'ob','Color','b','Markersize',mksz,'Markerfacecolor','b')
    hold on; grid on;
    aux=sortrows([Time,fmunew],1);
    plot(aux(:,1),aux(:,2),'-k','Linewidth',1.4);
    aux=sortrows([Time,uppermean],1);
    plot(aux(:,1),aux(:,2),'k--','Linewidth',1.2);
    aux=sortrows([Time,upperpredict],1);
    plot(aux(:,1),aux(:,2),'--','Color',[30 144 255]/255,'Linewidth',1.2);
    if ~isempty(idx) && ~isempty(idx{1})
        plot(Time(idx{1}),Hsmax(idx{1}),'or','MarkerSize',5);
    end
    if ~isempty(idx),
        if ~isempty(idx{1})
            h2=legend('Data','Mean','C.I. mean','C.I. model','Outliers','Location','NorthEast');
        end
    else
        h2=legend('Datos','Media','C.I. mean','C.I. model','Location','SouthEast');
    end
    set(h2,'fontsize',9,'fontweight','b');
    aux=sortrows([Time,lowermean],1);
    plot(aux(:,1),aux(:,2),'k--','Linewidth',1.2);
    aux=sortrows([Time,lowerpredict],1);
    plot(aux(:,1),aux(:,2),'b--','Color',[30 144 255]/255,'Linewidth',1.2);
    %label and title
    if iscell(Lab) && length(Lab)==2
        xlabel(Lab{1}); ylabel(Lab{2});
    elseif ischar(Lab)
        xlabel(Lab);
    end
    
    htl=title('');
    aux=get(htl,'Position');
    text(aux(1),aux(2)*1.035,modelstr,'Interprete','Latex','HorizontalAlignment','center');
end
