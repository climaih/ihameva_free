function heteroscedasticmodel(varargin)
%   heteroscedasticmodel model.
%   (heteroscedasticmodel.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%
% Examples 1
%   load ameva;
%   Tools-->Extreme statistic-->heteroscedasticmodel
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   heteroscedasticmodel(main program tool v0.5.5 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   23-04-2013 - Old distribution version
%   07-05-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v0.5.5 (140820)';
nmfctn='heteroscedasticmodel';
Ax1Pos=[0.39 0.15 0.55 0.7];% plot position
Ax2Pos=[0.01 0.50 0.30 0.28];% table position
Ax3Pos=[0.01 0.10 0.25 0.26];% table position
figPos=[];

idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

% Texto idioma:
texto=amevaclass.amevatextoidioma(idioma);

namesoftware=[texto('nameHeteroce') ' ',versionumber];


if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar

% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.135;
btnHt=0.05;
spacing=0.020;
top=0.95;
xPos=[0.01 0.15 0.28 0.42];

% compruebo que existe la tool
ecoinstall=amevaTheseToolboxesInstalled('Econometrics Toolbox');

%GLOBAL VARIALBES solo usar las necesarias OJO!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
newY=[];
Yaux=newY;
TimeX=[];
TimeXaux=[];
timedefine=false;
idx=[];model=[];fmunew=[];uppermean=[];lowermean=[];upperpredict=[];lowerpredict=[];residuo=[];
Hr=[]; pValuer=[]; Qstatr=[];
ksteststr='';
km=15;%tipo de modelo (1-6)
model=[1;1;1;1];
modelstr=['$$\renewcommand{\arraystretch}{1.4}\begin{array}{rcccccl}\mu(t) &=& p_1 &+& p_2t &+& p_3t^2 \\ ' ...
    '\sigma(t) &=& p_4 &+& p_5t &+& p_6t^2  \end{array} $$'];
modelstr1=['$$\begin{array}{rcl}\mu(t) &=& p_1 + p_2t + p_3t^2 \\ ' ...
    '\sigma(t) &=& p_4 + p_5t + p_6t^2  \end{array} $$'];
conf=0.90;
outlierconf=0.95;%conf outlier (0.99 0.95)
var_lab={'Time/X-Data:' '* Y-Data'};
var_name={'Time' 'Hs'};
var_unit={'' 'm'};
carpeta=[];
cityname = '';
frec=[];
svopt=[1 1 0];%save option 1-fig 2-png 3-eps (1/0)

%---LA FIGURA-VENTANA PRINCIPAL-
if isempty(figPos), figPos=awinc.amevaconst('mw'); end
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off','Position',figPos,'Tag',['Tag_' nmfctn],'NextPlot','New');
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');
set(hf,'CurrentAxes',ax1);
ax2=axes('Units','normalized','Parent',hf,'Position',Ax2Pos,'Visible','Off');
ax3=axes('Units','normalized','Parent',hf,'Position',Ax3Pos,'Visible','Off');

if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_1a'), ...
    'BackgroundColor',colorG,'Position',[xPos(2) 0.935 btnWid btnHt],'FontWeight','Bold');

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data & settings', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('On'));

ui_tp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_1'), ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaPlot);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_run=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String','Start', ...
    'Interruptible','on','Position',[xPos(1) yPos btnWid btnHt],'Callback',@AmevaRun);
uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_2'), ...
    'BackgroundColor',colorG,'Position',[xPos(2) yPos+0.036 btnWid btnHt*0.66]);
ui_pp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_3'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlotToPrint);

ui_kstest=uicontrol('Style','text','Units','normalized','Parent',hf,'String',ksteststr, ...
    'BackgroundColor',colorG,'Position',[xPos(1) 0.37 btnWid*2.28 btnHt*2],'HorizontalAlignment','left');

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',svopt(2),'Callback',@(source,event)EnableButton(ui_data,'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('dataButton')],'HorizontalAlignment','left');

%% Data
[figPos,xPos,btnWid,btnHt,top,spacing]=awinc.amevaconst('ds');
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',figPos,'Resize','Off','Visible','Off','NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(end) btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid(6) btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid(5) btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(ii));
    ui_vns(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_name(ii),'BackgroundColor','white','Position',[xPos(4) yPos btnWid(3) btnHt]);
    ui_vus(ii)=uicontrol('Style','edit','Units','normalized','Parent',hg,'String',var_unit(ii),'BackgroundColor','white','Position',[xPos(5) yPos btnWid(3) btnHt]);
end; clear ii;
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String',texto('interConfi'), ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid(6) btnHt],'HorizontalAlignment','right');
ui_c=uicontrol('Style','edit','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],...
    'String',conf,'Callback',@(source,event)ActConfO(source,event));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String',texto('filterOutli'), ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid(6) btnHt],'HorizontalAlignment','right');
ui_a=uicontrol('Style','edit','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid(1) btnHt],...
    'String',outlierconf,'Callback',@(source,event)ActConfO(source,event));
ui_rmo=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(2)+0.32 yPos btnWid(1) btnHt],'String','rm outlier','Value',0);
% xPos=[0.03 0.45 0.82];
% xPos=[0.01 0.37 0.67 0.82 0.92];
% btnWid=[0.30 0.15 0.10 0.09 0.14 0.27 0.35];

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String',texto('typeModel_1'), ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid(6) btnHt],'HorizontalAlignment','right');
% ui_k=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt], ...
%     'String',{'1' '2' '3' '4' '5' '6'},'callback',@musigmaparam,'HorizontalAlignment','center');
ui_p(1)=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(2)+0.00 yPos btnWid(5) btnHt],'String','p1','Value',1,'callback',@musigmaparam,'Enable','off');
ui_p(2)=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(2)+0.16 yPos btnWid(5) btnHt],'String','p2','Value',1,'callback',@musigmaparam);
ui_p(3)=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(2)+0.32 yPos btnWid(5) btnHt],'String','p3','Value',1,'callback',@musigmaparam);
ui_p(4)=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(2)+0.00 yPos-0.06 btnWid(5) btnHt],'String','p4','Value',1,'callback',@musigmaparam,'Enable','off');
ui_p(5)=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(2)+0.16 yPos-0.06 btnWid(5) btnHt],'String','p5','Value',1,'callback',@musigmaparam);
ui_p(6)=uicontrol('Style','check','Units','normalized','Parent',hg,'Position',[xPos(2)+0.32 yPos-0.06 btnWid(5) btnHt],'String','p6','Value',1,'callback',@musigmaparam);
%align([ui_p1 ui_p2 ui_p3],'distribute','bottom');%,'HorizontalAlignment','VerticalAlignment')
btnN=btnN+1.25;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ms=text('string',modelstr,'Position',[xPos(1) yPos-0.07], 'interpreter','latex');axis off;

btnN=btnN+1.75;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Set Data','Position',[xPos(2) yPos btnWid(1) btnHt],'Callback',@AmevaSetData);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close','Position',[xPos(1) yPos btnWid(1) btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'),'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');

if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;
musigmaparam;
%% C1 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);%Lista tambien las structuras
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data'] [upper(nmfctn(1)),nmfctn(2:end),'Settings']});
    end
%% C0
    function AmevaPlotToPrint(source,event) % List of fig
        awinc.AmevaPlotToPrint(ui_pp,carpeta);
    end
%% Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% acutaliza QUmbral cuando cambia el umbral y el quantil
    function ActConfO(source,event)
        conf=str2double(get(ui_c,'String'));
        if conf<0 || conf>1 || isnan(conf),
            conf=0.9;        set(ui_c,'String',num2str(conf));
            disp(texto('confValue'));
        end
        outlierconf=str2double(get(ui_a,'String'));
        if outlierconf<0 || outlierconf>1 || isnan(outlierconf),
            outlierconf=0.95;        set(ui_a,'String',num2str(outlierconf));
            disp(texto('confOutliValue'));
        end
        if outlierconf==1,
            set(ui_rmo,'Enable','Off','Value',0);
        else
            set(ui_rmo,'Enable','On');
        end
    end
%% Modelo
    function musigmaparam(source,event)
        aux=get(ui_p,'Value');
        km=bin2dec([num2str(aux{2}) num2str(aux{3}) num2str(aux{5}) num2str(aux{6})]);
        %   Defino el modelo binario
        mo = dec2bin(km,4);
        model = str2num(mo(1:4)');
        %escribo el modelo
        modmu = ''; frec=[]; frec{1}=''; frec{2}='P_1';
        if model(1),
            modmu = [modmu ' &+& p_2 t' ];frec{end+1}='P_2';
        else
            modmu = [modmu ' &&' ];
        end
        if model(2),
            modmu = [modmu ' &+& p_3 t^2' ];frec{end+1}='P_3';
        else
            modmu = [modmu ' &&' ];
        end
        modsigma = '';frec{end+1}='P_4';
        if model(3),
            modsigma = [modsigma ' &+& p_5 t' ];frec{end+1}='P_5';
        else
            modsigma = [modsigma ' &&' ];
        end
        if model(4),
            modsigma = [modsigma ' &+& p_6 t^2' ];frec{end+1}='P_6';
        else
            modsigma = [modsigma ' &&' ];
        end
        modelstr=['$$\renewcommand{\arraystretch}{1.4}\begin{array}{rcccccl}\mu(t) &=&p_1 ' modmu ...
            ' \\ \sigma(t) &=& p_4 ' modsigma ' \end{array} $$'];
        modelstr1=['$$\begin{array}{rcl}\mu(t) &=&p_1 ' regexprep(modmu, '&', '') ...
            ' \\ \sigma(t) &=& p_4 ' regexprep(modsigma, '&', '') ' \end{array} $$'];
        set(ui_ms,'String',modelstr);
    end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
        ltsplot(nval);% Refresh plot 
    end
%% C1 Refresh plot
    function ltsplot(nlv,source,event)
        try
            if get(ui_all(nlv),'Value')>1,
                nlvn=get(ui_td(nlv),'String');
                nlvn=char(nlvn(1));
                
                awinc.AmevaEvalData(nlv,ui_cm,ui_all);
                X_=awinc.data;
                if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end
                nmsize=size(X_);
                set(ui_td(nlv),'string',{nlvn,sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
                set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
                if min(nmsize)==1
                    plot(ax1,X_,'^k','MarkerSize',4);
                else
                    plot(ax1,X_);
                end
                grid on;
                title(ax1,texto('series_1'),'FontWeight','bold');
            end
        catch ME
            disp(texto('variableErase'));
            rethrow(ME);
        end
    end
%% Set data comprobamos que los Data para calcular la calibaracion son
    function AmevaSetData(source,event)
        set(hg,'Visible','off');
        set(ui_tbd,'String',texto('applyData'));drawnow;
        ActConfO;
        ione=get(ui_all(1),'Value');
        itwo=get(ui_all(2),'Value');
        for i=1:2
            var_name(i)=get(ui_vns(i),'String');
            var_unit(i)=get(ui_vus(i),'String');
        end
        if itwo>1,
            awinc.AmevaEvalData(2,ui_cm,ui_all);
            newY=awinc.data;Yaux=newY;
        else
            error(texto('needData'));
        end
        timedefine=false;
        if ione>1,  
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            TimeX=awinc.data;TimeXaux=TimeX; %Time/X-Data
            %compruebo que es un serie temporal solo para eje
            awinc.AmevaTimeCheck(ui_tbd,TimeX);
            if strcmp(get(ui_tbd,'String'),texto('temporaryVector'))
            	timedefine=true;
            end
        else
            TimeX=(1:length(newY))';TimeXaux=TimeX;
        end
        %compruebo que las tres variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,newY,TimeX,'Y-Data','X-Data');      if awinc.state, set(hg,'Visible','on'); return; end,
        %re-inicializo los ejes y variables
        set(ui_tbd,'String',texto('timeOptions'));drawnow;
        set(ui_run,'Enable','on');
        set(ui_tp,'Enable','off');
        set(ui_pp,'Enable','off');
        %set(hf,'Toolbar','figure');
        %
        set(0,'CurrentFigure',hf)
        set(hf,'CurrentAxes',ax1);cla(ax1,'reset');
        % Plot
        
        plot(ax1,TimeX,newY,'+b');
        ylabel(ax1,labelsameva(var_name(2),var_unit(2)));
        xlabel(ax1,labelsameva(var_name(1),var_unit(1)));
        grid(ax1,'on');
        title(ax1,cityname);
        if timedefine,
            datetick(ax1,'x','mmmyy','keepticks');
        end
        %reset default
        cla(ax2,'reset');set(ax2,'Visible','off');
        cla(ax3,'reset');set(ax3,'Visible','off');
        set(ui_kstest,'String','');
        set(ui_tb,'String',texto('clickStart'));drawnow ;
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
    end

%% Run data
    function AmevaRun(source,event)
        %Creo el directorio de trabajo para almacenamiento
        awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
        EnableButton([ui_tp,ui_pp,ui_run],'Enable','off');
        EnableButton(hg,'Visible','off');
        
        %Sting name for the plots
        tpstring{1,1}=texto('results');
        tpstring{1,2}=texto('autocorre');
        tpstring{1,3}=texto('partAutocorre');
        tpstring{1,4}=texto('resid');
        tpstring{1,5}=texto('normResidue');
        set(ui_tp,'String',tpstring);%Set Sting Plots. set(ui_pp,'String',texto('buttonPlot_2'));
        
        set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
        %Settings parameter
        conf=str2double(get(ui_c,'String'));%confidence interval
        outlierconf=str2double(get(ui_a,'String'));%outlier Significance level
        rmo=get(ui_rmo,'Value');
        if outlierconf==1,
            outlierconf=[];
            rmo=0;
        end
        
        %Run function
        try
            %Refresco las variables con el valor con el original
            newY=Yaux;TimeX=TimeXaux;
            %save options
            svopt(1)=get(ui_ckfig,'Value');%fig
            svopt(2)=get(ui_ckpng,'Value');%png
            svopt(3)=get(ui_ckeps,'Value');%eps
            
            [idx,pnew,model,pvalue,fval,exitflag,output,lambda,grad,hessian,residuo,rN,fmunew,fsigmanew,...
                Omega,upperp,lowerp,uppermean,lowermean,upperpredict,lowerpredict] = outlierfilterRNoCteC3E (TimeX,newY,km,conf,outlierconf);%0.9,0.95
            if rmo,
                if ~isempty(idx) && ~isempty(idx{1}),
                    TimeX(idx{1})=[];
                    newY(idx{1})=[];
                end
                [idx,pnew,model,pvalue,fval,exitflag,output,lambda,grad,hessian,residuo,rN,fmunew,fsigmanew,...
                    Omega,upperp,lowerp,uppermean,lowermean,upperpredict,lowerpredict] = outlierfilterRNoCteC3E (TimeX,newY,km,conf,1);%0.9,0.95
                idx=[];
            end
            % plots
            set(ui_tp,'Value',1);AmevaPlot;
            %   Kolmogorov-Smirnov test sobre normalidad del residuo
            [hr pvaluer] = kstest((residuo-mean(residuo))/std(residuo),[],1-conf);
            %   Si hr = 0 el residuo es normal. pvalue>1-alpha
            %   Si hr = 1 se rechaza la hipotesis de que sea normal el residuo.
            %   pvalue<1-alpha  -->  pvaluer<1-conf
            if hr
                ksteststr={texto('modelKolmoSmir') ' ', ...
                    [texto('notNormal') ' ','pValue = ' num2str(pvaluer)]};
            else
                ksteststr={texto('modelKolmoSmir') ' ', ...
                    [texto('yesNormal') ' ','pValue = ' num2str(pvaluer)]};
            end
            set(ui_kstest,'String',ksteststr);
            disp(ksteststr);
            if ecoinstall
                [Hr,pValuer,Qstatr,CriticalValuer] = lbqtest(residuo,[1 2 3 4 5],1-conf);
            end
            % table
            if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
            %plot table lowupx
            set(hf,'CurrentAxes',ax2);fs='%.4f';set(ax2,'visible','on');
            polycoef=[lowerp pnew upperp];
            vartipo={'Lower';'Pnew';'Upper'};
            [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs,frec);
            plottable(TB',lowupx,lowupy,9,fs,'Center');hold off;
            %save
            save([carpeta,filesep,'result_heter_model.mat'],'lowerp','pnew','upperp','residuo','outlierconf','idx','conf','Hr','pValuer','Qstatr','ksteststr');
            %save plots
            heteroscedasticplots(newY,TimeX,idx,fmunew,uppermean,lowermean,upperpredict,lowerpredict,...
                residuo,lowerp,pnew,upperp,Hr,pValuer,Qstatr,modelstr,frec,tpstring,svopt,carpeta,cityname,var_name,var_unit);
            set(hf,'CurrentAxes',ax1);
        catch ME
            EnableButton([ui_tp,ui_pp,ui_run],'Enable','on');
            rethrow(ME);
        end
        
        %listar los plot para imprimir
        awinc.ListBoxCallbackP(ui_pp,carpeta);% Methods for "amevaclass"
        
        set(ui_tb,'String',[nmfctn,texto('start')]);drawnow ;
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
        EnableButton([ui_tp,ui_pp,ui_run],'Enable','on');
    end

%% Ameva plot
    function AmevaPlot(source,event)
        set(hf,'CurrentAxes',ax1);
        EnableButton([ui_tp,ui_pp,ui_run],'Enable','off');
        set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
        iins=get(ui_tp,'String');[ni mi]=size(iins);
        if ~ishandle(ax1), disp([texto('plot') ' ' nmfctn]); return; end
        cla(ax3,'reset');cla(ax1,'reset');
        fs='%.3f';
        if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
        if ni==1, return; end;
        plotname=iins{get(ui_tp,'Value')};
        try
            switch plotname
                case texto('results')
                    HetePlot(newY,TimeX,idx,fmunew,uppermean,lowermean,upperpredict,lowerpredict,modelstr1,var_name,var_unit,ax1);
                    set(ax3,'visible','off');set(ui_kstest,'visible','off');
                case texto('autocorre')
                    if ecoinstall, autocorr(residuo); title([get(get(ax1,'title'),'string') ' of Residue']); end
                    %plot table H p-value Q_s
                    set(hf,'CurrentAxes',ax3);
                    set(ax3,'visible','on');set(ui_kstest,'visible','on');
                    polycoef=[Hr' pValuer' Qstatr'];
                    vartipo={'Hr';'P-value';'Qs'};
                    [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs);
                    plottable(TB',lowupx,lowupy,9,fs,'Center');hold off;
                case texto('partAutocorre')
                    if ecoinstall, parcorr(residuo); title([get(get(ax1,'title'),'string') ' of Residue']); end
                    %plot table H p-value Q_s
                    set(hf,'CurrentAxes',ax3);
                    set(ax3,'visible','on');set(ui_kstest,'visible','on');
                    polycoef=[Hr' pValuer' Qstatr'];
                    vartipo={'Hr';'P-value';'Qs'};
                    [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs);
                    plottable(TB',lowupx,lowupy,9,fs,'Center');hold off;
                case texto('resid')
                    plot(residuo);grid on;
                    ylabel(['\epsilon_{' char(labelsameva(var_name(2),var_unit(2))) '}']);
                    title('Model error');
                    %plot table H p-value Q_s
                    set(hf,'CurrentAxes',ax3);
                    set(ax3,'visible','on');set(ui_kstest,'visible','on');
                    polycoef=[Hr' pValuer' Qstatr'];
                    vartipo={'Hr';'P-value';'Qs'};
                    [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs);
                    plottable(TB',lowupx,lowupy,9,fs,'Center');hold off;
                case texto('normResidue')
                    normplot(residuo);
                    xlabel(['\epsilon_{' char(labelsameva(var_name(2),var_unit(2))) '}']);
                    %plot table H p-value Q_s
                    set(hf,'CurrentAxes',ax3);
                    set(ax3,'visible','on');set(ui_kstest,'visible','on');
                    polycoef=[Hr' pValuer' Qstatr'];
                    vartipo={'Hr';'P-value';'Qs'};
                    [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs);
                    plottable(TB',lowupx,lowupy,9,fs,'Center');hold off;
                otherwise
                    disp([texto('notPlotCase') ' ',plotname]);
                    cla(ax1,'reset');legend(ax1,'hide');axis(ax1,'xy');
                    set(ax3,'visible','off');set(ax2,'visible','off');
                    set(ui_kstest,'visible','off');
            end
        catch ME
            EnableButton([ui_tp,ui_pp,ui_run],'Enable','on');
            rethrow(ME);
        end
        set(ui_tb,'String',[nmfctn,texto('start')]);drawnow ;
        EnableButton([ui_tp,ui_pp,ui_run],'Enable','on');
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
    end
end

function [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs,frec)
n1=0; if nargin>3, n1=1; end;
[nx,ns]=size(polycoef');
nx=length(vartipo);
TB=cell(nx+n1,ns+1);
if n1
    for i=1:ns+1
        TB(1,i)=frec(ns+n1+1-i);
    end
end
for i=ns:-1:1,
    for j=2:nx+1,
        if abs(polycoef(i,j-1))>1e-3
            TB(j-1+n1,ns-i+1)={num2str(polycoef(i,j-1),fs)};
        else
            TB(j-1+n1,ns-i+1)={num2str(polycoef(i,j-1),'%1.1e')};
        end
    end
end
for i=1:nx
    TB(i+n1,ns+1)=vartipo(i);
end
if n1
    lowupx=[0 0.5:1:nx+n1-0.5];
else
    lowupx=(0:nx+n1);
end
lowupy=(0:ns+1);
end

