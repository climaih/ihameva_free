function heteroscedasticplots(X,Time,idx,fmunew,uppermean,lowermean,upperpredict,lowerpredict,residuo,lowerp,pnew,upperp,Hr,pValuer,Qstatr,modelstr,frec,tpstring,svopt,carpeta,cityname,var_name,var_unit)
% compruebo que existe la tool de mapas
ecoinstall=amevaTheseToolboxesInstalled('Econometrics Toolbox');

tpstring{end+1}='Table Lower Pnew Upper';
tpstring{end+1}='Table H Pvalue Qs';
[ni mi]=size(tpstring);
if mi==1, return; end;
fs='%.3f';
for i=1:mi,
    plothc=false;
    plotname=tpstring{i};
    h=figure('Visible','off');
    switch plotname
        case 'Results'
            HetePlot(X,Time,idx,fmunew,uppermean,lowermean,upperpredict,lowerpredict,modelstr,var_name,var_unit,gca)
        case 'Autocorrelation'
            if ecoinstall, autocorr(residuo); title([get(get(gca,'title'),'string') ' of Residue']); end
        case 'Partial Autocorrelation'
            if ecoinstall, parcorr(residuo); title([get(get(gca,'title'),'string') ' of Residue']); end
        case 'Residue'
            plot(residuo);grid on;
            ylabel(['\epsilon_{' char(labelsameva(var_name(2),var_unit(2))) '}'])
            title('Model error');
        case 'Table Lower Pnew Upper'
            polycoef=[lowerp pnew upperp];
            vartipo={'Lower';'Pnew';'Upper'};
            [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs,frec);
            plottable(TB',lowupx,lowupy,9,fs,'Center');hold off;
        case 'Residual normplot'
            normplot(residuo);
            xlabel(['\epsilon_{' char(labelsameva(var_name(2),var_unit(2))) '}'])
        case 'Table H Pvalue Qs'
            %plot table H p-value Q_s
            polycoef=[Hr' pValuer' Qstatr'];
            vartipo={'H';'P-value';'Q_s'};
            [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs);
            plottable(TB',lowupx,lowupy,9,fs,'Center');hold off;
        otherwise
            disp(['Warnig!.Not exist this plot case ',plotname]);
    end
    if ~isempty(h)
        plotname=strrep(plotname,' ', '');
        plotname=strrep(plotname,'/', '_');
        set(h,'Color','w');%solo en plot
        amevasavefigure(h,carpeta,svopt,plotname,plothc);
    end 
end

end

function [TB,lowupx,lowupy]=maketabla(polycoef,vartipo,fs,frec)
n1=0; if nargin>3, n1=1; end;
[nx,ns]=size(polycoef');
nx=length(vartipo);
TB=cell(nx+n1,ns+1);
if n1
    for i=1:ns+1
        TB(1,i)=frec(ns+n1+1-i);
    end
end
for i=ns:-1:1,
    for j=2:nx+1,
        TB(j-1+n1,ns-i+1)={num2str(polycoef(i,j-1),fs)};
    end
end
for i=1:nx
    TB(i+n1,ns+1)=vartipo(i);
end
if n1
    lowupx=[0 0.5:1:nx+n1-0.5];
else
    lowupx=(0:nx+n1);
end
lowupy=(0:ns+1);
end
