function disfitanalysis(varargin)
%   DISFITANALYSIS Distribution Fit Analysis Analysis of Environmental Variables
%   DISFITANALYSIS (disfitanalysis.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
% Data
%   The drop-down list in the Data field contains the names of all matrices 
%   and vectors, other than 1-by-1 matrices (scalars) in the MATLAB workspace.
%   Select the array containing the data you want to fit. The actual data you
%   import must be a vector. If you select a matrix in the Data field, the first
%   column of the matrix is imported by default. To select a different column
%   or row of the matrix, click Select Column or Row. This displays the matrix
%   in the Variable Editor, where you can select a row or column by highlighting
%   it with the mouse.
%   Alternatively, you can enter any valid MATLAB expression in the Data field.
%   When you select a vector in the Data field, a histogram of the data is 
%   displayed in the Data preview pane.
% Censoring
%   If some of the points in the data set are censored, enter a Boolean vector,
%   of the same size as the data vector, specifying the censored entries of
%   the data. A 1 in the censoring vector specifies that the corresponding 
%   entry of the data vector is censored, while a 0 specifies that the entry
%   is not censored. If you enter a matrix, you can select a column or row by
%   clicking Select Column or Row. If you do not want to censor any data,
%   leave the Censoring field blank.
% Frequency
%   Enter a vector of positive integers of the same size as the data vector
%   to specify the frequency of the corresponding entries of the data vector.
%   For example, a value of 7 in the 15th entry of frequency vector specifies
%   that there are 7 data points corresponding to the value in the 15th entry
%   of the data vector. If all entries of the data vector have frequency 1,
%   leave the Frequency field blank.
% Data name
%   Enter a name for the data set you import from the workspace, such as My data.
% 
% Examples 1
%   ameva-->Tools-->Distribution Fit Analysis
% Examples 2
%   disfitanalysis
% Examples Generate all randon data to probe the main program
% datanorm=normrnd(4,0.5,4000,1);   %Normal
% datalnrm=lognrnd(4,0.5,4000,1);   %Log-normal
% datawbl=wblrnd(1,2,4000,1);       %Weibull (min lamda=0 -> x>0)
% datawbl_min=wblrnd_min(10,1,4,4000,1);
% datawbl_max=wblrnd_max(10,1,2,4000,1);
% datagumbel=gevrnd(0,6,4,4000,1);  %Gumbel=gev(0,psi,mu)
% datagev=gevrnd(0.1,6,4,4000,1);   %extreme value
% datapareto=gprnd(-0.3,1,0,4000,1);%pareto
% dataexp=exprnd(5,4000,1);         %exponencial
% datarayl=raylrnd(5,4000,1);       %rayleigh
% datagam=gamrnd(2,4,4000,1);       %gamma
% databet=betarnd(4,5,4000,1);      %beta
% datalgtc=logisticrnd(4,2,4000,1); %logistic
% datatstd=trnd(6,4000,1);          %t-student
% datapoiss=poissrnd(6,4000,1);     %poisson
% See also: 
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   disfitanalysis(main program tool v1.5.3 (140820)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain. 
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   07-05-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v1.5.3 (140820)';
nmfctn='disfitanalysis';
Ax1Pos=[0.42 0.2 0.55 0.7];
figPos=[];

idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

% Texto idioma:
texto=amevaclass.amevatextoidioma(idioma);

namesoftware=[texto('nameDisFit') ' ',versionumber];


if ~isempty(findobj('Tag',['Tag_' nmfctn]));
   figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
   close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar
hh=[];%ventana auxiliar
hi=[];%ventana auxiliar

% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.135;			
btnHt=0.05;
spacing=0.020;			
top=0.95;
xPos=[0.01 0.15 0.28 0.42];

%GLOBAL VARIALBES solo usar las necesarias OJO!!
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
N=0;%number of selected data
ndx=10;%number of bins for histogram
X=[];X_=[];
Cens=[];
Frec=[];
fitnumber=1;
alldistri_result = struct('name',{},'param',{},'nlogL',{},'paramstr',{},'pci',{},'AVAR',{});%structura con los resultados de los fit
alfa=0.05;%confidence interval (1-alpha)*100
%Exponential probability plot (nonnegative values)
%Extreme value probability plot (all values)
%Lognormal probability plot (positive values)
%Normal probability plot (all values)
%Rayleigh probability plot (positive values)
%'weibull'  Weibull probability plot (positive values)
DistriName={''};

%name of variables globales
carpeta=[];
var_name='';
var_unit='';
xvar_name='';%nombre de la variable para la figura,fichero de salida
xmin=0;xmax=0;qxmin=0;qxmax=1;%x minimo, maximo, quantil inferior y superior
svopt=[1 1 0];%save option 1-fig 2-png 3-eps (1/0)

%---LA FIGURA-VENTANA PRINCIPAL-
if isempty(figPos), figPos=awinc.amevaconst('mw'); end
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
	'NumberTitle','Off','DockControls','Off','Position',figPos, 'Tag',['Tag_' nmfctn],'NextPlot','new');
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');
set(hf,'CurrentAxes',ax1);

if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_1a'), ...
    'Position',[xPos(2) 0.94 btnWid btnHt*0.66]);

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('on'));

ui_tp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_1'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlot);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('distri'),'Position',[xPos(1) yPos+0.036 btnWid btnHt*0.66]);
ui_dty_hf=uicontrol('Style','popup','Units','normalized','Parent',hf,'Callback',@AmevaPlot, ...
    'Position',[xPos(1) yPos-0.005 btnWid btnHt],'String',DistriName,'Enable','Off','Tag','Tag_dty');

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_2'), ...
    'Position',[xPos(2) yPos+0.036 btnWid btnHt*0.66]);
ui_pp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_3'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlotToPrint);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_nfit=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String',texto('newFit'), ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataO1('on'),'Enable','off');

btnN=btnN+1.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
hsp=subplot('position',[xPos(1) yPos .5 0.1],'Parent',hf);
htext(1)=text(0,0.65,texto('typeDistri'),'FontSize',10,'Parent',hsp);
htext(2)=text(0,0.0,'','Interpreter','latex','FontSize',11,'Parent',hsp);axis off;

btnN=btnN+1.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_best=uicontrol('Style','text','Units','normalized','Parent',hf,'String','',...
    'Position',[xPos(1) yPos btnWid*2 btnHt],'HorizontalAlignment','left');

ui_ttd=uicontrol('Style','listbox','Units','normalized','Parent',hf,'String','','FontSize',7.5, ...
    'Position',[xPos(1) 0.05 0.312 0.57-btnHt*1.5],'FontName', 'FixedWidth','Visible','on');%Tabla de salida

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',svopt(2),'Callback',@(source,event)EnableButton(ui_data,'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('dataButton')],'HorizontalAlignment','left');

%% Long-Term Statistics Data
btnWid=0.35;
btnHt=0.075;
spacing=0.02;
top=0.98;
xPos=[0.03 0.45 0.82];
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid*1.15 btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
rb(1)=uicontrol('Style','radiobutton','Units','normalized', ...
   'String',texto('continuosData'),'Value',1,'Position',[xPos(1) yPos btnWid*1.5 btnHt]);
rb(2)=uicontrol('Style','radiobutton','Units','normalized', ...
   'String',texto('discretData'),'Value',0,'Position',[xPos(2)+btnWid*.5 yPos btnWid*1.5 btnHt]);
set(rb(1),'userdata',rb(2));	set(rb(2),'userdata',rb(1));
call=['me = get(gcf,''CurrentObject'');',...
      	'if (get(me,''Value'')==1),',...
      	'set(get(me,''userdata''),''Value'',0),',...
      	'else,',		'set(me,''Value'',1),',		'end;clear me;'];
set(rb, 'callback',call);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(1)=uicontrol('Style','text','Units','normalized','Parent',hg,'String','Data:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(1)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','BackgroundColor','white','Callback',@UpdateNameAll,'UserData',1);
ui_cm(1)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','Off','Callback',@ltsplot);


btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(2)=uicontrol('Style','text','Units','normalized','Parent',hg,'String',texto('censored'), ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(2)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','BackgroundColor','white','Callback',@UpdateNameAll,'UserData',2,'Enable','Off');
ui_cm(2)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_td(3)=uicontrol('Style','text','Units','normalized','Parent',hg,'String',texto('frequency'), ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_all(3)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','BackgroundColor','white','Callback',@UpdateNameAll,'UserData',3,'Enable','Off');
ui_cm(3)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2 btnHt],'String','(none)','visible','off');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','X name & unit:', ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right','BackgroundColor',colorG);
ui_fs=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','Hs', ...
    'Position',[xPos(2) yPos btnWid/2 btnHt]);
ui_fu=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','m', ...
    'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Apply', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaSetData);

btnN=btnN+4;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)EnableButton(hg,'Visible','off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.04]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,'String',texto('dataWorkspace'), ...
    'Position',[0.01 0.004 0.99 0.03],'HorizontalAlignment','left');

%% Ameva New Fit & Ameva Settings
hh=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'NewFit'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataO1('off'),'WindowButtonMotionFcn',@ListBoxCallback2,...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','new');
if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hh,'String',texto('fitName'), ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right','BackgroundColor',colorG);
ui_fname=uicontrol('Style','Edit','Units','normalized','Parent',hh,'Position',[xPos(2) yPos btnWid btnHt],'String',[texto('fit') ' ',num2str(fitnumber)]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hh,'String',texto('distri'), ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right','BackgroundColor',colorG);
ui_dty_hh=uicontrol('Style','list','Units','normalized','Parent',hh,...
    'Position',[xPos(2) yPos-3.5*btnHt btnWid btnHt*4.5],'String',DistriName);

btnN=btnN+3.8;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hh,'String',texto('exclusionRule'), ...
    'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right','BackgroundColor',colorG);
ui_excl_ru=uicontrol('Style','popup','Units','normalized','Parent',hh,...
    'Position',[xPos(2) yPos btnWid btnHt],'String',' ','Enable','off');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hh,'Position',[xPos(1)+btnWid/2 yPos-spacing btnWid btnHt], ...
    'String','Settings','FontWeight','bold','BackgroundColor',colorG);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hh,'String',texto('significanLevel'), ...
    'Position',[xPos(2) yPos+2*spacing btnWid btnHt*0.9],'BackgroundColor',colorG);
ui_nc=uicontrol('Style','edit','Units','normalized','Parent',hh,'Position',[xPos(2) yPos btnWid btnHt], ...
    'String',num2str(alfa));
uicontrol('Style','text','Units','normalized','Parent',hh,'String',texto('bin'), ...
    'Position',[xPos(1) yPos+2*spacing btnWid btnHt*0.9],'BackgroundColor',colorG);
ui_nbin=uicontrol('Style','edit','Units','normalized','Parent',hh,'Position',[xPos(1) yPos btnWid btnHt], ...
    'String',num2str(ndx));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_apply=uicontrol('Style','push','Units','normalized','Parent',hh,'String','Start', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaRun);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hh,'String','Close', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)FigDataO1('off'));

uicontrol('Style','frame','Units','normalized','Parent',hh,'Position',[0 0 1 0.04]);
ui_tbn=uicontrol('Style','Text','Units','normalized','Parent',hh,'String',texto('start_2'), ...
    'Position',[0.01 0.004 0.99 0.03],'HorizontalAlignment','left');
set(hh,'HandleVisibility','off')

% %% Exclude Windows hi
% btnWid=0.3;
% btnHt=0.06;
% spacing=0.02;
% top=0.95;
% xPos=[0.03 0.35 0.67];
% FigPos=[7 300 250 350];
% figPos=[(screenSize(3)-FigPos(3)-FigPos(1)) (screenSize(4)-FigPos(4)-FigPos(2)) FigPos(3) FigPos(4)];
% hi=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Exclude'],'Color',colorG,'CloseRequestFcn',@(source,event)EnableButton(gcf,'Visible','off'),...
%     'NumberTitle','Off','Position',figPos,'Resize','on','Visible','off');%,'WindowButtonMotionFcn',@ListBoxCallback1
% ax2=axes('Units','normalized','Parent',hi,'Position',[xPos(4) 0.43 0.5 0.5],'FontSize',8,'DrawMode','fast');
% if not(isdeployed) && usertype==0, awinc.loadamevaico(gcf,true); end%Load ameva ico
% 
% btnN=2;
% yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
% uicontrol('Style','text','Units','normalized','Parent',hi,'String',texto('exclusionRuleName'), ...
%     'Position',[xPos(1) yPos btnWid/2 btnHt],'HorizontalAlignment','left');
% ui_all(1)=uicontrol('Style','popup','Units','normalized','Parent',hi,'Position',[xPos(1)+btnWid/2 yPos btnWid btnHt],...
%     'BackgroundColor','white','String','(none)');

%% C0 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0
    function AmevaPlotToPrint(source,event) % List of fig
        awinc.AmevaPlotToPrint(ui_pp,carpeta);
    end
%% C0 Open,Close figure Settings
    function FigDataO1(state,source,event)
        set(hh,'Visible',state);
        if strcmpi(state,'on'),
            %actualiza la distribucion en la ventana principal
            %set(findobj('Tag','Tag_dty'),'Value',get(gcbo,'Value'));
            %set(ui_dty_hh,'Callback','set(findobj(''Tag'',''Tag_dty''),''Value'',get(gcbo,''Value''))');
            fitnumber=fitnumber+1;
            set(ui_fname,'String',[texto('fit') ' ',num2str(fitnumber)]);
        end
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);
    end
%% C1
    function ListBoxCallback2(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all(1),true);
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg,hh], ...
        {[upper(nmfctn(1)),nmfctn(2:end),'Data'] [upper(nmfctn(1)),nmfctn(2:end),'NewFit']});
    end
%% C1 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
        ltsplot;% Refresh plot 
    end
%% C1 Refresh plot
function ltsplot(source,event)
    X_=[];
    nlv=1;
    ione=get(ui_all(nlv),'Value');
    if ione>1,
        try
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            X_=awinc.data;
            if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end
            nmsize=size(X_);
            set(ui_td(nlv),'string',{texto('data_1') ' ',sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
            set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
            if min(nmsize)==1
                plot(ax1,X_,'k');
            else
                plot(ax1,X_);
            end
            grid on;
            title(ax1,'Serie','FontWeight','bold');
        catch ME
            disp(texto('variableErase'));
            rethrow(ME);
        end
    end
end
%% Evaluar  data and expresion
function AmevaSetData(source,event)
    EnableButton([hg,hh],'Visible','off');
    set(0,'CurrentFigure',hf);
    set(hf,'CurrentAxes',ax1);
    %set(hf,'Toolbar','figure');
    set(ui_tbd,'String',texto('applyData'));drawnow;

    %Sting of variables to load
    ione=get(ui_all(1),'Value');sone=get(ui_all(1),'String');
    itwo=get(ui_all(2),'Value');stwo=get(ui_all(2),'String');
    ithr=get(ui_all(3),'Value');sthr=get(ui_all(3),'String');
    %Asigno las variables del workspace a las variables de paso(A,B,...,G)
    if ione>1, X = X_; end % OJO OJO evalin('base',sone{ione});
    if itwo>1,
        awinc.AmevaEvalData(2,ui_cm,ui_all);
        Cens=awinc.data;
    else
        Cens=[];
    end
    if ithr>1,
        awinc.AmevaEvalData(3,ui_cm,ui_all);
        Frec=awinc.data;
    else
        Frec=[];
    end
    var_name=get(ui_fs,'String');
    var_unit=get(ui_fu,'String');
    Lab{1}=labelsameva(var_name,var_unit);
	set(ui_tp,'String',texto('buttonPlot_1'),'Value',1);
    set(ui_pp,'String',texto('buttonPlot_2'),'Value',1);
    N=length(X);%Length of serie X
    set(ui_nbin,'String',num2str(round(3/2+log(N)/log(2))*2));
    if N<10, 
        EnableButton(hg,'Visible','on');
        msgbox(texto('notData'));
        return; 
    end
    %re-inicializo los ejes y variables
    cla(ax1,'reset');
	plot(ax1,X,'k');
	grid on;
	ylabel(ax1,Lab{1});
	title(ax1,[texto('series_2') ' ',var_name],'FontWeight','bold');
    set(ui_dty_hh,'String','','Value',1);
    set(ui_dty_hf,'String','','Value',1);
    % SELECCION DEL TIPO DE DISTRIBUCIONES DE ACUERDO AL TIPO DE DATOS
    
    %%%%%%%
    %%%%%%% distributions='';
    if get(rb(1),'Value')
        if all(X >= 0)
            if all(X > 0)
                if  all(X < 1)
                    %%%
                    %%%
                    %%%%%%% eval(['distributions=' texto('serieDistri_1') ';']);
                    %%% DistriName=distributions;
                    DistriName={'normal','uniform','lognormal','weibull','weibullmin','weibullmax','gumbel','extreme value','pareto',...
                                'exponential','rayleigh','gamma','beta','logistic','t-student'};
                else
                    %%%
                    %%%
                    %%%%%%%%%% eval(['distributions=' texto('serieDistri_2') ';'])
                    %%%DistriName=distributions;
                    DistriName={'normal','uniform','lognormal','weibull','weibullmin','weibullmax','gumbel','extreme value','pareto',...
                                'exponential','rayleigh','gamma','logistic','t-student'};
                    %%%DistriName=distributions;
                    disp(texto('distriNotApply_2'));
                end
            else
                %%%
                %%%
                %%%%%%%%%% eval(['distributions=' texto('serieDistri_3') ';'])
                %%% DistriName=distributions;
                DistriName={'normal','uniform','exponential','gamma','gumbel','extreme value','logistic','t-student'};
                disp(texto('distriNotApply_3'));
            end
        else
            %%%
            %%%
            %%%%%%%%%% eval(['distributions=' texto('serieDistri_4') ';'])
            %%%DistriName=distributions;
            DistriName={'normal','uniform','gumbel','extreme value','logistic','t-student'};
            disp(texto('distriNotApply_4'));
        end
    else
        if isequal(X,round(X)) && all(X>=0)
            DistriName={texto('poisson')};%unid
        else
            DistriName={''};
            disp(texto('notApplyPoisson'));
        end
    end
    if length(DistriName{1})<2,
        EnableButton(hg,'Visible','on');
        msgbox(texto('notApplyAdjustment'));
        return;
    end
    set(ui_dty_hh,'String',DistriName,'Min',1,'Max',length(DistriName),'Value',(1:length(DistriName)));
    updlistbox;
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;
    strttd={texto('distri');...
                    texto('logLikelihood');...
                    texto('domain');...
                    texto('mean');...
                    texto('variance');
                    '';...
                    texto('stdErr');'';'';'';'';'';'';'';'';'';};
    set(ui_ttd,'String',strttd,'Visible','on','FontSize',8,'FontWeight', 'light');
    set(ui_nfit,'Enable','on');
    set(ui_apply,'Enable','on');
    set(ui_best,'String','');
    set(ui_pp,'String',texto('buttonPlot_3'),'Enable','Off','Value',1);
    set(ui_tb,'String',[nmfctn,texto('start')]);drawnow;
    EnableButton([ui_pp,ui_tp,ui_dty_hf],'Enable','off');
    EnableButton(hh,'Visible','on');
end

%% Update List box Todas o solo seleccionadas OJO OJO  comprobar OJO
function updlistbox(source,event)
    updlbDN=get(ui_dty_hh,'String');
    updlbDV=get(ui_dty_hh,'Value');
    DistriName=updlbDN(updlbDV);
    set(ui_dty_hf,'String',DistriName);   
end

%% Run data
function AmevaRun(source,event)
    %Creo el directorio de trabajo para almacenamiento
    awinc.NewFolderCheck(carpeta,nmfctn);carpeta=awinc.carpeta;% Methods for "amevaclass"
    EnableButton([ui_data,ui_nfit,ui_apply,ui_pp,ui_tp,ui_dty_hf],'Enable','off');
    EnableButton([hg,hh],'Visible','off');
    set(ui_tb,'String',[nmfctn,texto('run')]);drawnow;
    disp([nmfctn,texto('run')]);

    try
        updlistbox;%Actualizo las distribuciones
        alfa=str2num(get(ui_nc,'string'));%confidence level
        %Fit a las distintas distribuciones implementadas
        for i=1:length(DistriName)
            alldistri_result(i).name=DistriName{i};
            [param,paramstr,pci]=disfitparam(DistriName{i},X,alfa);
            [y,x,nlogL,AVAR]=pdfcdffit(DistriName{i},X,param,alfa,'pdf');
            alldistri_result(i).param=param;
            alldistri_result(i).nlogL=nlogL;
            alldistri_result(i).paramstr=paramstr;
            alldistri_result(i).pci=pci;
            alldistri_result(i).AVAR=AVAR;
        end
        alldistri_result(1).data=X;
        save([pwd,filesep,carpeta,filesep,'all_distribution_result_',xvar_name,'_',var_name,'.mat'],'alldistri_result');
        %mejor ajuste and loglikelihood
        [aux,auxi]=max(disfitlike(DistriName,X,alldistri_result,alfa));
        set(ui_dty_hf,'Value',auxi);
        set(ui_best,'String',[texto('bestFit') ' ' upper(char(DistriName(auxi))) texto('distriLogLikeli') ' ' sprintf('%4.2f',aux)]);
        %%%%%
        %%%%%
        %%%%% list=''
        %%%%% eval(['list=' texto('list') ';'])
        %%%%% set(ui_tp,'String', list,'Value',2);
        set(ui_tp,'String', {'Serie(X)','PDF(X)','CDF(X)','ProbabilityPlot','QQplot','PPplot'},'Value',2);
        AmevaPlot;
        awinc.ListBoxCallbackP(ui_pp,carpeta);% Methods for "amevaclass"
    catch ME
        disp([texto('errorDistri'),DistriName{i}]);
        EnableButton([ui_data,ui_nfit,ui_apply,ui_pp,ui_tp,ui_dty_hf],'Enable','on');
        rethrow(ME);
    end
    %set plots
	EnableButton([ui_data,ui_nfit,ui_apply,ui_pp,ui_tp,ui_dty_hf],'Enable','on');
    set(ui_tb,'String',[nmfctn,texto('ready')]);drawnow;
end

%% Regimen Medio plot's
function AmevaPlot(source,event)
    [mins,nins]=size(get(ui_tp,'String'));
    iin=get(ui_tp,'Value');
	iins=get(ui_tp,'String');
    [ni mi]=size(iins);
    if ni==1, return; end;
    %save options
    svopt(1)=get(ui_ckfig,'Value');%fig
    svopt(2)=get(ui_ckpng,'Value');%png
    svopt(3)=get(ui_ckeps,'Value');%eps
    plotname=iins{get(ui_tp,'Value')};
    id=get(ui_dty_hf,'Value');%distribution index
    DistributionName=DistriName{id};
    param=alldistri_result(id).param;
    [fpdf,fcdf]=modelfunction(DistributionName);
    if strcmp(plotname,'CDF(X)')
        set(htext(1),'String',[DistributionName,texto('cumulativeDistri')]);
        set(htext(2),'String',fcdf);
    else
        set(htext(1),'String',[DistributionName,texto('probDensity')]);
        set(htext(2),'String',fpdf);
    end
    
    if ~ishandle(ax1), disp([texto('plot') nmfctn]); return; end
    set(hf,'CurrentAxes',ax1);cla(ax1,'reset');axis(ax1,'square');

    N=length(X);
    ndx=round(str2num(get(ui_nbin,'String')));

    strttd{1,1}=[texto('distri') ' ' DistributionName];
    strttd{1,2}=[texto('mean') ' ' num2str(mean(X))];
    strttd{1,3}=[texto('std') ' ' num2str(std(X))];
    strttd{1,4}='';
    strttd{1,5}='Log likelihood';
	strttd{1,6}=['L:              ' num2str(alldistri_result(id).nlogL)];
    strttd{1,7}='';
    %%%%
    %%%%
    %%%% strttd{1,8}=[texto('estimateCI')(',num2str((1-alfa)*100),char(37),')];
    strttd{1,8}=['Parameter  Estimate  CI(',num2str((1-alfa)*100),char(37),').'];
    strttd{1,9} =alldistri_result(id).paramstr{1,1};
    strttd{1,10}=alldistri_result(id).paramstr{1,2};
    strttd{1,11}=alldistri_result(id).paramstr{1,3};
    strttd{1,12}='';
    strttd{1,13}=texto('estimateCovari');
    if ~isempty(alldistri_result(id).AVAR)
        auxi=num2str(alldistri_result(id).AVAR);
        for j=1:length(alldistri_result(id).AVAR)
            strttd{1,13+j}=auxi(j,1:end);
        end
    end

    if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
    
    disfitanalysisplots(X,DistributionName,param,alfa,ndx,{plotname},hsp,svopt,carpeta,idioma,var_name,var_unit,ax1,hf);
    
    if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;
    set(ui_tb,'String',texto('newPlotDistri'));
    set(ui_ttd,'String',strttd);%Salida de la tabla con los resultados
	drawnow ;

    %listar los plot para imprimir
    awinc.ListBoxCallbackP(ui_pp,carpeta);% Methods for "amevaclass"
end

%% Latex text selected type fucntion adjust
function [fpdf,fcdf]=modelfunction(ID)
    switch ID
        case 'normal'
        fpdf='$$f(x;\mu,\sigma^2)=\frac{1}{\sigma\sqrt{2\pi}} e^{ -\frac{(x-\mu)^2}{2\sigma^2} }$$';
        fcdf='$$F(x;\,\mu,\sigma^2)=\Phi\left(\frac{x-\mu}{\sigma}\right)$$';
        case 'uniform'
        fpdf='$$f(x;\,a,b)=\frac{1}{b-a},\ \ x\left[a,b\right)$$';
        fcdf='$$F(x;\,a,b)=\frac{x-a}{b-a},\ \ x\left[a,b\right)$$';
        case 'lognormal'
        fpdf='$$f_X(x;\mu,\sigma)=\frac{1}{x \sigma \sqrt{2 \pi}}\, e^{-\frac{(\ln x - \mu)^2}{2\sigma^2}},\ \ x>0$$';
        fcdf='$$F_X(x;\mu,\sigma)=\frac12 erfc\!\left[-\frac{\ln x - \mu}{\sigma\sqrt{2}}\right]$$';
        case 'weibull'
        fpdf='$$f(x;k,s)= \frac{s}{k}\left(\frac{x}{k}\right)^{s-1}e^{-(x/k)^{s}}$$';
        fcdf='$$F(x;s,k) = 1- e^{-(x/k)^s}$$';
        case 'weibullmin'
        fpdf='$$f(x;\lambda,k,s)= \frac{s}{k}\left(\frac{x-\lambda}{k}\right)^{s-1}e^{-((x-\lambda)/k)^{s}}$$';
        fcdf='$$F(x;\lambda,k,s) = 1- e^{-((x-\lambda)/k)^s}$$';
        case 'weibullmax'
        fpdf='$$f(x;\lambda,k,s)= \frac{s}{k}\left(\frac{\lambda-x}{k}\right)^{s-1}e^{-((\lambda-x)/k)^{s}}$$';
        fcdf='$$F(x;\lambda,k,s) = e^{-((\lambda-x)/k)^s}$$';
        case 'gumbel'
        fpdf='$$f(x) = e^{-z} e^{-e^{-z}}\,\;z=\frac{x-\mu}{\sigma}$$';
        fcdf='$$F(x) = e^{-e^{(-z)}}\,\;z=\frac{x-\mu}{\sigma}$$';
        case 'extreme value'
        fpdf='$$f(x;\mu,\sigma,\xi)=\frac{1}{\sigma}\left[1+\xi\left(\frac{x-\mu}{\sigma}\right)\right]^{(-1/\xi)-1}F$$';
        fcdf='$$F(x;\mu,\sigma,\xi)=\exp\left\{-\left[1+\xi\left(\frac{x-\mu}{\sigma}\right)\right]^{-1/\xi}\right\}$$';
        case 'pareto'
        fpdf='$$f(x;u,\sigma,\xi)=\frac{1}{\sigma}\left[1+\xi\left(\frac{x-u}{\sigma}\right)\right]^{(-1/\xi)-1}$$';
        fcdf='$$F(x;u,\sigma,\xi)\;\; = 1-\left[1+\xi\left(\frac{x-u}{\sigma}\right)\right]^{-1/\xi}$$';
        case 'exponential'
        fpdf='$$f(x;\lambda)=\lambda e^{-\lambda x},  x \ge 0$$';
        fcdf='$$F(x;\lambda) = 1-e^{-\lambda x},  x \ge 0$$';
        case 'rayleigh'
        fpdf='$$f(x;b)=\frac{x}{b^2} e^{-x^2/2b^2}, \quad x \geq 0$$';
        fcdf='$$F(x) = 1 - e^{-x^2/2b^2}$$';
        case 'gamma'
        fpdf='$$f(x;k,\theta)= \frac{1}{\theta^k}\frac{1}{\Gamma(k)}x^{k-1}e^{-\frac{x}{\theta}}  x \geq 0, k, \theta > 0$$';
        fcdf='$$F(x;k,\theta)=\int_0^x f(u;k,\theta)\,du = \frac{\gamma\left(k, \frac{x}{\theta}\right)}{\Gamma(k)}$$';
        case 'beta'
        fpdf='$$f(x;\alpha,\beta)=\frac{1}{\mathrm{B}(\alpha,\beta)}\, x^{\alpha-1}(1-x)^{\beta-1}$$';
        fcdf='$$F(x;\alpha,\beta)=\frac{\mathrm{B}_x(\alpha,\beta)}{\mathrm{B}(\alpha,\beta)} = I_x(\alpha,\beta)$$';
        case 'logistic'
        fpdf='$$f(x; \mu,s)=\frac{e^{-(x-\mu)/s}} {s\left(1+e^{-(x-\mu)/s}\right)^2}$$';
        fcdf='$$F(x; \mu,s)=\frac{1}{1+e^{-(x-\mu)/s}}$$';
        case 't-student'
        fpdf='$$f(t)=\frac{\Gamma(\frac{\nu+1}{2})} {\sqrt{\nu\pi}\,\Gamma(\frac{\nu}{2})} \left(1+\frac{t^2}{\nu} \right)^{-\frac{\nu+1}{2}}$$';
        fcdf='$$\int_{-\infty}^t f(u)\,du = 1- \frac{1}{2} I_{x(t)}\left(\frac{\nu}{2},\frac{1}{2}\right)$$';
        otherwise
        fpdf='$$$$';
        fcdf='$$$$';
    end
end
%% hist_n
function [yout,xx] = hist_n(t,M)

[f,x]=ecdf(t);
[yout,xx]=ecdfhist(f,x,M);
if nargout==0,
    plot(xx,yout,'x');
    auxi=0;
    for k=1:length(xx)-1, auxi=auxi+yout(k)*(xx(k+1)-xx(k));end
    auxi=auxi+yout(end)*(xx(end)-xx(end-1));
    disp(['sum(f(x)*dx)=' num2str(auxi)]);
end
end

end
