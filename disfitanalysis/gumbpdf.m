

function y = gumbpdf(x,a,b,trunc)
%GUMBPDF Gumbel probability density function (pdf).
%
% CALL:  y = gumbpdf(x,a,b,trunc) 
%   
%   y    = Gumbel pdf evaluated at x
%  a, b  = parameters of the Gumbel distribution.
%  trunc = 0  regular gumbel distribution (default)
%          1  truncated gumbel distribution 
%
%  Gumbel PDF  is given by :                           
%      f(x) = exp(-(x-B)/A)*exp(-exp(-(x-B)/A))/A    -inf < x < inf, a>0
%  or the truncated
%      f(x) = exp(-(x-B)/A)*exp(-exp(-(x-B)/A))/A/(1-exp(-exp(B/A)))
%          0 < x < inf, a>0 
%
%   The size of P is the common size of the input arguments. A scalar input
%   functions as a constant matrix of the same size as the other inputs.    
%
% See also: gumbfit, gumbrnd, gumbcdf, gumbinv, gumbstat

%   References:
%      [1]  Michel K. Ochi,
%       OCEAN TECHNOLOGY series 6
%      "OCEAN WAVES, The stochastic approach", Cambridge
%      1998 p. 167-170.


%  tested on: matlab 5.2
% history
% revised pab 8.11.1999
% updated header info
%   Per A. Brodtkorb 17.10.98


if nargin < 4 | isempty(trunc),
    trunc=0; % default value is not truncated
end


if nargin < 3, 
    error('Requires three input arguments.'); 
end

[errorcode x a b] = distchck(3,x,a,b);
if errorcode > 0
    error('Requires non-scalar arguments to match in size.');
end

y = zeros(size(x));
k1 = find(a <= 0 );
if any(k1)
   tmp   = NaN;
   y(k1) = tmp(ones(size(k1)));
end

if trunc,
  k = find(x > 0 & a>0);
else 
  k = find(a>0);
end

if any(k),
  tmp=exp(-(x(k) -b(k))./a(k)  );
  y(k) =  tmp.* exp(-tmp)./a(k);
  if trunc,
    y(k)=y(k)./(1-exp(-exp(b(k)./a(k)  )));
  end
end


