function r = wblrnd_max(A,B,C,m,n)
%WBLRNDM

if nargin < 5
    error('stats:wblrndM:TooFewInputs Requires at least two input arguments.');
end

% Generate uniform random values, and apply the Weibull inverse CDF.
r = A - B * (-log(rand(m,n))) .^ (1/C); % == wblinvM(u, A, B, C)
