function [param,paramstr,pci]=disfitparam(DistributionName,data,alfa)

if ~exist('alfa','var'), alfa=0.05; end
if iscell(DistributionName)
    n=length(DistributionName);
    paramstr=repmat(cellstr(''), n,3);%Parameters String
    pci.pci=zeros(1,1);
    param.param=zeros(1,1);
    for i=1:n
        [param(i).param,paramstr(i,:),pci(i).pci]=LogLikehoodDistriName(DistributionName{i},data,alfa);
    end
else
    [param,paramstr,pci]=LogLikehoodDistriName(DistributionName,data,alfa);
end

end

function [param,paramstr,pci]=LogLikehoodDistriName(DistributionName,data,alfa)
paramstr={'' '' ''};
switch DistributionName
    case 'normal'
        [m1, m2, muci, sigmaci]=normfit(data,alfa);
        pci(:,1)=muci;pci(:,2)=sigmaci;
        param(1)=m1;param(2)=m2;
        paramstr{1,1}=['mu:         ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['sigma:      ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 'uniform'
        [m1, m2, muci, sigmaci]=unifit(data,alfa);
        pci(:,1)=muci;pci(:,2)=sigmaci;
        param(1)=m1;param(2)=m2;
        paramstr{1,1}=['a:          ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['b:          ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 'lognormal'
        [param, pci]=lognfit(data,alfa);
        paramstr{1,1}=['mu:         ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['sigma:      ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 'weibull' %(de minimos matlab lamda=0 -> x>0)
        [param, pci]=wblfit(data,alfa);
        paramstr{1,1}=['s:          ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['k:          ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 'weibullmin' %(de MINIMOS) ojo OJO AQUI MANHANA OJO PROGRAMAR TODO
        [param(1),param(2),param(3),pci]=wblfit_min (data);
        paramstr{1,1}=['lamda:      ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['s:          ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
        paramstr{1,3}=['k:          ' num2str(param(3)) ' ' char(177) ' ' num2str((pci(2,3)-pci(1,3))/2)];
    case 'weibullmax' %(de MAXIMOS)
        [param(1),param(2),param(3),pci]=wblfit_max (data);
        paramstr{1,1}=['lamda:      ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['s:          ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
        paramstr{1,3}=['k:          ' num2str(param(3)) ' ' char(177) ' ' num2str((pci(2,3)-pci(1,3))/2)];
    case 'gumbel' % gev with chi=0 uso este salvo el ppplot uso gumbelplot.m
        [param, pci]=gumbfit(data,alfa);
        paramstr{1,1}=['sigma scale:' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['mu location:' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 'extreme value'
        [param,pci]=gevfit(data,alfa);%param=gevfit_(data,'pwm',[],0);
        paramstr{1,1}=['xi shape:   ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['sigma scale:' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
        paramstr{1,3}=['mu location:' num2str(param(3)) ' ' char(177) ' ' num2str((pci(2,3)-pci(1,3))/2)];
    case 'pareto'
        umbral=0;
        [param,pci]=mle((data - umbral), 'dist','generalized pareto', 'alpha',alfa);%gpfit((data - umbral),alfa);  % Fit Generalized Pareto distribution
        param(3)=umbral;
        paramstr{1,1}=['xi shape:   ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['sigma scale:' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
        paramstr{1,3}=['u threshold:' num2str(param(3)) ' ' char(177) ' ' num2str(umbral)];
    case 'exponential'
        [param, pci]=expfit(data,alfa);
        paramstr{1,1}=['lamda:      ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
    case 'rayleigh'
        [param,pci]=raylfit(data,alfa);
        paramstr{1,1}=['b:          ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
    case 'gamma'
        [param, pci]=gamfit(data,alfa);%param=mle(data,'distribution','gamma','alpha',alfa);
        paramstr{1,1}=['k:          ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['theta:      ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 'beta'
        [param, pci]=betafit(data,alfa);%param=mle(data,'distribution','beta','alpha',alfa);
        paramstr{1,1}=['alpha:      ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['beta:       ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 'logistic'
        [param, pci]=mle(data, 'dist','logistic', 'alpha',alfa);  % Fit Logistic distribution
        paramstr{1,1}=['mu:         ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['s:          ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
    case 't-student'
        [param,pci]=mle(data, 'dist','tlocationscale', 'alpha',alfa);
        paramstr{1,1}=['mu:         ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
        paramstr{1,2}=['lambda:     ' num2str(param(2)) ' ' char(177) ' ' num2str((pci(2,2)-pci(1,2))/2)];
        paramstr{1,3}=['nu:         ' num2str(param(3)) ' ' char(177) ' ' num2str((pci(2,3)-pci(1,3))/2)];
    case 'poisson'
        [param, pci]=poissfit(data,alfa);%param=mle(data,'distribution','gamma','alpha',alfa);
        paramstr{1,1}=['lambda:     ' num2str(param(1)) ' ' char(177) ' ' num2str((pci(2,1)-pci(1,1))/2)];
    otherwise
        param=[];
end
end