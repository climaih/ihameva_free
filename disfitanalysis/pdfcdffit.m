function [A,B,C,D]=pdfcdffit(DistributionName,data,param,alfa,tipo,p)
%function [y,x,param,nlogL,pci,AVAR]=pdfcdffit(DistributionName,data,alfa,tipo,p,param)
% DistriName={'normal','lognormal','weibull','gumbel','gev','pareto',...
%             'exponential','rayleigh','gamma','beta','logistic','t-student'};
if ~exist('alfa','var'), alfa=0.05; end

if strcmp(tipo,'ppplot') || strcmp(tipo,'gumbelplot'), 
    x=sort(data);
elseif strcmp(tipo,'qqplot'),
    ppos=plotpos(sort(data));%(i-0.5)/n
else % PDF & CDF
    if strcmp(DistributionName,'poisson')
        x=min(data):max(data);%poisson discrto enteros
    else
        x=linspace(min(data),max(data),200);
    end
end

if exist('x','var') && size(x,1)==1
	x=x';
end
if size(data,1)==1
	data=data';
end
N=length(data);

nlogL=0;
AVAR=[];
if strcmp(tipo,'gumbelplot'), 
    [xsort,psort]=xsortfunction(x);
    n=length(xsort);
    y=zeros(n,3);%low,up
    y(:,1)=xsort;
end

switch DistributionName
    case 'normal'
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=normlike([param(1) param(2)],data);
                y=pdf('Normal',x,param(1),param(2));
            case 'qqplot'
                y=norminv(ppos,param(1),param(2));
            case 'probplot'
                y=probplot(gca,@normcdf,param);
            case 'gumbelplot'
                usort=normcdf(x,param(1),param(2))*(N+1);
                y(1:n,2)=norminv(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));%low
                y(1:n,3)=norminv(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));%up
            case 'ppplot'
                y=cdf('Normal',x,param(1),param(2));
            case 'cdf'
                y=cdf('Normal',x,param(1),param(2));
        end
    case 'uniform'
        switch tipo
            case 'pdf'
                nlogL=-sum(log(unifpdf(data,param(1),param(2))));
                y=pdf('Uniform',x,param(1),param(2));
            case 'qqplot'
                y=unifinv(ppos,param(1),param(2));
            case 'probplot'
                y=probplot(gca,@normcdf,param);
            case 'gumbelplot'
                usort=unifcdf(x,param(1),param(2))*(N+1);
                y(1:n,2)=norminv(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));%low
                y(1:n,3)=norminv(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));%up
            case 'ppplot'
                y=cdf('Uniform',x,param(1),param(2));
            case 'cdf'
                y=cdf('Uniform',x,param(1),param(2));
        end
    case 'lognormal'
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=lognlike(param,data);
                y=lognpdf(x,param(1),param(2));
            case 'qqplot'
                y=logninv(ppos,param(1),param(2));
            case 'probplot'
                y=probplot(gca,@logncdf,param);
            case 'gumbelplot'
                usort=logncdf(x,param(1),param(2))*(N+1);
                y(1:n,2)=norminv(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));%low
                y(1:n,3)=norminv(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));%up
            case 'ppplot'
                y=logncdf(x,param(1),param(2));
            case 'cdf'
                y=logncdf(x,param(1),param(2));
        end
    case 'weibull' %(de minimos)
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=wbllike(param,data);
                y=wblpdf(x,param(1),param(2));
            case 'qqplot'
                y=wblinv(ppos,param(1),param(2));
            case 'probplot'
                y=[];
                %y=probplot(gca,@wblcdf,param);
            case 'gumbelplot'
                x(:,1)=log(x);
                x(:,2)=param(2)*log( sort(data)/param(1) );
                potpos=(1:N)'/(N+1);
                x(:,3)=log(-log(1-potpos));
                usort=wblcdf(sort(data),param(1),param(2))*(N+1);
                y(:,1)=log(y(:,1));
                    y(1:n,2)=log(-log(1-betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                    y(1:n,3)=log(-log(1-betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                p=log(-log(p));
            case 'ppplot'
                y=wblcdf(x,param(1),param(2));
            case 'cdf'
                y=wblcdf(x,param(1),param(2));
        end
    case 'weibullmin' %(de MAXIMOS) ojo OJO AQUI MANHANA OJO PROGRAMAR TODO
        switch tipo
            case 'pdf'
                nlogL=-sum(log(wblpdf_min(data,param(1),param(2),param(3))));
                y=wblpdf_min(x,param(1),param(2),param(3));
            case 'qqplot'
                y=wblinv_min(ppos,param(1),param(2),param(3));
            case 'probplot'
                y=[];
            case 'gumbelplot'
                x(:,1)=log(sort(data)-param(1));
                x(:,2)=param(3)*log((sort(data)-param(1))/param(2) );
                potpos=(1:N)'/(N+1);
                x(:,3)=log(-log(1-potpos));
                usort=wblcdf_min(sort(data),param(1),param(2),param(3))*(N+1);
                y(:,1)=log(y(:,1)-param(1));
                    y(1:n,2)=log(-log(1-betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                    y(1:n,3)=log(-log(1-betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                p=log(-log(1-p));
            case 'ppplot'
                y=wblcdf_min(x,param(1),param(2),param(3));
            case 'cdf'
                y=wblcdf_min(x,param(1),param(2),param(3));
        end
    case 'weibullmax' %(de MAXIMOS) ojo OJO AQUI MANHANA OJO PROGRAMAR TODO
        switch tipo
            case 'pdf'
                nlogL=-sum(log(wblpdf_max(data,param(1),param(2),param(3))));
                y=wblpdf_max(x,param(1),param(2),param(3));
            case 'qqplot'
                y=wblinv_max(ppos,param(1),param(2),param(3));
            case 'probplot'
                y=[];
            case 'gumbelplot'
                x(:,1)=-log(param(1)-sort(data));
                x(:,2)=-param(3)*log((param(1)-sort(data))/param(2));
                potpos=(1:N)'/(N+1);
                x(:,3)=-log(-log(potpos));
                usort=wblcdf_max(sort(data),param(1),param(2),param(3))*(N+1);
                y(:,1)=-log(param(1)-y(:,1));
                    y(1:n,2)=-log(-log(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                    y(1:n,3)=-log(-log(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                p=-log(-log(p));
            case 'ppplot'
                y=wblcdf_max(x,param(1),param(2),param(3));
            case 'cdf'
                y=wblcdf_max(x,param(1),param(2),param(3));
        end
    case 'gumbel' % gev with chi=0 uso este salvo el ppplot uso gumbelplot.m
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=gevlike([0,param(1),param(2)],data);
                y=gevpdf(x,0,param(1),param(2));
            case 'qqplot'
                y=gevinv(ppos,0,param(1),param(2));
            case 'probplot'
                %y=probplot(gca,@gevcdf,[0,param(1),param(2)]);
                y=[];
            case 'gumbelplot'
                x(:,1)=x;
                x(:,2)=(x(:,1)-param(2))/param(1);
                potpos=(1:N)'/(N+1);
                x(:,3)=-log(-log(potpos));
                usort=gevcdf(x(:,1),0,param(1),param(2))*(N+1);%exp(-exp(-x(:,2)))*(N+1);
                    y(1:n,2)=-log(-log(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                    y(1:n,3)=-log(-log(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                p=-log(-log(p));
            case 'ppplot'
                y=gevcdf(x,0,param(1),param(2));
            case 'cdf'
                y=gevcdf(x,0,param(1),param(2));
        end
    case 'extreme value'
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=gevlike(param,data);
                y=gevpdf(x,param(1),param(2),param(3));
            case 'qqplot'
                y=gevinv(ppos,param(1),param(2),param(3));
            case 'probplot'
                %y=probplot(gca,@gevcdf,param);
                y=[];
            case 'gumbelplot'
                x(:,1)=x;
                x(:,2)=(1/param(1))*log(1+param(1)*(x(:,1)-param(3))/param(2));
                potpos=(1:N)'/(N+1);
                x(:,3)=-log(-log(potpos));
                usort=gevcdf(x(:,1),param(1),param(2),param(3))*(N+1);
                    y(1:n,2)=-log(-log(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                    y(1:n,3)=-log(-log(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                p=-log(-log(p));
            case 'ppplot'
                y=gevcdf(x,param(1),param(2),param(3));
            case 'cdf'
                y=gevcdf(x,param(1),param(2),param(3));
        end
    case 'pareto'
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=gplike(param,data);
                y=gppdf(x,param(1),param(2),param(3));
            case 'qqplot'
                y=gpinv(ppos,param(1),param(2),param(3));
            case 'probplot'
                %dist_=dfswitchyard('dfgetdistributions','generalized pareto');
                %y=probplot(gca,dist_.cdffunc,param);
                %y=probplot(gca,@gpcdf,param);
                y=[];
            case 'gumbelplot'
                x(:,1)=x;
                x(:,2)=log( 1+param(1)*x(:,1)/param(2) )/param(1);
                potpos=(1:N)'/(N+1);
                x(:,3)=-log(1-potpos);
                usort=gpcdf(x(:,1),param(1),param(2),param(3))*(N+1);
                    y(1:n,2)=-log(1-betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));
                    y(1:n,3)=-log(1-betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));
                p=-log(-log(p));
            case 'ppplot'
                y=gpcdf(x,param(1),param(2),param(3));
            case 'cdf'
                y=gpcdf(x,param(1),param(2),param(3));
        end
    case 'exponential'
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=explike(param,data);
                y=pdf('exp',x,param);
            case 'qqplot'
                y=expinv(ppos,param);
            case 'probplot'
                y=probplot(gca,@expcdf,param);
            case 'gumbelplot'
                usort=expcdf(x,param)*(N+1);
                    y(1:n,2)=-log(1-betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));
                    y(1:n,3)=-log(1-betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1));
            case 'ppplot'
                y=cdf('exp',x,param);
            case 'cdf'
                y=cdf('exp',x,param);
        end
    case 'rayleigh'
        switch tipo
            case 'pdf'
                nlogL=-sum(log(raylpdf(data,param)));
                y=pdf('ray',x,param);
            case 'qqplot'
                y=raylinv(ppos,param);
            case 'probplot'
                y=probplot(gca,@raylcdf,param);
            case 'gumbelplot'
                usort=cdf('ray',x,param)*(N+1);
                    y(1:n,2)=sqrt(2*-log(1-betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
                    y(1:n,3)=sqrt(2*-log(1-betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1)));
            case 'ppplot'
                y=cdf('ray',x,param);
            case 'cdf'
                y=cdf('ray',x,param);
        end
    case 'gamma'
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=gamlike(param,data);
                y=gampdf(x,param(1),param(2));
            case 'qqplot'
                y=gaminv(ppos,param(1),param(2));
            case 'probplot'
                %y=probplot(gca,@gamcdf,param);
                y=[];
            case 'gumbelplot'
                x(:,1)=x;
                x(:,2)=x;
                potpos=(1:N)'/(N+1);
                x(:,3)=gaminv(potpos,param(1),param(2));
                usort=gamcdf(x(:,1),param(1),param(2))*(N+1);
                    y(1:n,2)=gaminv(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1),param(1),param(2));
                    y(1:n,3)=gaminv(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1),param(1),param(2));
                p=gaminv(p,param(1),param(2));
            case 'ppplot'
                y=gamcdf(x,param(1),param(2));
            case 'cdf'
                y=gamcdf(x,param(1),param(2));
        end
    case 'beta'
        switch tipo
            case 'pdf'
                [nlogL,AVAR]=betalike(param,data);%%nlogL=sum(log(betapdf(data,param(1),param(2))));
                y=betapdf(x,param(1),param(2));
            case 'qqplot'
                y=betainv(ppos,param(1),param(2));
            case 'probplot'
                y=[];
            case 'gumbelplot'
                x(:,1)=x;
                x(:,2)=x;
                potpos=(1:N)'/(N+1);
                x(:,3)=betainv(potpos,param(1),param(2));
                usort=betacdf(x(:,1),param(1),param(2))*(N+1);
                    y(1:n,2)=betainv(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1),param(1),param(2));
                    y(1:n,3)=betainv(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1),param(1),param(2));
                p=betainv(p,param(1),param(2));
            case 'ppplot'
                y=betacdf(x,param(1),param(2));
            case 'cdf'
                y=betacdf(x,param(1),param(2));
        end
    case 'logistic'
        switch tipo
            case 'pdf'
                nlogL=-sum(log(logisticpdf(data,param(1),param(2))));
                y=logisticpdf(x,param(1),param(2));
            case 'qqplot'
                y=logisticinv(ppos,param(1),param(2));
            case 'probplot'
                dist_=dfswitchyard('dfgetdistributions','logistic');
                y=probplot(gca,dist_.cdffunc,param);
            case 'gumbelplot'
                y=[];
            case 'ppplot'
                y=logisticcdf(x,param(1),param(2));
            case 'cdf'
                y=logisticcdf(x,param(1),param(2));
        end
    case 't-student'
        switch tipo
            case 'pdf'
                nlogL=-sum(log(pdf('tlocationscale',data,param(1),param(2),param(3))));
                y=pdf('tlocationscale',x,param(1),param(2),param(3));
            case 'qqplot'
                %y=tlocationscaleinv(x,param);
                y=tinv(ppos,param(3));
            case 'probplot'
                y=[];%OJO Gumbel probplot
            case 'gumbelplot'
                x(:,1)=x;
                x(:,2)=(x(:,1)-param(1))/param(2);
                potpos=(1:N)'/(N+1);
                x(:,3)=tinv(potpos,param(3));
                usort=tcdf((x(:,1)-param(1))/param(2),param(3))*(N+1);
                    y(1:n,2)=tinv(betainv(alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1),param(3));
                    y(1:n,3)=tinv(betainv(1-alfa/2,usort(psort(1:n)),N-usort(psort(1:n))+1),param(3));
                p=tinv(p,param(3));
            case 'ppplot'
                y=cdf('tlocationscale',x,param(1),param(2),param(3));
            case 'cdf'
                y=cdf('tlocationscale',x,param(1),param(2),param(3));
        end
    case 'poisson'
        switch tipo
            case 'pdf'
                nlogL=-sum(log(poisspdf(data,param)));
                [x,y]=unique(data);
                %y=poisspdf(x,param);
            case 'qqplot'
                y=[];
            case 'probplot'
                y=[];
            case 'gumbelplot'
                y=[];
            case 'ppplot'
                y=[];
            case 'cdf'
                y=poisscdf(x,param);
        end
    otherwise
        param=[];
        nlogL=[];
        y=[];
        x=[];
        AVAR=[];
end
nlogL=-nlogL;%Cambio de signo de la verosimilitud

if strcmp(tipo,'qqplot') && ~strcmp(DistributionName,'poisson')
    x=sort(data);
    mx=[min(x); max(x)];
    my=[min(y); max(y)];
    if max(x)>=0.8 && max(y)>=0.8
        mm=[floor(min(mx(1),my(1))) ceil(max(mx(2),my(2)))];
    else
        mm=[min(mx(1),my(1)) max(mx(2),my(2))];
    end
end

switch tipo
    case 'pdf'
        A=y;
        B=x;
        C=nlogL;
        D=AVAR;
    case 'qqplot'
        A=y;
        B=x;
        C=mm;
        D=mm;
    case 'probplot'
        A=y;
    case 'gumbelplot'
        A=y;
        B=x;
        if exist('p','var'), C=p; end;
    case 'ppplot'
        A=y;
    case 'cdf'
        A=y;
        B=x;
end


end

%=====================helper function====================
function [pp,n]=plotpos(sx)
%PLOTPOS Compute plotting positions for a probability plot
%   PP=PLOTPOS(SX) compute the plotting positions for a probability
%   plot of the columns of SX (or for SX itself if it is a vector).
%   SX must be sorted before being passed into PLOTPOS.  The ith
%   value of SX has plotting position (i-0.5)/n, where n is
%   the number of rows of SX.  NaN values are removed before
%   computing the plotting positions.
%
%   [PP,N]=PLOTPOS(SX) also returns N, the largest sample size
%   among the columns of SX.  This N can be used to set axis limits.

[n, m]=size(sx);
if n==1
   sx=sx';
   n=m;
   m=1;
end

nvec=sum(~isnan(sx));
pp=repmat((1:n)', 1, m);
pp=(pp-.5) ./ repmat(nvec, n, 1);
pp(isnan(sx))=NaN;

if (nargout > 1)
   n=max(nvec);  % sample size for setting axis limits
end

end

function [xsort,psort]=xsortfunction(x)
% OJO OJO con la lognormal
    N=length(x);
    if N>5000
        naux=200;
    elseif N>1000
        naux=80;
    elseif N>100
        naux=20;
    elseif N>20
        naux=8;
    else
        error('The size of data is < 20....')
    end
    xsort=x(1:naux);
    psort=(1:naux);
    [xau1,iau1]=unique(x(naux:N-naux));
    nm=max(8,min(length(xau1),naux/2));
    xau2=linspace(xau1(1),xau1(end),nm);
    for i=2:nm-1
        [a,b]=min(abs(xau1-xau2(i)));% OJO OJO dis -> abs
        xsort(naux+i-1)=xau1(b);
        psort(naux+i-1)=naux+iau1(b);
    end
	xsort=[xsort;x(N-naux:N)];
	psort=[psort';(N-naux:N)'];
end