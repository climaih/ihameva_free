

function [logL, info] = gumblike(params,data,trunc)
%GUMBLIKE Gumbel log-likelihood function.
%
% CALL:  [L, info] = gumblike(params,data) 
%
%      L = Gumbel log-likelihood with parameters params given DATA. 
%   info = inverted Fisher's information matrix. INFO is the asymptotic
%          covariances of the parameters.
% params = [a b], the parameters of the distribution
% data   = data vector or matrix
%
%   GUMBLIKE is a utility function for maximum likelihood estimation. 
%
% See also: gumbfit, gumbpdf, gumbcdf, gumbinv, gumbstat

%   References:
%      [1]  Michel K. Ochi,
%       OCEAN TECHNOLOGY series 6
%      "OCEAN WAVES, The stochastic approach", Cambridge
%      1998 p. 167-170.


%  tested on: matlab 5.2
% history
% revised pab 8.11.1999
% updated header info
%   Per A. Brodtkorb 17.10.98

if nargin < 3 | isempty(trunc),
    trunc=0; % default value is not truncated
end

if nargin < 2, 
    error('Requires at least two input arguments'); 
end

[n, m] = size(data);

if nargout == 2 & max(m,n) == 1
    error('To compute the 2nd output, the 2nd input must have at least two elements.');
end

if n == 1
   data = data';
   n = m;
end

a = params(1);
b = params(2);

a = a(ones(n,1),:);

b = params(2);
b = b(ones(n,1),:);

x = gumbpdf(data,a,b,trunc)+eps;

logL = -sum(log(x));

if nargout == 2
  delta = sqrt(eps);
  xplus = gumbpdf([data data],[a+delta a],[b b+delta],trunc);
  J = zeros(n,2);
  J = (log(xplus) - log([x x]))./delta;
  [Q,R]= qr(J,0);
  Rinv = R\eye(2);
  info = Rinv'*Rinv;
end


