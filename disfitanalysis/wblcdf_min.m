function p = wblcdf_min(x,A,B,C)
%WBLCDFM

if nargin<4
    error('stats:wblcdfM:TooFewInputs Input argument X is undefined.');
end

% Force a zero for x menores que A.
x(x <= A) = 0;

try
    z = ((x-A)./B).^C;
catch
    error('stats:wblcdf:InputSizeMismatch Non-scalar arguments must match in size.');
end
p = 1-exp(-z);
