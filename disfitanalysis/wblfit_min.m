function [beta0,alpha0,gamma0,pci,fval,grad,hessian] = wblfit_min (x,pini,alfa)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function OptiParamHessian calculates the parameters of the
%   Time-Dependent GEV distribution using the maximum likelihood method
%
%   Input:
%       x -> data to be fitted
%       pini -> vector including initial values for the parameters in the
%       
%   Output:
%       beta0 -> Optimal constant parameter related to location
%       alpha0 -> Optimal constant parameter related to scale
%       gamma0 -> Optimal constant parameter related to shape
%       fval -> Optimal loglikelihood function with the sign changed
%       grad -> Gradient of the log-likelihood function with the sign
%       changed at the optimal solution
%       hessian -> Hessian of the log-likelihood function with the sign
%       changed at the optimal solution
%
%   Authors: R. Minguez, O. Castellanos,
%   Environmental and Hydraulics Institute "IH Cantabria"
%   University of Cantabria 
%   C/ Isabel Torres n 15
%   Parque Cientifico y Tecnologico de Cantabria
%   39011 Santander, Spain
%   Tfno.: +34 942 20 18 52 
%   Fax: +34 942 26 63 61
%   Corresponding author email: roberto.minguez@unican.es
%
%   Created: 03/2012
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin<2, pini = [];end
if nargin<3, alfa = 0.05;end

%   Initial bounds
p = zeros(3,1);
p(1) = max(x) + 1e-4;   %   Initial beta0 parameter
p(2) = std(x);          %   Initial alpha0 parameter
p(3) = 0.01;            %   Initial gamma0 parameter

%   Bounds on variables. Initially all parameters are unbounded
lb = [-inf 0 0];
up = [min(x) Inf Inf];

%   If an inital value for the parameters vector is provided, it is used
if ~isempty(pini),
    p = pini;
end

%   Set the options for the optimization routine, note that both gradients are provided
options = optimset('GradObj','on','TolFun',1e-10,'Hessian','off','MaxFunEvals',100000,'Algorithm','trust-region-reflective');
%   Call the optimization routine
%   Note that it is a minimization problem, instead a maximization problem,
%   for this reason the log likelihood function sign is changed
[p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x),p,[],[],[],[],lb,up,[],options);
if exitflag<1,
    %   Initial beta0 parameter
    p(1) = min(x) - 1e-4;
    %   Initial alpha0 parameter
    p(2) = std(x);
    %   Initial gamma0 parameter
    p(3) = 0.01;
    %   If an inital value for the parameters vector is provided, it is used
    if ~isempty(pini),
        p = pini;
    end
    p = fminsearch(@(p) loglikelihood (p,x),p);
    [p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x),p,[],[],[],[],lb,up,[],options);
end
%   Once the optimal solution is obtained redistribute the solution
%   storaged in p in the following order: beta0, beta, betaT, varphi,
%   alpha0, alpha, betaT2, varphi2, gamma0, gamma
beta0 = p(1);
alpha0 = p(2);
gamma0 = p(3);

if nargout>3,
    n=length(hessian);
    [LX,UX,PX] = lu(-hessian);
    %   Inverse, ie variance-covariance matrix
    varianzaP = -(UX)\(LX\(PX*eye(n)));
    quan=norminv(1-alfa/2);
    for i=1:3
        XLO(i)=p(i) - sqrt(varianzaP(i,i)) * quan;
        XUP(i)=p(i) + sqrt(varianzaP(i,i)) * quan;
    end
    pci=[XLO;XUP];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Loglikelihood function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [f,Jx] = loglikelihood (p,x)
    %   This function calculates the loglikelihood function with the
    %   sign changed for the values of the paremeters given by p
    mut = p(1)*ones(size(x));
    psit = p(2)*ones(size(x));
    epst = p(3)*ones(size(x));
    %   Evaluate auxiliary variables
    xn = (x-mut)./psit; 
    %   Evaluate the loglikelihood function with the sign changed, not
    %   that the general and Gumbel expresions are used
    f = -sum(log(epst)-log(psit)-xn.^epst+(epst-1).*log(xn));

    if nargout>1,
        %   Derivatives given by equations (A.1)-(A.3) in the paper
        Dmut = epst.*xn.^(epst-1)./psit-(epst-1)./xn./psit;
        Dpsit = -1./psit+epst.*xn.^(epst-1).*xn./psit-(epst-1)./psit;
        Depst = 1./epst-xn.^epst.*log(xn)+log(xn);       
        %   Set the Jacobian to zero
        Jx = zeros(3,1);
        %   Jacobian elements related to the location parameters beta0
        Jx(1,1) = sum(Dmut);
        %   Jacobian elements related to the scale parameters alpha0
        Jx(2,1) = sum(Dpsit);
        %   Jacobian elements related to the shape parameters gamma0
        Jx(3,1) = sum(Depst);

        %   Since the numerical problem is a minimization problem the
        %   Jacobian sign must be changed
        Jx = -Jx;
    end  
end

end
