function p = gumbcdf(x,a,b,trunc)
%GUMBCDF Gumbel cumulative  density function (cdf).
%
% CALL:  y = gumbpdf(x,a,b,trunc) 
%   
%   y    = Gumbel cdf evaluated at x
%  a, b  = parameters of the Gumbel distribution.
%  trunc = 0  regular gumbel distribution (default)
%          1  truncated gumbel distribution 
%
%  Gumbel CDF  is given by :                           
%       F(x) = exp(-exp(-(x-B)/A))    -inf < x < inf, a>0
%  or the truncated 
%       F(x) = [exp(-exp(-(x-B)/A))-exp(-exp(B/A))]/(1-exp(-exp(B/A))) 
%           0 < x < inf,  a>0
%
%   The size of P is the common size of the input arguments. A scalar input
%   functions as a constant matrix of the same size as the other inputs.    
%
% See also: gumbfit, gumbrnd, gumbpdf, gumbinv, gumbstat

%   References:
%      [1]  Michel K. Ochi,
%       OCEAN TECHNOLOGY series 6
%      "OCEAN WAVES, The stochastic approach", Cambridge
%      1998 p. 167-170.

%  tested on: matlab 5.2
% history
% revised pab 8.11.1999
% updated header info
%   Per A. Brodtkorb 17.10.98

if nargin < 4 | isempty(trunc),
    trunc=0; % default value is not truncated
end


if nargin < 3, 
    error('Requires three input arguments.'); 
end

[errorcode x a b] = distchck(3,x,a,b);
if errorcode > 0
    error('Requires non-scalar arguments to match in size.');
end

p = zeros(size(x));
k1 = find(a <= 0 );
if any(k1)
  disp('A must be larger than zero')
   tmp   = NaN;
   p(k1) = tmp(ones(size(k1)));
end

if trunc,
  k = find(x > 0 & a>0);
else 
  k = find(a>0);
end

if any(k),
  p(k)=exp(-exp(-(x(k) -b(k))./a(k)  ) );
  if trunc,
    tmp=exp(-exp(b(k)./a(k)  ));
    p(k)=(p(k)-tmp)./(1-tmp);
  end
end


