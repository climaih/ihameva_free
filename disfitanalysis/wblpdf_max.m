function p = wblpdf_max(x,A,B,C)
%WBLCDFM

if nargin<4
    error('stats:wblcdfM:TooFewInputs Input argument X is undefined.');
end

% Force a zero for x menores que A.
x(x >= A) = 0;

try
    z = ((A-x)./B).^C;
    zn = ((A-x)./B).^(C-1);
catch
    error('stats:wblcdf:InputSizeMismatch Non-scalar arguments must match in size.');
end
p = (C/B)*exp(-z).*zn;
