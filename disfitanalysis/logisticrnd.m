function x=logisticrnd(mu,sigma,m,n)
% Logistic(mu,sigma) generator using inversion

if nargin < 2
    error('logistic: Requires at least one input argument.');
end
if ~exist('m','var'), m=1; end
if ~exist('n','var'), n=1; end

% Generate uniform random values, and apply the logistic
U=rand([m,n]);
x=mu+sigma.*log(U./(1-U));
