function x = wblinv_min(p,a,b,c)
%WBLINVM
if nargin < 4,    
    error('stats:weibinv:TooFewInputs Requires three input arguments.'); 
end

%   Initialize X to zero.
x = zeros(size(p));

k = find(b > 0 & c > 0 & p > 0 & p < 1);
if any(k)
    x(k) =(  a + b * (-log(1 - p(k))) .^ (1/c) );
end

x=sort(x);% OJO por que sort preguntar a Robeto??

x(p == 1 ) = Inf;