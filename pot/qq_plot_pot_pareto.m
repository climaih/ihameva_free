function qq_plot_pot_pareto(xamax,umbral,ConfidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,tipo,cityname,var_name,gcax)

if ConfidenceLevelAlpha>0.5, error(['ConfidenceLevelAlpha=',num2str(ConfidenceLevelAlpha)]); end
if ~exist('cityname','var'), cityname=''; end
if ~exist('var_name','var'), var_name=''; end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! qq_plot_pot_pareto with axes'); end
set(gcf,'CurrentAxes',gcax)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   QQ plot POT Pareto
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Standarized empirical variable
Ze= sort(xamax);
%   Standarized adjustment variable
if strcmpi(tipo,'POT') %xamax
    [Zm,Dzm] = QuantilePOTsimple (((1:length(xamax))'/(length(xamax)+1)),umbral,lambda0,sigma0,gamma0);
    stdDwei = sqrt(sum((Dzm'*invI0).*Dzm',2));
    titulo=['Best model QQ plot (' cityname ')'];
    legdata='Data';
elseif strcmpi(tipo,'Pareto') %Hsmax
    [Zm,Dzm]= QuantileParetoSimple (((1:length(xamax))'/(length(xamax)+1)),umbral,sigma0,gamma0);
    auxDzm = [Dzm(:,:,1)];
    if ~isempty(gamma0),
        auxDzm = [auxDzm Dzm(:,:,2)];
    end
    stdDwei = sqrt(sum((auxDzm*invI0(2:end,2:end)).*auxDzm,2));
    titulo=['Best model QQ plot, Pareto (SURGE EXCEEDANCES)'];
    legdata=['Data ($',var_name,'^{\rm max}$)'];
else
    error('Please select tipo POT or Pareto!');
end
%   Sorted standarized adjustment variable
[Zmsort ordenZm]= sort(Zm);
%   Quantile-quantile plot
plot(gca,linspace(min(Zmsort),max(Zmsort)),linspace(min(Zmsort),max(Zmsort)),'k');hold(gca,'on')
plot(gca,Ze,Zmsort,'o','MarkerEdgeColor','k','MarkerFaceColor',[0.0 0.0 0.0],'MarkerSize',3)
plot(gca,Ze,Zmsort+norminv(1-ConfidenceLevelAlpha/2,0,1)*stdDwei(ordenZm),'--k')
plot(gca,Ze,Zmsort-norminv(1-ConfidenceLevelAlpha/2,0,1)*stdDwei(ordenZm),'--k')

hh=legend({'Bisector',legdata,'Lower','Upper'},'Location','SouthEast');
set(hh,'Interpreter','latex');
grid(gca,'on')
title(gca,titulo)
xlabel(gca,'Empirical');
ylabel(gca,'Fitted');
hold(gca,'off');
axis(gca,'square');
axis([min(min(Ze),min(Zmsort)) max(max(Ze),max(Zmsort)) min(min(Ze),min(Zmsort)) max(max(Ze),max(Zmsort))])
end
