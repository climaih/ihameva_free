function LocationShapePOT(quan95,stdDq,damax,xamax,psit,quanval,cityname,umbral)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Location and scale parameter plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
auxup = quan95+norminv(1-quanval/2,0,1)*stdDq;
auxlo = quan95-norminv(1-quanval/2,0,1)*stdDq;
fill([0; 1; 1;0], [auxlo; auxlo; auxup; auxup],[.7 .7 .7]);hold on
plot(linspace(0,1)',quan95*ones(1,100),'k--','LineWidth',2)

hold on
plot(mod(damax,1),xamax,'+r')
hold on
[AX,H1,H2] = plotyy(linspace(0,1)',umbral*ones(1,100),linspace(0,1)',psit*ones(1,100),'plot');
set(get(AX(1),'Ylabel'),'String','x_t (m)')
set(get(AX(2),'Ylabel'),'String','\sigma_t (m)')

xlabel('Time (yearly scale)')
title(['Threshold and scale parameters (' cityname ', POT) '])
grid on
hold on
set(H1,'LineWidth',2)
set(H2,'LineWidth',2)
legend('Conf-b.',['x_{' num2str((1-quanval),2) '}'],'H_s^{max}','umbral','\sigma_t','Location','Best')
