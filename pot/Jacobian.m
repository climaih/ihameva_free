function [Jx] = Jacobian (x,t,up0,up,Ts,dias,indicesT,indices,indices2,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2)
%
%   JACOBIAN calcula el Jacobiano de la funcion log-likelihood con
%   respecto a los coeficientes de optimizacion beta, alpha y gamma

%   Input:
%        x-> Valores de altura de ola significante en los que evaluar la
%        derivada
%        t-> Periodo de tiempo en el que seproduce el dato
%        up0-> Parametro constante de la onda para la seleccion de
%        umbrales
%        up-> Armonicos para la onda de seleccion de umbrales
%        Ts-> Intervalo de tiempo base considerado, generalmente un ano (365.25)
%        dias-> Dias del periodo de tiempo considerado
%        indicesT-> Valores de las covariables para cada dia considerado en
%        el parametro de localizacion
%        indices-> Datos de los indices
%        indices2-> Datos de los indices asociados a las varianzas
%        beta0,beta-> Parametros de localizacion
%        alpha0,alpha-> Parametros de escala
%        gamma0,gamma-> Parametros de forma
%        betaT-> Parametros de tendencia en la localizacion
%        varphi-> Parametros asociados a los indices o covariables
%        betaT2-> Parametros de tendencia en la escala
%        varphi2-> Parametros asociados a los indices o covariables
%
%   Output:
%        Jx-> Jacobiano
%
%   Parametros del modelo GEV
lambt = exp(parametro(t,beta0,beta,betaT,indices,varphi));
%   Escala
sigt = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2));
%   Forma
epst = parametro(t,gamma0,gamma);
%   Umbral
u = parametro(t,up0,up);
%   Parametros del modelo Pareto-Poisson
%   Calculo los puntos en los que la distribucion es de GUMBEL
posG = find(abs(epst)<=1e-8);

%   Calculo los puntos en los que la distribucion es de WEIBULL o FRECHET
pos  = find(abs(epst)>1e-8);

%   Modifico los valores problematicos de epst y los igualo a 1
epst(posG)=1;

%   Calculo las derivadas primeras de la funcion log-likelihood con
%   respecto a los parametros de localizacion, escala y forma,
%   respectivamente
Yt = x-u;
xn = Yt./sigt; 
z = 1 + epst.*xn; 
z = max(1e-4,z);
zn = z.^(-1./epst);

%   Derivadas primeras de la funcion loglikelihood con respecto a los
%   parametros de la distribucion Pareto -Poisson
Dlambt = 1./lambt;
Dsigt = -(1-xn)./(sigt.*z);
Depst = (-1-epst+(1+epst)./z+log(z))./(epst.^2);

%   GUMBEL DERIVATIVES
% Dlambt(posG) = 1./lambt(posG);
Dsigt(posG) = -(1-xn(posG))./(sigt(posG));
Depst(posG)=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Esto para todos los dias del periodo de estudio
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Parametros de Poisson para el resto de los dias
% dias = (1:Numdias)'/Ts;
lambtT = exp(parametro(dias,beta0,beta,betaT,indicesT,varphi));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DlambtT = -1/Ts*ones(size(dias));

neps0 = 1;
if length(posG)==length(epst),
    neps0 = 0;
end

%   Dimensiones del Jacobiano
nla = length(beta);
nsig = length(alpha);
neps = length(gamma);
ntend = length(betaT);
nind = length(varphi);
ntend2 = length(betaT2);
nind2 = length(varphi2);

Jx = sparse(zeros(2+neps0+nla+nsig+neps+ntend+nind+ntend2+nind2,1));

%   Elementos del Jacobiano asociados al parametro constante
%   beta0,alpha0,gamma0

Jx(1,1) = sum(Dlambt.*lambt)+sum(DlambtT.*lambtT);

%   Elementos asociados a la parte inferior de cada bloque
if nla>0,
    for i = 1:nla,
        %   Primer elemento asociado al valor constante (primera columna)
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dlambt(k)*Dparam (t(k),i)*lambt(k);
        end
        for k = 1:length(dias),
            aux = aux + DlambtT(k)*Dparam (dias(k),i)*lambtT(k);
        end
        Jx(1+i,1) = aux;
    end
end

if ntend>0,
    Jx(2+nla,1) =  sum(Dlambt.*t.*lambt)+sum(DlambtT.*dias.*lambtT);
end

if nind>0,
    for i = 1:nind,
        Jx(1+nla+ntend+i,1) =  sum(Dlambt.*indices(:,i).*lambt) + sum(DlambtT.*indicesT(:,i).*lambtT);
    end
end

Jx(2+ntend+nind+nla,1) = sum(Dsigt.*sigt);

if nsig>0,
    for i = 1:nsig,
        %   Primer elemento asociado al valor constante (primera columna)
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dsigt(k)*Dparam (t(k),i)*sigt(k);
        end
        Jx(2+nla+ntend+nind+i,1) = aux;
    end
end

if ntend2>0,
    Jx(3+nla+ntend+nind+nsig,1) =  sum(Dsigt.*t.*sigt);
end

if nind2>0,
    for i = 1:nind2,
        Jx(2+nla+ntend+nind+nsig+ntend2+i,1) =  sum(Dsigt.*indices2(:,i).*sigt);
    end
end


if neps0 == 1,
    Jx(2+neps0+ntend+nind+nla+nsig+ntend2+nind2,1) = sum(Depst);
end

if neps>0,
    for i = 1:neps,
        %   Primer elemento asociado al valor constante (primera columna)
        aux = 0;
        for k = 1:length(t),
            aux = aux + Depst(k)*Dparam (t(k),i);
        end
        Jx(neps0+2+nla+nsig+ntend+nind+ntend2+nind2+i,1) = aux;
    end
end

    function dp = Dparam (t,j)
    %
    %   Derivada con respecto a los parametros asociados a senos y cosenos
    %
    %   Input:
    %        t-> Abscisas o tiempo en la que calcular el valor de la 
    %            derivada (Entre 0 y 1)
    %        j-> Numero de armonico dentro del vector
    %
    %   Output:
    %        dp-> Valor de la derivada
    %
    
    if mod(j,2) == 0,
        dp = sin(j/2*2*pi*t);
    else
        dp = cos((j+1)/2*2*pi*t);
    end

    end

end



