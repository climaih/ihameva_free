function [lambda0,sigma0,gamma0,fval,grad,hessian,p] = ...
    OptiParamHessianPOT (x,u,Ts,pini,neps0)


if nargin<4, 
    pini = [];
    if nargin<5, neps0 = 1;end
end
if ~isempty(pini),
    if length(pini)<3,
        neps0 = 0;
    else
        neps0 = 1;
    end
end

%
%   OPTIPARAM es una funcion que calcula los parametros optimos obtenidos
%   de maximizar la funcion logaritmica de verosimilitud
%

%   Ajuste inicial de la distribucion de extremos estacionaria
if isempty(pini),
    p = zeros(2+neps0,1);
    p(1) = (length(x)/Ts);
    p(2) = log(std(x));
    if neps0,
        p(2+neps0) = 0.01;
    end
else
    p = pini;
end


%   Bounds on variables
lb = -Inf*ones(2+neps0,1);
up = Inf*ones(2+neps0,1);

%   Initial bounds for the parameters related to the shape, gamma0 and gamma 
if neps0,
    if isempty(pini),
        lb(2+neps0) = -0.2;
    else
        lb(2+neps0) = p(2+neps0)-0.2;
    end
end
if neps0,
    if isempty(pini),
        up(2+neps0) = 0.2;
    else
        up(2+neps0) = p(2+neps0)+0.2;
    end
end
%   If an inital value for the parameters vector is provided, it is used
if ~isempty(pini),
     p = min(pini,up);
     p = max(pini,lb);
end

% options =
% optimset('GradObj','on','Hessian','user-supplied','Display','iter','TolFun',1e-12);
% options = optimset('GradObj','on','Hessian','user-supplied','TolFun',1e-12,'Display','iter');
options = optimset('GradObj','on','Hessian','on','TolFun',1e-12,'Algorithm','trust-region-reflective');
[p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x,u,Ts),p,[],[],[],[],lb,up,[],options);


auxlo = find(abs(lambda.lower)>1e-8);
lb(auxlo)=lb(auxlo)-0.05;
auxup = find(abs(lambda.upper)>1e-8);
up(auxup)=up(auxup)+0.05;
it = 1;
while (~isempty(auxlo) | ~isempty(auxup)) & it<=10,
    it = it+1;
    [p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x,u,Ts),p,[],[],[],[],lb,up,[],options);
    auxlo = find(abs(lambda.lower)>1e-8);
    lb(auxlo)=lb(auxlo)-0.05;
    auxup = find(abs(lambda.upper)>1e-8);
    up(auxup)=up(auxup)+0.05;
end

lambda0 = p(1);
sigma0 = p(2);
if neps0,
    gamma0 = p(2+neps0);
else
    gamma0 = [];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function [f Jx Hxx] = loglikelihood (p,x,u,Ts)
        
        N = length(x);
        %   Localizacion
        lambt = p(1);
        %   Escala
        sigt = exp(p(2))*ones(size(x));
        %   Forma
        if neps0,
                epst = p(3)*ones(size(x));
        else
                epst = zeros(size(x));
        end
        
        %   Umbral
%         u
        
        %   Calculo los puntos en los que la distribucion es de GUMBEL
        posG = find(abs(epst)<=1e-8);

        %   Calculo los puntos en los que la distribucion es de WEIBULL o FRECHET
        pos  = find(abs(epst)>1e-8);
        %   Modifico los valores problematicos de epst y los igualo a 1
        epst(posG)=1;

        %   Calculo las derivadas primeras de la funcion log-likelihood con
        %   respecto a los parametros de localizacion, escala y forma,
        %   respectivamente
        Yt = x-u;
        xn = Yt./sigt; 
        z = 1 + epst.*xn; 
        z = max(1e-8,z);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %   Esto para todos los dias del periodo de estudio
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        f = N*log(lambt)-Ts*lambt+sum(-log(sigt(pos))-(1+1./epst(pos)).*log(z(pos)))...
              +sum(-log(sigt(posG))-xn(posG));
          
        f = -f;
        
        if nargout>1,
            %   Derivadas primeras de la funcion loglikelihood con respecto a los
            %   parametros de la distribucion Pareto -Poisson
            Dlambt = N/lambt-Ts;
            Dsigt = -(1-xn)./(sigt.*z);
            Depst = (-1-epst+(1+epst)./z+log(z))./(epst.^2);

            %   GUMBEL DERIVATIVES
            % Dlambt(posG) = 1./lambt(posG);
            Dsigt(posG) = -(1-xn(posG))./(sigt(posG));
            Depst(posG)=0;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Esto para todos los dias del periodo de estudio
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   Parametros de Poisson para el resto de los dias
            % dias = (1:Numdias)'/Ts;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


            %   Dimensiones del Jacobiano
            Jx = sparse(zeros(2+neps0,1));

            %   Elementos del Jacobiano asociados al parametro constante
            %   beta0,alpha0,gamma0

            Jx(1,1) = sum(Dlambt);

            Jx(2,1) = sum(Dsigt.*sigt);
            if neps0 == 1,
                Jx(2+neps0,1) = sum(Depst);
            end
            Jx = -Jx;
        end     
        %   HESSIAN MATRIX
        if nargout>2,
            %   Calculo las derivadas segundas de la funcion log-likelihood con
            %   respecto a los parametros de Pareto-Posson, escala y forma,
            %   respectivamente
            D2lambt = -1./(lambt.^2);
            D2sigt = (-1+(1+epst)./(z.^2))./(sigt.*sigt.*epst);
            D2epst = xn.*(2+epst.*(3+epst).*xn)./((epst.*z).^2)-2*log(z)./(epst.^3);
            Dlambtsigt = zeros(size(D2sigt));
            Dlambtepst = zeros(size(D2sigt));
            Dsigtepst = xn.*(1-xn)./(sigt.*z.^2);

            %   GUMBEL DERIVATIVES
            % D2lambt(posG) = -1./(lambt(posG).^2);
            D2sigt(posG) = (1-2*xn(posG))./(sigt(posG).*sigt(posG));
            D2epst(posG) = 0;
            Dlambtsigt(posG) = 0;
            Dlambtepst(posG) = 0;
            Dsigtepst(posG) = 0;

            %   Dimensiones del Hessiano
            Hxx = sparse(zeros(2+neps0));

            %   Elementos del Hessiano asociados al parametro constante
            %   beta0,alpha0,gamma0
            %   Elemento 1 (CAMBIO EXPONENCIAL)
            Hxx(1,1) = sum((D2lambt.*lambt).*lambt);
            %   Elemento 2 (CAMBIO EXPONENCIAL)
            
            %   Elemento 6 (CAMBIO EXPONENCIAL)
            Hxx(2,2) = sum((D2sigt.*sigt+Dsigt).*sigt);
            %   Elemento 7
            if neps0==1,
                Hxx(2+neps0,2+neps0) = sum(D2epst);
            end
            %   Elementos del Hessiano asociados al parametro constante
            %   beta0,alpha0,gamma0 cruzados
            %   Elemento 8 (CAMBIO EXPONENCIAL)
            Hxx(2,1) = sum(Dlambtsigt.*sigt*lambt);
            %   Elemento 9 (CAMBIO EXPONENCIAL)
            if neps0 == 1,
                Hxx(2+neps0,1) = sum(Dlambtepst.*lambt);
            %   Elemento 10 (CAMBIO EXPONENCIAL)
                Hxx(2+neps0,2) = sum(Dsigtepst.*sigt);
            end
                    

            Hxx = Hxx + tril(Hxx,-1)';
            
            Hxx = -Hxx;  
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
