function y = parametro(x,alpha0,alpha,betaT,indices,varphi,indicesint,times)
[m,n] = size(x);
if nargin<4, betaT = [];end
if nargin<5, indices = [];end
if nargin<6, varphi = [];end
if nargin<7, indicesint = [];end
if nargin<8, times = [];end

tend = length(betaT);
[na nind] = size(indices);
% if na ~= m && nind>0,
%     error('Size of data wrong');
% end
np = length(alpha);
if mod(np,2)~=0,
    error('Parameter number must be even');
end
if ~isempty(alpha0),
    y = alpha0*ones(m,n);
else
    y = zeros(m,n);
end
for i = 1:np/2,
    y = y + alpha(2*i-1)*cos(i*2*pi*x);
end
for i = 1:np/2,
    y = y + alpha(2*i)*sin(i*2*pi*x);
end
if tend>0,
    y = y + betaT*x;
end
if nind>0,
    if ~isempty(indicesint),
        if isempty(times),
            for i = 1:nind,
                y = y + varphi(i)*indicesint(i);
            end
        else
            for i = 1:nind,
                %   Usando splines
%                 indicesintaux = spline(times,indices(:,i),x);
                %   Buscando el valor mas proximo
                indicesintaux = buscar(times,indices(:,i),x);
                y = y + varphi(i)*indicesintaux;
            end
        end
    else
        for i = 1:nind,
            y = y + varphi(i)*indices(:,i);
        end
    end
    
end

    function yin = buscar(tiempos,valores,xs)
        n = length(tiempos);
        yin = zeros(size(xs));
        pos = 1;
        for j = 1:max(size(xs)),
            encontrado = 0;
            while ~encontrado && pos<=n,
                if xs(j)<tiempos(pos),
                    yin(j)=valores(pos);
                    encontrado = 1;
                else
                    pos = pos+1;
                end
            end
        end

    end



end