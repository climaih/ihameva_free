function [obj] = auxderiv (mut,psit,epst,x,t,u,Ts,indices,indices2,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2)



%   Parametros del modelo GEV
% mut = parametro (t,beta0,beta,betaT,indices,varphi);
%   Escala
% psit = exp(parametro (t,alpha0,alpha,betaT2,indices2,varphi2));
% psit = exp(psit);
%   Forma
% epst = parametro (t,gamma0,gamma);

%   Parametros del modelo Pareto-Poisson
auxn = (u-mut)./psit;
auxz = 1+epst.*auxn;
sigt = psit.*auxz;

%   Calculo los puntos en los que la distribucion es de GUMBEL
posG = find(abs(epst)<=1e-8);

%   Calculo los puntos en los que la distribucion es de WEIBULL o FRECHET
pos  = find(abs(epst)>1e-8);

%   Calculo las derivadas primeras de la funcion log-likelihood con
%   respecto a los parametros de localizacion, escala y forma,
%   respectivamente
Yt = x-u;
xn = Yt./sigt; 
z = 1 + epst.*xn; 
z = max(1e-4,z);

lambt=auxz.^(-1./epst);
lambt(posG) =exp(-auxn(posG));

obj = sum(log(lambt(pos))-lambt(pos)/Ts-log(sigt(pos))-(1+1./epst(pos)).*log(z(pos)))...
      +sum(log(lambt(posG))-lambt(posG)/Ts-log(sigt(posG))-xn(posG));