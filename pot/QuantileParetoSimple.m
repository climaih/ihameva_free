function [Q,Dq] = QuantileParetoSimple (F,u,sigma0,gamma0)

if nargin<3, error('Not enough parameters for the model'); end
if nargin<4, gamma0 = []; end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Check the number of input arguments
    if isempty(gamma0),
        neps0 = 0;
    else
        neps0 = 1;
    end
    
    [m,n] = size(F);
    
    
    %   Evaluate the scale parameter at each time t as a function
    %   of the actual values of the parameters given by p
    sigt = exp(sigma0)*ones(m,n);
    %   Evaluate the shape parameter at each time t as a function
    %   of the actual values of the parameters given by p
    if neps0
        epst = gamma0*ones(m,n);
    else
        epst = gamma0*zeros(m,n);
    end

    %   The values whose shape parameter is almost cero corresponds to
    %   the GUMBEL distribution, locate their positions if they exist
    posG = find(abs(epst)<=1e-8);
    %   The remaining values correspond to WEIBULL or FRECHET
    pos  = find(abs(epst)>1e-8);
    %   The corresponding GUMBEl values are set to 1 to avoid 
    %   numerical problems, note that for those cases the GUMBEL
    %   expressions are used
    epst(posG)=1;

    %   Evaluate the quantile, not
    %   that the general and Gumbel expresions are used
    Q = zeros(size(F));
    Q(pos) = u - (1-(1-F(pos)).^(-epst(pos))).*sigt(pos)./epst(pos);

    Q(posG) = u - sigt(posG).*log(1-F(posG));
    
    if nargout==2,
        Dqsigt = zeros(m,n);
        Dqepst = zeros(m,n);
        Dqsigt(pos) = -(1-(1-F(pos)).^(-epst(pos)))./epst(pos);
        Dqepst(pos) = sigt(pos).*(1-(1-F(pos)).^(-epst(pos)).*(1+epst(pos).*log(1-F(pos))))./(epst(pos).*epst(pos));

        %   Gumbel derivatives
        Dqsigt(posG) = -log(1-F(posG));
        Dqepst(posG) =zeros(size(sigt(posG)));
        
        Dq = zeros(m,n,1+neps0);
        %   Jacobian elements related to the scale parameters alpha0
        %   and alpha
    	Dq(:,:,1) = Dqsigt.*sigt;
        
        %   Jacobian elements related to the shape parameters gamma0
        %   and gamma
        if neps0 == 1,
            Dq(:,:,1+neps0) = Dqepst;
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    

    
end



