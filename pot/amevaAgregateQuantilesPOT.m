function [Ts,quanaggr,stdlo,stdup]=amevaAgregateQuantilesPOT(xamax,umbral,ConfidenceLevelAlpha,lambda0,sigma0,gamma0,invI0,ny,Ts,cityname,var_name,var_unit,gcax)
if ConfidenceLevelAlpha>0.5, error(['ConfidenceLevelAlpha=',num2str(ConfidenceLevelAlpha)]); end
if ~exist('cityname','var'), cityname=''; end
if ~exist('var_unit','var'), var_unit=''; end
if ~exist('var_name','var'), var_name=''; end
%Aggregate quantiles for different return periods Ts = [1.05 1.5 2 5 10 20 25 50 75 100 200 300 400 500];
if ~exist('Ts','var') || isempty(Ts), Ts=[1.05 2 5 10 20 25 50 75 100]; xlimtick=[2 10 50 100]; end 
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! amevaAgregateQuantilesPOT with axes'); end
set(gcf,'CurrentAxes',gcax)

if max(Ts)>100
    xlimtick=[2 10 50 100 Ts(end)];
end
nts = length(Ts);
quanaggr = zeros(nts,1);
stdup = zeros(nts,1);
stdlo = zeros(nts,1);
%   Sarting calculations
for j = 1:nts,
    %    Annual
    [quan,Dq] = QuantilePOTsimple (1-1/Ts(j),umbral,lambda0,sigma0,gamma0);
    stdDq = sqrt(sum((Dq'*invI0).*Dq',2));
    quanaggr(j)=quan;
    %   Intervalos de confianza del cuantil anual
    stdup(j)= quan+stdDq*norminv(1-ConfidenceLevelAlpha/2,0,1);
    stdlo(j)= quan-stdDq*norminv(1-ConfidenceLevelAlpha/2,0,1);
end
%   Datos anuales
Hsmaxor = sort(xamax);
%Proxor = (((1:ceil(ny)))/(ceil(ny)+1))';
Proxor = (((1:length(Hsmaxor)))/(length(Hsmaxor)+1))';
Tapprox = 1./(1-Proxor);
id = find(Tapprox>Ts(1));

if ~isempty(var_name) || ~isempty(var_unit)
    semilogx(gca,Tapprox(id),Hsmaxor(id),'.','Color',[0 0 0]);hold on;
    semilogx(gca,Ts,quanaggr,'r')
    semilogx(gca,Ts,stdlo,'--r')
    semilogx(gca,Ts,stdup,'--r'); hold off;
    set(gca,'XTick',xlimtick,'XTickLabel',num2cell(xlimtick),'Xlim',[xlimtick(1) xlimtick(end)])
    grid(gca,'on')
    title(['Aggregate Quantiles (' cityname ')']);
    xlabel(gca,'Return Period (years)');
    ylabel(gca,labelsameva(var_name,var_unit));
    legend('Anual data','Anual',[num2str((1-ConfidenceLevelAlpha),2),' Bounds'],'Location','SouthEast')
    hold(gca,'off')
end

end
