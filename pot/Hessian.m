function [Hxx] = Hessian (x,t,up0,up,Ts,dias,indicesT,indices,indices2,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2)

%
%   HESSIAN calcula el Hessiano de la funcion log-likelihood con
%   respecto a los coeficientes de optimizacion beta, alpha y gamma

%   Input:
%        x-> Valores de altura de ola significante en los que evaluar la
%        derivada
%        t-> Periodo de tiempo en el que seproduce el dato
%        up0-> Parametro constante de la onda para la seleccion de
%        umbrales
%        up-> Armonicos para la onda de seleccion de umbrales
%        Ts-> Intervalo de tiempo base considerado, generalmente un ano (365.25)
%        dias-> Dias del periodo de tiempo considerado
%        indicesT-> Valores de las covariables para cada dia considerado en
%        el parametro de localizacion
%        indices-> Datos de los indices
%        indices2-> Datos de los indices 2
%        beta0,beta-> Parametros de localizacion
%        alpha0,alpha-> Parametros de escala
%        gamma0,gamma-> Parametros de forma
%        betaT-> Parametros de tendencia en la localizacion
%        varphi-> Parametros asociados a los indices o covariables
%        betaT2-> Parametros de tendencia en la escala
%        varphi2-> Parametros asociados a los indices o covariables
%
%   Output:
%        Hxx-> Hessiano
%
lambt = exp(parametro(t,beta0,beta,betaT,indices,varphi));
%   Escala
sigt = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2));
%   Forma
epst = parametro(t,gamma0,gamma);
%   Umbral
u = parametro(t,up0,up);

%   Calculo los puntos en los que la distribucion es de GUMBEL
posG = find(abs(epst)<=1e-8);

%   Calculo los puntos en los que la distribucion es de WEIBULL o FRECHET
pos  = find(abs(epst)>1e-8);

%   Modifico los valores problematicos de epst y los igualo a 1
epst(posG)=1;

%   Calculo las derivadas primeras de la funcion log-likelihood con
%   respecto a los parametros de localizacion, escala y forma,
%   respectivamente
Yt = x-u;
xn = Yt./sigt; 
z = 1 + epst.*xn; 
z = max(1e-4,z);
zn = z.^(-1./epst);

%   Derivadas primeras de la funcion loglikelihood con respecto a los
%   parametros de la distribucion Pareto -Poisson
Dlambt = 1./lambt;
Dsigt = -(1-xn)./(sigt.*z);
Depst = (-1-epst+(1+epst)./z+log(z))./(epst.^2);

%   GUMBEL DERIVATIVES
% Dlambt(posG) = 1./lambt(posG);
Dsigt(posG) = -(1-xn(posG))./(sigt(posG));
Depst(posG)=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Esto para todos los dias del periodo de estudio
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Parametros de Poisson para el resto de los dias
% dias = (1:Numdias)'/Ts;
lambtT = exp(parametro(dias,beta0,beta,betaT,indicesT,varphi));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DlambtT = -1/Ts*ones(size(dias));

%   Calculo las derivadas segundas de la funcion log-likelihood con
%   respecto a los parametros de Pareto-Posson, escala y forma,
%   respectivamente
D2lambt = -1./(lambt.^2);
D2sigt = (-1+(1+epst)./(z.^2))./(sigt.*sigt.*epst);
D2epst = xn.*(2+epst.*(3+epst).*xn)./((epst.*z).^2)-2*log(z)./(epst.^3);
Dlambtsigt = zeros(size(D2sigt));
Dlambtepst = zeros(size(D2sigt));
Dsigtepst = xn.*(1-xn)./(sigt.*z.^2);

%   GUMBEL DERIVATIVES
% D2lambt(posG) = -1./(lambt(posG).^2);
D2sigt(posG) = (1-2*xn(posG))./(sigt(posG).*sigt(posG));
D2epst(posG) = 0;
Dlambtsigt(posG) = 0;
Dlambtepst(posG) = 0;
Dsigtepst(posG) = 0;

neps0 = 1;
if length(posG)==length(epst),
    neps0 = 0;
end

%   Dimensiones del Hessiano
nla = length(beta);
nsig = length(alpha);
neps = length(gamma);
ntend = length(betaT);
nind = length(varphi);
ntend2 = length(betaT2);
nind2 = length(varphi2);

Hxx = sparse(zeros(2+neps0+nla+nsig+neps+ntend+nind+ntend2+nind2));
 
%   Elementos del Hessiano asociados al parametro constante
%   beta0,alpha0,gamma0
%   Elemento 1 (CAMBIO EXPONENCIAL)
Hxx(1,1) = sum((D2lambt.*lambt+Dlambt).*lambt)+sum(DlambtT.*lambtT);
%   Elemento 2 (CAMBIO EXPONENCIAL)
if ntend>0
    Hxx(2+nla,2+nla) =  sum((D2lambt.*lambt+Dlambt).*lambt.*(t.^2))+sum(DlambtT.*lambtT.*(dias.^2));
end
%   Elemento 3 (CAMBIO EXPONENCIAL)
if nind>0
    for i = 1:nind,
        for j = 1:i,
            Hxx(1+nla+ntend+i,1+nla+ntend+j) =  sum((D2lambt.*lambt+Dlambt).*lambt.*indices(:,i).*indices(:,j))+...
                sum(DlambtT.*lambtT.*indicesT(:,i).*indicesT(:,j));                              
        end
    end
end
%   Elemento 4 (CAMBIO EXPONENCIAL)
if ntend2>0
    Hxx(3+nla+nsig+ntend+nind,3+nla+nsig+ntend+nind) =  sum((D2sigt.*sigt+Dsigt).*sigt.*(t.^2));
end
%   Elemento 5 (CAMBIO EXPONENCIAL)
if nind2>0
    for i = 1:nind2,
        for j = 1:i,
            Hxx(2+nla+nsig+ntend+nind+ntend2+i,2+nla+nsig+ntend+nind+ntend2+j) =  sum((D2sigt.*sigt+Dsigt).*sigt.*indices2(:,i).*indices2(:,j));
        end
    end
end
%   Elemento 6 (CAMBIO EXPONENCIAL)
Hxx(2+nla+ntend+nind,2+nla+ntend+nind) = sum((D2sigt.*sigt+Dsigt).*sigt);
%   Elemento 7
if neps0==1,
    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+neps0+nla+nsig+ntend+nind+ntend2+nind2) = sum(D2epst);
end
%   Elementos del Hessiano asociados al parametro constante
%   beta0,alpha0,gamma0 cruzados
%   Elemento 8 (CAMBIO EXPONENCIAL)
Hxx(2+nla+ntend+nind,1) = sum(Dlambtsigt.*sigt.*lambt);
%   Elemento 9 (CAMBIO EXPONENCIAL)
if neps0 == 1,
    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1) = sum(Dlambtepst.*lambt);
%   Elemento 10 (CAMBIO EXPONENCIAL)
    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind) = sum(Dsigtepst.*sigt);
end
%   Elemento 11 (CAMBIO EXPONENCIAL)
if ntend>0,
    Hxx(1+nla+ntend,1) = sum((D2lambt.*lambt+Dlambt).*lambt.*t)+sum(DlambtT.*lambtT.*dias);
end
%   Elemento 12 (CAMBIO EXPONENCIAL)
if ntend2>0,
    Hxx(2+nla+ntend+nind+nsig+ntend2,1) = sum(Dlambtsigt.*t.*sigt.*lambt);
end 
%   Elemento 52 (CAMBIO EXPONENCIAL)
if ntend2>0,
    Hxx(2+nla+ntend+nind+nsig+ntend2,2+nla+ntend+nind) = sum((D2sigt.*sigt+Dsigt).*t.*sigt);
end 
%   Elemento 48 (CAMBIO EXPONENCIAL)
if ntend>0 & ntend2>0,
    Hxx(2+nla+ntend+nind+nsig+ntend2,1+nla+ntend) = sum(Dlambtsigt.*t.*t.*sigt.*lambt);
end
%   Elemento 13 (CAMBIO EXPONENCIAL)
if nind>0,
    for i = 1:nind,
        Hxx(1+nla+ntend+i,1) = sum((D2lambt.*lambt+Dlambt).*lambt.*indices(:,i))+sum(DlambtT.*lambtT.*indicesT(:,i));
    end
end
%   Elemento 14 (CAMBIO EXPONENCIAL)
if nind2>0,
    for i = 1:nind2,
        Hxx(2+nla+ntend+nind+nsig+ntend2+i,1) = sum(Dlambtsigt.*indices2(:,i).*sigt.*lambt);
    end
end
%   Elemento 53 (CAMBIO EXPONENCIAL)
if nind2>0,
    for i = 1:nind2,
        Hxx(2+nla+ntend+nind+nsig+ntend2+i,2+nla+ntend+nind) = sum((D2sigt.*sigt+Dsigt).*indices2(:,i).*sigt);
    end
end
%   Elemento 49 (CAMBIO EXPONENCIAL)
if ntend>0 & nind2>0,
    for i = 1:nind2,
        Hxx(2+nla+ntend+nind+nsig+ntend2+i,1+nla+ntend) = sum(Dlambtsigt.*t.*indices2(:,i).*sigt.*lambt);
    end
end
%   Elemento 15 (CAMBIO EXPONENCIAL)
if nind>0 && ntend>0,
    for i = 1:nind,
        Hxx(1+nla+ntend+i,1+nla+ntend) = sum((D2lambt.*lambt+Dlambt).*lambt.*t.*indices(:,i))+...
            sum(DlambtT.*lambtT.*dias.*indicesT(:,i));                  
    end
end
%   Elemento 16 (CAMBIO EXPONENCIAL)
if nind2>0 && ntend2>0,
    for i = 1:nind2,
        Hxx(2+nla+ntend+nind+nsig+ntend2+i,2+nla+ntend+nind+nsig+ntend2) = sum((D2sigt.*sigt+Dsigt).*t.*indices2(:,i).*sigt);
    end
end
%   Elemento 17 (CAMBIO EXPONENCIAL)
if ntend>0,
    Hxx(2+nla+ntend+nind,1+nla+ntend) = sum(Dlambtsigt.*t.*sigt.*lambt);
end
%   Elemento 18 (CAMBIO EXPONENCIAL)
if ntend>0 && neps0 == 1,
    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1+nla+ntend) = sum(Dlambtepst.*t.*lambt);
end
%   Elemento 19 (CAMBIO EXPONENCIAL)
if ntend2>0 && neps0 == 1,
    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind+nsig+ntend2) = sum(Dsigtepst.*t.*sigt);
end
%   Elemento 20 (CAMBIO EXPONENCIAL)
if nind>0,
    for i = 1:nind,
        Hxx(2+nla+ntend+nind,1+nla+ntend+i) = sum(Dlambtsigt.*indices(:,i).*sigt.*lambt);
    end
end
%   Elemento 21 (CAMBIO EXPONENCIAL)
if nind>0 && neps0 == 1,
    for i = 1:nind,
        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1+nla+ntend+i) = sum(Dlambtepst.*indices(:,i).*lambt);
    end
end
%   Elemento 22 (CAMBIO EXPONENCIAL)
if nind2>0 && neps0 == 1,
    for i = 1:nind2,
        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind+nsig+ntend2+i) = sum(Dsigtepst.*indices2(:,i).*sigt);
    end
end

%   Elementos asociados a la parte unicamente asociada a localizacion
if nla>0,
    for i = 1:nla,
        %   Primer elemento asociado al valor constante (primera columna)
        aux = 0;
        for k = 1:length(t),
            aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*lambt(k)*Dparam (t(k),i);      
        end
        for k = 1:length(dias),
            aux = aux + DlambtT(k)*lambtT(k)*Dparam (dias(k),i);      
        end
        %   Elemento 23 (CAMBIO EXPONENCIAL)
        Hxx(1+i,1) = aux;
        %   Elementos asociados a los demas parametros
        for j = 1:i,
            aux = 0;
            for k = 1:length(t),
                aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*Dparam (t(k),i)*Dparam (t(k),j)*lambt(k);
            end
            for k = 1:length(dias),
                aux = aux + (DlambtT(k))*Dparam (dias(k),i)*Dparam (dias(k),j)*lambtT(k);
            end
            %   Elemento 24 (CAMBIO EXPONENCIAL)
            Hxx(1+i,1+j) = aux;
        end
    end
    %   Elementos de localizacion con respecto alpha0
    for i = 1:nla,
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dlambtsigt(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
        end
        %   Elemento 25 (CAMBIO EXPONENCIAL)
        Hxx(2+nla+ntend+nind,1+i) = aux; 
    end
    %   Elementos de localizacion con respecto gamma0
    if neps0 == 1,
    for i = 1:nla,
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dlambtepst(k)*lambt(k)*Dparam (t(k),i);
        end
        %   Elemento 26 (CAMBIO EXPONENCIAL)
        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,1+i) = aux; 
    end  
    end
    %   Elementos de localizacion con respecto a la tendencia
    if ntend>0,
        for i = 1:nla,
            aux = 0;
            for k = 1:length(t),
                aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*lambt(k)*t(k)*Dparam (t(k),i);
            end
            for k = 1:length(dias),
                aux = aux + DlambtT(k)*lambtT(k)*dias(k)*Dparam (dias(k),i);
            end
            %   Elemento 27  (CAMBIO EXPONENCIAL)
            Hxx(2+nla,1+i) = aux; 
        end
    end
    %   Elementos de localizacion con respecto a la tendencia 2
    if ntend2>0,
        for i = 1:nla,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dlambtsigt(k)*t(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
            end
            %   Elemento 46 (CAMBIO EXPONENCIAL)
            Hxx(2+nla+ntend+nind+nsig+ntend2,1+i) = aux;
        end
    end
    %   Elementos de localizacion con respecto a las covariables
    if nind>0,
        for i = 1:nla,
            for j = 1:nind,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + (D2lambt(k)*lambt(k)+Dlambt(k))*lambt(k)*indices(k,j)*Dparam (t(k),i);
                end
                for k = 1:length(dias),
                    aux = aux + DlambtT(k)*lambtT(k)*indicesT(k,j)*Dparam (dias(k),i);
                end
                %   Elemento 28 (CAMBIO EXPONENCIAL)
                Hxx(1+nla+ntend+j,1+i) = aux; 
            end
        end
    end 
    %   Elementos de localizacion con respecto a las covariables 2
    if nind2>0,
        for i = 1:nla,
            for j = 1:nind2,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dlambtsigt(k)*indices2(k,j)*Dparam (t(k),i)*sigt(k)*lambt(k);
                end
                %   Elemento 47 (CAMBIO EXPONENCIAL)
                Hxx(2+nla+ntend+nind+nsig+ntend2+j,1+i) = aux; 
            end
        end
    end 
end

%   Elementos asociados a la parte unicamente asociada a escala
if nsig>0,
    for i = 1:nsig,
    %   Primer elemento asociado al valor constante (primera columna)
    aux = 0;
    for k = 1:length(t),
        aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*Dparam (t(k),i)*sigt(k);
    end
    %   Elemento 29 (CAMBIO EXPONENCIAL)
    Hxx(2+nla+ntend+nind+i,2+ntend+nind+nla) = aux;
    %   Elementos asociados a los demas parametros
    for j = 1:i,
        aux = 0;
        for k = 1:length(t),
            aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*Dparam (t(k),i)*Dparam (t(k),j)*sigt(k);
        end
        %   Elemento 30 (CAMBIO EXPONENCIAL)
        Hxx(2+nla+ntend+nind+i,2+nla+ntend+nind+j) = aux;
    end
    end

    %   Elementos de escala con respecto gamma0
    if neps0 == 1,
    for i = 1:nsig,
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dsigtepst(k)*Dparam (t(k),i)*sigt(k);
        end
        %   Elemento 31 (CAMBIO EXPONENCIAL)
        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2,2+nla+ntend+nind+i) = aux; 
    end 
    end
    %   Elementos de escala con respecto beta0
    for i = 1:nsig,
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dlambtsigt(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
        end
        %   Elemento 32 (CAMBIO EXPONENCIAL)
        Hxx(2+nla+ntend+nind+i,1) = aux; 
    end 
    %   Elementos de localizacion con respecto a la tendencia
    if ntend>0,
        for i = 1:nsig,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dlambtsigt(k)*t(k)*Dparam (t(k),i)*sigt(k)*lambt(k);
            end
            %   Elemento 33 (CAMBIO EXPONENCIAL)
            Hxx(2+nla+ntend+nind+i,2+nla) = aux; 
        end
    end
    %   Elementos de localizacion con respecto a las covariables
    if nind>0,
        for i = 1:nsig,
            for j = 1:nind,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dlambtsigt(k)*indices(k,j)*Dparam (t(k),i)*sigt(k)*lambt(k);
                end
                %   Elemento 34 (CAMBIO EXPONENCIAL)
                Hxx(2+nla+ntend+nind+i,1+nla+ntend+j) = aux; 
            end
        end
    end    
end

%   Elementos asociados a la parte unicamente asociada a forma
if neps>0 && neps0 == 1,
    for i = 1:neps,
    %   Primer elemento asociado al valor constante (primera columna)
    aux = 0;
    for k = 1:length(t),
        aux = aux + D2epst(k)*Dparam (t(k),i);
    end
    %   Elemento 35
    Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+neps0+nla+nsig+ntend+nind+ntend2+nind2) = aux;
    %   Elementos asociados a los demas parametros
    for j = 1:i,
        aux = 0;
        for k = 1:length(t),
            aux = aux + D2epst(k)*Dparam (t(k),i)*Dparam (t(k),j);
        end
        %   Elemento 36
        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+neps0+nla+nsig+ntend+nind+ntend2+nind2+j) = aux;
    end
    end
    %   Elementos de escala con respecto alpha0
    for i = 1:neps,
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dsigtepst(k)*Dparam (t(k),i)*sigt(k);
        end
        %   Elemento 37 (CAMBIO EXPONENCIAL)
        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+ntend+nind) = aux; 
    end 

    %   Elementos de escala con respecto beta0
    for i = 1:neps,
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dlambtepst(k)*Dparam (t(k),i)*lambt(k);
        end
        %   Elemento 38 (CAMBIO EXPONENCIAL)
        Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,1) = aux; 
    end
    %   Elementos de forma con respecto a la tendencia
    if ntend>0,
        for i = 1:neps,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dlambtepst(k)*t(k)*Dparam (t(k),i)*lambt(k);
            end
            %   Elemento 39 (CAMBIO EXPONENCIAL)
            Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla) = aux; 
        end
    end
    %   Elementos de forma con respecto a la tendencia2
    if ntend2>0,
        for i = 1:neps,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dsigtepst(k)*t(k)*Dparam (t(k),i)*sigt(k);
            end
            %   Elemento 44 (CAMBIO EXPONENCIAL)
            Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+nsig+ntend+nind+ntend2) = aux; 
        end
    end
    %   Elementos de forma con respecto a las covariables
    if nind>0,
        for i = 1:neps,
            for j = 1:nind,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dlambtepst(k)*indices(k,j)*Dparam (t(k),i)*lambt(k);  
                end
                %   Elemento 40 (CAMBIO EXPONENCIAL)
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,1+nla+ntend+j) = aux; 
            end
        end
    end
    %   Elementos de forma con respecto a las covariables 2
    if nind2>0,
        for i = 1:neps,
            for j = 1:nind2,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + Dsigtepst(k)*indices2(k,j)*Dparam (t(k),i)*sigt(k);
                end
                %   Elemento 45 (CAMBIO EXPONENCIAL)
                Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+nsig+ntend+nind+ntend2+j) = aux; 
            end
        end
    end
end

%   TENDENCIA COVARIABLES
if nind>0 & ntend2>0,
    for i = 1:nind,
        aux = 0;
        for k = 1:length(t),
            aux = aux + Dlambtsigt(k)*t(k)*indices(k,i)*sigt(k)*lambt(k);
        end
        %   Elemento 50 (CAMBIO EXPONENCIAL)
        Hxx(2+nla+ntend+nind+nsig+ntend2,1+nla+ntend+i) = aux;
    end
end

%   COVARIABLES
if nind>0 & nind2>0,
    for i = 1:nind,
        for j = 1:nind2,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dlambtsigt(k)*indices2(k,j)*indices(k,i)*sigt(k)*lambt(k);
            end
            %   Elemento 51 (CAMBIO EXPONENCIAL)
            Hxx(2+nla+ntend+nind+nsig+ntend2+j,1+nla+ntend+i) = aux; 
        end
    end
end 

% ESCALA CON TENDENCIA
if ntend2>0,
    for i = 1:nsig,
        aux = 0;
        for k = 1:length(t),
            aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*t(k)*Dparam (t(k),i)*sigt(k);
        end
        %   Elemento 54 (CAMBIO EXPONENCIAL)
        Hxx(2+nla+ntend+nind+nsig+ntend2,2+nla+ntend+nind+i) = aux;
    end
end

%   COVARIABLES CON ESCALA
if nind2>0,
    for i = 1:nsig,
        for j = 1:nind2,
            aux = 0;
            for k = 1:length(t),
                aux = aux + (D2sigt(k)*sigt(k)+Dsigt(k))*indices2(k,j)*Dparam (t(k),i)*sigt(k);
            end
            %   Elemento 55 (CAMBIO EXPONENCIAL)
            Hxx(2+nla+ntend+nind+nsig+ntend2+j,2+nla+ntend+nind+i) = aux; 
        end
    end
end 

%   SUBBLOQUE 2-1
if nla>0 && nsig>0,
    for j = 1:nla,    
        %   betas y alphas
        %   Elementos asociados a los demas parametros
        for i = 1:nsig,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dlambtsigt(k)*Dparam (t(k),i)*Dparam (t(k),j)*sigt(k)*lambt(k);
            end
            %   Elemento 41 (CAMBIO EXPONENCIAL)
            Hxx(2+nla+ntend+nind+i,1+j) = aux; 
        end
    end
end



%   SUBBLOQUE 3-1
if nla>0 && neps>0 && neps0==1,
    for j = 1:nla,    
        %   betas y gamma
        %   Elementos asociados a los demas parametros
        for i = 1:neps,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dlambtepst(k)*lambt(k)*Dparam (t(k),i)*Dparam (t(k),j);
            end
            %   Elemento 42 (CAMBIO EXPONENCIAL)
            Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,1+j) = aux; 
        end
    end
end

%   SUBBLOQUE 3-2
if nsig>0 && neps>0 && neps0==1,
    for j = 1:nsig,    
        %   alphas y gamma
        %   Elementos asociados a los demas parametros
        for i = 1:neps,
            aux = 0;
            for k = 1:length(t),
                aux = aux + Dsigtepst(k)*Dparam (t(k),i)*Dparam (t(k),j)*sigt(k);
            end
            %   Elemento 43 (CAMBIO EXPONENCIAL)
            Hxx(2+neps0+nla+nsig+ntend+nind+ntend2+nind2+i,2+nla+ntend+nind+j) = aux; 
        end
    end
end            

Hxx = Hxx + tril(Hxx,-1)';

    function dp = Dparam (t,j)
    %
    %   Derivada con respecto a los parametros asociados a senos y cosenos
    %
    %   Input:
    %        t-> Abscisas o tiempo en la que calcular el valor de la 
    %            derivada (Entre 0 y 1)
    %        j-> Numero de armonico dentro del vector
    %
    %   Output:
    %        dp-> Valor de la derivada
    %
    
    if mod(j,2) == 0,
        dp = sin(j/2*2*pi*t);
    else
        dp = cos((j+1)/2*2*pi*t);
    end

    end

end



