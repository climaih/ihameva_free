function [ticks,etiqueta3]=PintaRosasProbabilisticas(varargin)
% Examples
%   PintaRosasProbabilisticas(excal_Dire,excal_HsR,'esp','N','m');
%   PintaRosasProbabilisticas(excal_Dire,excal_HsR,'esp','N','m',gca,[1,max(excal_HsR)],true);
%   PintaRosasProbabilisticas(excal_Dire,excal_HsR,excal_Dire,excal_HsI,'esp',['n','k'],['m','s'],'');
%   PintaRosasProbabilisticas(excal_Dire,excal_HsR,excal_Dire,excal_HsI,excal_Dire,excal_HsR,excal_Dire,excal_HsI,'esp',['n','k','n','k'],['m','s','n','k']);

nAngles=12;
usarlog=false;
if isnumeric(varargin{2}) && ischar(varargin{3})
    Dir=varargin{1};
    Hs=varargin{2};
	idioma=varargin{3};
	var_name=varargin{4};
	var_unit=varargin{5};
    if nargin<5 || nargin>9
        disp('Argumentos incorrectos:PintaRosasProbabilisticas');return;
    end
	Hlim=[0 ceil(max(Hs))];
    if nargin>5, gcax=varargin{6}; end
    if nargin>6, Hlim=varargin{7}; end
    if nargin>7, usarlog=varargin{8}; end
    if nargin>8, nAngles=varargin{9}; end
    tiporose=1;
elseif isnumeric(varargin{4}) && ischar(varargin{5})
    DH1(1,:)=varargin{1};
    DH1(2,:)=varargin{2};
    DH2(1,:)=varargin{3};
    DH2(2,:)=varargin{4};
	idioma=varargin{5};
	var_name=varargin{6};
	var_unit=varargin{7};
    if nargin<7 || nargin>11,
        disp('Argumentos incorrectos:PintaRosasProbabilisticas');return;
    end
    Hlim=[0 ceil(max([max(DH1(2,:)) max(DH2(2,:))]))];
    if nargin>7, gcax=varargin{8}; end
    if nargin>8, Hlim=varargin{9}; end
    if nargin>9, usarlog=varargin{10}; end
    if nargin>10, nAngles=varargin{11}; end
    tiporose=2;
elseif isnumeric(varargin{8}) && ischar(varargin{9})
    DH1(1,:)=varargin{1};
    DH1(2,:)=varargin{2};
    DH2(1,:)=varargin{3};
    DH2(2,:)=varargin{4};
    DH3(1,:)=varargin{5};
    DH3(2,:)=varargin{6};
    DH4(1,:)=varargin{7};
    DH4(2,:)=varargin{8};
	idioma=varargin{9};
	var_name=varargin{10};
	var_unit=varargin{11};
    if nargin<11 || nargin>15,
        disp('Argumentos incorrectos:PintaRosasProbabilisticas');return;
    end
    Hlim=[0 ceil(max([max(DH1(2,:)) max(DH2(2,:)) max(DH3(2,:)) max(DH4(2,:))]))];
    if nargin>11, gcax=varargin{12}; end
    if nargin>12, Hlim=varargin{13}; end
    if nargin>13, usarlog=varargin{14}; end
    if nargin>14, nAngles=varargin{15}; end
    tiporose=4;
else
    disp('Cant no use this function: PintaRosasProbabilisticas'); return;
end

if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name=''; end
if ~exist('var_unit','var'), var_unit=''; end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! PintaRosasProbabilisticas with axes'); end
set(gcf,'CurrentAxes',gcax)


% -- ROSA PROBABILISTICA CON VIENTOS BRUTOS
IncD=5;
if tiporose==1,
    [minVH,Hlim,usarlog]=cmprblim(Hlim,usarlog,Hs);
    [cant,cant_log,cantidad_log,VH]=PintaRosasAux(Dir,Hs,minVH,Hlim,IncD);
    etiqueta(1)=0;%ticks del colorbar
    etiqueta(2)=min(cantidad_log);
    etiqueta(12)=max(cantidad_log);
    dx=(etiqueta(12)-etiqueta(2))/10;
    for i=3:11, etiqueta(i)=(etiqueta(2)+dx*(i-2)); end
    %axis equal;
    pinta_rosa_prob (cant,cant_log,VH,Hlim,IncD,usarlog,nAngles,etiqueta,['Probabilistic Rose of ',var_name],var_unit);
    etiqueta2=exp(etiqueta);
    etiqueta3{1}=sprintf('%6.4f',0);
    for i=2:length(etiqueta2), etiqueta3{i}=sprintf('%4.2f',etiqueta2(i)); end
    ticks=linspace(1,11,12);
elseif tiporose==2,
    [minVH,Hlim,usarlog]=cmprblim(Hlim,usarlog,DH1(2,:),DH2(2,:));
    [cant1,cant_log1,cantidad_log1,VH1]=PintaRosasAux(DH1(1,:),DH1(2,:),minVH,Hlim,IncD);
    [cant2,cant_log2,cantidad_log2,VH2]=PintaRosasAux(DH2(1,:),DH2(2,:),minVH,Hlim,IncD);
    etiqueta(1)=0;%ticks del colorbar
    etiqueta(2)=min([min(cantidad_log1) min(cantidad_log2)]);
    etiqueta(12)=max([max(cantidad_log1) max(cantidad_log2)]);
    dx=(etiqueta(12)-etiqueta(2))/10;
    for i=3:11, etiqueta(i)=(etiqueta(2)+dx*(i-2)); end
    subplot(1,2,1);axis equal;
    pinta_rosa_prob (cant1,cant_log1,VH1,Hlim,IncD,usarlog,nAngles,etiqueta,'W_{mod}',var_unit(1));
    subplot(1,2,2);axis equal;
    pinta_rosa_prob (cant2,cant_log2,VH2,Hlim,IncD,usarlog,nAngles,etiqueta,'W_{mod}',var_unit(2));
    etiqueta2=exp(etiqueta);
    etiqueta3{1}=sprintf('%6.4f',0);
    for i=2:length(etiqueta2), etiqueta3{i}=sprintf('%4.2f',etiqueta2(i)); end
    ticks=linspace(1,11,12);
elseif tiporose==4,
    minVH=min([min(DH1(2,:)) min(DH2(2,:)) min(DH3(2,:)) min(DH4(2,:))]);
    [cant1,cant_log1,cantidad_log1,VH1]=PintaRosasAux(DH1(1,:),DH1(2,:),minVH,Hlim,IncD);
    [cant2,cant_log2,cantidad_log2,VH2]=PintaRosasAux(DH2(1,:),DH2(2,:),minVH,Hlim,IncD);
    [cant3,cant_log3,cantidad_log3,VH3]=PintaRosasAux(DH3(1,:),DH3(2,:),minVH,Hlim,IncD);
    [cant4,cant_log4,cantidad_log4,VH4]=PintaRosasAux(DH4(1,:),DH4(2,:),minVH,Hlim,IncD);
    etiqueta(1)=0;%ticks del colorbar
    etiqueta(2)=min([min(cantidad_log1) min(cantidad_log2) min(cantidad_log3) min(cantidad_log4)]);
    etiqueta(12)=max([max(cantidad_log1) max(cantidad_log2) max(cantidad_log3) max(cantidad_log4)]);
    dx=(etiqueta(12)-etiqueta(2))/10;
    for i=3:11
        etiqueta(i)=(etiqueta(2)+dx*(i-2));
    end
    if minVH<1, usarlog=false; end
    subplot(2,2,1);axis equal;
    pinta_rosa_prob (cant1,cant_log1,VH1,Hlim,IncD,usarlog,nAngles,etiqueta,'W_{mod}',var_unit(1));
    subplot(2,2,2);axis equal;
    pinta_rosa_prob (cant2,cant_log2,VH2,Hlim,IncD,usarlog,nAngles,etiqueta,'W_{mod}',var_unit(2));
    subplot(2,2,3);axis equal;
    pinta_rosa_prob (cant3,cant_log3,VH3,Hlim,IncD,usarlog,nAngles,etiqueta,'W_{mod}',var_unit(3));
    subplot(2,2,4);axis equal;
    pinta_rosa_prob (cant4,cant_log4,VH4,Hlim,IncD,usarlog,nAngles,etiqueta,'W_{mod}',var_unit(4));
    
    etiqueta2=exp(etiqueta);
    etiqueta3{1}=sprintf('%6.4f',0);
    for i=2:length(etiqueta2)
        etiqueta3{i}=sprintf('%4.2f',etiqueta2(i));
    end
    ticks=linspace(1,11,12);
end

end

function [cant1,cant_log1,cantidad_log1,VH]=PintaRosasAux(Dir,Hs,minVH,Hlim,IncD)
    Dir=Dir(:);
    Hs=Hs(:);
    pos=0;
    num=90;
    IncH=((Hlim(2)-Hlim(1))/num);
    VH=minVH:IncH:Hlim(2);
    VD=0:IncD:360;
    can1=zeros(length(VH),length(VD));
    for i=1:(length(VH)-1)
        D1=Dir(find(Hs>=VH(i) & Hs<=VH(i+1)));
        for j=1:(length(VD)-1)
            pos=pos+1;
            da=find(D1>=VD(j) & D1<=VD(j+1));
            if isempty(da)==0
                can1(i,j)=length(da);
            end
        end
    end
    cant1=can1./length(Hs)*100;
    [mc,nc]=size(cant1);
    cantidad1=reshape(cant1,mc*nc,1);
    orden1=sort(cantidad1);
    dd=find(orden1==0);%Probabilidad=0 (se iguala a un valor menor al minimo para que no de problemas al tomar logaritmos)
    minimo=orden1(dd(end)+1);
    probn=find(cant1==0);
    cant1(probn)=minimo;
    cant_log1=log(cant1);
    cantidad_log1=reshape(cant_log1,mc*nc,1);
end

function [miv,hl,ul]=cmprblim(Hlim,usarlog,varargin)
    miv=min(min(varargin{1:end}));
    mav=max(max(varargin{1:end}));
    hl=[miv, mav];
    ul=usarlog;%la escala logaritmica solo se utilizan con valores mayores que 1
    if usarlog && miv<1, ul=false; disp('No se puede usar escala logaritmica!!'); end
    if ~isempty(Hlim) && length(Hlim)==2 && Hlim(2)>Hlim(1) && Hlim(2)>mav, hl=Hlim; end %compruebo el max
    if hl(2)>2*mav, hl(2)=mav; disp('El limite superior se ha cambiado por el maximo!!'); end

end