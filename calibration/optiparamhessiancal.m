function [p,f,exitflag,output,lambda,grad,hessian,residuo] =  optiparamhessiancal (x,y,pini,ID,W)

%   Initial bounds and possible initial values for the constant parameters
if ~isempty(pini),
    p = pini;
else
    p = zeros(4,1);
end

if isempty(W) || nargin<5,
    W = ones(size(x));
end

up = Inf*ones(length(p),1);
lb = -Inf*ones(length(p),1);

%   p2 always >0
lb(2) = 0;
%   p3 always >0
lb(3) = -1;
if ID==3 || ID==4 || ID==5 || ID==6,
    lb(1) = 0;
end
if ID==6,
    lb(3) = 0.05;
end

% options = optimset('GradObj','off','Hessian','off','TolFun',1e-12,'MaxFunEvals',1000,'MaxIter',1000);
% [paaa,faaa,exitflagaaa,outputaaa,lambdaaaa,gradaaa,hessianaaa] = fmincon(@(p) loglikelihood (p,x,y,ID),p,[],[],[],[],lb,up,[],options);

%   Set the options for the optimization routine, note that Gradients are
%   provided but not Hessian
options = optimset('GradObj','on','Hessian','on','TolFun',1e-12,'Algorithm','trust-region-reflective','MaxFunEvals',1000,'MaxIter',1000);
%   Call the optimization routine
%   Note that it is a minimization problem, instead a maximization problem,
%   for this reason the log likelihood function sign is changed
[p,f,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x,y,ID,W),p,[],[],[],[],lb,up,[],options);


[f Jx Hxx residuo] = loglikelihood (p,x,y,ID,W);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Loglikelihood function definition of the nonlinear regression with 
%   variance being a function of x (HETEROCEDASTIC)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [f Jx Hxx residuo] = loglikelihood (p,x,y,ID,W)
        %   This function calculates the loglikelihood function with the
        %   sign changed for the values of the paremeters given by p
        n = max(size(y));
        %   It Calculates the gradients of each parameter p
        switch ID
            case 1
                fsigma = p(3)+p(4)*x;
                fmu = p(1)+p(2)*x;
            case 2
                fsigma = p(3)+p(4)*sqrt(x);
                fmu = p(1)+p(2)*x;
            case 3
                fsigma = p(3)+p(4)*x;
                fmu = p(1)*x.^p(2);
            case 4
                fsigma = p(3)+p(4)*sqrt(x);
                fmu = p(1)*x.^p(2);
            case 5
                fsigma = p(3)*(1-exp(-p(4)*x));
                fmu = p(1)*x.^p(2); 
            case 6
                fsigma = p(3)*x.^p(4);
                fmu = p(1)*x.^p(2); 
            otherwise
                disp('ID no valido');
        end
            
        %   Evaluate the loglikelihood function 
        f =  -0.5*sum(log(2*pi*fsigma.^2))-0.5*sum(W.*((y-fmu)./fsigma).^2);
        %   convert a maximization problem into a minimization problem
        f=-f;
        
        %   Jacobian of the log-likelihood function
        if nargout >1,
            Jx = zeros(length(p),1);
            switch ID
                case 1
%       Model 1: fmu = p1+p2*x
%                fsigma = p3+p4*x
                    Df1 = ones(n,1);
                    Df2 = x;
                    Ds1 = ones(n,1);
                    Ds2 = x;
%                     Jx(1)=sum((y-fmu)./fsigma.^2);  %p(1)
%                     Jx(2)=sum(x.*(y-fmu)./fsigma.^2);  %p(2)
%                     Jx(3)=sum(-1./fsigma)+sum((y-fmu).^2./fsigma.^3);  %p(3)
%                     Jx(4)=sum(-x./fsigma)+sum(x.*(y-fmu).^2./fsigma.^3);  %p(4)
                case 2
%       Model 2: fmu = p1+p2*x
%                fsigma = p3+p4*sqrt(x)   
                    Df1 = ones(n,1);
                    Df2 = x;
                    Ds1 = ones(n,1);
                    Ds2 = sqrt(x);
%                     Jx(1)=sum((y-fmu)./fsigma.^2);  %p(1)
%                     Jx(2)=sum(x.*(y-fmu)./fsigma.^2);  %p(2)
%                     Jx(3)=sum(-1./fsigma)+sum((y-fmu).^2./fsigma.^3);  %p(3)
%                     Jx(4)=sum(-sqrt(x)./fsigma)+sum(sqrt(x).*(y-fmu).^2./fsigma.^3);  %p(4)
                case 3
%       Model 3: fmu = p1*x^p2
%                fsigma = p3+p4*x 
                    Df1 = (x.^p(2));
                    Df2 = (p(1)*x.^p(2)).*log(x);
                    Ds1 = ones(n,1);
                    Ds2 = x;
%                     Jx(1)=sum((x.^p(2)).*(y-fmu)./fsigma.^2);  %p(1)
%                     Jx(2)=sum((p(1)*x.^p(2)).*log(x).*(y-fmu)./fsigma.^2);  %p(2)
%                     Jx(3)=sum(-1./fsigma)+sum(((y-fmu).^2)./fsigma.^3);  %p(3)
%                     Jx(4)=sum(-x./fsigma)+sum(x.*(y-fmu).^2./fsigma.^3);  %p(4)                
                case 4
%       Model 4: fmu = p1*x^p2
%                fsigma = p3+p4*sqrt(x)
                    Df1 = (x.^p(2));
                    Df2 = (p(1)*x.^p(2)).*log(x);
                    Ds1 = ones(n,1);
                    Ds2 = sqrt(x);
%                     Jx(1)=sum((x.^p(2)).*(y-fmu)./(fsigma.^2));  %p(1)
%                     Jx(2)=sum((p(1)*x.^p(2)).*log(x).*(y-fmu)./fsigma.^2);  %p(2)
%                     Jx(3)=sum(-1./fsigma)+sum((y-fmu).^2./fsigma.^3);  %p(3)
%                     Jx(4)=sum(-sqrt(x)./fsigma)+sum(sqrt(x).*(y-fmu).^2./
%                     fsigma.^3);  %p(4)  
                case 5
%       Model 5: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*(1-exp(-p(4)*x));
                    Df1 = (x.^p(2));
                    Df2 = (p(1)*x.^p(2)).*log(x);
                    Ds1 = (1-exp(-p(4)*x));
                    Ds2 = p(3)*exp(-p(4)*x).*x;
                case 6
%       Model 6: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*x.^p(4);      
                    Df1 = (x.^p(2));
                    Df2 = (p(1)*x.^p(2)).*log(x);
                    Ds1 = (x.^p(4));
                    Ds2 = (p(3)*x.^p(4)).*log(x);
                otherwise
                    disp('id no valido');
            end
            Jx(1)=sum(W.*Df1.*(y-fmu)./fsigma.^2);  %p(1)
            Jx(2)=sum(W.*Df2.*(y-fmu)./fsigma.^2);  %p(2)
            Jx(3)=-sum(W.*Ds1./fsigma)+sum(W.*Ds1.*(y-fmu).^2./fsigma.^3);  %p(3)
            Jx(4)=-sum(W.*Ds2./fsigma)+sum(W.*Ds2.*(y-fmu).^2./fsigma.^3);  %p(4)
            Jx=-Jx;
        end
        
        %   Hessian of the log-likelihood function
        if nargout >2,
            Hxx = zeros(length(p));
            switch ID
                case 1
%       Model 1: fmu = p1+p2*x
%                fsigma = p3+p4*x
                    D2f11 = zeros(n,1);
                    D2f22 = zeros(n,1);
                    D2f12 = zeros(n,1);
                    D2s11 = zeros(n,1);
                    D2s22 = zeros(n,1);
                    D2s12 = zeros(n,1);
                case 2
%       Model 2: fmu = p1+p2*x
%                fsigma = p3+p4*sqrt(x)              
                    D2f11 = zeros(n,1);
                    D2f22 = zeros(n,1);
                    D2f12 = zeros(n,1);
                    D2s11 = zeros(n,1);
                    D2s22 = zeros(n,1);
                    D2s12 = zeros(n,1);
                case 3
%       Model 3: fmu = p1*x^p2
%                fsigma = p3+p4*x          
                    D2f11 = zeros(n,1);
                    D2f22 = (p(1)*x.^p(2)).*log(x).*2;
                    D2f12 = (x.^p(2)).*log(x);
                    D2s11 = zeros(n,1);
                    D2s22 = zeros(n,1);
                    D2s12 = zeros(n,1);           
                case 4
%       Model 4: fmu = p1*x^p2
%                fsigma = p3+p4*sqrt(x)
                    D2f11 = zeros(n,1);
                    D2f22 = (p(1)*x.^p(2)).*log(x).*2;
                    D2f12 = (x.^p(2)).*log(x);
                    D2s11 = zeros(n,1);
                    D2s22 = zeros(n,1);
                    D2s12 = zeros(n,1);
                case 5
%       Model 5: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*(1-exp(-p(4)*x));
                    D2f11 = zeros(n,1);
                    D2f22 = (p(1)*x.^p(2)).*log(x).*2;
                    D2f12 = (x.^p(2)).*log(x);
                    D2s11 = zeros(n,1);
                    D2s22 = -p(3)*exp(-p(4)*x).*x.^2;
                    D2s12 = exp(-p(4)*x).*x;
                case 6
%       Model 6: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*x.^p(4);  
                    D2f11 = zeros(n,1);
                    D2f22 = (p(1)*x.^p(2)).*log(x).*2;
                    D2f12 = (x.^p(2)).*log(x);
                    D2s11 = zeros(n,1);
                    D2s22 = (p(3)*x.^p(4)).*log(x).*2;
                    D2s12 = (x.^p(4)).*log(x);
                otherwise
                    disp('id no valido');
            end
            Hxx(1,1) = sum(W./fsigma.^2.*((y-fmu).* D2f11 - W.*Df1.^2)); 
            Hxx(2,2) = sum(W./fsigma.^2.*((y-fmu).* D2f22 - W.*Df2.^2)); 
            Hxx(1,2) = sum(W./fsigma.^2.*((y-fmu).* D2f12 - W.*Df1.*Df2)); 
            Hxx(3,3) = -sum(W./fsigma.^2.*(fsigma.* D2s11 - Ds1.^2))+sum(W.*(y-fmu).^2./fsigma.^3.*(D2s11 - 3./fsigma.* Ds1.^2));
            Hxx(4,4) = -sum(W./fsigma.^2.*(fsigma.* D2s22 - Ds2.^2))+sum(W.*(y-fmu).^2./fsigma.^3.*(D2s22 - 3./fsigma.* Ds2.^2));
            Hxx(3,4) = -sum(W./fsigma.^2.*(fsigma.* D2s12 - Ds1.*Ds2))+sum(W.*(y-fmu).^2./fsigma.^3.*(D2s12 - 3./fsigma.* Ds1.*Ds2));
            Hxx(1,3) = -2*sum(W.*(y-fmu)./fsigma.^3.*Df1.*Ds1);
            Hxx(1,4) = -2*sum(W.*(y-fmu)./fsigma.^3.*Df1.*Ds2);
            Hxx(2,3) = -2*sum(W.*(y-fmu)./fsigma.^3.*Df2.*Ds1);
            Hxx(2,4) = -2*sum(W.*(y-fmu)./fsigma.^3.*Df2.*Ds2);
            Hxx=-(Hxx+triu(Hxx,1)');
        end
        
        if nargout>3,
            residuo = (y-fmu);
        end

    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Comprobacion de derivadas primeras
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % epsi = 0.00001;
% % % i = 4;
% % % p(i) = p(i)*(1+epsi);
% % % f1 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1+epsi);
% % % p(i) = p(i)*(1-epsi);
% % % f2 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1-epsi);
% % % derivada = (f1-f2)/(2*p(i)*epsi);
% % % [derivada Jx(i)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Comprobacion de derivadas segundas (diagonal principal)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % epsi = 0.0001;
% % % i = 4;
% % % f3 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)*(1+epsi);
% % % f1 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1+epsi);
% % % p(i) = p(i)*(1-epsi);
% % % f2 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1-epsi);
% % % derivada = (f1-2*f3+f2)/(p(i)*epsi)^2;
% % % [derivada Hxx(i,i)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Comprobacion de derivadas segundas (fuera de la diagonal principal)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % epsi = 0.0001;
% % % i = 1;
% % % j = 3;
% % % p(i) = p(i)*(1+epsi);
% % % p(j) = p(j)*(1+epsi);
% % % f1 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1+epsi);
% % % p(j) = p(j)/(1+epsi);
% % % p(i) = p(i)*(1-epsi);
% % % p(j) = p(j)*(1-epsi);
% % % f2 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1-epsi);
% % % p(j) = p(j)/(1-epsi);
% % % p(i) = p(i)*(1+epsi);
% % % p(j) = p(j)*(1-epsi);
% % % f3 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1+epsi);
% % % p(j) = p(j)/(1-epsi);
% % % p(i) = p(i)*(1-epsi);
% % % p(j) = p(j)*(1+epsi);
% % % f4 = loglikelihood (p,x,y,ID);
% % % p(i) = p(i)/(1-epsi);
% % % p(j) = p(j)/(1+epsi);
% % % derivada = (f1-f3-f4+f2)/(4*p(i)*epsi*p(j)*epsi);
% % % [derivada Hxx(i,j)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Comprobacion de derivadas segundas (fuera de la diagonal principal)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % L2ThetaY(:,1)=(Df1./(fsigma.^2));
% % % L2ThetaY(:,2)=(Df2./(fsigma.^2));
% % % L2ThetaY(:,3)=2*(Ds1.*residuo./(fsigma.^3));
% % % L2ThetaY(:,4)=2*(Ds2.*residuo./(fsigma.^3));
% % % %
% % % epsi = 0.0001;
% % % i = 250;
% % % j = 2;
% % % y(i) = y(i)*(1+epsi);
% % % p(j) = p(j)*(1+epsi);
% % % f1 = loglikelihood (p,x,y,ID);
% % % y(i) = y(i)/(1+epsi);
% % % p(j) = p(j)/(1+epsi);
% % % y(i) = y(i)*(1-epsi);
% % % p(j) = p(j)*(1-epsi);
% % % f2 = loglikelihood (p,x,y,ID);
% % % y(i) = y(i)/(1-epsi);
% % % p(j) = p(j)/(1-epsi);
% % % y(i) = y(i)*(1+epsi);
% % % p(j) = p(j)*(1-epsi);
% % % f3 = loglikelihood (p,x,y,ID);
% % % y(i) = y(i)/(1+epsi);
% % % p(j) = p(j)/(1-epsi);
% % % y(i) = y(i)*(1-epsi);
% % % p(j) = p(j)*(1+epsi);
% % % f4 = loglikelihood (p,x,y,ID);
% % % y(i) = y(i)/(1-epsi);
% % % p(j) = p(j)/(1+epsi);
% % % derivada = (f1-f3-f4+f2)/(4*y(i)*epsi*p(j)*epsi);
% % % [derivada L2ThetaY(i,j)]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Comprobacion de derivadas segundas (fuera de la diagonal principal)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % switch ID,
% % %     case 1
% % % %       Model 1: fmu = p1+p2*x
% % % %                fsigma = p3+p4*x
% % %         fsigma = p(3)+p(4)*x;
% % %         fmu = p(1)+p(2)*x;
% % %         Df1 = ones(n,1);
% % %         Df2 = x;
% % %         Ds1 = ones(n,1);
% % %         Ds2 = x;
% % %         Dfx = p(2); 
% % %         Dsx = p(4);
% % %         D2fx1 = zeros(n,1);
% % %         D2fx2 = ones(n,1);
% % %         D2sx3 = zeros(n,1);
% % %         D2sx4 = ones(n,1);
% % %     case 2
% % % %       Model 2: fmu = p1+p2*x
% % % %                fsigma = p3+p4*sqrt(x)  
% % %         fsigma = p(3)+p(4)*sqrt(x);
% % %         fmu = p(1)+p(2)*x;
% % %         Df1 = ones(n,1);
% % %         Df2 = x;
% % %         Ds1 = ones(n,1);
% % %         Ds2 = sqrt(x);
% % %         Dfx = p(2); 
% % %         Dsx = p(4)*x.^(-1/2)/2;
% % %         D2fx1 = zeros(n,1);
% % %         D2fx2 = ones(n,1);
% % %         D2sx3 = zeros(n,1);
% % %         D2sx4 = x.^(-1/2)/2;
% % %     case 3
% % % %       Model 3: fmu = p1*x^p2
% % % %                fsigma = p3+p4*x 
% % %         fsigma = p(3)+p(4)*x;
% % %         fmu = p(1)*x.^p(2);
% % %         Df1 = (x.^p(2));
% % %         Df2 = (p(1)*x.^p(2)).*log(x);
% % %         Ds1 = ones(n,1);
% % %         Ds2 = x;
% % %         Dfx = p(2)*p(1)*x.^(p(2)-1); 
% % %         Dsx = p(4);
% % %         D2fx1 = p(2)*x.^(p(2)-1);
% % %         D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
% % %         D2sx3 = zeros(n,1);
% % %         D2sx4 = ones(n,1);
% % %     case 4
% % % %       Model 4: fmu = p1*x^p2
% % % %                fsigma = p3+p4*sqrt(x)
% % %         fsigma = p(3)+p(4)*sqrt(x);
% % %         fmu = p(1)*x.^p(2);
% % %         Df1 = (x.^p(2));
% % %         Df2 = (p(1)*x.^p(2)).*log(x);
% % %         Ds1 = ones(n,1);
% % %         Ds2 = sqrt(x);
% % %         Dfx = p(2)*p(1)*x.^(p(2)-1); 
% % %         Dsx = p(4)*x.^(-1/2)/2;
% % %         D2fx1 = p(2)*x.^(p(2)-1);
% % %         D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
% % %         D2sx3 = zeros(n,1);
% % %         D2sx4 = x.^(-1/2)/2;
% % %     case 5
% % % %       Model 5: fmu = p(1)*x.^p(2)
% % % %                fsigma = p(3)*(1-exp(-p(4)*x));
% % %         fsigma = p(3)*(1-exp(-p(4)*x));
% % %         fmu = p(1)*x.^p(2);   
% % %         Df1 = (x.^p(2));
% % %         Df2 = (p(1)*x.^p(2)).*log(x);
% % %         Ds1 = (1-exp(-p(4)*x));
% % %         Ds2 = p(3)*exp(-p(4)*x).*x;
% % %         Dfx = p(2)*p(1)*x.^(p(2)-1); 
% % %         Dsx = p(4)*p(3)*exp(-p(4)*x);
% % %         D2fx1 = p(2)*x.^(p(2)-1);
% % %         D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
% % %         D2sx3 = p(4)*exp(-p(4)*x);
% % %         D2sx4 =p(3)*(1-p(4)*x).*exp(-p(4)*x);
% % %     case 6
% % % %       Model 6: fmu = p(1)*x.^p(2)
% % % %                fsigma = p(3)*x.^p(4);
% % %         fsigma = p(3)*x.^p(4);
% % %         fmu = p(1)*x.^p(2);   
% % %         Df1 = (x.^p(2));
% % %         Df2 = (p(1)*x.^p(2)).*log(x);
% % %         Ds1 = (x.^p(4));
% % %         Ds2 = (p(3)*x.^p(4)).*log(x);
% % %         Dfx = p(2)*p(1)*x.^(p(2)-1); 
% % %         Dsx = p(4)*p(3)*x.^(p(4)-1);
% % %         D2fx1 = p(2)*x.^(p(2)-1);
% % %         D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
% % %         D2sx3 = p(4)*x.^(p(4)-1);
% % %         D2sx4 =p(3)*x.^(p(4)-1).*(1+p(4)*log(x));
% % %     otherwise
% % %         disp('ID no valido');
% % % end
% % % L2ThetaX(:,1)=-(Df1.*Dfx./(fsigma.^2))-2*(Df1.*Dsx.*residuo./(fsigma.^3))+(D2fx1.*residuo./(fsigma.^2));
% % % L2ThetaX(:,2)=-(Df2.*Dfx./(fsigma.^2))-2*(Df2.*Dsx.*residuo./(fsigma.^3))+(D2fx2.*residuo./(fsigma.^2));
% % % L2ThetaX(:,3)=(Ds1.*Dsx./(fsigma.^2))-(D2sx3./(fsigma))-2*(Ds1.*Dfx.*residuo./(fsigma.^3))-3*(Ds1.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx3.*residuo.^2./(fsigma.^3));
% % % L2ThetaX(:,4)=(Ds2.*Dsx./(fsigma.^2))-(D2sx4./(fsigma))-2*(Ds2.*Dfx.*residuo./(fsigma.^3))-3*(Ds2.*Dsx.*residuo.^2./(fsigma.^4))+(D2sx4.*residuo.^2./(fsigma.^3));
% % % %
% % % epsi = 0.0001;
% % % i = 250;
% % % j = 4;
% % % x(i) = x(i)*(1+epsi);
% % % p(j) = p(j)*(1+epsi);
% % % f1 = loglikelihood (p,x,y,ID);
% % % x(i) = x(i)/(1+epsi);
% % % p(j) = p(j)/(1+epsi);
% % % x(i) = x(i)*(1-epsi);
% % % p(j) = p(j)*(1-epsi);
% % % f2 = loglikelihood (p,x,y,ID);
% % % x(i) = x(i)/(1-epsi);
% % % p(j) = p(j)/(1-epsi);
% % % x(i) = x(i)*(1+epsi);
% % % p(j) = p(j)*(1-epsi);
% % % f3 = loglikelihood (p,x,y,ID);
% % % x(i) = x(i)/(1+epsi);
% % % p(j) = p(j)/(1-epsi);
% % % x(i) = x(i)*(1-epsi);
% % % p(j) = p(j)*(1+epsi);
% % % f4 = loglikelihood (p,x,y,ID);
% % % x(i) = x(i)/(1-epsi);
% % % p(j) = p(j)/(1+epsi);
% % % derivada = (f1-f3-f4+f2)/(4*x(i)*epsi*p(j)*epsi);
% % % [derivada L2ThetaX(i,j)]
% % % disp('')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
