function PlotEmpiricalDist(Hs_S,Hs_SAT,Hs_Scal,Hsup,Hslo,Hlim,n,nivconf,idioma,var_name,var_unit,gcax)

if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! PlotEmpiricalDist with axes'); end
set(gcf,'CurrentAxes',gcax)
if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name=''; end
if ~exist('var_unit','var'), var_unit=''; end

texto=amevaclass.amevatextoidioma(idioma);auxt='';
eval(['auxt=' texto('ircalPlotLegend') ';']);
hkk=plot(gcax,Hlim);%seleccion de los ticks de Hlim
set(gcax,'dataaspectratio',[1 1 1],'plotboxaspectratiomode','auto');
vkk = get(gcax,'ylim');
rticks = length(get(gcax,'ytick'))-1;
delete(hkk);
if rticks > 9   % see if we can reduce the number
    if rem(rticks,2) == 0
        rticks = rticks/2;
    elseif rem(rticks,3) == 0
        rticks = rticks/3;
    end
end
rinc = (vkk(2)-vkk(1))/rticks;
vector=(vkk(1):rinc:vkk(2));

P=[0.0001 0.05 0.5 0.95 0.995 0.9995 0.9999];

Prob=(cumsum(ones(1,n))-0.5)./n; % Hazen

xx1IC = [-log(-log(Prob)) -log(-log(sort(Prob,'descend')))];
yy1IC = [sort(Hsup); sort(Hslo,'descend')]';

fill(xx1IC,yy1IC,[1 0.6 0.6],'LineStyle','none')%,'handlevisibility','off')
hold on;
plot(gcax,-log(-log(Prob)),sort(Hs_SAT),'b','linewidth',1.5);
plot(gcax,-log(-log(Prob)),sort(Hs_S),'g','linewidth',1.5);
plot(gcax,-log(-log(Prob)),sort(Hs_Scal),'r','linewidth',1.5);

legend(['CI^{',num2str((1-nivconf)*100),'%}'],auxt{1},auxt{2},auxt{3},4,'Location','SouthEast');
set(gcax,'fontsize',8)

ylim(gcax,Hlim)
xlim(gcax,[-log(-log(P(1))) -log(-log(P(end)))])
set(gcax,'xtick', -log(-log(P(2:end-1))))

cont=0;
for II=2:length(P)-1
    cont=cont+1;
    strArray(cont) = java.lang.String([num2str(100*P(II)) '%']);
end
cellArray = cell(strArray);
set(gcax,'xticklabel', cellArray)
set(gcax,'ytick', vector)

for I=2:length(vector)-1
    plot(gcax,[-log(-log(P(1))) -log(-log(P(end)))],vector(I)*[1 1],':k','linewidth',1);
end

for I=2:length(P)-1
    plot(gcax,-log(-log(P(I)))*[1 1],[vector(1) vector(end)],':k','linewidth',1);
end
vv=axis;

Lab=labelsameva(var_name,var_unit);%Labels

xlabel(gcax,texto('plotempdistXLabel'),'fontsize',10);
ylabel(gcax,Lab,'fontsize',10)
title(gcax,texto('plotempdistTitulo'),'fontsize',11,'fontweight','b','verticalalignment','baseline')
text((vv(1)+diff(vv(1:2))*0.03),(vv(4)-diff(vv(3:4))*0.05),texto('plotempdistTexto'),'fontsize',8,'fontweight','b','Parent',gcax);%,'HorizontalAlignment','center');


end
