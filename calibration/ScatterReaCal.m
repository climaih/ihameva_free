function [polycoef,hh]=ScatterReaCal(XXi,YYi,MB,SB,Hlim,isdire,var_name,var_unit,IR,fs,scattipo,nx)
%
if ~exist('Hlim','var') || isempty(Hlim), Hlim=[0 ceil(max(max(XXi),max(YYi)))]; end
if ~exist('isdire','var') || isempty(isdire), isdire=false; end
if ~exist('var_name','var'), var_name=''; end
if ~exist('var_unit','var'), var_unit=''; end
if nargin<10, % IF NO FONT SIZE, INITIALIZE IT
    fs = 11;
end
if nargin<11, % IF NO SCATTER TIPO, INITIALIZE IT
    scattipo = 'OC';
end

%elimino los nans
nansposi=find(isnan(XXi) | isnan(YYi));
XXi(nansposi)=[];
YYi(nansposi)=[];
%nuevas variables
XX=XXi;YY=YYi;
N=length(XX);
incx=max(XX)/100;incy=max(YY)/100;
%seleccion de colores de los scatter
map = repmat(linspace(0.3,0.76,12)',1,3);%grises 0.3
if exist('IR','var') && ~isempty(IR) && strcmp(IR(2),'R'), %Reanalisis-verde;
    map = [ 0.1059    0.3098    0.2078; 0.0706    0.4026    0.1386;
        0.0353    0.4954    0.0693;      0    0.5882         0;
        0.1176    0.6912    0.1176; 0.2353    0.7941    0.2353;
        0.3529    0.8971    0.3529; 0.4706    1.0000    0.4706;
        0.5765    0.9853    0.5784; 0.6824    0.9706    0.6863;
        0.7882    0.9559    0.7941; 0.8941    0.9412    0.9020];
elseif exist('IR','var') && ~isempty(IR) && strcmp(IR(2),'C'), %Calibrado-rojo; 0.6000
    map = [ 0.6000    0.2000         0; 0.7333    0.1333         0;
        0.8667    0.0667         0; 1.0000         0         0;
        1.0000    0.1578    0.1578; 1.0000    0.3157    0.3157;
        1.0000    0.4735    0.4735; 1.0000    0.6314    0.6314;
        0.9735    0.7088    0.6990; 0.9471    0.7863    0.7667;
        0.9206    0.8637    0.8343; 0.8941    0.9412    0.9020];
end
% PARA los ajuste de calibracion antes de modificar XX o YY si se usan
if exist('IR','var') && exist('var_name','var') && ~isempty(IR) && ~isempty(var_name)
    % QQ-PLOT. calcular los cuantiles equiespaciados en probabilidad
    [y,pr,YpB]  =DetClas2N(XX,30,0.01,0.99999,10);
    [y,pr,YpC,p]=DetClas2N(YY,30,0.01,0.99999,10);
end

% Modificaciones si es direcion
if isdire==1 && not(strcmp(scattipo,'OC')),
    naux=11;
    dum1=find(YY<=0+naux);
    dum2=find(YY>=360-naux);
    YY1=YY(dum1)+360;
    YY2=YY(dum2)-360;
    XX1=XX(dum1);
    XX2=XX(dum2);
    XX=[XX;XX1;XX2];
    YY=[YY;YY1;YY2];
    N=length(XX);
    incx=max(XX)/100;incy=max(YY)/100;
end
%SCATTER
Minimo=Hlim(1);
Maximo=Hlim(2);
VH=min(min(XX),Minimo):incx:max(max(XX),Maximo);
VD=min(min(YY),Minimo):incy:max(max(YY),Maximo);

% Inicio SCATTER Melisa MM, ATomas AT, Simple scatter %%%%
if strcmp(scattipo,'MM')
    scatterqqm(XX,YY,VH,VD,map);
elseif strcmp(scattipo,'AT')
    scatterqqa(XX,YY,VH,VD,map);
else
    plot(XX,YY,'.','MarkerEdgeColor',map(end-2,:),'MarkerSize',1.4);hold on;%,'MarkerSize',4.5
end
% Fin SCATTER Melisa MM, ATomas AT, Simple scatter %%%%%%

if Maximo>0
    if ~isempty(var_unit) && ~isempty(var_unit(1)) && ~isempty(var_unit(2)) && isequal(cell2mat(var_unit(1)),cell2mat(var_unit(2)))
        plot([Minimo Maximo],[Minimo Maximo],'-','Color',[.1 .1 .1]);
        axis([Minimo Maximo Minimo Maximo]);
        axis square;
    else
        axis([0 max(XXi) 0 max(YYi)]);
    end
end
%grid on
box on


if exist('IR','var') && exist('var_name','var') && ~isempty(IR) && ~isempty(var_name) %para las figuras de calibracion sin ajustes
    xlabel(['{',var_name{1},'}^',IR{1},' (',var_unit{1},')'],'fontsize',fs+1,'fontweight','b');
    ylabel(['{',var_name{2},'}^',IR{2},' (',var_unit{2},')'],'fontsize',fs+1,'fontweight','b');
    plot(YpB,YpC,'--k');
    db=find(p<=0.9);
    dex=find(p>0.9);
    plot(YpB(db),YpC(db),'ok','Markersize',4,'Markerfacecolor','k'); hold on
    plot(YpB(dex),YpC(dex),'dk','Markersize',4,'Markerfacecolor','none');
elseif exist('IR','var') && exist('var_name','var') && isempty(IR) && ~isempty(var_name) %para las figuras de regimen medio con ajustes
    Labs=labelsameva(var_name,var_unit);
    xlabel(Labs(1),'fontsize',fs+1,'fontweight','b');
    ylabel(Labs(2),'fontsize',fs+1,'fontweight','b');
    %pintamos los polyfit, medias y std
    xsup=SB(:,4)+SB(:,2);
    xslw=SB(:,4)-SB(:,2);
    hh(1)=plot(MB(:,1),MB(:,2),'sk','Markersize',5,'Markerfacecolor','blue','Marker','square');hold on
    if exist('nx','var'), set(gca,'xtick',nx);set(gca,'xlim',[nx(1) nx(end)]); end
    xlm=get(gca,'xlim');xtl=get(gca,'xtick');
    ri=linspace(xlm(1),max(XX),100);
    py(1,:)=polyfit(MB(:,1),MB(:,2),3);
    if isdire==0
        plot(ri,polyval(py,ri),'-b','LineWidth',1.5);
        hh(2)=plot(SB(:,3),xsup,'ok','Markersize',4.5,'Markerfacecolor','r');
        plot(SB(:,3),xslw,'ok','Markersize',4.5,'Markerfacecolor','r');
        aux=find(xtl>SB(end,3));
        %ri=linspace(xlm(1),xtl(aux(1)),100);
        ri=linspace(min(XX),max(SB(:,3)),100);
        %plot(ri,polyval(polyfit(SB(:,3),xsup,2),ri),'--r','LineWidth',1.2);
        py(2,:)=polyfit(SB(:,3),SB(:,2),3);
        plot(ri,polyval(sum(py,1),ri),'--r','LineWidth',1.2);
        plot(ri,polyval(-diff(py,1),ri),'--r','LineWidth',1.2);
    elseif isdire==1,
        xsup(find(xsup>360))=xsup(find(xsup>360))-360;
        xslw(find(xslw<0))=360+xslw(find(xslw<0));
        hh(2)=plot(SB(:,3),xsup,'ok','Markersize',4.5,'Markerfacecolor','r');
        plot(SB(:,3),xslw,'ok','Markersize',4.5,'Markerfacecolor','r');
    else
        hh(2)=plot(SB(:,3),xsup,'ok','Markersize',4.5,'Markerfacecolor','r');
        plot(SB(:,3),xslw,'ok','Markersize',4.5,'Markerfacecolor','r');
    end
    hold on;
end
if nargout==1,
    polycoef=py;
elseif nargout==2,
    polycoef=py;
    
end

if length(Hlim)==2 && Hlim(2)>0
    vv=axis(gca);
    A=[vv(1)+diff(vv(1:2))*0.63 vv(1)+diff(vv(1:2))*0.63 vv(1)+diff(vv(1:2))*0.98 vv(1)+diff(vv(1:2))*0.98 vv(1)+diff(vv(1:2))*0.63];
    B=[vv(4)-diff(vv(3:4))*0.98 vv(4)-diff(vv(3:4))*0.76 vv(4)-diff(vv(3:4))*0.76 vv(4)-diff(vv(3:4))*0.98 vv(4)-diff(vv(3:4))*0.98];
    fill(A,B,[1 1 1],'linestyle','none');
    YT=get(gca, 'ytick');
    set(gca, 'xtick',YT);
    for I=1:length(YT)
        plot(gca,[YT(1)-2 YT(end)+2],YT(I)*[1 1],':k','linewidth',1);
        plot(gca,YT(I)*[1 1],[YT(1)-2 YT(end)+2],':k','linewidth',1);
    end
end


%% Melisa M Scatter
    function scatterqqm (XX_,YY_,VH,VD,map)
        colormap(map);
        % Calculo Frecuencias Distribucion Conjunta y PINTO los ptos con color asociado a densidad..
        disp('Pintando ScatterQQ.........');
        hold on;
        n = length(XX_);
        for i = 1:(length(VH)-1)
            d1 = (XX_ >= VH(i) & XX_ < VH(i+1));
            xx = XX_(d1);
            yy = YY_(d1);
            for k = 1:(length(VD)-1),
                d2 = (yy >= VD(k) & yy < VD(k+1));
                nd2 = sum(d2);
                if nd2 > 0,
                    xx2 = xx(d2);
                    yy2 = yy(d2);
                    zz = log(nd2/n);% densidad de probabilidad conjunta de Pxy:
                    scatter(xx2,yy2,10,ones(length(xx2),1).*zz,'filled','Marker','o','SizeData',3) ;
                end
            end
        end
        freezeColors; %buscar alternativas al freezeColors aqui se va el tiempo OJO OJO
    end
%% Scatter
    function scatterqqa (XX_,YY_,VH,VD,map)
        n = length(XX_);
        nvh = length(VH);
        nvd = length(VD);
        cant = zeros(nvh,nvd);
        NumeroHD = ones(1,n);
        for i = 1:(nvh-1)
            d1 = find(XX_>=VH(i) & XX_<=VH(i+1));
            D1 = YY_(d1);
            for k = 1:(nvd-1)
                d2 = find(D1>=VD(k) & D1<=VD(k+1));
                if isempty(d2)==0
                    cant(i,k) = length(d2)/n;
                    NumeroHD(d1(d2)) = length(d2)*ones(1,length(d2));
                end
            end
        end
        
        cant=cant.*100;
        [mc,nc]=size(cant);
        orden=sort(reshape(cant,mc*nc,1));
        dd=find(orden==0);
        cant(find(cant==0))=orden(dd(end)+1);
        [x_,y_]=meshgrid(VH,VD);
        pcolor(x_,y_,log(cant)');
        hold on;
        shading flat;
        col=repmat([1;1;1;1;1;1;.65;.70;.75;.80;.85;.90;.95;1],1,3);
        colormap(col);
        freezeColors;
        NHD=normaliza(log(NumeroHD),[0.4 1]);
        dum8=find(not(isnan(XX_)) & NHD'<0.8);
        if abs(map(1,1)-0.3)<0.001 %Por defecto grises
            NHD=repmat(NHD,3,1)';
        elseif abs(map(1,1)-0.1059)<0.001 %Calibration Rea Verdes
            NHD=[NHD(:) ones(length(NHD),1) NHD(:)];
        elseif abs(map(1,1)-0.6000)<0.001 %Calibration Cal Rojos
            NHD=[ones(length(NHD),1) NHD(:) NHD(:)];
        end
        for i=dum8'
            plot(XX_(i),YY_(i),'.','MarkerEdgeColor',NHD(i,:));%,'MarkerSize',4);
            hold on;
        end
        
    end

end