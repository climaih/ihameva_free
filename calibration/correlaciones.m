

function [R2, RMSE, BIAS,CORR,SI] = correlaciones (x,y)

    temp=isnan(x);
    d=find(temp==0);
    x1=x(d);
    y1=y(d);
    temp2=isnan(y1);
    d2=find(temp2==0);
    x2=x1(d2);
    y2=y1(d2);
    
    n=length(x2);
    
 %   R2=(sum(y2.^2)/sum(x2.^2)).^0.5; 
    R2=sum( (x2-mean(y2) ).^2 ) / sum( (y2-x2 ).^2 + (x2-mean(y2) ).^2 );
  
    RMSE= (1/n*sum((x2-y2).^2)).^0.5;
    
    BIAS= sum(y2-x2)./n;
    
    cc=corrcoef(x2,y2);
    CORR=cc(1,2);
    
    SI=RMSE/mean(x2);
    