function sal = rms(x,y)

sal = sqrt(sum((x-y).^2)/length(x));