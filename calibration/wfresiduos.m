function wfresiduos(rN,conf)

    aux = find(abs(rN)>norminv(1-(1-conf(end))/2,0,1));
    plot([1 length(rN)],norminv(1-(1-conf(end))/2,0,1)*ones(2,1),'-.','Color',[100 100 100]/255);
    hold on
    plot(aux,rN(aux),'o','Color',[0 0 0],'MarkerFaceColor',[0 0 0],'MarkerSize',2.5);
    h=legend('99.99%','Removed data','location','NorthEast');
    set(h,'FontSize',7)
    plot(rN,'Color',[100/255 149/255 237/255])
    plot([1 length(rN)],-norminv(1-(1-conf(end))/2,0,1)*ones(2,length(rN)),'r-.','Color',[100 100 100]/255)
    grid on
    xlim([0 length(rN)])
    title('Standarized Residuals','fontsize',11,'fontweight','bold');
    set(gca,'XTickLabel',[])
    ylabel('z','fontsize',9,'fontweight','bold');

end
