function InsReaCalPlot(Time,Hs_Ins,Hs_Rea,Hscal,Time_n,Hs_n,Hscal_n,Hsup,Hslo,Hsup_n,Hslo_n,Tipo,idioma,var_name,new_var_name,var_unit,tvr_name,gcax)

if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('Acthung! InsReaCalPlot with axes'); end
set(gcf,'CurrentAxes',gcax)
texto=amevaclass.amevatextoidioma(idioma);
axis(gcax,'xy'); auxt='';
if Tipo==0,
    eval(['auxt=' texto('ircalPlotLegend') ';']);
    if length(Hscal)==length(Hs_Ins),
        plot(gcax,Time,Hs_Ins,'g',Time,Hs_Rea,'b',Time,Hscal,'r');
        hold(gcax,'on');
        plot(gcax,Time,Hsup,'--k','Color',[0.2 0.2 0.2]);
        legend(gcax,auxt);
        plot(gcax,Time,Hslo,'--k','Color',[0.2 0.2 0.2]);
        hold(gcax,'off');
    else
        if length(Time)~=length(Hs_Ins), disp('Date vector must be the same lengths'); return; end
        plot(gcax,Time,Hs_Ins,'g',Time,Hs_Rea,'b');
        legend(gcax,auxt{1},auxt{2});
    end
    grid on;
    taux=datevec(Time(1));
    if taux(1)>1947
        set(gcax,'XTick',linspace(Time(1),Time(end),12));
        datetick(gcax,'x','mmmyy','keepticks');
    end
    ylabel(gcax,[var_name,' (',var_unit,')']);
    xlabel(gcax,tvr_name);
elseif Tipo==1 && length(Hs_n)>1,
    eval(['auxt=' texto('ircalPlotLegNewSerie') ';']);
    plot(gcax,Time_n,Hs_n,'b',Time_n,Hscal_n,'r');
    legend(gcax,auxt{1},auxt{2});
    if length(Hsup_n)>1,
        hold(gcax,'on');
        plot(gcax,Time_n,Hsup_n,'--k','Color',[0.2 0.2 0.2]);
        legend(gcax,auxt);
        plot(gcax,Time_n,Hslo_n,'--k','Color',[0.2 0.2 0.2]);
        hold(gcax,'off');
    end
    grid on;
    taux=datevec(Time_n(1));
    if taux(1)>1947
        set(gcax,'XTick',linspace(Time_n(1),Time_n(end),12));
        datetick(gcax,'x','mmmyy','keepticks');
    end
    ylabel(gcax,[new_var_name,',  ',new_var_name,'^c (',var_unit,')']);
    xlabel(gcax,tvr_name);
end

end
