function [f,flog,var,residuo,Hscal]= fobjNLP(Hs_B,Hs_S,Dir,thetaj,x)

np = length(thetaj);
n = length(Dir);

a = [x(1:np-1); x(1)];
b = [x(np:2*np-2); x(np)];
d = [x(2*np-1:end); x(2*np-1)];

%   Calculo los incrementos angulares y los almaceno en h
h = (thetaj(2:np)-thetaj(1:np-1));

C = (zeros(np));
TIA = (zeros(np,1));
TIB = (zeros(np,1));
TID = (zeros(np,1));

C(1,1)=2*h(1);
C(1,2)=h(1);
for i=1:np-2,
    C(i+1,i)=h(i);
    C(i+1,i+1)=2*(h(i)+h(i+1));
    C(i+1,i+2)=h(i+1);
end
C(np,np)=2*h(np-1);
C(np,np-1)=h(np-1);

TIA(1)=3*((a(2)-a(1))/h(1)-(a(1)-a(np-1))/h(np-1))/2;
for i=2:np-1,
    TIA(i)=3*((a(i+1)-a(i))/h(i)-(a(i)-a(i-1))/h(i-1));
end
% TIA(np)=3*((a(2)-a(1))/h(1)-(a(1)-a(np-1))/h(np-1))/2;
TIA(np)=TIA(1);
%   Solucion del sistema
yaj = (C\TIA);
xaj = (a(2:np)-a(1:np-1))./h(1:np-1)-h(1:np-1).*(2*yaj(1:np-1)+yaj(2:np))/3;
zaj = (yaj(2:np)-yaj(1:np-1))./(3*h);


TIB(1)=3*((b(2)-b(1))/h(1)-(b(1)-b(np-1))/h(np-1))/2;
for i=2:np-1,
    TIB(i)=3*((b(i+1)-b(i))/h(i)-(b(i)-b(i-1))/h(i-1));
end
% TIB(np)=3*((b(2)-b(1))/h(1)-(b(1)-b(np-1))/h(np-1))/2;
TIB(np)=TIB(1);
%   Solucion del sistema
ybj = (C\TIB);
xbj = (b(2:np)-b(1:np-1))./h(1:np-1)-h(1:np-1).*(2*ybj(1:np-1)+ybj(2:np))/3;
zbj = (ybj(2:np)-ybj(1:np-1))./(3*h);

TID(1)=3*((d(2)-d(1))/h(1)-(d(1)-d(np-1))/h(np-1))/2;
for i=2:np-1,
    TID(i)=3*((d(i+1)-d(i))/h(i)-(d(i)-d(i-1))/h(i-1));
end
TID(np)=TID(1);
%   Solucion del sistema
ydj = (C\TID);
xdj = (d(2:np)-d(1:np-1))./h(1:np-1)-h(1:np-1).*(2*ydj(1:np-1)+ydj(2:np))/3;
zdj = (ydj(2:np)-ydj(1:np-1))./(3*h);

A = [a [xaj;0] yaj [zaj;0]];
B = [b [xbj;0] ybj [zbj;0]];
D = [d [xdj;0] ydj [zdj;0]];
%   Calculo el resto de los parametros

Pos = zeros(n,1);
%   Esto vale si los datos estan ordenados o desordenados

for i = 1:n  
    jref = 1;
    for j = jref:np-1, 
        if Dir(i)>=thetaj(j) && Dir(i)<=thetaj(j+1),
            Pos(i) = j;
            break;
%         else
%              jref = j;
        end
    end
end

%   Completar todos los parametros
% A
% B

[f c] = size(A);
[f1 c1] = size(B);
if f~=f1 | c~=c1,
    error('Dimensiones de A y B no concordantes')
end

Hscal = zeros(n,1);
for i = 1:n,
    ai = A(Pos(i),1);
    for j = 2:c,
        ai = ai + A(Pos(i),j)*(Dir(i)-thetaj(Pos(i)))^(j-1);
    end
    bi = B(Pos(i),1);
    for j = 2:c,
        bi = bi + B(Pos(i),j)*(Dir(i)-thetaj(Pos(i)))^(j-1);
    end
    di = D(Pos(i),1);
    for j = 2:c,
        di = di + D(Pos(i),j)*(Dir(i)-thetaj(Pos(i)))^(j-1);
    end
    if Hs_S(i)>di,
        Hscal(i) = ai*Hs_S(i)^bi;  
    else
        aip = ai*di^(bi-1);
        Hscal(i) = aip*Hs_S(i); 
    end
end

residuo = Hs_B-Hscal;
f = sum((residuo.^2));

if nargout>1
    var = f/n;
    flog = -n*log(2*pi*var)/2-f/(2*var);
end
