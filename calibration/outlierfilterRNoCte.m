function [id,p,rN,fmu,upperx,lowerx,Derivatives,fval,exitflag,output,lambda,grad,hessian,residuo,fsigma,Omega,wJ] = outlierfilterRNoCte (x,y,alpha,ID,conf,W,pini)
%   Funtion outlierfilterRNoCte automatically detects the presence of
%   outliers for a confidence level alpha and considering different
%   regression and residual variance models identified by ID. The methods
%   frist estimates the optimal parameters of the regression model
%   maximizing the log-likelihood function, then calculates the normalized
%   residuals and finally locates data which can be considered as outlier.
%
%   INPUT:
%
%   y-> Response variable (altimetry data)
%   x->Predictor variable (reanalysis data)
%   alpha-> Confidence level
%   ID-> Model identifier
%       Model 1: fmu = p1+p2*x
%                fsigma = p3+p4*x
%       Model 2: fmu = p1+p2*x
%                fsigma = p3+p4*sqrt(x)
%       Model 3: fmu = p1*x^p2
%                fsigma = p3+p4*x
%       Model 4: fmu = p1*x^p2
%                fsigma = p3+p4*sqrt(x)
%       Model 5: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*(1-exp(-p(4)*x));
%       Model 6: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*x.^p(4);
%
%   OUTPUT:
%
%   id-> Positions of the data which can be considered as outliers
%   p-> Optimal estimates of the regression parameters
%

if nargin<6,
   W = ones(size(x)); 
end
if nargin<7,
   pini = []; 
end

%   Dimensions
    [n m] = size(x);
    if min(n,m)>1,
        error('Data x must be a row or column vector')
    elseif n==1,
        %   Transform data to a column vector
        x = x';
        n = m;
        m = 1;
    end
    [n1 m1] = size(y);
    if min(n1,m1)>1,
        error('Data y must be a row or column vector')
    elseif n1==1,
        %   Transform data to a column vector
        y = y';
        n1 = m1;
        m1 = 1;
    end
    if n1~=n,
       error('Data x and y must be consistent') 
    end

%   Initial estimates for the parameters
X = [ones(n,1) x];
%   Solucion de las ecuaciones normales
L = chol(X'*X,'lower'); 
Xy = X'*y;
%   Parameter estimates with the linear model
beta = L'\(L\Xy);
%   Residual standard deviation
stde = std(y-X*beta);
if isempty(pini),
    switch ID
        case {1,2}
    %       Model 1: fmu = p1+p2*x
    %                fsigma = p3+p4*x
    %       Model 2: fmu = p1+p2*x
    %                fsigma = p3+p4*sqrt(x)
            %   Initial values
            pini = zeros(4,1);
            pini(1) = beta(1);
            pini(2) = beta(2);
            pini(3) = stde;
            pini(4) = 0;
        case {3,4}
    %       Model 3: fmu = p1*x^p2
    %                fsigma = p3+p4*x
    %       Model 4: fmu = p1*x^p2
    %                fsigma = p3+p4*sqrt(x)
            %   Initial values
            pini = zeros(4,1);
            pini(1) = 1;
            pini(2) = 1;
            pini(3) = stde;
            pini(4) = 0;
         case {5,6}
    %       Model 5: fmu = p(1)*x.^p(2)
    %                fsigma = p(3)*(1-exp(-p(4)*x));
    %       Model 6: fmu = p(1)*x.^p(2)
    %                fsigma = p(3)*x.^p(4);
            %   Initial values
            pini = zeros(4,1);
            pini(1) = 1;
            pini(2) = 1;
            pini(3) = stde;
            pini(4) = 1;  
        otherwise
            disp('ID no valido');
    end
end

%   Parameter estimation using the maximum likelihood method
[p,fval,exitflag,output,lambda,grad,hessian,residuo]=  optiparamhessiancal (x,y,pini,ID,W);

%   LU decomposition of the Information matrix
[LX,UX,PX] = lu(hessian);
%   Inverse
invI0 = (UX)\(LX\(PX*eye(length(p))));
%    Standard deviation
stdpara = sqrt(diag(invI0));

%   Bounds
% upperx = p+norminv(1-(1-alpha)/2,0,1)*stdpara;
% lowerx = p-norminv(1-(1-alpha)/2,0,1)*stdpara;
upperx = p+tinv(1-(1-alpha)/2,length(x)-length(p)-1)*stdpara;
lowerx = p-tinv(1-(1-alpha)/2,length(x)-length(p)-1)*stdpara;

%   Second order derivatives with respecto to data and parameters
switch ID,
    case 1
%       Model 1: fmu = p1+p2*x
%                fsigma = p3+p4*x
        fsigma = p(3)+p(4)*x;
        fmu = p(1)+p(2)*x;
        Df1 = ones(n,1);
        Df2 = x;
        Ds1 = ones(n,1);
        Ds2 = x;
        Dfx = p(2); 
        Dsx = p(4);
        D2fx1 = zeros(n,1);
        D2fx2 = ones(n,1);
        D2sx3 = zeros(n,1);
        D2sx4 = ones(n,1);
    case 2
%       Model 2: fmu = p1+p2*x
%                fsigma = p3+p4*sqrt(x)  
        fsigma = p(3)+p(4)*sqrt(x);
        fmu = p(1)+p(2)*x;
        Df1 = ones(n,1);
        Df2 = x;
        Ds1 = ones(n,1);
        Ds2 = sqrt(x);
        Dfx = p(2); 
        Dsx = p(4)*x.^(-1/2)/2;
        D2fx1 = zeros(n,1);
        D2fx2 = ones(n,1);
        D2sx3 = zeros(n,1);
        D2sx4 = x.^(-1/2)/2;
    case 3
%       Model 3: fmu = p1*x^p2
%                fsigma = p3+p4*x 
        fsigma = p(3)+p(4)*x;
        fmu = p(1)*x.^p(2);
        Df1 = (x.^p(2));
        Df2 = (p(1)*x.^p(2)).*log(x);
        Ds1 = ones(n,1);
        Ds2 = x;
        Dfx = p(2)*p(1)*x.^(p(2)-1); 
        Dsx = p(4);
        D2fx1 = p(2)*x.^(p(2)-1);
        D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
        D2sx3 = zeros(n,1);
        D2sx4 = ones(n,1);
    case 4
%       Model 4: fmu = p1*x^p2
%                fsigma = p3+p4*sqrt(x)
        fsigma = p(3)+p(4)*sqrt(x);
        fmu = p(1)*x.^p(2);
        Df1 = (x.^p(2));
        Df2 = (p(1)*x.^p(2)).*log(x);
        Ds1 = ones(n,1);
        Ds2 = sqrt(x);
        Dfx = p(2)*p(1)*x.^(p(2)-1); 
        Dsx = p(4)*x.^(-1/2)/2;
        D2fx1 = p(2)*x.^(p(2)-1);
        D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
        D2sx3 = zeros(n,1);
        D2sx4 = x.^(-1/2)/2;
    case 5
%       Model 5: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*(1-exp(-p(4)*x));
        fsigma = p(3)*(1-exp(-p(4)*x));
        fmu = p(1)*x.^p(2);   
        Df1 = (x.^p(2));
        Df2 = (p(1)*x.^p(2)).*log(x);
        Ds1 = (1-exp(-p(4)*x));
        Ds2 = p(3)*exp(-p(4)*x).*x;
        Dfx = p(2)*p(1)*x.^(p(2)-1); 
        Dsx = p(4)*p(3)*exp(-p(4)*x);
        D2fx1 = p(2)*x.^(p(2)-1);
        D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
        D2sx3 = p(4)*exp(-p(4)*x);
        D2sx4 =p(3)*(1-p(4)*x).*exp(-p(4)*x);
    case 6
%       Model 6: fmu = p(1)*x.^p(2)
%                fsigma = p(3)*x.^p(4);
        fsigma = p(3)*x.^p(4);
        fmu = p(1)*x.^p(2);   
        Df1 = (x.^p(2));
        Df2 = (p(1)*x.^p(2)).*log(x);
        Ds1 = (x.^p(4));
        Ds2 = (p(3)*x.^p(4)).*log(x);
        Dfx = p(2)*p(1)*x.^(p(2)-1); 
        Dsx = p(4)*p(3)*x.^(p(4)-1);
        D2fx1 = p(2)*x.^(p(2)-1);
        D2fx2 = p(1)*x.^(p(2)-1).*(1+p(2)*log(x));
        D2sx3 = p(4)*x.^(p(4)-1);
        D2sx4 =p(3)*x.^(p(4)-1).*(1+p(4)*log(x));
    otherwise
        disp('ID no valido');
end


if ~isempty(find(fsigma<0)),
    error('La varianza de cada dato ha de ser positiva')
end

%   Second Order Cross Derivatives
D2etay(:,1)=W.*(Df1./(fsigma.^2));
D2etay(:,2)=W.*(Df2./(fsigma.^2));
D2etay(:,3)=2*W.*(Ds1.*residuo./(fsigma.^3));
D2etay(:,4)=2*W.*(Ds2.*residuo./(fsigma.^3));

D2etax(:,1)=-W.*(Df1.*Dfx./(fsigma.^2))-2*W.*(Df1.*Dsx.*residuo./(fsigma.^3))+W.*(D2fx1.*residuo./(fsigma.^2));
D2etax(:,2)=-W.*(Df2.*Dfx./(fsigma.^2))-2*W.*(Df2.*Dsx.*residuo./(fsigma.^3))+W.*(D2fx2.*residuo./(fsigma.^2));
D2etax(:,3)=W.*(Ds1.*Dsx./(fsigma.^2))-W.*(D2sx3./(fsigma))-2*W.*(Ds1.*Dfx.*residuo./(fsigma.^3))-3*W.*(Ds1.*Dsx.*residuo.^2./(fsigma.^4))+W.*(D2sx3.*residuo.^2./(fsigma.^3));
D2etax(:,4)=W.*(Ds2.*Dsx./(fsigma.^2))-W.*(D2sx4./(fsigma))-2*W.*(Ds2.*Dfx.*residuo./(fsigma.^3))-3*W.*(Ds2.*Dsx.*residuo.^2./(fsigma.^4))+W.*(D2sx4.*residuo.^2./(fsigma.^3));

%   Derivatives of eta (optimal parameters) with respect to y
DetaY = (invI0*D2etay')';
%   Derivatives of eta (optimal parameters) with respect to x
DetaX = (invI0*D2etax')';

DmuY=sum([Df1 Df2]'.*DetaY(:,1:2)')';
DsigmaY = sum([Ds1 Ds2]'.*DetaY(:,3:4)')';
DmuX=sum([Df1 Df2]'.*DetaX(:,1:2)')';
DsigmaX = sum([Ds1 Ds2]'.*DetaX(:,3:4)')';

Derivatives = [DmuY DsigmaY DmuX DsigmaX];

%   Diagonal of the sensitivity matrix
S=ones(length(x),1)-DmuY;
%   All sensitivity matrix
% % % Saux = eye(length(x))-[Df1 Df2]*DetaY(:,1:2)';

Omega=(S).^2.*(fsigma).^2;

rN=residuo./sqrt(Omega);

% rNadj = mle(rN, 'dist','tlocationscale');
% rNStudent = (rN-rNadj(1))/rNadj(2);
% id = find(abs(rNStudent)>tinv(1-(1-alpha)/2,rNadj(3)));

% rNadj = mle(rN, 'dist','tlocationscale');
% rNStudent = (rN-rNadj(1))/rNadj(2);
% id = find(abs(rNStudent)>tinv(1-1/(length(x)+1),rNadj(3)));

for i=1:length(conf),
    id{i} = find(abs(rN)>norminv(1-(1-conf(i))/2,0,1));
end

wJ = sum([Df1 Df2]'.*DetaY(:,1:2)')';

% deltabeta =
% -DetaY.*repmat(y,1,size(DetaY,2))-DetaX.*repmat(x,1,size(DetaX,2));
% deltabeta = -DetaY.*repmat(y-fmu,1,size(DetaY,2))-DetaX.*repmat(x,1,size(DetaX,2));


%   Aproximacion lineal de la matrix de diseno
% X = [Df1 Df2 Ds1 Ds2];

% lambdast = sum((DetaY.*DetaY)')';
% lambda=DmuY;

% deltabeta = DetaY.*repmat((fmu-y)./S,1,4);
% deltabeta = DetaY(:,1:2).*repmat((fmu-y),1,2);

% traces = trace(invI0)+sum(DetaY.*DetaY,2)./S;

% me = mean(X,2);
% varY = var(y);
% WSSD = ((X-repmat(me,1,4))*p).^2/varY;

% id = find(abs(rN)>norminv(0.95,0,1))

% id = find(abs(rN)>norminv(1-(1-alpha)/2,0,1));
%  tinv(1-(1-alpha)/2,(length(x)-length(p)));



