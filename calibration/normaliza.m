function M=normaliza(M,rM)

M(:)=(rM(2)-rM(1))*(M(:)-min(M(:)))./(max(M(:))-min(M(:)))+rM(1);

end
