function ParamTable(np,aj,bj,dj,ajlo,bjlo,djlo,thetaj,qlim,idioma,gcax)

    if ~exist('idioma','var'), idioma='eng'; end
	if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
	if ~ishandle(gcax), disp('WARNING! ParamTable with axes'); end
	set(gcf,'CurrentAxes',gcax)
    
    if qlim==0, 
        x=[0.2 0.24 0.6 0.64];
    else
        x=[0.1 0.14 0.4 0.44 0.7 0.74];
    end
    box(gcax,'on'); %   Eliminacion de los label
    set(gcax,'XTick',[])
    set(gcax,'XTickLabel',[])
    set(gcax,'YTick',[])
    set(gcax,'YTickLabel',[])
   
    text(0.075,0.95,'\theta','FontSize',11,'FontWeight','bold');
    hold(gcax,'on');
    if strcmp(idioma,'esp') %En Esp
        text(x(2),0.95,'a \pm IC^{95%}','FontSize',11,'FontWeight','bold')
        text(x(4),0.95,'b \pm IC^{95%}','FontSize',11,'FontWeight','bold')
        if qlim~=0, text(x(6),0.95,'c \pm IC^{95%}','FontSize',11,'FontWeight','bold'); end
    elseif strcmp(idioma,'eng') %En Ingles
        text(x(2),0.95,'a \pm CI^{95%}','FontSize',11,'FontWeight','bold')
        text(x(4),0.95,'b \pm CI^{95%}','FontSize',11,'FontWeight','bold')
        if qlim~=0, text(x(6),0.95,'c \pm CI^{95%}','FontSize',11,'FontWeight','bold'); end
    end

    plot(gcax,[0 1],[1 1],'k','linewidth',0.4);
    plot(gcax,[0 1],0.905*[1 1],'k','linewidth',0.4);
    plot(gcax,[x(1) x(1)],[0 1],'k','linewidth',0.4);
    plot(gcax,[x(3) x(3)],[0 1],'k','linewidth',0.4);
	if qlim~=0, plot(gcax,[x(5) x(5)],[0 1],'k','linewidth',0.4); end

    for I=1:np
        LocY=0.93-I/(np+1.8);
        text(0.025,LocY,[num2str(thetaj(I)*180/pi,'%3.1f') '{\circ}'],'FontSize',8);
        text(x(2),LocY,[num2str(aj(I),'%1.2f') ' \pm ' num2str(aj(I)-ajlo(I),'%1.3f')],'FontSize',8);
        text(x(4),LocY,[num2str(bj(I),'%1.2f') ' \pm ' num2str(bj(I)-bjlo(I),'%1.3f')],'FontSize',8);
    	if qlim~=0, text(x(6),LocY,[num2str(dj(I),'%1.2f') ' \pm ' num2str(dj(I)-djlo(I),'%1.3f')],'FontSize',8); end
        if I<np
            plot(gcax,[0 1],LocY*[1 1]-0.025,'k','linewidth',0.4);
        end
    end
end
    
