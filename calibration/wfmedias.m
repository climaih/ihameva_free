function wfmedias(p,auxmean,auxstd,ID)

    [auxmean odt]=sort(auxmean);
    plot(log(auxmean),log(auxstd(odt)),'Color',[100/255 149/255 237/255],'LineWidth',2)
    grid on
    hold on
     switch ID
        case {1,3}
            plot(log(auxmean),log(p(3)+p(4)*(auxmean)),'g--','LineWidth',2)
        case {2,4}
            plot(log(auxmean),log(p(3)+p(4)*sqrt(auxmean)),'g--','LineWidth',2)
         case {5}
            plot(log(auxmean),log(p(3)*(1-exp(-p(4)*auxmean))),'g--','LineWidth',2)
         case {6}
            plot(log(auxmean),log(p(3)*auxmean.^p(4)),'g--','LineWidth',2)
        otherwise
            disp('ID no valido');
    end
    minimo = (min(log(auxmean)));
    maximo = (max(log(auxmean)));
    xlim([minimo maximo])
    xlabel(['log(\mu)'],'fontsize',8,'fontweight','bold')
    ylabel(['log(\sigma)'],'fontsize',8,'fontweight','bold');
    title(['Mean vs. Standard deviation'],'fontsize',11,'fontweight','bold');
    legend('Empirical model','Fitted model','location','SouthEast');
    set(gca,'FontSize',7);

end
