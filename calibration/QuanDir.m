function QuanDir(Hs_SATQ,DirQ,nq,Hlim,tipo,idioma,var_name,var_unit,gcax) 
%  tipo: R-reanalisis, I-instrumental, C-calibrada    

if ~exist('idioma','var'), idioma='eng'; end
if ~exist('var_name','var'), var_name = ''; end
if ~exist('var_unit','var'), var_unit = ''; end
if ~exist('gcax','var') || isempty(gcax), gcax=axes('units','normalized');else gcax=get(gcf,'CurrentAxes');end
if ~ishandle(gcax), disp('WARNING! QuanDir with axes'); end
set(gcf,'CurrentAxes',gcax)
texto=amevaclass.amevatextoidioma(idioma);
    if tipo=='C', %red-calibrado
        axcol=1;
        aycol=[2 3];
    elseif tipo=='R', %green-reanalisis
        axcol=2;
        aycol=[1 3];
    elseif tipo=='I', %blue-reanalisis
        axcol=3;
        aycol=[1 2];
    else
        error(['QuanDir. No existe este tipo(',tipo,') de grafico, seleccione R, I o C']);
    end
    if ~isempty(char(var_name))
        var_name=['{',char(var_name),'}^',tipo];
    else
        var_name='';
    end
    if ~isempty(char(var_unit))
        var_unit=char(var_unit);
    else
        var_unit='';
    end
    
	col=ones(nq,3)*0.6;
    col(1,axcol)=1;
    if rem(1,5)==0
        col(1,aycol)=[0 0];
    end
    [q,gtxt]=polargeo_markercolor(DirQ*pi/180,Hs_SATQ(1,:),'.',col(1,:),Hlim(2),var_unit);hold on;
    for i = 1:nq
        col(i,axcol)=1;
        if rem(i,5)==0
            col(i,aycol)=[0 0];
        end
        polargeo_markercolor(DirQ*pi/180,Hs_SATQ(i,:),'.',col(i,:),Hlim(2),var_unit);hold on;
    end
    if ~isempty(gtxt)
        text(gtxt.px,gtxt.py,gtxt.tx,'verticalalignment','bottom','handlevisibility','off','fontsize',7);
    end
    title([texto('quanDirTitulo'),var_name],'fontsize',11,'fontweight','bold');
    
end
