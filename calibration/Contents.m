% CALIBRATION
%
% Files
%   arc2                           - arc2(r,az1, az2)
%   arcq                           - arc(r,az1, az2)
%   bias                           - 
%   calibraciongowfig              - Consideramos que se va a necesitar de 1 a 1.5 semanas para
%   CalibraSplineNLP               - La funcion CalibraSplineNLP 
%   CalibraSplineParam             - 
%   CalibraSplineQuantile          - La funcion CALIBRASPLINEQUEANTILE permite el calibrado direccional
%   calibration                    - tool.
%   cbfreeze                       - Freezes the colormap of a colorbar.
%   cbhandle                       - Handle of current colorbar axes.
%   centax2                        - CENTAXES can be used to change the origin of a 2-D axes
%   coefCORR                       - 
%   correlaciones                  - 
%   DetClas2N                      - P1=0.1;
%   FigICParams                    - 
%   fobjNLP                        - 
%   freezeColors                   - freezeColors  Lock colors of an image to current colors
%   GraficosAdicionales            - Los angulos deben estar en grados
%   groseq                         - ff = groseq2(az, nb, typ, fscale,levels)
%   InsReaCalPlot                  - 
%   IntepLineal                    - *********************************************************************************
%   lineaABC                       - 
%   niveles                        - 
%   normaliza                      - 
%   optiparamhessiancal            - Initial bounds and possible initial values for the constant parameters
%   outlierfilterRNoCte            - Funtion outlierfilterRNoCte automatically detects the presence of
%   ParamTable                     - 
%   ParamTableL                    - 
%   Pinta_felpudo                  - 
%   pinta_rosa_prob                - Se usa por medio de PintaRosasProbabilisticas.m
%   Pinta_RosaPcolor               - OJO OJO unificar en una sola funcion con pinta_rosa_prob OJO OJO
%   PintaRosasProbabilisticas      - Examples
%   PlotEmpiricalDist              - 
%   polargeo                       - Polar geographic coordinate plot.
%   polargeo_markercolor           - POLARGEO  Polar geographic coordinate plot.
%   polarLabels_IC_modOrigenSombra - 
%   QQgeneration                   - La funcion QQGENERACION calcula los quantiles que hay que calibrar teniendo en
%   QuanDirBlue                    - 
%   QuanDirGreen                   - 
%   QuanDirRed                     - 
%   rms                            - 
%   rsi                            - 
%   ScatterCal                     - 
%   ScatterQQ                      - 
%   ScatterQQ_newGreen             - 
%   ScatterQQ_newRed               - 
%   ScatterRea                     - 
%   tlabel                         - Full date formatted tick labels with ZOOM, PAN and LINKAXES.
%   TransCarte                     - 
%   uplownewcal                    - 
%   worldfilter                    - WORLDFILTER. Outlier filter
%   worldfilterfig                 - Consideramos que se va a necesitar de 1 a 1.5 semanas para
