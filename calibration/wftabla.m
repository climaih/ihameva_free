function wftabla(p,upper,conf,idx,lngh)

    box(gca,'on'); %   Eliminacion de los label
    set(gca,'XTick',[])
    set(gca,'XTickLabel',[])
    set(gca,'YTick',[])
    set(gca,'YTickLabel',[])
    
    text(0.05,0.0625*15-0.02,'Parameters CI^{95%}:','FontSize',9,'fontweight','bold');
    text(0.1,0.0625*13+0.0625/2-0.02,['p_1:          ',num2str(p(1),'%1.4f'), ' \pm ',num2str(abs(p(1)-upper(1)),'%1.4f')] ,'FontSize',9);
    text(0.1,0.0625*12+0.0625/2-0.02,['p_2:          ',num2str(p(2),'%1.4f'), ' \pm ',num2str(abs(p(2)-upper(2)),'%1.4f')] ,'FontSize',9);
    text(0.1,0.0625*11+0.0625/2-0.02,['p_3:          ',num2str(p(3),'%1.4f'), ' \pm ',num2str(abs(p(3)-upper(3)),'%1.4f')] ,'FontSize',9);
    text(0.1,0.0625*10+0.0625/2-0.02,['p_4:          ',num2str(p(4),'%1.4f'), ' \pm ',num2str(abs(p(4)-upper(4)),'%1.4f')] ,'FontSize',9);
    text(0.05,0.0625*8+0.0625-0.02,'Data Removed:','FontSize',9,'fontweight','bold');
    for i=1:length(idx)
        text(0.1,0.0625*(8.5-i)-0.02,[num2str(conf(i)*100,'%2.2f'),'%:     ',num2str(length(idx{i}))],'FontSize',9);
%         text(0.1,0.0625*6.5-0.02,[num2str(conf(i)*100,'%2.2f'),'%:     ',num2str(length(idx{2}))],'FontSize',9);
%         text(0.1,0.0625*5.5-0.02,[num2str(conf(3)*100,'%2.2f'),'%:     ',num2str(length(idx{3}))],'FontSize',9);
%         text(0.1,0.0625*4.5-0.02,[num2str(conf(4)*100,'%2.2f'),'%:     ',num2str(length(idx{4}))],'FontSize',9);
%         text(0.1,0.0625*3.5-0.02,[num2str(conf(5)*100,'%2.2f'),'%:     ',num2str(length(idx{5}))],'FontSize',9);
        text(0.6,0.0625*(8.5-i)-0.02,[num2str((length(idx{i}))/lngh,'%1.4f'),'%'],'FontSize',9);
%         text(0.6,0.0625*6.5-0.02,[num2str((length(idx{2}))/lngh,'%1.4f'),'%'],'FontSize',9);
%         text(0.6,0.0625*5.5-0.02,[num2str((length(idx{3}))/lngh,'%1.4f'),'%'],'FontSize',9);
%         text(0.6,0.0625*4.5-0.02,[num2str((length(idx{4}))/lngh,'%1.4f'),'%'],'FontSize',9);
%         text(0.6,0.0625*3.5-0.02,[num2str((length(idx{5}))/lngh,'%1.4f'),'%'],'FontSize',9);
    end
    text(0.05,0.0625*1.5,['Sample size: ',num2str(lngh),' data'],'FontSize',9,'fontweight','bold');

end
