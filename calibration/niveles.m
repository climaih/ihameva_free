function EE = niveles (E,TickColores)

[m,n]=size(E);
E2=reshape(E,m*n,1);
E3=zeros(m*n,1);
dum=find(isnan(E2));
E3(dum)=NaN; %length(TickColores); %
for i=1:(length(TickColores)-1)
    
    dum=find(E2>=TickColores(i) & E2<=TickColores(i+1));
    E3(dum)=i;
end
%dum=find(E2>=TickColores(end));
%E3(dum)=length(TickColores);

EE=reshape(E3,m,n);
    