function [final ,bmus ,acierto_cien ,datos]=KMA_3D_PreMDA_function(datos,escalar,direccional,tam,Var,Unit,carpeta,tipo,svopt)

[N,dim]=size(datos);

%datos a utilizar en el entrenamiento de la SOM
cantidad=10000;
if cantidad>N-2, error('N-longitud del vector tiene que ser mayor que 10002'); end

%Normalizacion de los datos
minimos=zeros(length(escalar),1);
maximos=zeros(length(escalar),1);
for i=1:length(escalar)
    minimos(i)=min(datos(:,escalar(i)));
    maximos(i)=max(datos(:,escalar(i)));
end

datos_n=zeros(N,dim);
for i=1:length(escalar)
    datos_n(:,escalar(i))=(datos(:,escalar(i))-minimos(i))./(maximos(i)-minimos(i));
end
for i=1:length(direccional)
    datos_n(:,direccional(i))=datos(:,direccional(i))*pi/180;
end

%pre mda
semilla=find(max(datos(:,1))==datos(:,1));
[subset, ncenters] = algoritmo_MaxDiss_MaxMinSimplificado_SinUmbral(semilla(1), cantidad, datos_n, escalar, direccional);

% %Size KMedias
% tam=t^2;
% tam=round(tam);%%%% OC to intro the squared 

%Obtener agrupamiento, con K-medias
mascara=ones(dim,1);
%Inicializacion MDA
[bmus_entrenamiento,centers]=kmeans_modificado_initMDA(subset,tam,escalar,direccional,mascara);

%calculo de los bmus de todos los datos
bmus=zeros(N,1);
m=ones(tam,1);
for i=1:N
    retro=datos_n(i,:);
    retro2=retro(m,:);
    [qerr,bmu]=som_distancia_seq(centers,retro2,mascara,'euclidiana2',escalar,direccional);
    bmus(i)=bmu;
end

%Desnormalizar los resultados
final=zeros(tam,dim);
for i=1:length(escalar)
    final(:,escalar(i))=centers(:,escalar(i))*(maximos(i)-minimos(i))+minimos(i);
end
for i=1:length(direccional)
    final(:,direccional(i))=centers(:,direccional(i))*180/pi;
end

%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto=zeros(tam,1);
for i=1:tam
    acierto(i)=sum(bmus==i);
end

%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto_cien=acierto/length(bmus)*100;

%PLOTS
if exist('svopt','var') && sum(svopt)>0
    if ~exist('carpeta','var') || isempty(carpeta), carpeta='classification_sample_dir'; end;
    if ~exist([pwd filesep carpeta],'dir'),  mkdir(carpeta); end;
    
    lab=cell(dim,1);
    for i=1:dim
        if exist('Var','var') && not(isempty(Var)) && length(Var)==dim && exist('Unit','var') && not(isempty(Unit)) && length(Unit)==dim,
        	lab(i)={[char(Var(i)) ' (' char(Unit(i)) ')']};
        else
            lab(i)={['var' num2str(i)]};
        end
    end
    
    titulo=[tipo,' ',num2str(tam)];
    if not(isempty(strfind(tipo,'_')))
        titulo=[regexprep(tipo,'_',' ') ,' ',num2str(tam)];
    end
    
    h=figure ('visible','off');
    Dibuja_2D3D_DistribucionPtos(datos,final, 'k.', 'r.', 0.5, floor(minimos), ceil(maximos),lab,titulo,direccional,'2D')
    amevasavefigure(h,carpeta,svopt,[tipo,'_',num2str(tam),'_2D'])
    
    h=figure ('visible','off');
    Dibuja_2D3D_DistribucionPtos(datos,final, 'k.', 'r.', 0.5, floor(minimos), ceil(maximos),lab,titulo,direccional,'3D')
    amevasavefigure(h,carpeta,svopt,[tipo,'_',num2str(tam),'_3D'])
end