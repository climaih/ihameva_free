function Dibuja_2D3D_DistribucionPtos(datos1,datos2,tipo1,tipo2,tam,minimo,maximo,Lab,titulo,direccional,typeplot,hf) 
    
%GRAFICAS 2D/3D Datos - Casos seleccionados
N=min(size(datos1));
if N<2 && N>4, disp(['No esta preparado para dibujar para N=' num2str(N)]); return; end
nx=[1 2 3 1 2 3 1 2 3 4];%solo para N=2,3,4,5
ny=[2 3 1 4 4 4 5 5 5 5];%solo para N=2,3,4,5
nz=[3 4 4 2 5 5 3 1 2 1];%solo para N=2,3,4,5
nmaxfig=[];
if strcmp(typeplot,'2D'),
    nmaxfig=factorial(N)/(factorial(2)*factorial(N-2));% 2D) N=2->1, N=3->3, N=4->6, N=5->10,  n!/(k!(n-k)!)
elseif strcmp(typeplot,'3D') && N>2
    nmaxfig=factorial(N)/(factorial(3)*factorial(N-3));% 3D) N=2->0, N=3->1, N=4->4, N=5->10
end
if N>5, nmaxfig=min(nmaxfig,10); disp('Solo se dibujan las primeras 5 combinaciones'); end%maximo 10
if isempty(nmaxfig) || nmaxfig<1, disp(['No esta preparado para dibujar para N=' num2str(N) ' ' typeplot '. Solo 2D o 3D->N>2']); return; end
for i=1:nmaxfig,
    if exist('hf','var'),
        sp=subplot(nmaxfig,1,i,'Parent',hf);
    else
        sp=subplot(nmaxfig,1,i);
    end
    if strcmp(typeplot,'2D'),
        plot(sp,datos1(:,nx(i)),datos1(:,ny(i)),tipo1,'MarkerSize',tam); hold on
        if ~isempty(datos2), plot(sp,datos2(:,nx(i)),datos2(:,ny(i)),tipo2); end
    elseif strcmp(typeplot,'3D') && N>2
        if length(datos1(:,3))>100
            plot3(sp,datos1(:,nx(i)),datos1(:,ny(i)),datos1(:,nz(i)),tipo1,'MarkerSize',tam);
        else
            plot3(sp,datos1(:,nx(i)),datos1(:,ny(i)),datos1(:,nz(i)),'+','MarkerSize',3);
        end; hold on;
        if ~isempty(datos2), plot3(sp,datos2(:,nx(i)),datos2(:,ny(i)),datos2(:,nz(i)),tipo2); end
        view(142.5,30);    box on;
    end
    grid on;
    xlabel(Lab(nx(i)))
    ylabel(Lab(ny(i)))
    if strcmp(typeplot,'3D'), zlabel(Lab(nz(i))); end %3D
    if i==1, title(gca,titulo); end
    if not(isempty(direccional)) && max(size(direccional))==1 && nx(i)==direccional,
        set(gca,'XTick',0:90:360,'Xlim',[0 360]);
    elseif not(isempty(minimo)) && length(minimo)>=nx(i)
        set(gca,'Xlim',[minimo(nx(i)) maximo(nx(i))]);
    end
    if not(isempty(direccional)) && max(size(direccional))==1 && ny(i)==direccional,
        set(gca,'YTick',0:90:360,'Ylim',[0 360]);
    elseif not(isempty(minimo)) && length(minimo)>=ny(i)
        set(gca,'Ylim',[minimo(ny(i)) maximo(ny(i))]);
    end
    if not(isempty(direccional)) && max(size(direccional))==1 && nz(i)==direccional && strcmp(typeplot,'3D'), 
        set(gca,'ZTick',0:90:360,'Zlim',[0 360]); 
    end %3D
    if strcmp(typeplot,'3D') && ~isempty(datos2),
        legend(sp,'All data','Selected data','Location','NorthWest'); 
    end %3D
end