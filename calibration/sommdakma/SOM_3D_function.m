function [final2,bmus,acierto_cien,datos]=SOM_3D_function(datos,escalar,direccional,t,Var,Unit,carpeta,tipo,svopt)

[N,dim]=size(datos);

%Normalizacion de los datos
minimos=zeros(length(escalar),1);
maximos=zeros(length(escalar),1);
for i=1:length(escalar)
    minimos(i)=min(datos(:,escalar(i)));
    maximos(i)=max(datos(:,escalar(i)));
end

datos_n=zeros(N,dim);
for i=1:length(escalar)
    datos_n(:,escalar(i))=(datos(:,escalar(i))-minimos(i))./(maximos(i)-minimos(i));
end
for i=1:length(direccional)
    datos_n(:,direccional(i))=datos(:,direccional(i))*pi/180;
end


%Datos en la estructura que necesita la SOM (los seleccionados con MaxDiss y normalizados)
% sD=som_data_struct(subset);
sD=som_data_struct(datos_n);
%creo los label para la som
lab=cell(dim,1);
for i=1:dim
    if exist('Var','var') && not(isempty(Var)) && length(Var)==dim && exist('Unit','var') && not(isempty(Unit)) && length(Unit)==dim,
        lab(i)={[char(Var(i)) ' (' char(Unit(i)) ')']};
    else
        lab(i)={['var' num2str(i)]};
    end
end
sD.comp_names=lab;

%Size
tam=t(1)*t(2);

%Calculo de la SOM
mascara=ones(dim,1);
sM = som_make_modificado_InitEOFs(sD,'msize',[t(1) t(2)],'algorithm','seq','init','lininit','shape','toroid','training','long','mask',mascara,...
    'tipo','euclidiana2','escalares',escalar','direccionales',direccional);

%calculo de los bmus de todos los datos
bmus=zeros(N,1);
m=ones(length(sM.codebook),1);
for i=1:N
    retro=datos_n(i,:);
    retro2=retro(m,:);
    [qerr,bmu]=som_distancia_seq(sM.codebook,retro2,mascara,'euclidiana2',escalar,direccional);
    bmus(i)=bmu;
end

SM=sM.codebook;

%Desnormalizar los resultados
final=zeros(tam,dim);
for i=1:length(escalar)
    final(:,escalar(i))=sM.codebook(:,escalar(i))*(maximos(i)-minimos(i))+minimos(i);
end
for i=1:length(direccional)
    final(:,direccional(i))=sM.codebook(:,direccional(i))*180/pi;
end

%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto=zeros(tam,1);
for i=1:length(sM.codebook)
    acierto(i)=sum(bmus==i);
end

%frecuencia de presentacion de los centroides segun todos los datos de retroanalisis
acierto_cien=acierto/length(bmus)*100;
% maximo=max(acierto_cien);

%Desplamiento de la SOM para que la celda con la mayor altura de ola quede en el centro
xx=reshape(final(:,1),t(1),t(2));
temp=max(xx);
cmaxx=find(max(temp)==temp);    %columna con la maxima altura de ola
temp2=xx(:,cmaxx);
fmaxx=find(max(temp2)==temp2);  %fila con la maxima altura de ola
final2(:,1) = desplazamiento (t, cmaxx, fmaxx, xx);
for i=2:dim
    final2(:,i) = desplazamiento (t, cmaxx, fmaxx, final(:,i));
    %final2(:,3) = desplazamiento (t, cmaxx, fmaxx, final(:,3));
end
acierto_cien2 = desplazamiento (t, cmaxx, fmaxx, acierto_cien);

for i=1:dim
    SM2(:,i) = desplazamiento (t, cmaxx, fmaxx, SM(:,i));
    %     SM2(:,2) = desplazamiento (t, cmaxx, fmaxx, SM(:,2));
    %     SM2(:,3) = desplazamiento (t, cmaxx, fmaxx, SM(:,3));
end

%calculo de los bmus de todos los datos
bmus2=zeros(N,1);
m=ones(length(SM2),1);
for i=1:N
    retro=datos_n(i,:);
    retro2=retro(m,:);
    [qerr,bmu]=som_distancia_seq(SM2,retro2,mascara,'euclidiana2',escalar,direccional);
    bmus2(i)=bmu;
end

%PLOTS
if exist('svopt','var') && sum(svopt)>0
    if ~exist('carpeta','var') || isempty(carpeta), carpeta='classification_sample_dir'; end;
    if ~exist([pwd filesep carpeta],'dir'),  mkdir(carpeta); end;
    
    lab=cell(dim,1);
    for i=1:dim
        if exist('Var','var') && not(isempty(Var)) && length(Var)==dim && exist('Unit','var') && not(isempty(Unit)) && length(Unit)==dim,
        	lab(i)={[char(Var(i)) ' (' char(Unit(i)) ')']};
        else
            lab(i)={['var' num2str(i)]};
        end
    end
    
    titulo=[tipo,' ',num2str(tam)];
    if not(isempty(strfind(tipo,'_')))
        titulo=[regexprep(tipo,'_',' ') ,' ',num2str(tam)];
    end
    
    if not(isempty(escalar)) && length(escalar)>=2 && escalar(1)==1 && escalar(2)==2 && (not(isempty(direccional)) && direccional(1)==3)
        h=figure ('color',[1 1 1],'visible','off');
        set(h, 'Renderer', 'ZBuffer');
        graficas_SOM_J (final2, acierto_cien2, t,lab{1},lab{2},titulo);
        amevasavefigure(h,carpeta,svopt,[tipo,'_',num2str(tam)])
    end
    
    h=figure ('color',[1 1 1],'visible','off');
    set(h, 'Renderer', 'ZBuffer');
    sM.codebook=[SM2 acierto_cien2(:)];
    sM.comp_names(end+1)={'Frequency'};
    sM.comp_norm(end+1)=sM.comp_norm(end);
    sM.mask(end+1)=sM.mask(end);
    ncp=65;
    cm=1-copper(ncp+10);%Escala de azules (65 gamas)%acierto mes/anual
    cprob=cm(1:ncp,:);%Se eliminan las gamas mas oscuras
    som_show(sM,'comp',1:min(size(sM.codebook)),'colormap',cprob);
    amevasavefigure(h,carpeta,svopt,[tipo,'_',num2str(tam),'_'])
    
    h=figure ('visible','off');
    Dibuja_2D3D_DistribucionPtos(datos,final2, 'k.', 'r.', 0.5, floor(minimos), ceil(maximos),lab,titulo,direccional,'2D')
    amevasavefigure(h,carpeta,svopt,[tipo,'_',num2str(tam),'_2D'])
    
    h=figure ('visible','off');
    Dibuja_2D3D_DistribucionPtos(datos,final2, 'k.', 'r.', 0.5, floor(minimos), ceil(maximos),lab,titulo,direccional,'3D')
    amevasavefigure(h,carpeta,svopt,[tipo,'_',num2str(tam),'_3D'])
end

