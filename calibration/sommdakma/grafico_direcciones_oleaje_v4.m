function grafico_direcciones_oleaje_v4 (t,dato1,dato2,dato3,acierto_cien,maximo)
                               
% %subplot(2,2,3)
% M=normaliza(dato1,[0.3,1]);
% Dir=pi/2-dato3;
% d1=find(Dir(:,1)<-pi);
% %d2=find(Dir(:,2)<-pi);
% Dir(d1)=Dir(d1)+2*pi;
% %Dir(d2,2)=Dir(d2,2)+2*pi;
% U=-M.*cos(Dir);
% V=M.*sin(Dir);


M=normaliza(dato1,[0.3,1]);
Dir=pi/2-dato3*pi/180;
d1=find(Dir(:,1)<-pi);
Dir(d1)=Dir(d1)+2*pi;
U=-M.*cos(Dir);
V=M.*sin(Dir);



%Escala de frecuencia de presentacion
%Escala de azules (10 gamas)
cm=1-copper(15);
%Se eliminan las gamas mas oscuras 
cm1=[cm(1:9,:); cm(12,:)];  
%Se buscan los centroides con frecuencia de presentacion nula
dum=find(acierto_cien==0);
if isempty(dum)==0 
   acierto_cien(dum)=maximo;   %los valores nulos se igual al valor maximo para que no haya problemas al tomar logaritmos
end   
acierto_log=log(acierto_cien);
%Normalizacion de los valores de la frecuencia de presentacion en la escala logaritmica
CM=round(normaliza(acierto_log,[2 10]));
%Los centroides con frecuencia de presentacion nula se igualan a 1 (para que tengan color blanco) 
CM(dum)=1;
%Hexagonos con el fondo en la escala de grises, segun la frecuencia de presentacion
% som_cplane_p1('hexa',[t t],cm1(CM,:));
som_cplane('hexa',[t(1) t(2)],cm1(CM,:));
hold on

%cm2=autumn(256);
cm2=hot(256);
CM2=round(normaliza(dato1,[256 1]));
% som_cplane_p1('hexa',[t t],cm2(CM2,:),0.3);
som_cplane('hexa',[t(1) t(2)],cm2(CM2,:),0.3);

hold on 

Co = som_vis_coords('hexa',[t(1) t(2)]);

%flechas de direccion, con escala de periodos
%c1=1-copper(256);
c1=repmat([1 1 1],14,1)-gray(14);
c2=[c1(4:12,:); c1(end,:)];
dum=find(dato2~=0);
per=dato2(dum);
mmax=max(per);
mmin=min(per);
%mmin=0;

C1=round(normaliza2(dato2,mmax,mmin,[1 10]));
h=miquiver(Co(:,1),Co(:,2),U(:,1),V(:,1),1,c2(C1,:));


