function graficas_SOM_J (som, acierto_cien, t, LabX,LabY,titulo)

pos_colorbars='vertical';

% grafico som + 3 escalas de colores
if strcmp(pos_colorbars,'horizontal')
    pfig=[360   300   420 560];
    posSom=[0.1 0.27 0.8 0.7]; 
    posEsc1=[0.10 0.05 0.77 0.05]; 
    posEsc2=[0.10 0.14 0.77 0.05]; 
    posEsc3=[0.10 0.22 0.77 0.05]; 
else
    pfig=[360   502   560   420];
    posSom=[0.05 0.05 0.60 0.9]; 
    posEsc1=[0.70 0.1 0.05 0.80]; 
    posEsc2=[0.80 0.1 0.05 0.80]; 
    posEsc3=[0.90 0.1 0.05 0.80]; 
end

maximo=max(acierto_cien);

set(gcf,'position',pfig)          % posicion de la fig
subplot('position',posSom)  
grafico_direcciones_oleaje_v4(t,som(:,1),som(:,2),som(:,3),acierto_cien',maximo)
title(titulo,'Fontsize',12,'Fontweight','bold')


subplot('position',posEsc1)   % frecuencia de presentacion
%Escala de azules
cm=1-copper(15);
%Se eliminan algunos colores (nos quedamos con 11)
cm1=[cm(1,:); cm(3:9,:); cm(11:12,:); cm(14,:)];
%Se buscan los centroides con frecuencia de presentacion nula
maximo=max(acierto_cien);
dum=find(acierto_cien==0);
if isempty(dum)==0
   acierto_cien(dum)=maximo;   %los valores nulos se igual al valor maximo para que no haya problemas al tomar logaritmos
end
acierto_cien_log=log(acierto_cien);
%Normalizacion de los valores de la frecuencia de presentacion en la escala logaritmica (de 1 a 10)
CM=round(normaliza(acierto_cien_log,[1 10]));
%Se pasan a una escala de 1 a 11
CM=CM+1;
%Los centroides con frecuencia de presentacion nula se igualan a 1 (para que tengan color blanco)
CM(dum)=1;
%Etiquetas
etiqueta(1)=0;
%El minimo se asigna a la etiqueta 2, el maximo a la 12
etiqueta(2)=exp(min(acierto_cien_log));
etiqueta(12)=exp(max(acierto_cien_log));
dx=(max(acierto_cien_log)-min(acierto_cien_log))/10;
for i=3:11
    etiqueta(i)=exp(min(acierto_cien_log)+dx*(i-2));
end
for i=1:length(etiqueta)
    etiqueta2{i}=sprintf('%4.2f',etiqueta(i));
end
%x=linspace(etiqueta(1),etiqueta(end),100);
x=linspace(0,length(etiqueta)-1,100);
y=x; 
[xx,yy]=meshgrid(x,y); yy=flipud(yy); 

%pcolor
if strcmp(pos_colorbars,'horizontal')
    h=pcolor(xx,yy,xx); 
else
    h=pcolor(xx,yy,yy); 
end
shading interp
colormap(cm1)
if strcmp(pos_colorbars,'horizontal')
    daspect([0.05 0.9 1])
    ylabel('Freq (%)','rotation',0,'HorizontalAlignment','left','VerticalAlignment','middle')
    set(gca,'YAxisLocation','right')
    set(gca,'yticklabel',etiqueta2),set(gca,'ytick',etiqueta)
else
    daspect([0.9 0.05 1])
    title({'Frequency' '(%)'})
    set(gca,'yticklabel',etiqueta2,'ytick',[0:1:11],'xtick',[],'xticklabel','')
end
box on

clear etiqueta* xx yy x y 
freezeColors

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('position',posEsc2)   % escala de Hs
Hc=som(:,1);
cm2=flipud(hot(256));
%ticks del colorbar
x=[1  32    64    96   128   160   192   224   256];
%valor de la altura de ola en esos ticks, que seran las estiquetas
etiqueta=[prctile(Hc,100/x(end)*0);
prctile(Hc,100/x(end)*x(end-7));
prctile(Hc,100/x(end)*x(end-6)); 
prctile(Hc,100/x(end)*x(end-5)); 
prctile(Hc,100/x(end)*x(end-4)); 
prctile(Hc,100/x(end)*x(end-3)); 
prctile(Hc,100/x(end)*x(end-2));
prctile(Hc,100/x(end)*x(end-1));
prctile(Hc,100/x(end)*x(end))];
for i=1:length(etiqueta)
    etiqueta2{i}=sprintf('%4.2f',etiqueta(i));
end
x=linspace(0,length(etiqueta)-1,100);
y=x; 
[xx,yy]=meshgrid(x,y); yy=flipud(yy); 

% pcolor        
if strcmp(pos_colorbars,'horizontal')
    h=pcolor(xx,yy,xx); 
else
    h=pcolor(xx,yy,yy); 
end
shading interp
colormap(cm2)
if strcmp(pos_colorbars,'horizontal')
    daspect([0.05 0.9 1])
    ylabel(LabX,'rotation',0,'HorizontalAlignment','left','VerticalAlignment','middle')
    set(gca,'YAxisLocation','right')
    set(gca,'yticklabel',''),set(gca,'ytick',[])
else
    daspect([0.9 0.05 1])
    title(LabX)
    set(gca,'yticklabel',etiqueta2,'ytick',get(gca,'ytick'),'xtick',[],'xticklabel','')
end
freezeColors
clear xx yy x y etiqueta*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot('position',posEsc3)  % escala de T
%escala de los periodos
Tm=som(:,2);
cm2=flipud(gray(256));
%ticks del colorbar
x=[1  32    64    96   128   160   192   224   256];
%valor de la altura de ola en esos ticks, que seran las estiquetas
etiqueta=[prctile(Tm,100/x(end)*0);
prctile(Tm,100/x(end)*x(end-7));
prctile(Tm,100/x(end)*x(end-6)); 
prctile(Tm,100/x(end)*x(end-5)); 
prctile(Tm,100/x(end)*x(end-4)); 
prctile(Tm,100/x(end)*x(end-3)); 
prctile(Tm,100/x(end)*x(end-2));
prctile(Tm,100/x(end)*x(end-1));
prctile(Tm,100/x(end)*x(end))];
for i=1:length(etiqueta)
    etiqueta2{i}=sprintf('%4.2f',etiqueta(i));
end
x=linspace(0,length(etiqueta)-1,100);
y=x; 
[xx,yy]=meshgrid(x,y); yy=flipud(yy); 

%pcolor
if strcmp(pos_colorbars,'horizontal')
    h=pcolor(xx,yy,xx); 
else
    h=pcolor(xx,yy,yy); 
end
shading interp
colormap(cm2)
if strcmp(pos_colorbars,'horizontal')
    daspect([0.05 0.9 1])
    ylabel(LabY,'rotation',0,'HorizontalAlignment','left','VerticalAlignment','middle')
    set(gca,'YAxisLocation','right')
    set(gca,'yticklabel',''),set(gca,'ytick',[])
else
    daspect([0.9 0.05 1])
    title(LabY)
    set(gca,'yticklabel',etiqueta2,'ytick',get(gca,'ytick'),'xtick',[],'xticklabel','')
end
box on

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%