function [linea]=lineaABC(thetaj,aj,ajup,bj,bjup,dj,djup)
    
    linea='';
    if length(thetaj)>=5
        linea=thetaj;
    elseif length(thetaj)==4
        linea=[linea,' ',thetaj];
    elseif length(thetaj)==3
        linea=[linea,'  ',thetaj];
    elseif length(thetaj)==2
        linea=[linea,'   ',thetaj];
    elseif length(aj)==1
        linea=[linea,'    ',thetaj];
    end
    linea=[linea,char(186),' '];
    
    if length(aj)>=3
        linea=[linea,aj];
    elseif length(aj)==2;
        linea=[linea,' ',aj];
    elseif length(aj)==1;
        linea=[linea,'  ',aj];
    end
    linea=[linea,' ' char(177) ' '];
    
    if length(ajup)>=4
        linea=[linea,ajup];
    elseif length(ajup)==3;
        linea=[linea,' ',ajup];
    elseif length(ajup)==2;
        linea=[linea,'  ',ajup];
    elseif length(ajup)==1;
        linea=[linea,'   ',ajup];
    end
    linea=[linea,' '];
    
    if length(bj)>=3
        linea=[linea,bj];
    elseif length(bj)==2;
        linea=[linea,' ',bj];
    elseif length(bj)==1;
        linea=[linea,'  ',bj];
    end
    linea=[linea,' ' char(177) ' '];
    
    if length(bjup)>=4
        linea=[linea,bjup];
    elseif length(bjup)==3;
        linea=[linea,' ',bjup];
    elseif length(bjup)==2;
        linea=[linea,'  ',bjup];
    elseif length(bjup)==1;
        linea=[linea,'   ',bjup];
    end
    
    if exist('dj','var'),
        linea=[linea,' '];
        if length(dj)>=3
            linea=[linea,dj];
        elseif length(dj)==2;
            linea=[linea,' ',dj];
        elseif length(dj)==1;
            linea=[linea,'  ',dj];
        end
        linea=[linea,' ' char(177) ' '];

        if length(djup)>=4
            linea=[linea,djup];
        elseif length(djup)==3;
            linea=[linea,' ',djup];
        elseif length(djup)==2;
            linea=[linea,'  ',djup];
        elseif length(djup)==1;
            linea=[linea,'   ',djup];
        end
    end
    
end
