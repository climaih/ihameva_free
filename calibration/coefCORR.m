function sal = coefCORR(x,y)

my = mean(y);

sal = sum((x-my).^2)/sum((y-x).^2+(x-my).^2);