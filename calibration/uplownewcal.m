function [Hsup,Hslo]=uplownewcal(Dir,Hs_S,Hs_Scal,x_G,y_G,nivconf,stdvHs_G)

[x_S,y_S] = TransCarte(Hs_S,Dir);

n=length(Dir);

%   Calculo para cada punto en el que quiero interpolar a que triangulo
%   pertence
% TRI = delaunay(x_G,y_G);
% T = tsearch(x_G,y_G,TRI,x_S,y_S);
TRI = DelaunayTri(x_G,y_G);
T = pointLocation(TRI,[x_S y_S]);

%   Procedimiento de interpolacion punto a punto
maximo = max(stdvHs_G);
stdvHs = zeros (n,1);
for i = 1:n,
    if ~isnan(T(i)),
        stdvHs(i) = IntepLineal(stdvHs_G(TRI(T(i),1)),stdvHs_G(TRI(T(i),2)),stdvHs_G(TRI(T(i),3)),...
            x_S(i),y_S(i),x_G(TRI(T(i),1)),y_G(TRI(T(i),1)),x_G(TRI(T(i),2)),y_G(TRI(T(i),2)),x_G(TRI(T(i),3)),y_G(TRI(T(i),3)));
    else
        stdvHs(i) = maximo;
    end
Hsup = Hs_Scal+stdvHs*norminv(1-nivconf/2);
Hslo = Hs_Scal-stdvHs*norminv(1-nivconf/2);

end