function calibrationplots(HsI,HsR,Dir,nq,Hscal,Hlim,Hsup,Hslo,n,nivconf,Hs_NCQ,Hs_CQ,Hs_SATQ,DirQ,qlim,aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,thetaj,calidadRHO,statmoments,tpstring,svopt,carpeta,idioma,var_name,var_unit,ax1)
%Prepro. label (var_name and var_unit)
if iscell(var_name),
    if ~isempty(char(var_name))
        var_name=char(var_name);
    else
        var_name='';
    end
end
if iscell(var_unit),
    if ~isempty(char(var_unit))
        var_unit=char(var_unit);
    else
        var_unit='';
    end
end
%Calibration plot's
[ni mi]=size(tpstring);
if mi<1, return; end;
h=[];
for i=1:mi,
    plothc=false;
    plotname=tpstring{i};
    if ~exist('ax1','var') || ~ishandle(ax1), h=figure('Visible','Off'); ax1 = axes('Parent',h); end
    switch plotname     %   Grafico verde
        case 'Scatter IR'
        ScatterQQ(HsI,HsR,Hlim,calidadRHO(1),statmoments,var_name,var_unit,{'I' 'R'},7,'AT',ax1);%'OC'
        case 'Scatter CI'
        ScatterQQ(HsI,Hscal,Hlim,calidadRHO(2),statmoments,var_name,var_unit,{'I' 'C'},7,'AT',ax1);%'OC'
        case 'Quantiles R'  %   Cuantiles direccioneales
        QuanDir(Hs_NCQ,DirQ,nq,Hlim,'R',idioma,var_name,var_unit,ax1);
        case 'Quantiles C'
        QuanDir(Hs_CQ,DirQ,nq,Hlim,'C',idioma,var_name,var_unit,ax1);
        case 'Quantiles I'
        QuanDir(Hs_SATQ,DirQ,nq,Hlim,'I',idioma,var_name,var_unit,ax1);
        case 'Parameters CI'  %   IC parametros
        FigICParams(aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,thetaj,var_name,0,ax1)
        case 'Parameters CIc'
        FigICParams(aj,ajlo,ajup,bj,bjlo,bjup,dj,djlo,djup,DirNoQ,np,thetaj,var_name,qlim,ax1)
        case 'Parameters Table'  %   Tabla de parametros
        ParamTable(np,aj,bj,dj,ajlo,bjlo,djlo,thetaj,qlim,idioma,ax1)
        case 'CDF'  %   Funcion de distribucion empirica  
        PlotEmpiricalDist(HsR,HsI,Hscal,Hsup,Hslo,Hlim,n,nivconf,idioma,var_name,var_unit,ax1);
        case 'Rose Cal/Rea'   %Pinta Rosa
        Pinta_felpudo(aj,bj,thetaj,np,[Hs_NCQ Hs_NCQ(:,1)]',Hlim,var_unit);%Unificar con Pinta_RosaPcolor OJO OJO
        vv=axis;
        text(vv(1)-abs(vv(1)/1.5),-vv(3),[var_name,'^C/',var_name,'^R'],'fontsize',10,'FontWeight','b');
        plothc=true;
        case 'Rose Rea',   %   Funcion Pinta Rosas ProbabilisticasD
        [ticks,etiqueta3]=PintaRosasProbabilisticas(Dir,HsR,idioma,var_name,var_unit,ax1);
        title(['{',var_name,'}^R'],'fontsize',11,'fontweight','bold')
        set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        if ~isempty(h)
            pcaux=colorbar('peer',ax1,'location','southoutside','position',[1/3 0.065 1/3 0.03],'xtick',[ticks(1:3:end) ticks(end)],'XtickLabel',['0.0' etiqueta3(4:3:end) strcat(etiqueta3(end),' (f)')]);
            set(pcaux,'fontsize',7)
        end
        case 'Rose Ins',   %   Funcion Pinta Rosas ProbabilisticasD
        %No se puede pintar las dos rosas en la ventana principal pero pero si imprimir
        [ticks,etiqueta3]=PintaRosasProbabilisticas(Dir,HsI,idioma,var_name,var_unit,ax1);
        title(['{',var_name,'}^I'],'fontsize',11,'fontweight','bold')
        set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        if ~isempty(h)
            colorbar('location','southoutside','position',[1/3 0.065 1/3 0.03],'xtick',[ticks(1:3:end) ticks(end)],'XtickLabel',['0.0' etiqueta3(4:3:end) strcat(etiqueta3(end),' (f)')],'FontSize',7);
        end
        case 'RoseP'   %   Funcion Pinta Rosas ProbabilisticasD
        [ticks,etiqueta3]=PintaRosasProbabilisticas(Dir,HsR,Dir,HsI,idioma,[var_name,var_name],[var_unit,var_unit],ax1);
        vv=axis;
        text(vv(1)-abs(vv(1)/1.5),-vv(3),['{',var_name,'}^R             {',var_name,'}^I'],'fontsize',10,'FontWeight','b')
        set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1 1 1]);
        if ~isempty(h)
            colorbar('location','southoutside','position',[1/3 0.065 1/3 0.03],'xtick',[ticks(1:3:end) ticks(end)],'XtickLabel',['0.0' etiqueta3(4:3:end) strcat(etiqueta3(end),' (f)')],'FontSize',7);
        end
        plothc=true;
        case 'X rea, X ins'
        InsReaCalPlot((1:length(HsI)),HsI,HsR,Hscal,[],[],[],Hsup,Hslo,[],[],0,idioma,var_name,'',var_unit,'',ax1);
        plothc=true;
        otherwise
        disp(['Warnig!.Not exist this plot case ',plotname]);
    end
    if ~isempty(h)
        plotname=strrep(plotname,' ', '');
        plotname=strrep(plotname,'/', '_');
        set(h,'Color','w');%solo en plot
        amevasavefigure(h,carpeta,svopt,plotname,plothc);
    end 
end

end
