function [Q] = QuantileGEV (F,t,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,indices,indices2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function QuantileGEV calculates the quantile q associated with a given
%   parameterization
%
%   Input:
%       F -> Probability
%       t -> time when the data occur within a yearly scale
%       kt -> Frequency parameter to measure the importance od the number
%       beta0 -> Optimal constant parameter related to location
%       beta -> Optimal harmonic vector associated with location
%       alpha0 -> Optimal constant parameter related to scale
%       alpha -> Optimal harmonic vector associated with scale
%       gamma0 -> Optimal constant parameter related to shape
%       gamma -> Optimal harmonic vector associated with shape
%       betaT -> Optimal location trend parameter
%       varphi -> Optimal location covariate vector
%       betaT2 -> Optimal scale trend parameter
%       varphi2 -> Optimal scale covariate vector
%       indices -> covariates data related to the location parameter, a
%       matrix including the data at time t for each covariate
%       indices2 -> covariates data related to the scale parameter, a
%       matrix including the data at time t for each covariate
%       
%   Output:
%
%       Q -> Quantile related to the given probability
%
%   Authors: R. Minguez, F.J. Mendez, C. Izaguirre, M. Menendez,
%   and I.J. Losada 
%   Environmental and Hydraulics Institute "IH Cantabria"
%   University of Cantabria 
%   E.T.S. de Ingenieros de Caminos, Canales y Puertos 
%   Avda de los Castros, s/n 
%   39005 Santander, Spain
%   Tfno.: +34 942 20 18 52 
%   Fax: +34 942 20 18 60 
%   Corresponding author email: roberto.minguez@unican.es
%
%   Created: 09/09/2009
%
%   For more details see the paper:
%   "Pseudo-Optimal Parameter Selection of Non-Stationary 
%   Generalized Extreme Value Models for Environmental Variables". 
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Check the number of input arguments
    
    if nargin<3, kt = [];end
    if nargin<4, beta0 = []; end 
    if nargin<5, beta = []; end 
    if nargin<6, alpha0 = []; end 
    if nargin<7, alpha = [];end
    if nargin<8, gamma0 = [];end
    if nargin<9, gamma = [];end
    if nargin<10, betaT = [];end
    if nargin<11, varphi = [];end
    if nargin<12, betaT2 = [];end
    if nargin<13, varphi2 = [];end
    if nargin<14, indices = [];end
    if nargin<15, indices2 = [];end

    %   Check consistency of data
    [na nind] = size(indices);
    if ~isempty(indices),
    if na~=length(t),
        error('Check data t, indices: funcion QuantileGEV')
    end
    end
    [na2 nind2] = size(indices2);
    if ~isempty(indices2),
    if na2~=length(t),
        error('Check data t, indices2: funcion QuantileGEV')
    end
    end
    
    if isempty(kt),
        kt = ones(size(t));
    end

    nmu=length(beta);
    npsi=length(alpha);
    neps=length(gamma);
    ntend=length(betaT);
    nind=length(varphi);
    ntend2=length(betaT2);
    nind2=length(varphi2);
    
    %   Evaluate the location parameter at each time t as a function
    %   of the actual values of the parameters given by p
    mut1 = parametro(t,beta0,beta,betaT,indices,varphi);
    %   Evaluate the scale parameter at each time t as a function
    %   of the actual values of the parameters given by p
    psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2));
    %   Evaluate the shape parameter at each time t as a function
    %   of the actual values of the parameters given by p
    epst = parametro(t,gamma0,gamma);

    %   The values whose shape parameter is almost cero corresponds to
    %   the GUMBEL distribution, locate their positions if they exist
    posG = find(abs(epst)<=1e-8);
    %   The remaining values correspond to WEIBULL or FRECHET
    pos  = find(abs(epst)>1e-8);
    %   The corresponding GUMBEl values are set to 1 to avoid 
    %   numerical problems, note that for those cases the GUMBEL
    %   expressions are used
    epst(posG)=1;

    %   Modifico los parametros para incluir el numero de datos
    mut = mut1;
    psit = psit1;
    mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
    psit(pos) = psit1(pos).*kt(pos).^epst(pos);
    %   Modifico los parametros para incluir el numero de datos para GUMBEL
    mut(posG) = mut(posG)+psit(posG).*log(kt(posG));
    
    %   Evaluate the quantile, not
    %   that the general and Gumbel expresions are used
    Q(pos) = mut(pos) - (1-(-log(F)./kt(pos)).^(-epst(pos))).*psit(pos)./epst(pos);

    Q(posG) = mut(posG) - psit(posG).*log(-log(F)./kt(posG));

end



