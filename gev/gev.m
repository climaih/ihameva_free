function gev(varargin)
%   GEV Generalized Extreme Value tool.
%   GEV (gev.m) opens a graphical user interface for displaying the main program posibility.
%   Tested on Ubuntu trusty v.14.04.1(developed), Windows 7 and Mac Os X v10.9.4 (MATLAB:R2013a)
%   To load test data, please click on: -Help, Load ameva test data-, to send
%   data to matlab worksapce and work with this.
%
% Examples 1
%   load ameva;
%   Tools-->Extreme statistic-->GEV
%
% See also:
%   http://ihameva.ihcantabria.com
% -------------------------------------------------------------------------
%   gev(main program tool v2.2.7 (271004)).
%   Environmental Hydraulics Institute (IH Cantabria)
%   Santander, Spain.
% -------------------------------------------------------------------------
%   castello@unican.es
%   created with MATLAB ver.: 7.7.0.471 (R2008b) on Windows 7
%   04-07-2011 - The first distribution version
%   07-05-2013 - Old distribution version
%   20-08-2014 - Last distribution version

versionumber='v2.2.7 (271004)';
nmfctn='gev';
Ax1Pos=[0.39 0.2 0.55 0.7];
figPos=[];

idioma='eng'; usertype=1;
if length(varargin)==1, idioma=varargin{1}.idioma; usertype=varargin{1}.usertype; end

%Texto idioma
texto=amevaclass.amevatextoidioma(idioma);auxTexto='';

namesoftware=[texto('nameGev') ' ',versionumber];
if ~isempty(findobj('Tag',['Tag_' nmfctn]));
    figPos=get(findobj('Tag',['Tag_' nmfctn]),'Position');
    close(findobj('Tag',['Tag_' nmfctn]));
end

%Lista de las figuras que se van a usar en este programa
hf=[];%ventana principal
hg=[];%ventana auxiliar
hi=[];%ventana auxiliar

% Information for all buttons and Spacing between the button
colorG=[0.94 0.94 0.94];	%Color general
btnWid=0.135;
btnHt=0.05;
spacing=0.020;
top=0.95;
xPos=[0.01 0.15 0.28 0.42];

%GLOBAL VARIALBES solo usar las necesarias OJO!!
validchildren=[];
awinc=amevaclass;%ameva windows commons methods
awinc.nmfctn=nmfctn;
info=[0 0 0 0 12 1 0 0 0];%por defecto para c++ functions
criterio='Akaike'; if info(1), criterio='ProfLike'; end
OPHr=amevaOPH.oph_r;%inicializo la estructura del resultado vacio
OPHobj.x=[]; OPHobj.t=[]; OPHobj.indices=[]; OPHobj.kt=[];%Los datos de entrada vacios
parametros_all=[];
opciones=ameva_options([1 1 0],[],idioma,'','','',[]);
opciones.carpeta=[];
ConfidenceLevel=0.95;
ConfidenceLevelAlpha=1-ConfidenceLevel;

maxtype='';
sfdefine='off';
enblcode=false; %compruebo que se puede usar la dll de ameva(c++) por defecto false
if ispc && strcmp(getenv('LM_LICENSE_FIL'),'') && exist('numerical.dat','file')==2 && exist('imslcmath_imsl_dll.dll','file')>=2 && ~isdeployed,
    enblcode=true;
end

%---LA FIGURA-VENTANA PRINCIPAL-
if isempty(figPos), figPos=awinc.amevaconst('mw'); end
hf=figure('Name',namesoftware,'Color',colorG,'CloseRequestFcn',@AmevaClose, ...
    'NumberTitle','Off','DockControls','Off','Position',figPos,'Tag',['Tag_' nmfctn],'NextPlot','New');
ax1=axes('Units','normalized','Parent',hf,'Position',Ax1Pos,'Visible','Off');
set(hf,'CurrentAxes',ax1);
axdir=axes('Position',get(ax1,'Position'),'Parent',hf,'YAxisLocation','right','Color','none');
delete(axdir);
if not(isdeployed) && usertype==0, awinc.loadamevaico(hf,true); end%Load ameva ico

btnN=0.80;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_1a'), ...
    'BackgroundColor',colorG,'Position',[xPos(2) yPos+0.036 btnWid btnHt],'FontWeight','Bold');
ui_data=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'String','Data', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('on'));

ui_tp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_1'), ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaPlot);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_avm=uicontrol('Style','push','Units','normalized','Parent',hf,'Enable','Off','String','Settings', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataO1('on'));

uicontrol('Style','text','Units','normalized','Parent',hf,'String',texto('buttonPlot_2'), ...
    'BackgroundColor',colorG,'Position',[xPos(2) yPos+0.036 btnWid btnHt*0.66]);
ui_pp=uicontrol('Style','popup','Units','normalized','Parent',hf,'Enable','Off','String',texto('buttonPlot_3'), ...
    'Position',[xPos(2) yPos-0.005 btnWid btnHt],'Callback',@AmevaPlotToPrint);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_run=uicontrol('Style','pushbutton','Units','normalized','Parent',hf,'Enable','Off','String','Start', ...
    'Interruptible','on','Position',[xPos(1) yPos btnWid btnHt],'Callback',@AmevaRun);

ui_ttd=uicontrol('Style','listbox','Units','normalized','Parent',hf,'String','','FontSize',7.5, ...
    'Position',[xPos(1) 0.275 0.312 0.495],'FontName', 'FixedWidth','Visible','on');%Tabla de salida

uicontrol('Style','frame','Units','normalized','Parent',hf,'Position',[0 0 1 0.04]);
uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.59 0.004 0.18 0.03],'String',texto('saveFigure'));
ui_ckeps=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.77 0.004 btnWid/2 0.03],'String','*.eps','Value',opciones.svopt(3));
ui_ckfig=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.85 0.004 btnWid/2 0.03],'String','*.fig','Value',opciones.svopt(1));
ui_ckpng=uicontrol('Style','check','Units','normalized','Parent',hf,'Position',[0.92 0.004 btnWid/2 0.03],'String','*.png','Value',opciones.svopt(2),'Callback',@(source,event)EnableButton(ui_data,'Enable','on'));
SetSaveOpt;
ui_tb   =uicontrol('Style', 'Text','Units','normalized','Parent',hf,'Position',[0.01 0.004 0.58 0.03],...
    'String',[nmfctn,texto('dataButton')],'HorizontalAlignment','left');
%this have a valid children for figure
if isempty(validchildren), validchildren=get(hf,'children'); end

%% Data
btnWid=0.385;
btnHt=0.0725;
spacing=0.02;
top=0.98;
xPos=[0.02 0.42 0.83];
hg=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Data'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataOO('off'),'WindowButtonMotionFcn',@ListBoxCallback1,...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(hf,true); end%Load ameva ico
amvcnf.idioma=idioma;amvcnf.usertype=usertype;

btnN=1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid btnHt],...
    'String','Data & WorkSpace','Callback',@(source,event)amevaworkspace(amvcnf));
uicontrol('Style','Push','Units','normalized','Parent',hg,'String','Max. Selection', ...
    'BackgroundColor',colorG,'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)maximostemporales(amvcnf));

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','Name:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_fn=uicontrol('Style','edit','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],...
    'String','Bretagne');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','Type of max.:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_mt=uicontrol('Style','text','Units','normalized','Parent',hg,'String','','Position',[xPos(2) yPos btnWid btnHt]);
%name of variables globales
var_lab={'* Time max.:' '* X max.:' 'Sample factor:' 'Covariate:'};
for ii=1:length(var_lab)
    btnN=btnN+1;
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_td(ii)=uicontrol('Style','text','Units','normalized','Parent',hg,'Position',[xPos(1) yPos btnWid btnHt],'String',var_lab(ii),'BackgroundColor',colorG,'HorizontalAlignment','right');
    ui_all(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(2) yPos btnWid btnHt],'String','(none)','Callback',@UpdateNameAll,'UserData',ii);
    ui_cm(ii)=uicontrol('Style','popup','Units','normalized','Parent',hg,'Position',[xPos(3) yPos btnWid/2.7 btnHt],'String','(none)','visible','off','Callback',@(source,event)ltsplot(ii));
end; clear ii;

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hg,'String','X name & unit:', ...
    'BackgroundColor',colorG,'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right');
ui_fs=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','Hs', ...
    'Position',[xPos(2) yPos btnWid/2 btnHt]);
ui_fu=uicontrol('Style','edit','Units','normalized','Parent',hg,'String','m', ...
    'Position',[xPos(2)+btnWid/2 yPos btnWid/2 btnHt]);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Apply', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@AmevaSetData);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hg,'String','Close', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)FigDataOO('off'));

uicontrol('Style','frame','Units','normalized','Parent',hg,'Position',[0 0 1 0.05]);
ui_tbd=uicontrol('Style','Text','Units','normalized','Parent',hg,...
    'String',texto('dataWorkspace'), ...
    'Position',[0.01 0.004 0.99 0.04],'HorizontalAlignment','left');


%% Settings
btnWid=0.38;
xPos=[0.01 0.50];
hi=figure('Name',[upper(nmfctn(1)),nmfctn(2:end),'Settings'],'Color',colorG,'CloseRequestFcn',@(source,event)FigDataO1('off'),...
    'NumberTitle','Off','MenuBar','none','Position',awinc.amevaconst('ds'),'Resize','Off','Visible','Off','NextPlot','New');
if not(isdeployed) && usertype==0, awinc.loadamevaico(hf,true); end%Load ameva ico

% uicontrol('Style','frame','Units','normalized','Parent',hi,'Position',[0 0.85 1 0.150]);
btnN=0.90;
if enblcode
    yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
    ui_cpp=uicontrol('Style','check','Units','normalized','Parent',hi,'String',texto('settings_1'), ...
        'Position',[xPos(1) yPos btnWid btnHt],'HorizontalAlignment','right','Callback',@SetGamma0);
    uicontrol('Style','text','Units','normalized','Parent',hi,'String',texto('settings_3'), ...
        'BackgroundColor',colorG,'Position',[xPos(2)-0.125 yPos btnWid btnHt],'HorizontalAlignment','right');
    ui_lu=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+btnWid/2+0.075 yPos btnWid/2 btnHt], ...
        'String','0.2','Enable','Off');
end

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_pool=[];
if feature('numcores')-1>1
    ui_pool=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid btnHt], ...
    'String','Parallel Pool*','HorizontalAlignment','right','Value',0);
end
% uicontrol('Style','text','Units','normalized','Parent',hi,'String','Ret. Period Ts:', ...
%     'BackgroundColor',colorG,'Position',[xPos(2)-0.125 yPos btnWid btnHt],'HorizontalAlignment','center');
eval(['auxTexto=' texto('meslong') ';']);
ui_Ts=uicontrol('Style','edit','Units','normalized','Parent',hi, ...
    'Position',[xPos(2)-3*spacing yPos btnWid*.7 btnHt],'String','');
ui_Ms=uicontrol('Style','list','max',12,'min',1,'Value',1:12,'Units','normalized','Parent',hi, ...
    'Position',[xPos(2)+btnWid*.6 yPos btnWid*.7 btnHt],'String',auxTexto);
uicontrol('Style','text','Units','normalized','Parent',hi,'String','Ret.Period Ts', ...
    'Position',[xPos(2)-3*spacing yPos+spacing*3.5 btnWid*.8 btnHt*.5],'HorizontalAlignment','left');
uicontrol('Style','text','Units','normalized','Parent',hi,'String','R.P. Months', ...
    'Position',[xPos(2)+btnWid*.6 yPos+spacing*3.5 btnWid*.8 btnHt*.5],'HorizontalAlignment','left');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
eval(['auxTexto=' texto('settings_2') ';']);
ui_fg=uicontrol('Style','popup','Units','normalized','Parent',hi,'Position',[xPos(1) yPos 1 btnHt],'String',auxTexto);

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_kt=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos 1 btnHt],...
    'String',texto('settings_4'),'value',0,'Enable',sfdefine);

btnN=btnN+1.5;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ac(1)=uicontrol('Style','radiobutton','Units','normalized', ...
    'String','PLC','Value',1,'Position',[xPos(1) yPos btnWid*0.5 btnHt]);
ui_ac(2)=uicontrol('Style','radiobutton','Units','normalized', ...
    'String','AIC','Value',0,'Position',[xPos(1)+btnWid*0.5 yPos btnWid*0.5 btnHt]);
set(ui_ac(1),'userdata',ui_ac(2));	set(ui_ac(2),'userdata',ui_ac(1));
call=['me = get(gcf,''CurrentObject'');',...
    'if (get(me,''Value'')==1),',...
    'set(get(me,''userdata''),''Value'',0),',...
    'else,',		'set(me,''Value'',1),',		'end;clear me;'];
set(ui_ac, 'callback',call);
ui_nq=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid btnHt],...
    'String',num2str(ConfidenceLevelAlpha));
uicontrol('Style','text','Units','normalized','Parent',hi,'String',texto('settings_5'), ...
    'Position',[xPos(1)+spacing yPos+spacing*3.5 btnWid*1.2 btnHt*.5],'HorizontalAlignment','left');
uicontrol('Style','text','Units','normalized','Parent',hi,'String',texto('significanLevel'), ...
    'Position',[xPos(2)+spacing yPos+spacing*3.5 btnWid*1.2 btnHt*.5],'HorizontalAlignment','left');

btnN=btnN+1.25;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','text','Units','normalized','Parent',hi,'String',texto('settings_6'), ...
    'Position',[xPos(1)+spacing yPos+spacing*3.5 btnWid*2.5 btnHt*.5],'HorizontalAlignment','left');
ui_il=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid*2 btnHt],...
    'String',texto('settings_7'),'Value',0,'Callback',@actcdfgev);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);%texto('settings_8')
ui_ie=uicontrol('Style','popup','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid*2 btnHt],...
    'String',{'0-Not Covariance','1-Covariance(Locate,Scale)','2-Covariance(Loc.,Scale,Shape)'},'Value',0,'Callback',@CovariableGev);
btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_ia=uicontrol('Style','check','Units','normalized','Parent',hi,'Position',[xPos(1) yPos btnWid*2 btnHt],...
    'String',texto('settings_9'),'Value',0,'Callback',@ArmonicGev);

btnN=btnN+0.85;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
ui_aa=uicontrol('Style','check','Units','normalized','Parent',hi,'String',texto('settings_10'),'Enable','Off', ...
    'Position',[xPos(1)+spacing yPos btnWid*1.2 btnHt],'HorizontalAlignment','right','value',0,'Callback',@AutoArmonicGev);
ui_al=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2) yPos btnWid/3 btnHt],...
    'String','0');
ui_ae=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+btnWid/3 yPos btnWid/3 btnHt],...
    'String','0');
ui_af=uicontrol('Style','edit','Units','normalized','Parent',hi,'Position',[xPos(2)+2*btnWid/3 yPos btnWid/3 btnHt],...
    'String','0');

btnN=btnN+1;
yPos=top-btnHt-(btnN-1)*(btnHt+spacing);
uicontrol('Style','push','Units','normalized','Parent',hi,'String','Apply', ...
    'Position',[xPos(2) yPos btnWid btnHt],'Callback',@(source,event)AmevaSetSett(true));
uicontrol('Style','push','Units','normalized','Parent',hi,'String','Close', ...
    'Position',[xPos(1) yPos btnWid btnHt],'Callback',@(source,event)FigDataO1('off'));

actcdfgev;%actulizo la cdf gev

%% C0 Open,Close figure data
    function FigDataOO(state,source,event)
        set(hg,'Visible',state);
    end
%% C0 Open,Close figure Settings
    function FigDataO1(state,source,event)
        set(hi,'Visible',state);
        if strcmp(state,'on'), actcdfgev; end %actulizo la cdf gev
    end
%% C0 Actulizo los botones Enable on/off & The figure open/close visible on/off
    function EnableButton(listbutton,statetyp,statebutton)
        set(listbutton,statetyp,statebutton);
    end
%% C0
    function ListBoxCallback1(source,event) % Load workspace vars into list box
        awinc.ListBoxCallback(ui_all,true);%Lista tambien las structuras
    end
%% C0
    function SetSaveOpt
        if isdeployed,
            set(ui_ckfig,'Value',1,'Enable','On','Visible','On');
            set(ui_ckeps,'Value',0,'Enable','On','Visible','On');
        end
    end
%% C0
    function AmevaClose(source,event) % Close all Ameva windows
        awinc.AmevaClose([hf,hg,hi], ...
            {[upper(nmfctn(1)),nmfctn(2:end),'Data'] [upper(nmfctn(1)),nmfctn(2:end),'Settings']});
    end
%% C0
    function AmevaPlotToPrint(source,event) % List of fig
        awinc.AmevaPlotToPrint(ui_pp,opciones.carpeta);
    end
%% C0 Update cell and matrix name
    function UpdateNameAll(source,event)
        nval=get(source,'UserData');
        awinc.AmevaUpdateName(nval,ui_cm,ui_all,ui_td);
        ltsplot(nval);% Refresh plot
    end
%% C1 Refresh plot
    function ltsplot(nlv,source,event)
        if get(ui_all(nlv),'Value')>1,
            nlvn=get(ui_td(nlv),'String');
            nlvn=char(nlvn(1));
            try
                awinc.AmevaEvalData(nlv,ui_cm,ui_all);
                X_=awinc.data;
                if not(isfloat(X_)), disp(texto('notDoubleVector')); return; end
                nmsize=size(X_);
                set(ui_td(nlv),'string',{nlvn,sprintf('(%dx%d) %s',nmsize(1),nmsize(2),class(X_))});
                set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');set(ax1,'Position',Ax1Pos);
                if min(nmsize)==1
                    plot(ax1,X_,'k');
                else
                    plot(ax1,X_);
                end
                grid on;
                title(ax1,'Serie','FontWeight','bold');
            catch ME
                disp(texto('variableErase'));
                rethrow(ME);
            end
        end
    end
%% actualizo la CDF gev
    function actcdfgev(source,event)
        %aqui el plot
        LocScaleShapeState(1)=get(ui_ia,'Value');
        LocScaleShapeState(2)=get(ui_il,'Value');
        LocScaleShapeState(3)=get(ui_ie,'Value')-1;
        set(0,'CurrentFigure',hf); set(hf,'CurrentAxes',ax1); cla(ax1,'reset');
        amevaOPH.GEV_expression(LocScaleShapeState,OPHr,ax1);
    end
%%
    function SetGamma0(source,event)
        auxi=get(ui_cpp,'Value');
        if auxi,
            set(ui_lu,'Enable','On','String','0.2');
        else
            set(ui_lu,'Enable','Off');
        end
    end
%% Armonicos GEV
    function ArmonicGev(source,event)
        auxi=get(ui_ia,'Value');
        if auxi
            set(ui_aa,'Enable','on');
            set(ui_aa,'Value',1);
        else
            set(ui_aa,'Enable','off');
            set(ui_aa,'Value',0);
        end
        AutoArmonicGev;
    end
%% Auto-Armonicos GEV
    function AutoArmonicGev(source,event)
        if get(ui_ia,'Value')
            auxi=get(ui_aa,'Value');
            if auxi
                EnableButton([ui_al,ui_ae,ui_af],'Enable','off');
                set(ui_al,'String','0');
                set(ui_ae,'String','0');
                set(ui_af,'String','0');
            else
                EnableButton([ui_al,ui_ae,ui_af],'Enable','on');
                set(ui_al,'String','2');
                set(ui_ae,'String','2');
                set(ui_af,'String','1');
            end
        end
        actcdfgev;drawnow;
    end
%% Covariables GEV
    function CovariableGev(source,event)
        auxi=get(ui_ie,'Value')-1
        if auxi>0
            set(ui_ia,'Value',1);
            set(ui_il,'Value',1);
        end
        ArmonicGev;
    end
%% Apply comprobamos que los Data para calcular gev
    function AmevaSetSett(ASSstate,source,event)
        %Creo el directorio de trabajo para almacenamiento
        awinc.NewFolderCheck(opciones.carpeta,nmfctn);
        opciones.carpeta=awinc.carpeta;% Methods for "amevaclass"        
        EnableButton([ui_tp,ui_pp],'Enable','off');
        %Settings parameter
        opciones.cityname=get(ui_fn,'String');%region name
        %en la funcion de matlab se usa 2:4 armonico=info(2);trends=info(3);indice=info(4);
        info(1)=get(ui_ac(1),'Value');%aic Chi2 check (0=aic, 1=Chi2)
        criterio='Akaike'; if info(1), criterio='ProfLike'; end
        info(2)=get(ui_ia,'Value');%intra_annual check
        info(3)=get(ui_il,'Value');%long term(Trend) check
        info(4)=get(ui_ie,'Value')-1;%inter-annual check
        info(5)=12;%max. armonicos X3(se usa en c++). En Matlab itermax=100
        info(6)=get(ui_aa,'Value');%auto-armonicos
        info(7)=str2double(get(ui_al,'String'));%arm. location
        info(8)=str2double(get(ui_ae,'String'));%arm. scale
        info(9)=str2double(get(ui_af,'String'));%arm. shape
        info(10)=get(ui_fg,'Value')-1;%force gamma0
        ktbool=get(ui_kt,'Value');%kt check
        if ktbool==0, OPHobj.kt=ones(size(OPHobj.x)); end;%Si esta sin seleccionar no se usa kt
        ConfidenceLevelAlpha=str2double(get(ui_nq,'String'));%quantile chi2
        if enblcode, opciones.luint=str2double(get(ui_lu,'String')); end; %low up limit form parameter
        FigDataO1('off');
        if ASSstate,
            OPHr=amevaOPH.oph_r;%reset la estructura del resultado vacio
            actcdfgev; 
        end; drawnow;
        if ~strcmp(get(hg,'NextPlot'),'new'), set(hg,'NextPlot','new'); end;refresh(hg);
        if ~strcmp(get(hi,'NextPlot'),'new'), set(hi,'NextPlot','new'); end;refresh(hi);
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
    end
%% Apply comprobamos que los Data para calcular la calibaracion son
    function AmevaSetData(source,event)
        %reset variables
        OPHobj.x=[]; OPHobj.t=[]; OPHobj.kt=[]; OPHobj.indices=[];
        %leo el nombre de variable y la magnitud
        opciones.var_name={get(ui_fs,'String')};
        opciones.var_unit={get(ui_fu,'String')};
        opciones.cityname=get(ui_fn,'String');
        ione=get(ui_all(1),'Value');
        itwo=get(ui_all(2),'Value');
        ithr=get(ui_all(3),'Value');
        ifou=get(ui_all(4),'Value');
        sfou=get(ui_all(4),'String');
        if ione>1,
            awinc.AmevaEvalData(1,ui_cm,ui_all);
            awinc.AmevaTimeCheck(ui_tbd,awinc.data);	
            if awinc.timestate==true, 
                OPHobj.t=yearlytimescale(awinc.data,'R');
            elseif awinc.timestate==false,
                OPHobj.t=awinc.data;
            else
                disp(texto('dataFormat'));
                set(hg,'Visible','on'); return;
            end %maximum annual time scale normalizado para usar GEV
            %tipo de maximo
            [Km,maxtype]=amevaOPH.FrecTemporal(OPHobj.t,awinc.data,awinc.timestate);
            set(ui_mt,'String',maxtype); drawnow;
        else
            %disp(texto('notTemporaryData'));
            %Time=(1:length(OPHobj.x))';
            error(texto('timeNecessary'));
        end
        if itwo>1,
            awinc.AmevaEvalData(2,ui_cm,ui_all);
            OPHobj.x=awinc.data;
        else
            error(texto('dataHsmax'));
        end
        set(hg,'Visible','off');set(ui_tbd,'String',texto('applyData'));drawnow;
        if ithr>1, %compruebo que se va a cargar kt
            awinc.AmevaEvalData(3,ui_cm,ui_all);
            OPHobj.kt=awinc.data;
            sfdefine='On';
            set(ui_kt,'Value',1);
        else
            OPHobj.kt=ones(size(OPHobj.x));
            set(ui_kt,'Value',0);
            sfdefine='Off';
        end
        set(ui_kt,'Enable',sfdefine);
        if ifou>1, %compruebo que se van a cargar indices
            awinc.AmevaEvalData(4,ui_cm,ui_all);
            OPHobj.indices=awinc.data;
            %OPHobj.indices=evalin('base',sfou{ifou});
        
            set(ui_ie,'Value',1+1,'Enable','on');
            set(ui_ia,'Value',1);
            set(ui_il,'Value',1);
        else
            OPHobj.indices=[];
            set(ui_ie,'Value',0+1,'Enable','off');
            set(ui_ia,'Value',0);
            set(ui_il,'Value',0);
        end
        %compruebo y elimino si existen los NaNs en OPHobj.x
        if ~isempty(OPHobj.indices), OPHobj.indices(isnan(OPHobj.x),:) = []; end
        OPHobj.t(isnan(OPHobj.x)) = [];
        OPHobj.kt(isnan(OPHobj.x)) = [];
        if sum(isnan(OPHobj.x))>0, disp(texto('deleteData')); end
        OPHobj.x(isnan(OPHobj.x)) = [];
        %compruebo que las variables sean diferentes
        awinc.AmevaVarCheck(ui_tbd,OPHobj.x,OPHobj.t,'Hsmax','datemax');	if awinc.state, set(hg,'Visible','on'); return; end,
        CovariableGev;
        AmevaSetSett(false);% Set default setting
        set(hg,'Visible','off');
        set(hi,'Visible','off');
        set(ui_tbd,'String',texto('applyData'));drawnow ;
        
        if(length(OPHobj.x)<50), set(ui_tbd,'String',texto('notEnoughData'));end  
        %re-inicializo los ejes y variables
        set(ui_avm,'Enable','on');
        set(ui_run,'Enable','on');
        set(ui_tp,'Enable','off');
        set(ui_pp,'Enable','off');
        %set(hf,'Toolbar','figure');
        
        set(0,'CurrentFigure',hf);
        set(hf,'CurrentAxes',ax1);cla(ax1,'reset');
        OPHobj.Km=Km;
        amevaOPH.gevplots(OPHobj,[],[],[],[],{'Hsmax'},opciones,ax1);

        set(ui_tb,'String',[texto('clickStart') ', (Your data are: ' maxtype ')']);drawnow ;
    end % listBoxCallback

%% Run data
    function AmevaRun(source,event)
        EnableButton([ui_data,ui_tp,ui_pp,ui_avm,ui_run],'Enable','off');
        EnableButton([hg,hi],'Visible','off');
        SolTextLatex='';
        set(ui_ttd,'String',SolTextLatex);
        set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
        
        %Run function
%         try
            %save options
            opciones.svopt(1)=get(ui_ckfig,'Value');%fig
            opciones.svopt(2)=get(ui_ckpng,'Value');%png
            opciones.svopt(3)=get(ui_ckeps,'Value');%eps
            opciones.myMeses=get(ui_Ms,'Value');%Periodos de retorno Meses
            Ts=str2num(get(ui_Ts,'String'));%Periodos de retorno
            if ~isempty(Ts) && isfloat(Ts) && Ts(1)>1, opciones.Ts=Ts; end
            ConfidenceLevel=1-ConfidenceLevelAlpha;
            codestr='MatLab';
            if  enblcode==false || get(ui_cpp,'Value')==0,
                opciones.poolparalelo=false;
                if ishandle(ui_pool), opciones.poolparalelo=get(ui_pool,'Value'); end%Activar el cal. en paralelo
                
                [OPHr,OPHobj,parametros_all,IT,tpstring] = ...
                    amevaFuncAutoAdjust.AutoAjuste(OPHobj.x,OPHobj.t,OPHobj.kt,OPHobj.indices,info,ConfidenceLevel,opciones);
            else
                codestr='C++';
                [OPHr,OPHobj,parametros_all,IT,tpstring] = ...
                    amevaFuncAutoAdjust.c_AutoAjuste(OPHobj.x,OPHobj.t,OPHobj.kt,OPHobj.indices,info,ConfidenceLevel,opciones);
                % [exito,P,LNG,ALLP,loglikeobj,grad,hessian,mut,psit,epst,lista,lista2] = ...
                % c_ophgev(OPHobj.x,OPHobj.t,info,OPHobj.kt,OPHobj.indices,OPHobj.indices,opciones.luint,ConfidenceLevel);
                % if (~isempty(hessian) && exito==0),
                % Ts=[2.0 5.0 10.0 20.0 25.0 75.0 100.0 200.0 300.0 400.0 500.0];
                % [Z,ZLO,ZUP,Ts]=c_ConfidInterQuanAggregate(hessian,OPHobj.t,P,LNG,0,1,Ts,OPHobj.indices,OPHobj.indices,maxtype);
                % semilogx(ax1,Ts,Z,'--r',Ts,ZLO,'--b',Ts,ZUP,'--g');xlabel('T years');title(opciones.cityname);
                % end
            end
%         catch ME
%             EnableButton([ui_data,ui_tp,ui_pp,ui_avm,ui_run],'Enable','on');
%             rethrow(ME);
%         end
        set(ui_tp,'String', tpstring);%Set Sting Plots. set(ui_pp,'String','Plot to print');
        %listar los plot para imprimir
        awinc.ListBoxCallbackP(ui_pp,opciones.carpeta);% Methods for "amevaclass"
        
        %Expresiones para poner en axis ax1, Salida de la tabla con los resultados
        rowLabels=parametros_all.rowLabels;
        numabc=length(parametros_all.solucion);
        rowLabels=strrep(rowLabels, '$', '');
        rowLabels=strrep(rowLabels, '\', ' ');
        rowLabels=strrep(rowLabels, '{', '');
        rowLabels=strrep(rowLabels, '{', '');
        rowLabels=strrep(rowLabels, '}', '');
        rowLabels=strrep(rowLabels, '_', ' ');
        rowLabels=strrep(rowLabels, '^', '');
        rowLabels=char(rowLabels);
        SolTextLatex=[char([rowLabels,{[texto('code') codestr],texto('stopCri')}]) char([num2str(parametros_all.solucion,4) {'',criterio}]) char([rowLabels,{'',''}]) ];
        
        set(ui_ttd,'String',SolTextLatex,'Value',numabc+2,'Visible','on','Max',numabc+2,'FontSize',8,'FontWeight', 'light');
        set(ui_tb,'String',[nmfctn,texto('start')]);drawnow ;

        %this have a valid children for figure
        if isempty(validchildren)
            validchildren=get(gcf,'children');
        else
            newchildren=get(gcf,'children');%borro los nuevos children de la figura
            if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
                [CI,II]=setdiff(newchildren,validchildren);
                delete(newchildren(II));
            end
        end
        
        set(ui_tp,'Value',1);AmevaPlot;

        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
        EnableButton([ui_data,ui_tp,ui_pp,ui_avm,ui_run],'Enable','on');
    end

%% Ameva plot
    function AmevaPlot(source,event)
        iins=get(ui_tp,'String');
        [ni mi]=size(iins);
        if ni==1, return; end;
        plotname=iins(get(ui_tp,'Value'));%plot name
        
        if ~ishandle(ax1), disp([texto('plot') ' ',nmfctn]); return; end
        if ishandle(axdir),  delete(axdir); end
        legend(ax1,'off')
        
        newchildren=get(gcf,'children');%borro los nuevos children de la figura
        if length(newchildren)>length(validchildren) || not(isequal(newchildren,validchildren)),
            [CI,II]=setdiff(newchildren,validchildren);
            delete(newchildren(II));
        end
        
        newchildren=get(gcf,'children');%reset all axis
        for i=1:length(newchildren)%reset only axes !!
            if strcmp(get(newchildren(i),'type'),'axes')
                newchildren(i);
                cla(newchildren(i),'reset');
                %axes(newchildren(i));
                axis normal;%selects ax1 as the current axes
                %if ishandle(cbar_ax1), colorbar('delete'); end
                %cbar_ax1 = colorbar;
            end
        end
        
        if ~strcmp(get(hf,'NextPlot'),'add'), set(hf,'NextPlot','add'); end;
        EnableButton([ui_data,ui_tp,ui_pp,ui_avm,ui_run],'Enable','off');
        try
            set(ui_tb,'String',[nmfctn,texto('run')]);drawnow ;
            LocScaleShapeState=info(2:4);
            amevaOPH.gevplots(OPHobj,OPHr,parametros_all,ConfidenceLevelAlpha,LocScaleShapeState,plotname,opciones,ax1);
        catch ME
            EnableButton([ui_data,ui_tp,ui_pp,ui_avm,ui_run],'Enable','on');
            rethrow(ME);
        end
        if ~strcmp(get(hf,'NextPlot'),'new'), set(hf,'NextPlot','new'); end;refresh(hf);
        set(ui_tb,'String',[nmfctn, texto('ready_1')]);drawnow ;
        EnableButton([ui_data,ui_tp,ui_pp,ui_avm,ui_run],'Enable','on');
    end

end
