function [stdQuan,jacob] = ConfidInterQuanAggregate (q,t0,t1,Km,x,t,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2)

if nargin<13, betaT = 0;end
if nargin<14, betaT2 = 0;end
if nargin<15, varphi = [];end
if nargin<16, varphi2 = [];end
if nargin<17, indices = [];end
if nargin<18, indices2 = [];end


%   Vector lengths
nmu=length(beta);
npsi=length(alpha);
neps0=length(gamma0);
neps=length(gamma);
ntend=length(betaT);
ntend2=length(betaT2);
nind=length(varphi);
nind2=length(varphi2);
%   Total length
n = 1+nmu+ntend+1+npsi+ntend2+neps0+neps+nind+nind2;
%   Initializing the Jacobian
jacob = zeros(n,1);

epsi = 10^(-4);
%   beta0 derivative
aux = 0;

if ~isempty(beta0),
    aux = aux+1;
    jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0*(1+epsi),beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t)-...
              QuantileAgregado (q,t0,t1,Km,kt,beta0*(1-epsi),beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t))/(2*beta0*epsi);
end
%   beta derivative
if ~isempty(beta),
    for i = 1:nmu,
        aux = aux+1;
        beta1 = beta; beta2=beta;
        beta2(i) = beta(i)*(1+epsi); 
        beta1(i) = beta(i)*(1-epsi); 
        jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta2,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t)-...
              QuantileAgregado (q,t0,t1,Km,kt,beta0,beta1,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t))/(2*beta(i)*epsi);
    end
end
%   betaT derivative
if ~isempty(betaT),
    aux = aux+1;
    jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT*(1+epsi),betaT2,varphi,varphi2,indices,indices2,t)-...
          QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT*(1-epsi),betaT2,varphi,varphi2,indices,indices2,t))/(2*betaT*epsi);
end
%   varphi derivative
if ~isempty(varphi),
    for i = 1:nind,
        aux = aux+1;
        if varphi(i)~=0,
            varphi1b = varphi; varphi2b=varphi;
            varphi2b(i) = varphi(i)*(1+epsi); 
            varphi1b(i) = varphi(i)*(1-epsi); 
            jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2b,varphi2,indices,indices2,t)-...
                  QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi1b,varphi2,indices,indices2,t))/(2*varphi(i)*epsi);
        else
            jacob(aux) = 0;
        end
    end
end
%   alpha0 derivative
if ~isempty(beta0),
    aux = aux+1;
    jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0*(1+epsi),alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t)-...
        QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0*(1-epsi),alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t))/(2*alpha0*epsi);
end
%   alpha derivative
if ~isempty(alpha),
    for i = 1:npsi,
        aux = aux+1;
        alpha1 = alpha; alpha2=alpha;
        alpha2(i) = alpha(i)*(1+epsi); 
        alpha1(i) = alpha(i)*(1-epsi); 
        jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha2,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t)-...
              QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha1,gamma0,gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t))/(2*alpha(i)*epsi);
    end
end
%   betaT2 derivative
if ~isempty(betaT2),
    aux = aux+1;
    jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2*(1+epsi),varphi,varphi2,indices,indices2,t)-...
          QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2*(1-epsi),varphi,varphi2,indices,indices2,t))/(2*betaT2*epsi);
end
%   varphi2 derivative
if ~isempty(varphi2),
    for i = 1:nind2,
        aux = aux+1;
        if varphi2(i)~=0,
        varphi1b = varphi2; varphi2b=varphi2;
        varphi2b(i) = varphi(i)*(1+epsi); 
        varphi1b(i) = varphi(i)*(1-epsi); 
        jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi2b,indices,indices2,t)-...
              QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi,varphi1b,indices,indices2,t))/(2*varphi2(i)*epsi);
        else
            jacob(aux) = 0;
        end
    end
end
%   gamma0 derivative
if ~isempty(gamma0),
    aux = aux+1;
    jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0*(1+epsi),gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t)-...
                  QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0*(1-epsi),gamma,betaT,betaT2,varphi,varphi2,indices,indices2,t))/(2*gamma0*epsi);
end
%   gamma derivative
if ~isempty(gamma),
    for i = 1:neps,
        aux = aux+1;
        gamma1 = gamma; gamma2=gamma;
        gamma2(i) = gamma(i)*(1+epsi);
        gamma1(i) = gamma(i)*(1-epsi);
        jacob(aux) = (QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma2,betaT,betaT2,varphi,varphi2,indices,indices2,t)-...
              QuantileAgregado (q,t0,t1,Km,kt,beta0,beta,alpha0,alpha,gamma0,gamma1,betaT,betaT2,varphi,varphi2,indices,indices2,t))/(2*gamma(i)*epsi);
    end
end

%   Evaluate the requires hessian for confidence interval estimation
[f Jx Hxx] = loglikelihood (x,t,kt,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,indices,indices2);

%   LU decomposition of the Information matrix
[LX,UX,PX] = lu(-Hxx);

%   Inverse, ie variance-covariance matrix
varcov = (UX)\(LX\(PX*eye(n)));
%   Standard deviation
% if ~isempty(varphi) & ~isempty(varphi2),
%     varcov = [varcov(:,1:1+nmu+ntend) varcov(:,1+nmu+ntend+nind+1:1+nmu+ntend+nind+1+npsi+ntend2) varcov(:,1+nmu+ntend+nind+1+npsi+ntend2+nind2+1:end)];
%     varcov = [varcov(1:1+nmu+ntend,:); varcov(1+nmu+ntend+nind+1:1+nmu+ntend+nind+1+npsi+ntend2,:); varcov(1+nmu+ntend+nind+1+npsi+ntend2+nind2+1:end,:)];
% end

stdQuan = sqrt(jacob'*varcov*jacob);
