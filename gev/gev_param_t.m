function [mut,psit,epst,pos,posG]=gev_param_t(t,kt,indices,OPHr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Evaluation of the final parameters for plotting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% t = linspace(0,1,100)';

[beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,betaT3,varphi,varphi2,varphi3,lista,lista2,lista3,p,np]=amevaOPH.gevparamsol2array(OPHr);

%   Evaluate the location parameter at each time t as a function
%   of the actual values of the parameters
mut1 = parametro(t,beta0,beta,betaT,indices(:,lista),varphi);
%   Evaluate the scale parameter at each time t as a function
%   of the actual values of the parameters
psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices(:,lista2),varphi2));

%   Evaluate the shape parameter at each time t as a function
%   of the actual values of the parameters
epst = parametro(t,gamma0,gamma);

%   The values whose shape parameter is almost cero corresponds to
%   the GUMBEL distribution, locate their positions if they exist
posG = find(abs(epst)<=1e-8);
%   The remaining values correspond to WEIBULL or FRECHET
pos  = find(abs(epst)>1e-8);

%   Modifico los parametros para incluir el numero de datos
mut = mut1;
psit = psit1;
mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
psit(pos) = psit1(pos).*kt(pos).^epst(pos);
%   Modifico los parametros para incluir el numero de datos para GUMBEL
mut(posG) = mut(posG)+psit(posG).*log(kt(posG));

%   The values whose shape parameter is almost cero corresponds to
%   The corresponding GUMBEl values are set to 1 to avoid 
%   numerical problems, note that for those cases the GUMBEL
%   expressions are used
epst(posG)=1;


end