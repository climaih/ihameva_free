function Zt = Zstandardt (x,t,kt,indices,indices2,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   ZSTANDARD function calculates the standardized variable corresponding
%    to the given parameters
%
%   Input:
%       x -> maximum data
%       t -> time when the data occur within a yearly scale
%       kt -> Frequency parameter to measure the importance od the number
%       of points
%       betaT -> tendency for the location parameter, just need to include
%       a number in order to consider it, otherwise leave it empty []
%       indices -> covariates data related to the location parameter, a
%       matrix including the data at time t for each covariate
%       varphi -> covariate parameter vector associated with the location
%       parameter, just need to include a list of numbers of length the
%       number of covariates, otherwise leave it empty []
%       betaT2 -> tendency for the scale parameter, just need to include
%       a number in order to consider it, otherwise leave it empty []
%       indices2 -> covariates data related to the scale parameter, a
%       matrix including the data at time t for each covariate
%       varphi2 -> covariate parameter vector associated with the scale
%       parameter, just need to include a list of numbers of length the
%       number of covariates, otherwise leave it empty []
%
%   Output:
%        Zt-> Standardized values
%
%
%   Authors: R. Minguez, F.J. Mendez, C. Izaguirre, M. Menendez,
%   and I.J. Losada 
%   Environmental and Hydraulics Institute "IH Cantabria"
%   University of Cantabria 
%   E.T.S. de Ingenieros de Caminos, Canales y Puertos 
%   Avda de los Castros, s/n 
%   39005 Santander, Spain
%   Tfno.: +34 942 20 18 52 
%   Fax: +34 942 20 18 60 
%   Corresponding author email: roberto.minguez@unican.es
%
%   Created: 09/09/2009
%
%   For more details see the paper:
%   "Pseudo-Optimal Parameter Selection of Non-Stationary 
%   Generalized Extreme Value Models for Environmental Variables". 
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Size of data
[m,n] = size(x);
[m1,n1] = size(t);
if m1 ~= m || n1 ~= n,
    error('Las dimensiones de Hs y t han de ser las mismas')
end
if isempty(kt),
    kt = ones(size(x));
end
%   Evaluate the location parameter at each time t as a function
%   of the actual values of the parameters
mut1 = parametro(t,beta0,beta,betaT,indices,varphi);
%   Evaluate the scale parameter at each time t as a function
%   of the actual values of the parameters
psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2));
%   Evaluate the shape parameter at each time t as a function
%   of the actual values of the parameters
epst = parametro(t,gamma0,gamma);

%   The values whose shape parameter is almost cero corresponds to
%   the GUMBEL distribution, locate their positions if they exist
posG = find(abs(epst)<=1e-8);
%   The remaining values correspond to WEIBULL or FRECHET
pos  = find(abs(epst)>1e-8);

%   Modifico los parametros para incluir el numero de datos
mut = mut1;
psit = psit1;
mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
psit(pos) = psit1(pos).*kt(pos).^epst(pos);
%   Modifico los parametros para incluir el numero de datos para GUMBEL
mut(posG) = mut(posG)+psit(posG).*log(kt(posG));

Zt = zeros (m,1);
%   WEIBULL or FRECHET values
Zt(pos) = (1./epst(pos)).*log(1+epst(pos).*((x(pos)-mut(pos))./psit(pos)));
%   GUMBEL values
Zt(posG) = (x(posG)-mut(posG))./psit(posG);
