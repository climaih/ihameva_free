function zqout = QuantileAgregadoII (q,t0,t1,kt,beta0,beta,alpha0,alpha,gamma0,gamma,zqini,betaT,betaT2,varphi,varphi2,indices,indices2,times)
%   Util para incluir las covariables, e integrar en varios anos pero
%   expresando todo en escala anual

if nargin<11, zqini = [];end
if nargin<12, betaT = 0;end
if nargin<13, betaT2 = 0;end
if nargin<14, varphi2 = [];end
if nargin<15, varphi = [];end
if nargin<16, indices = [];end
if nargin<17, indices2 = [];end
if nargin<18, times = [];end


[m,n] = size(q);
[m1,n1] = size(t0);
if m1~=m | n1~=n,
    error('Initial quantile aggregated integration time size must be equal than the quantile size');
end
[m2,n2] = size(t1);
if m2~=m | n2~=n,
    error('Final quantile aggregated integration time size must be equal than the quantile size');
end

%   For the required period the mean value of the corresponding covariates
%   is calculated and considered constant for the rest of the study
if ~isempty(times),
    pos = find(times>=t0 & times<=t1);
    indicesint = zeros(length(varphi),1);
    indices2int = zeros(length(varphi2),1);
    if ~isempty(pos),
        for i = 1:length(varphi),
            indicesint(i)=mean(indices(pos,i));
        end
        for i = 1:length(varphi2),
            indices2int(i)=mean(indices2(pos,i));
        end
    end
else
    indicesint = [];
    indices2int = [];
end    
 
%   Requires quantile
zqout = zeros(m,n);

% times = [];

[media cnt]= bsimp(@(x)parametro(x,beta0,beta,betaT,indices,varphi,indicesint,times),0,1);
[std cnt]= bsimp(@(x)exp(parametro(x,alpha0,alpha,betaT2,indices2,varphi2,indices2int,times)),0,1);

mediaM = media;
        
for i1 = 1:m,
    for j1 = 1:n,
        if isempty(zqini)
            zq = media;
        else
            zq = zqini;
        end
        err = 1;
        iter = 1;
        while err>10^(-4) && iter<1000,
            zqold = zq;
            [int cnt]=bsimp(@(x)fzeroquanint(x,zqold,q(i1,j1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2,varphi,indices,indices2,indicesint,indices2int,times,kt),t0(i1,j1),t1(i1,j1));
            int=int+log(q(i1,j1))/12*(t1(i1,j1)-t0(i1,j1));
            [dint cnt]=bsimp(@(x)fzeroderiquanint(x,zqold,q(i1,j1),beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2,varphi,indices,indices2,indicesint,indices2int,times,kt),t0(i1,j1),t1(i1,j1));
            zq = zq - int/dint;
            if abs(zqold)>10^(-5),
               err = abs((zq - zqold)/zqold);
           else
               err = abs((zq - zqold));
            end
           iter = iter + 1;
        end
        if iter==1000,
           zq = NaN;
           disp(['WARNING: Maximum number of Newton iterations: error= ' num2str(err)]) 
        end
        if int>10^(-5),
            disp('WARNING: False zero, check it')
            zq = NaN;
        end
        zqout(i1,j1)=zq;
        
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function to solve the quantile
    function zn = fzeroquanint(t,zq,q,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2,varphi,indices,indices2,indicesint,indices2int,times,ktold)
        %   Location
        mut1 = parametro(t,beta0,beta,betaT,indices,varphi,indicesint,times);
        %   Scale
        psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2,indices2int,times));
        %   Shape
        epst = parametro(t,gamma0,gamma);

        %   The values whose shape parameter is almost cero corresponds to
        %   the GUMBEL distribution, locate their positions if they exist
        posG = find(abs(epst)<=1e-8);
        %   The remaining values correspond to WEIBULL or FRECHET
        pos  = find(abs(epst)>1e-8);
        %   The corresponding GUMBEl values are set to 1 to avoid 
        %   numerical problems, note that for those cases the GUMBEL
        %   expressions are used
        epst(posG)=1;
        
        if ~isempty(times),
            kt2 = spline(times,ktold,t);
        else
            kt2 = ones(size(mut1));
        end

        %   Modifico los parametros para incluir el numero de datos
        mut = mut1;
        psit = psit1;
        mut(pos) = mut1(pos)+psit1(pos).*(kt2(pos).^epst(pos)-1)./epst(pos);
        psit(pos) = psit1(pos).*kt2(pos).^epst(pos);
        %   Modifico los parametros para incluir el numero de datos para GUMBEL
        mut(posG) = mut(posG)+psit(posG).*log(kt2(posG));

        %   Evaluate auxiliary variables
        xn = (zq-mut)./psit; 
        z = 1 + epst.*xn; 
        %   Since the z-values must be greater than zero in order to avoid
        %   numerical problems their values are set to be greater than 1e-4
        z = max(1e-8,z);
        zn =z.^(-1./epst); 
        %    For the Gumbel case
        zn(posG) = exp(-xn(posG));
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function to solve the quantile
    function zn = fzeroderiquanint(t,zq,q,beta0,beta,alpha0,alpha,gamma0,gamma,betaT,betaT2,varphi2,varphi,indices,indices2,indicesint,indices2int,times,ktold2)
        %   Location
        mut1 = parametro(t,beta0,beta,betaT,indices,varphi,indicesint,times);
        %   Scale
        psit1 = exp(parametro(t,alpha0,alpha,betaT2,indices2,varphi2,indices2int,times));
        %   Shape
        epst = parametro(t,gamma0,gamma);

        %   The values whose shape parameter is almost cero corresponds to
        %   the GUMBEL distribution, locate their positions if they exist
        posG = find(abs(epst)<=1e-8);
        %   The remaining values correspond to WEIBULL or FRECHET
        pos  = find(abs(epst)>1e-8);
        %   The corresponding GUMBEl values are set to 1 to avoid 
        %   numerical problems, note that for those cases the GUMBEL
        %   expressions are used
        epst(posG)=1;
        
        if ~isempty(times),
            kt2 = spline(times,ktold2,t);
        else
            kt2 = ones(size(mut1));
        end

        %   Modifico los parametros para incluir el numero de datos
        mut = mut1;
        psit = psit1;
        mut(pos) = mut1(pos)+psit1(pos).*(kt2(pos).^epst(pos)-1)./epst(pos);
        psit(pos) = psit1(pos).*kt2(pos).^epst(pos);
        %   Modifico los parametros para incluir el numero de datos para GUMBEL
        mut(posG) = mut(posG)+psit(posG).*log(kt2(posG));
        
        %   Evaluate auxiliary variables
        xn = (zq-mut)./psit; 
        z = 1 + epst.*xn; 
        %   Since the z-values must be greater than zero in order to avoid
        %   numerical problems their values are set to be greater than 1e-4
        z = max(1e-8,z);
        zn =-z.^(-1-1./epst)./psit; 
        %    For the Gumbel case
        zn(posG) = -exp(-xn(posG))./psit(posG);
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function to integrate
    function [I,cnt]=bsimp(funfcn,a,b)
    %	BSIMP   Numerically evaluate integral, low order method.
    %   I = BSIMP('F',A,B) approximates the integral of F(X) from A to B 
    %   within a relative error of 1e-3 using an iterative
    %   Simpson's rule.  'F' is a string containing the name of the
    %   function.  Function F must return a vector of output values if given
    %   a vector of input values.%
    %   I = BSIMP('F',A,B,EPSILON) integrates to a total error of EPSILON.  %
    %   I = BSIMP('F',A,B,N,EPSILON,TRACE,TRACETOL) integrates to a 
    %   relative error of EPSILON, 
    %   beginning with n subdivisions of the interval [A,B],for non-zero 
    %   TRACE traces the function 
    %   evaluations with a point plot.
    %   [I,cnt] = BSIMP(F,a,b,epsilon) also returns a function evaluation count.%
    %   Roberto Minguez Solana%   Copyright (c) 2001 by Universidad de Cantabria
        np = round(365*8*(b-a));
        if rem(np,2)~= 0,
            np = np+1;
%            warning('The number of initial subintervals must be pair');
        end
        %Step 1
        h = (b-a)/np;
        % Step 3
        x = linspace(a,b,np+1);
        y = feval(funfcn,x);
        AuxI=0;
        Ainit=y(1)+y(np+1);
        AuxI1=0;
        AuxI2=0;
        for i=1:1:np/2,
           AuxI1=AuxI1+y(2*i);
        end
        for i=1:1:np/2-1,
           AuxI2=AuxI2+y(2*i+1);
        end
        cnt=np;
        %Step 4
        I=(Ainit+4*AuxI1+2*AuxI2)*h/3;
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end