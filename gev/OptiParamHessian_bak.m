function [beta0,beta,alpha0,alpha,gamma0,gamma,betaT,varphi,betaT2,varphi2,fval,grad,hessian,p] = ...
    OptiParamHessian_bak (nmu,npsi,neps,x,t,kt,betaT,indices,varphi,betaT2,indices2,varphi2,pini)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Function OptiParamHessian calculates the parameters of the
%   Time-Dependent GEV distribution using the maximum likelihood method
%
%   Input:
%       nmu -> number of sinusoidal harmonics related to the location 
%       npsi -> number of sinusoidal harmonics related to the scale 
%       neps -> number of sinusoidal harmonics related to the shape 
%       x -> data to be fitted
%       t -> time when the data occur within a yearly scale
%       kt -> Frequency parameter to measure the importance od the number
%       of points
%       betaT -> tendency for the location parameter, just need to include
%       a number in order to consider it, otherwise leave it empty []
%       indices -> covariates data related to the location parameter, a
%       matrix including the data at time t for each covariate
%       varphi -> covariate parameter vector associated with the location
%       parameter, just need to include a list of numbers of length the
%       number of covariates, otherwise leave it empty []
%       betaT2 -> tendency for the scale parameter, just need to include
%       a number in order to consider it, otherwise leave it empty []
%       indices2 -> covariates data related to the scale parameter, a
%       matrix including the data at time t for each covariate
%       varphi2 -> covariate parameter vector associated with the scale
%       parameter, just need to include a list of numbers of length the
%       number of covariates, otherwise leave it empty []
%       pini -> vector including initial values for the parameters in the
%       following order: beta0, beta, betaT, varphi, alpha0, alpha,
%       betaT2, varphi2, gamma0, gamma
%       
%   Output:
%       beta0 -> Optimal constant parameter related to location
%       beta -> Optimal harmonic vector associated with location
%       alpha0 -> Optimal constant parameter related to scale
%       alpha -> Optimal harmonic vector associated with scale
%       gamma0 -> Optimal constant parameter related to shape
%       gamma -> Optimal harmonic vector associated with shape
%       betaT -> Optimal location trend parameter
%       varphi -> Optimal location covariate vector
%       betaT -> Optimal scale trend parameter
%       varphi -> Optimal scale covariate vector
%       fval -> Optimal loglikelihood function with the sign changed
%       grad -> Gradient of the log-likelihood function with the sign
%       changed at the optimal solution
%       hessian -> Hessian of the log-likelihood function with the sign
%       changed at the optimal solution
%       p -> vector including the optimal values for the parameters in the
%       following order: beta0, beta, betaT, varphi, alpha0, alpha,
%       betaT2, varphi2, gamma0, gamma
%
%   Authors: R. Minguez, F.J. Mendez, C. Izaguirre, M. Menendez,
%   and I.J. Losada 
%   Environmental and Hydraulics Institute "IH Cantabria"
%   University of Cantabria 
%   E.T.S. de Ingenieros de Caminos, Canales y Puertos 
%   Avda de los Castros, s/n 
%   39005 Santander, Spain
%   Tfno.: +34 942 20 18 52 
%   Fax: +34 942 20 18 60 
%   Corresponding author email: roberto.minguez@unican.es
%
%   Created: 09/09/2009
%
%   For more details see the paper:
%   "Pseudo-Optimal Parameter Selection of Non-Stationary 
%   Generalized Extreme Value Models for Environmental Variables". 
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global neps0

%   Check the number of input arguments
if nargin<6, kt = [];end
if nargin<7, betaT = [];end
if nargin<8, indices = [];end
if nargin<9, varphi = [];end
if nargin<10, betaT2 = [];end
if nargin<11, indices2 = [];end
if nargin<12, varphi2 = [];end
if nargin<13, pini = [];end

%   Check consistency of data dimensions
[na nind2] = size(indices);
nind = length(varphi);
if nind2~=nind,
    error(['The number of columns in the covariate data matrix (indices) does not'...
        ' coincide with the number of covariate parameters (varphi)'])
end
if ~isempty(indices)
if na~=length(x) || na~=length(t) || length(t)~=length(x),
    error('Check dimensions of data x, t and indices')
end
end
[na2 nind2] = size(indices2);
if nind2~=length(varphi2),
    error(['The number of columns in the covariate data matrix (indices2) does not'...
        ' coincide with the number of covariate parameters (varphi2)'])
end
if ~isempty(indices2),
if na2~=length(x) || na2~=length(t) || length(t)~=length(x),
    error('Check dimensions of data x, t and indices2')
end
end

%   Calculating the number of total parameters to be estimated
%   Related to the location
nmu = 2*nmu;
%   Related to the scale
npsi = 2*npsi;
%   Related to the shape
neps = 2*neps;
%   Related to the tendency for the location
ntend = length(betaT);
%   Related to the tendency for the scale
ntend2 = length(betaT2);
%   The number of covariates related to location and scale is storaged in
%   nind and nind2, respectively

%   Initial bounds and possible initial values for the constant parameters
%   related to location, scala and shape
p = zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2,1);
%   Initial beta0 parameter
p(1) = mean(x);
%   Initial alpha0 parameter
p(2+nmu+ntend+nind) = log(std(x));
%   Initial gamma0 parameter
if neps0,
    p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = 0.01;
end
if neps>0,
    p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps) = 0.01;
end
%   Bounds on variables
%   Initially all parameters are unbounded
lb = -Inf*ones(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2,1);
up = Inf*ones(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2,1);

%   Initial bounds for the parameters related to the shape, gamma0 and gamma 
if neps0,
    lb(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = -0.2;
end
if neps>0,
    lb(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps) = -0.15;    
end
if neps0,
    up(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = 0.2;
end
if neps>0,
    up(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps) = 0.15;    
end

%   If an inital value for the parameters vector is provided, it is used
if ~isempty(pini),
    for ii=1:length(p)
        p(ii) = min(pini(ii),up(ii));
        p(ii) = max(pini(ii),lb(ii));
    end
end

%   Set the options for the optimization routine, note that both gradients
%   and Hessian are provided
if isempty(kt) || sum(kt)==length(kt),
    options = optimset('GradObj','on','Hessian','on','TolFun',1e-12,'Algorithm','trust-region-reflective');
else
    options = optimset('GradObj','on','Hessian','off','TolFun',1e-12,'Algorithm','trust-region-reflective');
end
%   Call the optimization routine
%   Note that it is a minimization problem, instead a maximization problem,
%   for this reason the log likelihood function sign is changed
[p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x,t,kt,nmu,npsi,neps,ntend,indices,ntend2,indices2),p,[],[],[],[],lb,up,[],options);

%   Check if any of the bounds related to shape parameters become active,
%   if active increase or decrease the bound and call the optimization
%   routine again
auxlo = find(abs(lambda.lower)>1e-8);
lb(auxlo)=lb(auxlo)-0.05;
auxup = find(abs(lambda.upper)>1e-8);
up(auxup)=up(auxup)+0.05;
it = 1;
while (~isempty(auxlo) || ~isempty(auxup)) && it<=10,
    it = it+1;
    [p,fval,exitflag,output,lambda,grad,hessian] = fmincon(@(p) loglikelihood (p,x,t,kt,nmu,npsi,neps,ntend,indices,ntend2,indices2),p,[],[],[],[],lb,up,[],options);
    auxlo = find(abs(lambda.lower)>1e-8);
    lb(auxlo)=lb(auxlo)-0.05;
    auxup = find(abs(lambda.upper)>1e-8);
    up(auxup)=up(auxup)+0.05;
end

%   Once the optimal solution is obtained redistribute the solution
%   storaged in p in the following order: beta0, beta, betaT, varphi,
%   alpha0, alpha, betaT2, varphi2, gamma0, gamma
beta0 = p(1);
if nmu>0,
    beta = p(2:1+nmu);  
else
    beta = [];
end
if ntend>0,
    betaT = p(2+nmu);
else
    betaT = [];
end
if nind>0,
    varphi = p(1+nmu+ntend+1:1+nmu+ntend+nind);
else
    varphi = [];
end
alpha0 = p(2+ntend+nind+nmu);
if npsi>0,
    alpha = p(2+nmu+ntend+nind+1:2+nmu+ntend+nind+npsi);
else
    alpha = [];
end
if ntend2>0,
    betaT2 = p(2+nmu+ntend+nind+npsi+1);
else
    betaT2 = [];
end
if nind2>0,
    varphi2 = p(2+nmu+ntend+nind+npsi+ntend2+1:2+nmu+ntend+nind+npsi+ntend2+nind2);
else
    varphi2 = [];
end
if neps0,
    gamma0 = p(2+neps0+nmu+ntend+nind+npsi+ntend2+nind2);
else
    gamma0 = [];
end
if neps>0,
    gamma = p(2+neps0+nmu+ntend+nind+npsi+ntend2+nind2+1:2+neps0+nmu+ntend+nind+npsi+ntend2+nind2+neps);
else
    gamma = [];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Loglikelihood function definition of the GEV distribution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function [f Jx Hxx] = loglikelihood (p,x,t,kt,nmu,npsi,neps,ntend,indices,ntend2,indices2)
        %   This function calculates the loglikelihood function with the
        %   sign changed for the values of the paremeters given by p
        
        if nargin<8, ntend =0; end
        if nargin<9, indices = [];end
        if nargin<10, ntend2 =0; end
        if nargin<11, indices2 = [];end
        
        %   Check consistency of data
        [na nind] = size(indices);
        if ~isempty(indices),
        if na~=length(x) || na~=length(t) || length(t)~=length(x),
            error('Check data x, t, indices: funcion loglikelihood')
        end
        end
        [na2 nind2] = size(indices2);
        if ~isempty(indices2),
        if na2~=length(x) || na2~=length(t) || length(t)~=length(x),
            error('Check data x, t, indices2: funcion loglikelihood')
        end
        end
        
        if isempty(kt),
            kt = ones(size(x));
        end
        
        %   Evaluate the location parameter at each time t as a function
        %   of the actual values of the parameters given by p
        if ntend == 0 && nind == 0,
            mut1 =parametro(t,p(1),p(2:1+nmu));
        elseif ntend == 0 && nind ~= 0,
            mut1 =parametro(t,p(1),p(2:1+nmu),[],indices,p(1+nmu+ntend+1:1+nmu+ntend+nind));
        elseif ntend ~= 0 && nind == 0,
            mut1 =parametro(t,p(1),p(2:1+nmu),p(2+nmu),[],[]);
        else
            mut1 =parametro(t,p(1),p(2:1+nmu),p(2+nmu),indices,p(1+nmu+ntend+1:1+nmu+ntend+nind));
        end
        %   Evaluate the scale parameter at each time t as a function
        %   of the actual values of the parameters given by p
        if ntend2 == 0 && nind2 == 0,
            psit1 = exp(parametro(t,p(2+nmu+ntend+nind),p(2+nmu+ntend+nind+1:2+nmu+ntend+nind+npsi)));
        elseif ntend2 == 0 && nind2 ~= 0,
            psit1 = exp(parametro(t,p(2+nmu+ntend+nind),p(2+nmu+ntend+nind+1:2+nmu+ntend+nind+npsi),[],indices2,p(2+nmu+ntend+nind+npsi+ntend2+1:2+nmu+ntend+nind+npsi+ntend2+nind2)));
        elseif ntend2 ~= 0 && nind2 == 0,
            psit1 = exp(parametro(t,p(2+nmu+ntend+nind),p(2+nmu+ntend+nind+1:2+nmu+ntend+nind+npsi),p(2+nmu+ntend+nind+npsi+ntend2),[],[]));
        else
            psit1 = exp(parametro(t,p(2+nmu+ntend+nind),p(2+nmu+ntend+nind+1:2+nmu+ntend+nind+npsi),p(2+nmu+ntend+nind+npsi+ntend2),indices2,p(2+nmu+ntend+nind+npsi+ntend2+1:2+nmu+ntend+nind+npsi+ntend2+nind2)));
        end
        %   Evaluate the shape parameter at each time t as a function
        %   of the actual values of the parameters given by p
        if neps == 0,
            if neps0,
                epst =parametro(t,p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2),[]);
            else
                epst = 0*mut1;
            end
        else
            if neps0,
                epst =parametro(t,p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2),p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps));
            else
                epst =parametro(t,0,p(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+1:2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+neps));
            
            end
        end       
        %   The values whose shape parameter is almost cero corresponds to
        %   the GUMBEL distribution, locate their positions if they exist
        posG = find(abs(epst)<=1e-8);
        %   The remaining values correspond to WEIBULL or FRECHET
        pos  = find(abs(epst)>1e-8);
        %   The corresponding GUMBEl values are set to 1 to avoid 
        %   numerical problems, note that for those cases the GUMBEL
        %   expressions are used
        epst(posG)=1;
        
        %   Modifico los parametros para incluir el numero de datos
        mut = mut1;
        psit = psit1;
        mut(pos) = mut1(pos)+psit1(pos).*(kt(pos).^epst(pos)-1)./epst(pos);
        psit(pos) = psit1(pos).*kt(pos).^epst(pos);
        %   Modifico los parametros para incluir el numero de datos para GUMBEL
        mut(posG) = mut(posG)+psit(posG).*log(kt(posG));
        
        %   Evaluate auxiliary variables
        xn = (x-mut)./psit; 
        z = 1 + epst.*xn; 
        %   Since the z-values must be greater than zero in order to avoid
        %   numerical problems their values are set to be greater than 1e-4
        z = max(1e-8,z);
        zn =z.^(-1./epst); 
        
        %   Evaluate the loglikelihood function with the sign changed, not
        %   that the general and Gumbel expresions are used
        f = sum(-log(kt(pos))+log(psit(pos))+(1+1./epst(pos)).*log(z(pos))+kt(pos).*zn(pos))...
              +sum(-log(kt(posG))+log(psit(posG))+xn(posG)+kt(posG).*exp(-xn(posG)));
        
        %   Gradient of the loglikelihood function
        if nargout>1,
            %   Derivatives given by equations (A.1)-(A.3) in the paper
            Dmut = (1+epst-kt.*zn)./(psit.*z);
            Dpsit = -(1-xn.*(1-kt.*zn))./(psit.*z);
            Depst = zn.*(xn.*(kt-(1+epst)./zn)+z.*(-kt+1./zn).*log(z)./epst)./(epst.*z);       
                        
            %   GUMBEL derivatives given by equations (A.4)-(A.5) in the paper
            Dmut(posG) = (1-kt(posG).*exp(-xn(posG)))./(psit(posG));
            Dpsit(posG) = (xn(posG)-1-kt(posG).*xn(posG).*exp(-xn(posG)))./(psit(posG));
            Depst(posG)=0;

%             neps0 = 1;
%             if length(posG)==length(epst),
%                 neps0 = 0;
%             end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %   NUEVAS DERIVADAS
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Dmutastmut = ones(size(kt));
            Dmutastpsit = (-1+kt.^epst)./epst;
            Dmutastepst = psit1.*(1+kt.^epst.*(epst.*log(kt)-1))./(epst.^2);

            Dpsitastpsit = kt.^epst;
            Dpsitastepst = log(kt).*psit1.*kt.^epst;

            Dmutastpsit(posG) = log(kt(posG));
            Dmutastepst(posG) = 0;

            Dpsitastpsit(posG) = 1;
            Dpsitastepst(posG) = 0;

            %   Set the Jacobian to zero
            Jx = sparse(zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2,1));
            %   Jacobian elements related to the location parameters beta0
            %   and beta, equation A.6 in the paper
            Jx(1,1) = sum(Dmut.*Dmutastmut);
            %   If location harmonics are included
            if nmu>0,
                for i = 1:nmu,
                    aux = 0;
                    for k = 1:length(t),
                        %   Funtion Dparam is explained below
                        aux = aux + Dmut(k)*Dmutastmut(k)*Dparam (t(k),i);
                    end
                    Jx(1+i,1) = aux;
                end
            end
            %   Jacobian elements related to the location parameters betaT,
            %   and varphi, equation A.9
            if ntend>0
                Jx(2+nmu,1) =  sum(Dmut.*t.*Dmutastmut);
            end
            if nind>0
                for i = 1:nind,
                    Jx(1+nmu+ntend+i,1) =  sum(Dmut.*indices(:,i).*Dmutastmut);
                end
            end
            %   Jacobian elements related to the scale parameters alpha0
            %   and alpha, equation A.7 in the paper
            Jx(2+ntend+nind+nmu,1) = sum(psit1.*(Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit));
            %   If scale harmonics are included
            if npsi>0,
                for i = 1:npsi,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dparam (t(k),i)*psit1(k)*(Dpsit(k)*Dpsitastpsit(k)...
                                    +Dmut(k)*Dmutastpsit(k));
                    end
                    Jx(2+nmu+ntend+nind+i,1) = aux;
                end
            end
            %   Jacobian elements related to the scale parameters alphaT,
            %   and varphi, equation A.10
            if ntend2>0,
                Jx(2+ntend2+nmu+ntend+nind+npsi,1) =  sum((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*t.*psit1);
            end
            if nind2>0,
                for i = 1:nind2,
                    Jx(2+nmu+ntend+nind+npsi+ntend2+i,1) =  sum((Dpsit.*Dpsitastpsit+Dmut.*Dmutastpsit).*indices2(:,i).*psit1);
                end
            end
            %   Jacobian elements related to the shape parameters gamma0
            %   and gamma, equation A.10 in the paper
            if neps0 == 1,
                Jx(2+neps0+ntend+nind+nmu+npsi+ntend2+nind2,1) = sum(Depst+Dpsit.*Dpsitastepst+Dmut.*Dmutastepst);
            end  
            %   If shape harmonics are included
            if neps>0,
                for i = 1:neps,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + (Depst(k)+Dpsit(k)*Dpsitastepst(k)+Dmut(k)*Dmutastepst(k))*Dparam (t(k),i);
                    end
                    Jx(neps0+2+nmu+npsi+ntend+nind+ntend2+nind2+i,1) = aux;
                end
            end  
            %   Since the numerical problem is a minimization problem the
            %   Jacobian sign must be changed
            Jx = -Jx;
        end  
        %   Hessian matrix of the loglikelihood function
        if nargout>2,
            %   Derivatives given by equations A.13-A.17 in the paper
            D2mut = (1+epst).*zn.*(-1+epst.*z.^(1./epst))./((z.*psit).^2);
            D2psit = (-zn.*xn.*((1-epst).*xn-2)+(((1-2*xn)-epst.*(xn).^2)))./((z.*psit).^2);
            D2epst = -zn.*(...
            +xn.*(xn.*(1+3*epst)+2+(-2-epst.*(3+epst).*xn).*z.^(1./epst))...
            +z./(epst.*epst).*log(z).*(2*epst.*(-xn.*(1+epst)-1+z.^(1+1./epst))+z.*log(z))...
            )./(epst.*epst.*z.^2);
            Dmutpsit = -(1+epst-(1-xn).*zn)./((z.*psit).^2);
            Dmutepst = -zn.*(epst.*(-(1+epst).*xn-epst.*(1-xn).*z.^(1./epst))+z.*log(z))./(epst.*epst.*psit.*z.^2);
            Dpsitepst = xn.*Dmutepst;
            %   Corresponding Gumbel derivatives given by equations 
            %   A.18-A.20 in the paper 
            D2mut(posG) = -(exp(-xn(posG)))./(psit(posG).^2);
            D2psit(posG) = ((1-2*xn(posG))+exp(-xn(posG)).*(2-xn(posG)).*xn(posG))./(psit(posG).^2);
            D2epst(posG) = 0;
            Dmutpsit(posG) = (-1+exp(-xn(posG)).*(1-xn(posG)))./(psit(posG).^2);
            Dmutepst(posG) = 0;
            Dpsitepst(posG) = 0;
            %   Initialize the hessian matrix
            Hxx = sparse(zeros(2+neps0+nmu+npsi+neps+ntend+nind+ntend2+nind2));
            %   Elements of the Hessian matrix
            %   Sub-blocks following the order shown in Table 4 of the
            %   paper
            %   Sub-block number 1
            Hxx(1,1) = sum(D2mut);
            %   Sub-block number 2
            if ntend>0
                Hxx(2+nmu,2+nmu) =  sum(D2mut.*(t.^2));
            end
            %   Sub-block number 3
            if nind>0
                for i = 1:nind,
                    for j = 1:i,
                        Hxx(1+nmu+ntend+i,1+nmu+ntend+j) =  sum(D2mut.*indices(:,i).*indices(:,j));
                    end
                end
            end
            %   Sub-block number 4 (Scale exponential involved)
            if ntend2>0
                Hxx(2+ntend2+nmu+npsi+ntend+nind,2+ntend2+nmu+npsi+ntend+nind) =  sum((D2psit.*psit+Dpsit).*psit.*(t.^2));
            end
            %   Sub-block number 5 (Scale exponential involved)
            if nind2>0
                for i = 1:nind2,
                    for j = 1:i,
                        Hxx(2+nmu+npsi+ntend+nind+ntend2+i,2+nmu+npsi+ntend+nind+ntend2+j) =  sum((D2psit.*psit+Dpsit).*psit.*indices2(:,i).*indices2(:,j));
                    end
                end
            end
            %   Sub-block number 6 (Scale exponential involved)
            Hxx(2+nmu+ntend+nind,2+nmu+ntend+nind) = sum((D2psit.*psit+Dpsit).*psit);
            %   Sub-block number 7
            if neps0==1,
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = sum(D2epst);
            end
            %   Sub-block number 8 (Scale exponential involved)
            Hxx(2+nmu+ntend+nind,1) = sum(Dmutpsit.*psit);
            %   Sub-block number 9
            if neps0 == 1,
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1) = sum(Dmutepst);
            %   Sub-block number 10 (Scale exponential involved)
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+ntend+nind) = sum(Dpsitepst.*psit);
            end
            %   Sub-block number 11
            if ntend>0,
                Hxx(1+nmu+ntend,1) = sum(D2mut.*t);
            end
            %   Sub-block number 12 (Scale exponential involved)
            if ntend2>0,
                Hxx(2+nmu+ntend+nind+npsi+ntend2,1) = sum(Dmutpsit.*t.*psit);
            end 
            %   Sub-block number 52 (Scale exponential involved)
            if ntend2>0,
                Hxx(2+nmu+ntend+nind+npsi+ntend2,2+nmu+ntend+nind) = sum((D2psit.*psit+Dpsit).*t.*psit);
            end 
            %   Sub-block number 48 (Scale exponential involved)
            if ntend>0 & ntend2>0,
                Hxx(2+nmu+ntend+nind+npsi+ntend2,1+nmu+ntend) = sum(Dmutpsit.*t.*t.*psit);
            end
            %   Sub-block number 13
            if nind>0,
                for i = 1:nind,
                    Hxx(1+nmu+ntend+i,1) = sum(D2mut.*indices(:,i));
                end
            end
            %   Sub-block number 14 (Scale exponential involved)
            if nind2>0,
                for i = 1:nind2,
                    Hxx(2+nmu+ntend+nind+npsi+ntend2+i,1) = sum(Dmutpsit.*indices2(:,i).*psit);
                end
            end
            %   Sub-block number 53 (Scale exponential involved)
            if nind2>0,
                for i = 1:nind2,
                    Hxx(2+nmu+ntend+nind+npsi+ntend2+i,2+nmu+ntend+nind) = sum((D2psit.*psit+Dpsit).*indices2(:,i).*psit);
                end
            end
            %   Sub-block number 49 (Scale exponential involved)
            if ntend>0 & nind2>0,
                for i = 1:nind2,
                    Hxx(2+nmu+ntend+nind+npsi+ntend2+i,1+nmu+ntend) = sum(Dmutpsit.*t.*indices2(:,i).*psit);
                end
            end
            %   Sub-block number 15
            if nind>0 && ntend>0,
                for i = 1:nind,
                    Hxx(1+nmu+ntend+i,1+nmu+ntend) = sum(D2mut.*t.*indices(:,i));
                end
            end
            %   Sub-block number 16
            if nind2>0 && ntend2>0,
                for i = 1:nind2,
                    Hxx(2+nmu+ntend+nind+npsi+ntend2+i,2+nmu+ntend+nind+npsi+ntend2) = sum((D2psit.*psit+Dpsit).*t.*indices2(:,i).*psit);
                end
            end
            %   Sub-block number 17
            if ntend>0,
                Hxx(2+nmu+ntend+nind,1+nmu+ntend) = sum(Dmutpsit.*t.*psit);
            end
            %   Sub-block number 18
            if ntend>0 && neps0 == 1,
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1+nmu+ntend) = sum(Dmutepst.*t);
            end
            %   Sub-block number 19 (Scale exponential involved)
            if ntend2>0 && neps0 == 1,
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+ntend+nind+npsi+ntend2) = sum(Dpsitepst.*t.*psit);
            end
            %   Sub-block number 20 (Scale exponential involved)
            if nind>0,
                for i = 1:nind,
                    Hxx(2+nmu+ntend+nind,1+nmu+ntend+i) = sum(Dmutpsit.*indices(:,i).*psit);
                end
            end
            %   Sub-block number 21
            if nind>0 && neps0 == 1,
                for i = 1:nind,
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1+nmu+ntend+i) = sum(Dmutepst.*indices(:,i));
                end
            end
            %   Sub-block number 22 (Scale exponential involved)
            if nind2>0 && neps0 == 1,
                for i = 1:nind2,
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+ntend+nind+npsi+ntend2+i) = sum(Dpsitepst.*indices2(:,i).*psit);
                end
            end
            if nmu>0,
                for i = 1:nmu,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + D2mut(k)*Dparam (t(k),i);
                    end
                    %   Sub-block number 23
                    Hxx(1+i,1) = aux;
                    for j = 1:i,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + D2mut(k)*Dparam (t(k),i)*Dparam (t(k),j);
                        end
                        %   Sub-block number 24
                        Hxx(1+i,1+j) = aux;
                    end
                end
                for i = 1:nmu,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutpsit(k)*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 25 (Scale exponential involved)
                    Hxx(2+nmu+ntend+nind,1+i) = aux; 
                end
                if neps0 == 1,
                for i = 1:nmu,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutepst(k)*Dparam (t(k),i);
                    end
                    %   Sub-block number 26 (Scale exponential involved)
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,1+i) = aux; 
                end  
                end
                if ntend>0,
                    for i = 1:nmu,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + D2mut(k)*t(k)*Dparam (t(k),i);
                        end
                        %   Sub-block number 27
                        Hxx(2+nmu,1+i) = aux; 
                    end
                end
                if ntend2>0,
                    for i = 1:nmu,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dmutpsit(k)*t(k)*Dparam (t(k),i)*psit(k);
                        end
                        %   Sub-block number 46 (Scale exponential involved)
                        Hxx(2+nmu+ntend+nind+npsi+ntend2,1+i) = aux;
                    end
                end
                if nind>0,
                    for i = 1:nmu,
                        for j = 1:nind,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + D2mut(k)*indices(k,j)*Dparam (t(k),i);
                            end
                            %   Sub-block number 28
                            Hxx(1+nmu+ntend+j,1+i) = aux; 
                        end
                    end
                end 
                if nind2>0,
                    for i = 1:nmu,
                        for j = 1:nind2,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dmutpsit(k)*indices2(k,j)*Dparam (t(k),i)*psit(k);
                            end
                            %   Sub-block number 47 (Scale exponential
                            %   involved)
                            Hxx(2+nmu+ntend+nind+npsi+ntend2+j,1+i) = aux; 
                        end
                    end
                end 
            end
            if npsi>0,
                for i = 1:npsi,
                aux = 0;
                for k = 1:length(t),
                    aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*Dparam (t(k),i)*psit(k);
                end
                %   Sub-block number 29 (Scale exponential involved)
                Hxx(2+nmu+ntend+nind+i,2+ntend+nind+nmu) = aux;
                for j = 1:i,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*Dparam (t(k),i)*Dparam (t(k),j)*psit(k);
                    end
                    %   Sub-block number 30 (Scale exponential involved)
                    Hxx(2+nmu+ntend+nind+i,2+nmu+ntend+nind+j) = aux;
                end
                end
                if neps0 == 1,
                for i = 1:npsi,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dpsitepst(k)*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 31 (Scale exponential involved)
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2,2+nmu+ntend+nind+i) = aux; 
                end 
                end
                for i = 1:npsi,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutpsit(k)*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 32 (Scale exponential involved)
                    Hxx(2+nmu+ntend+nind+i,1) = aux; 
                end 
                if ntend>0,
                    for i = 1:npsi,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dmutpsit(k)*t(k)*Dparam (t(k),i)*psit(k);
                        end
                        %   Sub-block number 33 (Scale exponential involved)
                        Hxx(2+nmu+ntend+nind+i,2+nmu) = aux; 
                    end
                end
                if nind>0,
                    for i = 1:npsi,
                        for j = 1:nind,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dmutpsit(k)*indices(k,j)*Dparam (t(k),i)*psit(k);
                            end
                            %   Sub-block number 34 (Scale exponential
                            %   involved)
                            Hxx(2+nmu+ntend+nind+i,1+nmu+ntend+j) = aux; 
                        end
                    end
                end    
            end
            if neps>0,
                for i = 1:neps,
                %   Primer elemento asociado al valor constante (primera columna)
                aux = 0;
                for k = 1:length(t),
                    aux = aux + D2epst(k)*Dparam (t(k),i);
                end
                %   Sub-block number 35
                Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+neps0+nmu+npsi+ntend+nind+ntend2+nind2) = aux;
                for j = 1:i,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + D2epst(k)*Dparam (t(k),i)*Dparam (t(k),j);
                    end
                    %   Sub-block number 36
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+j) = aux;
                end
                end
                for i = 1:neps,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dpsitepst(k)*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 37 (Scale exponential involved)
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+ntend+nind) = aux; 
                end 
                for i = 1:neps,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutepst(k)*Dparam (t(k),i);
                    end
                    %   Sub-block number 38
                    Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,1) = aux; 
                end
                if ntend>0,
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dmutepst(k)*t(k)*Dparam (t(k),i);
                        end
                        %   Sub-block number 39
                        Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu) = aux; 
                    end
                end
                if ntend2>0,
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dpsitepst(k)*t(k)*Dparam (t(k),i)*psit(k);
                        end
                        %   Sub-block number 44 (Scale exponential involved)
                        Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+npsi+ntend+nind+ntend2) = aux; 
                    end
                end
                if nind>0,
                    for i = 1:neps,
                        for j = 1:nind,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dmutepst(k)*indices(k,j)*Dparam (t(k),i);
                            end
                            %   Sub-block number 40 
                            Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,1+nmu+ntend+j) = aux; 
                        end
                    end
                end
                if nind2>0,
                    for i = 1:neps,
                        for j = 1:nind2,
                            aux = 0;
                            for k = 1:length(t),
                                aux = aux + Dpsitepst(k)*indices2(k,j)*Dparam (t(k),i)*psit(k);
                            end
                            %   Sub-block number 45 (Scale exponential
                            %   involved)
                            Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+npsi+ntend+nind+ntend2+j) = aux; 
                        end
                    end
                end
            end
            if nind>0 & ntend2>0,
                for i = 1:nind,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + Dmutpsit(k)*t(k)*indices(k,i)*psit(k);
                    end
                    %   Sub-block number 50 (Scale exponential involved)
                    Hxx(2+nmu+ntend+nind+npsi+ntend2,1+nmu+ntend+i) = aux;
                end
            end
            if nind>0 & nind2>0,
                for i = 1:nind,
                    for j = 1:nind2,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dmutpsit(k)*indices2(k,j)*indices(k,i)*psit(k);
                        end
                        %   Sub-block number 51 (Scale exponential involved)
                        Hxx(2+nmu+ntend+nind+npsi+ntend2+j,1+nmu+ntend+i) = aux; 
                    end
                end
            end 
            if ntend2>0,
                for i = 1:npsi,
                    aux = 0;
                    for k = 1:length(t),
                        aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*t(k)*Dparam (t(k),i)*psit(k);
                    end
                    %   Sub-block number 54 (Scale exponential involved)
                    Hxx(2+nmu+ntend+nind+npsi+ntend2,2+nmu+ntend+nind+i) = aux;
                end
            end
            if nind2>0,
                for i = 1:npsi,
                    for j = 1:nind2,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + (D2psit(k)*psit(k)+Dpsit(k))*indices2(k,j)*Dparam (t(k),i)*psit(k);
                        end
                        %   Sub-block number 55 (Scale exponential involved)
                        Hxx(2+nmu+ntend+nind+npsi+ntend2+j,2+nmu+ntend+nind+i) = aux; 
                    end
                end
            end 
            if nmu>0 && npsi>0,
                for j = 1:nmu,    
                    for i = 1:npsi,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dmutpsit(k)*Dparam (t(k),i)*Dparam (t(k),j)*psit(k);
                        end
                        %   Sub-block number 41 (Scale exponential involved)
                        Hxx(2+nmu+ntend+nind+i,1+j) = aux; 
                    end
                end
            end
            if nmu>0 && neps>0,
                for j = 1:nmu,    
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dmutepst(k)*Dparam (t(k),i)*Dparam (t(k),j);
                        end
                        %   Sub-block number 42
                        Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,1+j) = aux; 
                    end
                end
            end
            if npsi>0 && neps>0,
                for j = 1:npsi,    
                    for i = 1:neps,
                        aux = 0;
                        for k = 1:length(t),
                            aux = aux + Dpsitepst(k)*Dparam (t(k),i)*Dparam (t(k),j)*psit(k);
                        end
                        %   Sub-block number 43 (Scale exponential involved)
                        Hxx(2+neps0+nmu+npsi+ntend+nind+ntend2+nind2+i,2+nmu+ntend+nind+j) = aux; 
                    end
                end
            end            
            %   Simmetric part of the Hessian
            Hxx = Hxx + tril(Hxx,-1)';
            %   Since the numerical problem is a minimization problem the
            %   Hessian sign must be changed
            Hxx = -Hxx;  
        end
%         disp(p);
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function dp = Dparam (t,j)
    %
    %   Derivative of the location, scale and shape functions with respect
    %   to harmonic parameters. It correspond to the right hand side 
    %   in equation A.11 of the paper    
    %
    %   Input:
    %        t-> Time (Yearly scale)
    %        j-> Harmonic number
    %
    %   Output:
    %        dp-> Corresponding derivative
    %
        if mod(j,2) == 0,
            dp = sin(j/2*2*pi*t);
        else
            dp = cos((j+1)/2*2*pi*t);
        end

    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
