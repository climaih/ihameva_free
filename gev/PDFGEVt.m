function f = PDFGEVt(x,t,kt,indices,OPHr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   PDFGEVt function calculates the GEV distribution function corresponding
%    to the given parameters
%
%   Input:
%       x -> maximum data
%       t -> time when the data occur within a yearly scale
%       kt -> Frequency parameter to measure the importance od the number
%       betaT -> tendency for the location parameter, just need to include
%       a number in order to consider it, otherwise leave it empty []
%       indices -> covariates data related to the location parameter, a
%       matrix including the data at time t for each covariate
%       varphi -> covariate parameter vector associated with the location
%       parameter, just need to include a list of numbers of length the
%       number of covariates, otherwise leave it empty []
%       betaT2 -> tendency for the scale parameter, just need to include
%       a number in order to consider it, otherwise leave it empty []
%       indices -> covariates data related to the scale parameter, a
%       matrix including the data at time t for each covariate
%       varphi2 -> covariate parameter vector associated with the scale
%       parameter, just need to include a list of numbers of length the
%       number of covariates, otherwise leave it empty []
%
%   Output:
%        F-> Values of the distribution function
%   Example to plot PDF
%   x=0:.1:20;
%   f = PDFGEVt(x,10,[],[],OPHr)
%   plot(x,f)
%
%   Authors: R. Minguez, F.J. Mendez, C. Izaguirre, M. Menendez,
%   and I.J. Losada 
%   Environmental and Hydraulics Institute "IH Cantabria"
%   University of Cantabria 
%   E.T.S. de Ingenieros de Caminos, Canales y Puertos 
%   Avda de los Castros, s/n 
%   39005 Santander, Spain
%   Tfno.: +34 942 20 18 52 
%   Fax: +34 942 20 18 60 
%   Corresponding author email: roberto.minguez@unican.es
%
%   Created: 09/09/2009
%
%   For more details see the paper:
%   "Pseudo-Optimal Parameter Selection of Non-Stationary 
%   Generalized Extreme Value Models for Environmental Variables". 
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Size of data
if length(t)==1
    t=t*ones(size(x));
end

[m,n] = size(x);
[m1,n1] = size(t);
if m1~=m || n1~=n,
    error('x and t must be vectors of equal length')
end
if isempty(kt),
    kt = ones(m,n);
end

%   Evaluate the location,scale and shape parameter at each time t as a function
[mut,psit,epst,pos,posG]=gev_param_t(t,kt,indices,OPHr);

xn = (x-mut)./psit;
aux = (1+epst(pos).*xn(pos));
posS = find(aux<=0);
aux(posS)=0;

f = zeros (size(x));
%   WEIBULL or FRECHET distribution function
f(pos) = exp(-aux.^(-1./epst(pos))).*aux.^(-1-1./epst(pos))./psit(pos);
%   GUMBEL distribution function
f(posG) = exp(-exp(-xn(posG))-xn(posG))./psit(posG);

if nargout==0,
    figure;plot(x,f,'*r');
end




